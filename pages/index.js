import { StyleSheet, Text, View } from 'react-native'

import {combineReducers, createStore} from "redux";
import {Provider} from "react-redux";

import {userOperations} from "./app/redux/reducers/UserReducer";
import {UserFavorite} from "./app/redux/reducers/UserFavorite";
import {navigationOperation} from "./app/redux/reducers/NavigationReducer";
import {checkoutDetailOperation} from "./app/redux/reducers/CheckoutReducer";
import MainContainer from "./app/containers/MainContainer";
import React from "react";

const rootReducer = combineReducers({
  userOperations: userOperations,
  navigationReducer: navigationOperation,
  checkoutReducer: checkoutDetailOperation,
  userFavorite: UserFavorite
});



const JustOrderGlobalStore = createStore(rootReducer);

export default function App(props) {
  return (
      <Provider store={JustOrderGlobalStore}>
        <MainContainer/>
      </Provider>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center',
  },
  link: {
    color: 'blue',
  },
  textContainer: {
    alignItems: 'center',
    marginTop: 16,
  },
  text: {
    alignItems: 'center',
    fontSize: 24,
    marginBottom: 24,
  },
})
