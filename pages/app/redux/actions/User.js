export const TYPE_SAVE_LOGIN_DETAILS = "TYPE_SAVE_LOGIN_DETAILS"

export function saveUserDetailsInRedux(details) {
    return {
        type: TYPE_SAVE_LOGIN_DETAILS,
        value: details
    }
}

export const TYPE_SAVE_USER_ADDRESSES = "TYPE_SAVE_USER_ADDRESSES"
export function saveUserAddressesInRedux(details) {
    return {
        type: TYPE_SAVE_USER_ADDRESSES,
        value: details
    }
}

export const TYPE_SAVE_CART_ADDRESSES = "TYPE_SAVE_CART_ADDRESSES"
export function saveCartAddressesInRedux(details) {
    return {
        type: TYPE_SAVE_CART_ADDRESSES,
        value: details
    }
}

export const TYPE_SAVE_LOGIN_FCM = "TYPE_SAVE_LOGIN_FCM"

export function saveUserFCMInRedux(details) {
    return {
        type: TYPE_SAVE_LOGIN_FCM,
        value: details
    }
}

export const TYPE_SAVE_COUNTRY_CODE = "TYPE_SAVE_COUNTRY_CODE"

export function saveCountryCodeInRedux(details) {
    return {
        type: TYPE_SAVE_COUNTRY_CODE,
        value: details
    }
}

export const TYPE_SAVE_CURRENT_ADDRESS = "TYPE_SAVE_CURRENT_ADDRESS"

export function saveCurrentAddressInRedux(details) {
    //console.log(details);
    return {
        type: TYPE_SAVE_CURRENT_ADDRESS,
        value: details
    }
}

export const TYPE_SAVE_CURRENT_RESTAURANT_ID = "TYPE_SAVE_CURRENT_RESTAURANT_ID"

export function saveCurrentRestaurantIdInRedux(details) {
    return {
        type: TYPE_SAVE_CURRENT_RESTAURANT_ID,
        value: details
    }
}

export const TYPE_SAVE_FAVORITE_ITEMS = "TYPE_SAVE_FAVORITE_ITEMS"

export function saveFavoriteItemsInRedux(details) {
    return {
        type: TYPE_SAVE_FAVORITE_ITEMS,
        value: details
    }
}

export const TYPE_SAVE_FAVORITE_SHOPS = "TYPE_SAVE_FAVORITE_SHOPS"

export function saveFavoriteShopsInRedux(details) {
    return {
        type: TYPE_SAVE_FAVORITE_SHOPS,
        value: details
    }
}

