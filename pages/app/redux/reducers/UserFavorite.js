import {TYPE_SAVE_FAVORITE_ITEMS, TYPE_SAVE_FAVORITE_SHOPS} from "../actions/User";

const initialFavorite = {
    favoriteItems: {
        items: []
    },
    favoriteShops: {
        shops: []
    }
};

export function UserFavorite(state = initialFavorite, action) {
    switch (action.type) {
        case TYPE_SAVE_FAVORITE_ITEMS: {
            return Object.assign({}, state, {
                favoriteItems: action.value
            });
        }
        case TYPE_SAVE_FAVORITE_SHOPS: {
            return Object.assign({}, state, {
                favoriteShops: action.value
            });
        }
        default:
            return state;
    }
}
