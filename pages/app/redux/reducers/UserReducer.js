import {
    TYPE_SAVE_COUNTRY_CODE,
    TYPE_SAVE_CURRENT_ADDRESS,
    TYPE_SAVE_CURRENT_RESTAURANT_ID,
    TYPE_SAVE_LOGIN_DETAILS,
    TYPE_SAVE_LOGIN_FCM,
    TYPE_SAVE_USER_ADDRESSES,
    TYPE_SAVE_CART_ADDRESSES
} from "../actions/User";

const initialStateUser = {
    // LOGIN DETAILS
    phoneNumberInRedux: undefined,
    userIdInRedux: undefined,
    currentRestaurantId:undefined,
    currentAddress: {
        landmark: undefined,
        address: undefined,
        latitude: undefined,
        longitude: undefined
    },
    savedCartAddress:undefined,
};

export function userOperations(state = initialStateUser, action) {
    switch (action.type) {
        case TYPE_SAVE_LOGIN_DETAILS: {
            return Object.assign({}, state, {
                phoneNumberInRedux: action.value.PhoneNumber,
                userIdInRedux: action.value.UserID,
                // savedAddresses: action.value.savedAddresses
            });
        }
        case TYPE_SAVE_USER_ADDRESSES: {
            return Object.assign({}, state, {
                savedAddresses: action.value
            });
        }
        case TYPE_SAVE_CART_ADDRESSES: {
            return Object.assign({}, state, {
                savedCartAddress: action.value
            });
        }

        case TYPE_SAVE_LOGIN_FCM: {
            return Object.assign({}, state, {
                token: action.value
            });
        }
        case TYPE_SAVE_COUNTRY_CODE: {
            return Object.assign({}, state, {
                code: action.value
            });
        }
        case TYPE_SAVE_CURRENT_ADDRESS: {
            return Object.assign({}, state, {
                currentAddress: action.value
            });
        }
        case TYPE_SAVE_CURRENT_RESTAURANT_ID: {
            return Object.assign({}, state, {
                currentRestaurantId: action.value
            });
        }
        // case SAVE_CART_COUNT: {
        //   return Object.assign({}, state, {
        //     cartCount: action.value
        //   });
        // }
        default:
            return state;
    }
}
