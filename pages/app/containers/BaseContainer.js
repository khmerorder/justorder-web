import React from "react";
//import {Container} from "native-base";
import {View} from "react-native";
//import NavBar from "./NavBar";
//import BottomMenu from "../components/BottomMenu";
//import ProgressLoader from "../components/ProgressLoader";
//import MainLoader from "../components/MainLoader";
import {EDColors} from "../assets/Colors";
//import {netStatusEvent} from "../utils/NetworkStatusConnection";

export default class BaseContainer extends React.Component {
    // constructor(props) {
    //     super(props);
    // }
    //
    // componentDidMount() {
    //     netStatusEvent(status => {
    //
    //     });
    // }

    render() {
        return (
            <View style={{overflow:"scroll",height:"100%"}}>
                {/*<NavBar*/}
                {/*    title={this.props.title}*/}
                {/*    otherTitle={this.props.otherTitle}*/}
                {/*    left={this.props.left}*/}
                {/*    onLeft={() => {*/}
                {/*        this.props.onLeft();*/}
                {/*    }}*/}
                {/*    right={this.props.right}*/}
                {/*    onRight={(operator) => {*/}
                {/*        this.props.onRight(operator);*/}
                {/*    }}*/}
                {/*    isFavorite={this.props.isFavorite}*/}
                {/*    searchBox={this.props.searchBox}*/}
                {/*    onSearchBoxTextChange={this.props.onSearchBoxTextChange}*/}
                {/*    searchBoxIsFocus={this.props.searchBoxIsFocus}*/}
                {/*    searchBoxButton={this.props.searchBoxButton}*/}
                {/*    searchBoxButtonIcon={this.props.searchBoxButtonIcon}*/}
                {/*    searchBoxButtonPress={this.props.searchBoxButtonPress}*/}
                {/*/>*/}

                {/*{this.props.showSplashLoader ? <MainLoader/> : (this.props.loading ?<ProgressLoader/>: null) }*/}

                <View style={{backgroundColor: EDColors.backgroundDark}}>
                    {this.props.children}
                    {/*{this.props.hideBottomMenu ? null :<View style={{height:50}}/>}*/}
                    {/*{this.props.hideBottomMenu ? null :*/}
                    {/*    <BottomMenu*/}

                    {/*        button1Active={this.props.button1Active}*/}
                    {/*        button2Active={this.props.button2Active}*/}
                    {/*        //button3Active={this.props.button3Active}*/}
                    {/*        button4Active={this.props.button4Active}*/}
                    {/*        button5Active={this.props.button5Active}*/}

                    {/*        _cartCount={this.props.cartCount}*/}

                    {/*        footerButton1Click={() => {*/}
                    {/*            this.props.footerButton1Click()*/}
                    {/*        }}*/}
                    {/*        footerButton2Click={() => {*/}
                    {/*            this.props.footerButton2Click()*/}
                    {/*        }}*/}
                    {/*        footerButton3Click={() => {*/}
                    {/*            this.props.footerButton3Click()*/}
                    {/*        }}*/}
                    {/*        footerButton4Click={() => {*/}
                    {/*            this.props.footerButton4Click()*/}
                    {/*        }}*/}
                    {/*        footerButton5Click={() => {*/}
                    {/*            this.props.footerButton5Click()*/}
                    {/*        }}*/}
                    {/*    />}*/}
                </View>
            </View>
        );
    }
}
