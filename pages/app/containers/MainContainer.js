import React from "react";

import {
    FlatList,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    ImageBackground,
    Image
} from "react-native";
import BaseContainer from "./BaseContainer";
import Assets from "../assets";
import {APPFONTS} from "../assets/FontConstants";

import {apiPost} from "../api/ServiceManager";
//import {netStatus} from "../utils/NetworkStatusConnection"

import {REGISTRATION_HOME, RESPONSE_SUCCESS} from "../utils/Constants";
import PopularRestaurants from "../components/PopularRestaurants";
import {APPCOLORS, EDColors} from "../assets/Colors";
import ETextViewNormalLabel from "../components/ETextViewNormalLabel";

export default class MainContainer extends React.Component {

    state = {
        arrayRestaurants:undefined
    }

    showRestaurantRows() {
        return (
            <PopularRestaurants
                // onScrollEndDrag={(e)=>{
                //     //this.setState({scolledOffsetY:e.nativeEvent.contentOffset.y});
                //     // console.log(e.nativeEvent.contentOffset.y);
                //     //this.setState({showScrollUpButton: e.nativeEvent.contentOffset.y >= height})
                //
                // }}
                restaurants={this.state.arrayRestaurants}
                //itemPress={this.gotoRestaurant}
                //refreshScreen={this.refreshScreen}
                //nextPage={this.nextRestaurantPage}
                //isLoading={this.state.loadingRestaurant}
            />
        );
    }
    allRestaurantRows = () => {
        let popularRestaurants = (
            this.state.arrayRestaurants != undefined &&
            this.state.arrayRestaurants != null &&
            this.state.arrayRestaurants.length > 0 ? (
                this.showRestaurantRows()

            ) : this.state.arrayRestaurants != undefined &&
            this.state.arrayRestaurants != null &&
            this.state.arrayRestaurants.length == 0 ? (
                null
            ) : (this.state.isLoading ? null :
                    <TouchableOpacity style={{
                        alignSelf: 'center',
                        backgroundColor: APPCOLORS.group1Main,
                        padding: 10,
                        marginVertical: 100
                    }}
                    // onPress={() => this.refreshScreen}
                    >
                        {/* <Image source = {Assets.refresh}/> */}
                        <Text style={{color: EDColors.white}}>
                            Reload
                        </Text>
                    </TouchableOpacity>
            )
        );

        return (
            <View style={{flex: 1, minHeight: 200}}>
                <ETextViewNormalLabel
                    style={{marginLeft: 10, marginBottom: 10}}
                    text="Featured"/>

                {popularRestaurants}
            </View>
        );

    }

    getData(){
        const param = {}
        apiPost(
            REGISTRATION_HOME,
            param,
            resp => {
                if (resp != undefined) {

                    if (resp.status == RESPONSE_SUCCESS) {

                        let restaurants = resp.restaurants;//.splice(0,10);

                        console.log("restaurants ::::: ", restaurants)

                        this.setState({arrayRestaurants: undefined}, () => {
                            this.setState({arrayRestaurants: restaurants});
                        });

                        this.last_page = resp.last_page;
                        this.current_page = 1;
                        console.log("current_page : ", this.current_page)

                    }
                }

                this.setState({isLoading: false});

                if (this.state.showSplashLoader) {
                    setTimeout(() => {
                        this.setState({showSplashLoader: false});
                    }, 3000);
                }
            },
            err => {
                this.setState({isLoading: false});
            }
        );
    }

    allMainRow(){
        let rows = [];

        // rows.push(this.slider());
        // rows.push(this.businessIcons());
        // rows.push(this.anouncementRow());

        rows.push(
            <View style={{flexDirection:"row",height:100,marginHorizontal:7,marginVertical: 10}}>
                <TouchableOpacity
                    onPress={()=>{
                        // if(this.merchantModal){
                        //     this.merchantModal.show();
                        // }
                    }}
                    style={{flex:1,paddingHorizontal:3}}
                >
                    <View style={{width:"100%",backgroundColor:"#7d3e1e",height:"100%",overflow:"hidden",borderRadius:5}}>
                        <ImageBackground source={Assets.merchantAds} resizeMode={"contain"} style={{width:"100%",height:"100%"}}>
                            <View style={{paddingHorizontal:5,paddingVertical:0}}>
                                <Text style={{fontFamily:APPFONTS.regular,fontSize:14,letterSpacing:-0.2}}>{"Become a Merchant Partner"}</Text>
                                <Text style={{fontFamily:APPFONTS.regular,fontSize:12}}>{"Help you to Increase Revenue"}</Text>
                            </View>
                        </ImageBackground>
                        <Text style={styles.joinNow}>
                            {"Join Now"}
                        </Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={()=>{
                        // if(this.jobVacancyModal){
                        //     this.jobVacancyModal.show();
                        // }
                    }}
                    style={{flex:1,paddingHorizontal:3}}>
                    <View style={{width:"100%",backgroundColor:"#7d3e1e",height:"100%",overflow:"hidden",borderRadius:5}}>
                        <ImageBackground source={Assets.vacancyAds} resizeMode={"contain"} style={{width:"100%",height:"100%"}}>
                            <View style={{paddingHorizontal:5,paddingVertical:0}}>
                                <Text style={{fontFamily:APPFONTS.regular,fontSize:14,letterSpacing: -1}}>{"Job Vacancy"}</Text>
                                <Text style={{fontFamily:APPFONTS.regular,fontSize:12}}>{"New job is available now"}</Text>
                            </View>
                        </ImageBackground>
                        <Text style={styles.joinNow}>
                            {"Apply Now"}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
        // Loading
        rows.push(this.allRestaurantRows());

        return rows;
    }

    async componentDidMount() {
        this.getData()
    }

    render() {
        return (
            <BaseContainer>
                <FlatList
                    // onScroll={this._onListScroll}

                    ref={(ref)=>this.mainFlatList = ref}
                    // refreshControl={
                    //     <RefreshControl
                    //         refreshing={this.state.refreshing}
                    //         onRefresh={this._onRefresh}
                    //     />
                    // }
                    data={this.allMainRow()}
                    renderItem={({item}) => (
                        item
                    )}
                    keyExtractor={(item, index) => String(index)}
                />
            </BaseContainer>
        )
    }
}

const styles = StyleSheet.create({
    scrollUpButton:{
        zIndex:999,
        right:10,
        bottom:60,
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 20,
        //     height: 20,
        // },
        // shadowOpacity: 1,
        // shadowRadius: 9,
        // elevation: 9,

        backgroundColor:"rgba(0,0,0,0.6)"
    },
    discount_stick: {
        position: "absolute",
        right: 0,
        top: 0,
        zIndex: 1,
        // borderColor:"red",
        // borderWidth:1,
        // backgroundColor:"white",
        height: 35,
        width: 30,
        alignItems: "center",
        borderRadius: 2,
    },
    itemImage: {
        width: "100%",
        height: "100%",
        alignSelf: "center",
        resizeMode: 'cover'
    },
    spinner: {
        flex: 1,
        alignSelf: "center",
        zIndex: 1000
    },
    anouncementRow: {
        marginTop: 10,
        marginStart: 10,
        marginEnd: 10,
        backgroundColor: "white",
        borderRadius: 5
    },
    joinNow:{
        position:"absolute",
        textAlign: "center",
        textAlignVertical:"center",
        // justifyContent:"center",
        // alignSelf:"center",
        flex:1,
        bottom:10,
        left:5,
        backgroundColor:"yellow",
        borderRadius:5,
        borderWidth:1,
        borderColor:"yellow",
        width:70,
        height:25,
        fontSize:12
    }
});