import Moment from "moment";

// API URL CONSTANTS

// export const BASE_URL_API = "http://27.54.170.187/~restaura/v1/api/";
// export const BASE_URL_API = "http://eatance.evincedev.com/v1/api/";
// export const BASE_URL_API = "http://192.168.1.146/fooddeli/v1/api/";

//export const BASE_URL = "http://192.168.0.31";

export const BASE_URL = "https://foods.khmerorder.com";

export const BASE_URL_API = BASE_URL + "/public/api/";

export const REGISTRATION_URL = BASE_URL_API + "app_register";

export const LOGIN_URL = BASE_URL_API + "app_login";

export const OTP_VERIFY = BASE_URL_API + "app_otp_verify";

export const OTP_RESEND = BASE_URL_API + "app_otp_resend";

export const PRODUCTS_BY_CATE = BASE_URL_API + "app_get_product_by_cate";

export const NEW_PRODUCTS = BASE_URL_API + "app_get_products";

export const RECENT_PRODUCTS = BASE_URL_API + "app_get_recent_products";

export const PRODUCT_DETAIL = BASE_URL_API + "app_get_product_detail";

export const ADD_TO_CART = BASE_URL_API + "app_addtocart";

export const ADD_ADDRESS = BASE_URL_API + "app_save_address";
export const GET_ADDRESS = BASE_URL_API + "app_get_addresses";
export const DELETE_ADDRESS = BASE_URL_API + "app_delete_address";

export const ORDER_LISTING = BASE_URL_API + "app_get_orders";

export const LOGOUT_URL = BASE_URL_API + "logout";
export const COUNTRY_CODE_URL = BASE_URL_API + "getCountryPhoneCode";
export const DRIVER_TRACKING = BASE_URL_API + "delivery-guy-order-tracking";
export const CHANGE_TOKEN = BASE_URL_API + "changeToken";
export const CHECK_ORDER_URL = BASE_URL_API + "checkOrderDelivery";
export const CHECK_ORDER_DELIVERED_URL = BASE_URL_API + "checkOrderDelivered";
export const GET_RESTAURANT_DETAIL = BASE_URL_API + "get-restaurant-info-by-id";

export const GET_NOTIFICATION = BASE_URL_API + "getNotification";
export const ADD_REVIEW = BASE_URL_API + "addReview";
export const ADD_ORDER = BASE_URL_API + "placeorderapi";
export const CMS_PAGE = BASE_URL_API + "getCMSPage";
export const PROMO_CODE_LIST = BASE_URL_API + "couponList";
export const APPLY_PROMO_CODE = BASE_URL_API + "checkPromocode";
export const GET_RECIPE_LIST = BASE_URL_API + "getReceipe";

export const REGISTRATION_HOME = BASE_URL_API + "getHome";
export const GET_TAGS = BASE_URL_API + "getTags";

export const GET_POPULAR_TAGS = BASE_URL_API + "getPopularTags";

export const GET_MORE_PRODUCTS = BASE_URL_API + "app_get_products";

export const GET_ITEM_CATEGORIES = BASE_URL_API + "getCategories";

export const SEARCH_RESTAURANTS = BASE_URL_API + "search-restaurants";

export const CHECK_BOOKING_AVAIL = BASE_URL_API + "bookingAvailable";
export const BOOKING_EVENT = BASE_URL_API + "bookEvent";
export const BOOKING_HISTORY = BASE_URL_API + "getBooking";
export const DELETE_EVENT = BASE_URL_API + "deleteBooking";
export const UPDATE_PROFILE = BASE_URL_API + "editProfile";
export const RESET_PASSWORD_REQ_URL = BASE_URL_API + "changePassword";
export const FORGOT_PASSWORD = BASE_URL_API + "forgotpassword";
export const VERIFY_OTP = BASE_URL_API + "verifyOTP";
export const INR_SIGN = "USD \u0024";
export const INR_SHORT_SIGN = "\u0024";

// ALERT CONSTANTS
export const RESERVE_STATIC = "/reserve";
export const DOCUMENTS_STATIC = "/documents";
export const APP_NAME = "Just Order";
export const DEFAULT_ALERT_TITLE = APP_NAME;
export const AlertButtons = {
    ok: "OK",
    cancel: "Cancel",
    notNow: "Not now",
    yes: "Yes",
    no: "No"
};

// REQUESTS CONSTANTS
export const RequestKeys = {
    contentType: "Content-Type",
    json: "application/json",
    authorization: "Authorization",
    bearer: "Bearer"
};

// LOAN TYPES CONSTANTS
export const LoanTypes = {
    available: "available",
    reserved: "reserved",
    closed: "closed"
};

// LOCATION LATITUDE LONGITUDE
export const DEF_LATITUDE = 11.5605504;
export const DEF_LONGITUDE = 104.8838144;

// STORAGE CONSTANTS
export const StorageKeys = {
    user_details: "UserDetails"
};

// REDUX CONSTANTS
export const ACCESS_TOKEN = "ACCESS_TOKEN";
export const RESPONSE_FAIL = 0;
export const RESPONSE_SUCCESS = 1;
export const COUPON_ERROR = 2;

export const GOOGLE_API_KEY = "AIzaSyCAzKH0AVRyXkKrP6XEcK2i9bGMHwr771c";


//NOTIFICATION_TYPE
export const ORDER_TYPE = "orderNotification";
export const NOTIFICATION_TYPE = "notification";
export const DEFAULT_TYPE = "default";

//MESSAGES
export const CART_PENDING_ITEMS =
    "You have pending items in cart from another restaurant";
export const SEARCH_PLACEHOLDER = "Search for restaurant, cuisine or dish";

//CMS PAGE
export const ABOUT_US = 1;
export const CONTACT_US = 2;
export const PRIVACY_POLICY = 3;

export const funGetTime = date => {
    var d = new Date(date);
    return Moment(d).format("LT");
};

export const checkNull = data => {
    if (data != undefined && data != "") {
        return data;
    } else {
        return;
    }
};

export const funGetDate = date => {
    var d = new Date(date);
    return Moment(d).format("DD-MM-YYYY");
};

export const funGetTomorrowDate = () => {
    var d = new Date();
    var newDate = Moment(d).add(1, "day");

    return new Date(newDate);
};

export function funGetDateStr(date, formats) {
    if (formats == undefined) {
        formats = "DD-MM-YYYY";
    }
    Moment.locale("en");

    var d = new Date("" + date.replaceAll("-", "/"));

    return Moment(d).format(formats);
}

export function funGetTimeStr(date) {
    Moment.locale("en");

    var d = new Date("" + date.replaceAll("-", "/"));

    return Moment(d).format("LT");
}

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

export function removeChars(str) {
    var target = "";

    var strArray = str.split(",");
    const rowLen = strArray.length;
    try {
        if (strArray != undefined)
            strArray.map((data, i) => {
                if (data.trim() === "" || data.trim() === undefined) {
                } else {
                    if (rowLen === i + 1) {
                        target = target + data;
                    } else {
                        target = target + data + ",";
                    }
                }
            });
    } catch (e) {
    }
    return target;
}

export function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export function capiString(str) {
    var splitStr = str.toLowerCase().split(" ");
    for (var i = 0; i < splitStr.length; i++) {
        // You do not need to check if i is larger than splitStr length, as your for does that for you
        // Assign it back to the array
        splitStr[i] =
            splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(" ");
}
