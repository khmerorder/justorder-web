export const ETFonts = {
  // bold: "Lato-Bold",
  // black: "Lato-Black",
  // regular: "Lato-Regular",
  // light: "Lato-Light",
  // hairline: "Lato-Hairline",
  // satisfy : "Satisfy-Regular",
  // iconFont: "iconfont",

    bold: "System",
    black: "System",
    regular: "System",
    light: "System",
    hairline: "System",
    satisfy : "System",
    iconFont: "System",
};

export const APPFONTS = {
    bold:"Roboto-Bold",
    black:"Robot-Black",
    boldItalic:"Roboto-BoldItalic",
    italic:"Roboto-Italic",
    light:"Roboto-Light",
    lightItalic:"Roboto-LightItalic",
    medium: "Roboto-Medium",
    meduimItalic:"Roboto-MediumItalic",
    regular: "Roboto-Regular",
    thin:"Roboto-Thin",
    thinItalic:"Roboto-ThinItalic",
}
