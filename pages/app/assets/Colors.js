export const EDColors = {
  primary: "#e5006c",
  primaryDisabled: "#b0739f",
  LPrimary: "#f9daec",
  secondary: "#233646",
  secondaryButtons: "#2c6fae",
  background: "#F7F8F9",
  borderColor: 'rgb(229,227,228)',
  backgroundDark: "#eee",
  backgroundLight: 'rgb(240,240,240)',
  placeholder: "#BABAC4",
  text: "gray",
  darkText: "darkgray",
  activeTabColor:"#2F4050",
  inactiveTabColor:"#BABAC4",
  error:"red",
  themeGreen: 'rgb(31,191,193)',
  black: "black",
  white: "white",
  offWhite:'rgb(247,247,247)',
  buttonReserve: 'rgb(24,121,191)',
  buttonUnreserve: 'rgba(255, 179, 0, 0.3)',
  buttonViewDocuments: 'rgb(25,170,137)',
  paleYellow: 'rgb(252,247,223)',
  paleBlue: 'rgb(212,234,246)',
  shadow:'#ddd',
  DARKGRAY:'#5f5f5a',
  LGRAY:'#efefea',
  CGRAY:'#c6c6c1',
  GRAY:'#b3b3ae',
  WHITE:'#fff',
  LIGHTGRAY:'#d0d0cb'
};

export const APPCOLORS = {
group1Left5:"#ffe5ec",
group1Left4:"#ffd1cc",
group1Left3:"#ffadc3",
group1Left2:"#FF87A5",
group1Left1:"#FF5781",
group1Main:"#FF2B60",	/* Main Primary color */
group1Right1:"#FF0040",
group1Right2:"#FE0040",

group2Left5:"#c7c4ac",
group2Left4:"#ffe1a5",
group2Left3:"#ffd4a1",
group2Left2:"#FFAE88",
group2Left1:"#FF8D57",
group2Main:"#FF702B",	/* Main Secondary color (1) */
group2Right1:"#FF5300",
group2Right2:"#FF5300",


group3Left2:"#08FCA6",
group3Left1:"#00FFA5",
group3Main:"#00CB84",	/* Main Secondary color (2) */
group3Right1:"#009C65",
group3Right2:"#007A4F",

group4Left3:"#9efe5c",
group4Left2:"#64FE00",
group4Left1:"#60F300",
group4Main:"#55D900",	/* Main Complement color */
group4Right1:"#45AF00",
group4Right2:"#348500",



/* As RGBa codes */

rgbaPrimary0:"rgba(255, 43, 96,1)",	/* Main Primary color */
rgbaPrimary1:"rgba(255,135,165,1)",
rgbaPrimary2:"rgba(255, 87,129,1)",
rgbaPrimary3:"rgba(255,  0, 64,1)",
rgbaPrimary4:"rgba(254,  0, 64,1)",

rgbaSecondary10:"rgba(255,112, 43,1)",	/* Main Secondary color (1) */
rgbaSecondary11:"rgba(255,174,136,1)",
rgbaSecondary12:"rgba(255,141, 87,1)",
rgbaSecondary13:"rgba(255, 83,  0,1)",
rgbaSecondary14:"rgba(255, 83,  0,1)",

rgbaSecondary20:"rgba(  0,203,132,1)",	/* Main Secondary color (2) */
rgbaSecondary21:"rgba(  8,252,166,1)",
rgbaSecondary22:"rgba(  0,255,165,1)",
rgbaSecondary23:"rgba(  0,156,101,1)",
rgbaSecondary24:"rgba(  0,122, 79,1)",

rgbaComplement20:"rgba( 85,217,  0,1)",	/* Main Complement color */
rgbaComplement21:"rgba(100,254,  0,1)",
rgbaComplement22:"rgba( 96,243,  0,1)",
rgbaComplement23:"rgba( 69,175,  0,1)",
rgbaComplement24:"rgba( 52,133,  0,1)",
}


