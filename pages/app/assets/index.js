"use strict";

const Assets = {
    bgHome: require("./image/bg_splash.png"),
    // bgSignup:require("./image/bg_signup.png"),
    bgSignup: require("./image/sign_up.png"),
    setting: require("./image/settings_selected.png"),
    backWhite: require("./image/back.png"),
    menu: require("./image/menu.png"),
    more: require("./image/more.png"),
    add: require("./image/add.png"),
    logo: require("./image/logo.png"),
    driver: require("./image/driver.png"),
    call_order: require("./image/call_order.png"),
    refresh: './image/refresh.png',

    //Eatance
    // bg_login:require("./image/sign_in.png"),
    bg_login: require("./image/bg_login.png"),
    ic_location: require("./image/location.png"),

    rating: require("./image/rateus_selected.png"),
    time: require("./image/time.png"),
    restMenu: require("./image/restaurant_menu.png"),
    address: require("./image/address.png"),
    ic_search: require("./image/search.png"),
    recipe_search: require("./image/recipe_search.png"),
    ic_location_grey: require("./image/location_grey.png"),
    ic_plus: require("./image/plusqty.png"),
    ic_minus: require("./image/minusqty.png"),
    ic_reservation_back: require("./image/ic_back_reservation.png"),
    ic_plus_yellow: require("./image/plus.png"),
    ic_filter: require("./image/filter.png"),
    ic_cart: require("./image/cart.png"),
    bl_cart: require("./image/cart_black.png"),
    ic_up_arrow: require("./image/up_arrow.png"),
    ic_down_arrow: require("./image/down.png"),

    currency: require("./image/currency.png"),

    sample_rest_banner: require("./image/banner-restaurant.jpg"),
    sample_rest_logo: require("./image/rest_logo.png"),
    star: require("./image/star.png"),

    verified: require("./image/verified-shield.png"),

    // deselected icons
    home_deselect: require("./image/home_deselected.png"),
    recipe_deselect: require("./image/recipe_deselected.png"),
    order_deselect: require("./image/order_deselected.png"),
    eventbooking_deselect: require("./image/eventbooking_deselected.png"),
    mybooking_deselect: require("./image/mybooking_deselected.png"),
    notification_deselect: require("./image/notification_deselected.png"),
    rateus_deselect: require("./image/rateus_deselected.png"),
    shareus_deselect: require("./image/shareus_deselected.png"),
    privacypolicy_deselect: require("./image/privacypolicy_deselected.png"),
    aboutus_deselect: require("./image/aboutusdeselected.png"),
    contactus_deselect: require("./image/contactus_deselected.png"),
    signout_deselect: require("./image/signout_deselected.png"),

    // selected icons
    home_select: require("./image/home_selected.png"),
    recipe_select: require("./image/recipe_selected.png"),
    order_select: require("./image/order_selected.png"),
    eventbooking_select: require("./image/eventbooking_selected.png"),
    mybooking_select: require("./image/mybooking_selected.png"),
    notification_select: require("./image/notification_selected.png"),
    rateus_select: require("./image/rateus_selected.png"),
    shareus_select: require("./image/shareus_selected.png"),
    privacypolicy_select: require("./image/privacypolicy_selected.png"),
    aboutus_select: require("./image/aboutus_selected.png"),
    contactus_select: require("./image/contactus_selected.png"),
    signout_select: require("./image/signout_selected.png"),

    deliveredselected: require("./image/deliveredselected.png"),
    delivereddeselected: require("./image/delivereddeselected.png"),
    orderplacedselected: require("./image/orderplacedselected.png"),
    onthewayselected: require("./image/onthewayselected.png"),
    onthewaydeselected: require("./image/onthewaydeselected.png"),
    preparingdeselected: require("./image/preparingdeselected.png"),
    preparingselected: require("./image/preparingselected.png"),
    bookingavailable: require("./image/bookingavailable.png"),
    bookingnotavailable: require("./image/bookingnotavailable.png"),
    delete_cart: require("./image/delete.png"),
    ic_close: require("./image/ic_close.png"),
    about_us: require("./image/about_us.png"),
    privacy_policy: require("./image/privacy_policy.png"),
    confirm_background: require("./image/confirm_background.png"),
    confirm_thumb: require("./image/confirm_thumb.png"),
    header_placeholder: require("./image/banner_placeholder.png"),
    delete_White: require("./image/delete_White.png"),
    //profile screen
    call: require("./image/call.png"),
    our_address: require("./image/profile_address.png"),
    name: require("./image/name.png"),
    password: require("./image/password.png"),
    notification: require("./image/notification_selected.png"),
    edit: require("./image/edit_profile.png"),

    rating_white: require("./image/star_white.png"),
    clock_white: require("./image/clock_white.png"),
    star_white: require("./image/star_white.png"),
    people_white: require("./image/people.png"),
    calender_white: require("./image/calendarwhite.png"),
    calender: require("./image/calendar.png"),
    user_placeholder: require("./image/user_placeholder.png"),
    plus_round: require("./image/plus_round.png"),
    minus_round: require("./image/minus_round.png"),
    delete_gray: require("./image/delete_gray.png"),
    tick: require("./image/tick.png"),
    camera_white: require("./image/camera_white.png"),

    discount_stick: require("./image/discount_stick.png"),

    // Custom
    lang: require("./image/lang.png"),
    lang_2: require("./image/lang.png"),
    noproduct: require("./image/noproducts.jpg"),
    supper_deal_icon: require("./image/super_deals-icon.png"),
    arrow_right: require("./image/right-arrow.png"),
    headline: require("./image/headline.png"),
    home_icon: require("./image/home-icon.png"),
    hot_icon: require("./image/hot-icon.png"),
    brand_icon: require("./image/brand-icon.png"),
    category_icon: require("./image/category-icon.png"),

    //home functions
    fn_category: require("./image/home_functions/category.png"),
    fn_community: require("./image/home_functions/community.png"),
    fn_coupon_deals: require("./image/home_functions/coupon_deals.png"),
    fn_flash_sale: require("./image/home_functions/flash_sale.png"),
    fn_hot: require("./image/home_functions/hot.png"),
    fn_new: require("./image/home_functions/new.png"),
    fn_uner10: require("./image/home_functions/under10.png"),
    fn_share_gifts: require("./image/home_functions/share_gifts.png"),

    //Home Top Icons
    icon_food: require("./image/business_icons/food.png"),
    icon_alcohol: require("./image/business_icons/alcohol.png"),
    icon_grocery: require("./image/business_icons/grocery.png"),
    icon_express: require("./image/business_icons/express.png"),

    //Marker
    marker_customer: require("./image/map_markers/customer.png"),
    map_cropped: require("./image/map_markers/map-cropped.jpg"),

    pizza: require("./image/pizza.jpg"),

    countdown: require("./image/coundown.jpg"),

    moto: require("./image/moto.png"),

    cuisines_example1: require("./image/cuisines_examples/Coffee.jpg"),
    cuisines_example2: require("./image/cuisines_examples/Japanese.jpg"),
    cuisines_example3: require("./image/cuisines_examples/Milk Tea.jpg"),
    cuisines_example4: require("./image/cuisines_examples/Western.jpg"),

    icon_food_offers: require("./image/food_icons/offers.png"),
    icon_food_view_all: require("./image/food_icons/view_all.png"),

    icon_food_all_cuisines: require("./image/food_icons/all_cuisines.png"),
    icon_food_bbq: require("./image/food_icons/bbq.png"),
    icon_food_chinese: require("./image/food_icons/chinese.png"),
    icon_food_coffee: require("./image/food_icons/coffee.png"),
    icon_food_italian: require("./image/food_icons/italian.png"),
    icon_food_japanese: require("./image/food_icons/japanese.png"),
    icon_food_pizza: require("./image/food_icons/bbq.png"),
    icon_food_soup: require("./image/food_icons/soup.png"),
    icon_food_asian: require("./image/food_icons/asian.png"),

    // slideshow example

    slide_1: require("./image/slideshow_example/1.png"),
    slide_2: require("./image/slideshow_example/2.png"),
    slide_3: require("./image/slideshow_example/3.png"),
    slide_4: require("./image/slideshow_example/4.png"),

    flag_kh: require("./image/flags/kh.png"),

    leftTopRibbon: require("./image/leftTopRibbon.png"),

    rightTopRibbon: require("./image/rightTopRibbon.png"),

    vacancyAds: require("./image/vacancy-ads.png"),
    merchantAds: require("./image/merchan-ads.png"),

    cash_icon: require("./image/payment_gateways/card.png"),
    wing_icon: require("./image/payment_gateways/wing.png"),
    abapay_icon: require("./image/payment_gateways/abapay.png"),
    visacard_icon: require("./image/payment_gateways/visa_card.png"),

    empty_cart: require("./image/empty-cart.png"),

    itemNotFound: require("./image/itemNotFound.png"),


};

export default Assets;
