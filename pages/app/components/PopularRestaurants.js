import React from 'react';
import {FlatList,StyleSheet,View,ActivityIndicator} from 'react-native';
import PopularRestaurantCard from "./PopularRestaurantCard";

export default class PopularRestaurants extends React.PureComponent {

    constructor(props){
        super(props)
        //this.renderFooter = this.renderFooter.bind(this);
    }
    state = {
        arrayRestaurants: this.props.restaurants,
        showFilter: this.props.show,
        isLoading:this.props.isLoading
    }

    componentWillReceiveProps(nextProps) {
        this.setState({arrayRestaurants: nextProps.restaurants,isLoading:nextProps.isLoading});
    }

    // shouldComponentUpdate() {
    //     return false
    // }

    nextPage = () =>{
        //if(this.props.nextPage){
        //    this.props.nextPage()
        //}
    }

    renderFooter () {
        return (
            this.state.isLoading ? (
                <View style={style.footer}>
                <ActivityIndicator color="black" size={35} style={{ margin: 15 }} />
                </View>
            ) : null
        );
    }


    render() {
        if(!this.state.arrayRestaurants) return null;

        return (
            <FlatList
                onScroll={this.props.onScroll}
                disableVirtualization={true}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                horizontal={this.props.horizontal}

                // initialNumToRender={!this.props.horizontal ? 20 : 10}
                // maxToRenderPerBatch={!this.props.horizontal ? 20 : 10}
                // removeClippedSubviews={true}

                onEndReachedThreshold={3}

                //onEndReached={this.nextPage}

                style={[this.props.horizontal ? {marginLeft:5} : {marginHorizontal:10},this.props.style || {} ]}
                data={this.state.arrayRestaurants}
                extraData={this.state}
                keyExtractor={(item, index) => String(index)}
                renderItem={({item, index}) => {
                    return (
                        <PopularRestaurantCard
                            //offerBannerStyle={this.props.offerBannerStyle}
                            horizontal={this.props.horizontal}
                            restObjModel={item}
                            //itemPress={this.props.itemPress}
                            //style={this.props.itemStyle}
                            index={index}
                            //freeshipBannerStyle={this.props.freeshipBannerStyle}
                        />
                    );
                }}

                ListFooterComponent={()=>this.renderFooter()}

                // getItemLayout={(data, index) => (
                //     {length: 350, offset: 350 * index, index}
                // )}

            />
        );
    }
}
const style = StyleSheet.create({
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
})
