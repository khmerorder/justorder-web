import React from "react";
import {StyleSheet, Text} from "react-native";
import {APPFONTS} from "../assets/FontConstants";

export default class ETextViewNormalLabel extends React.Component {
    render() {
        return <Text style={[stylesLable.textLable, this.props.style || {}]}>{this.props.text}</Text>;
    }
}
const stylesLable = StyleSheet.create({
    textLable: {
        marginStart: 10,
        marginEnd: 10,
        color: "#000",
        fontSize: 16,
        marginTop: 8,
        marginLeft: 20,
        fontFamily: APPFONTS.regular
    }
});
