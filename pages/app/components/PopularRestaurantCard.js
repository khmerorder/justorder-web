import React from "react";
import {Dimensions, StyleSheet, Text,TouchableOpacity, ImageBackground, View,Image} from "react-native";
import Assets from "../assets";
//import Icon from 'react-native-vector-icons/FontAwesome';
//import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
//import FIcon from 'react-native-vector-icons/Fontisto';
//import AntIcon  from 'react-native-vector-icons/AntDesign';

import {BASE_URL, INR_SHORT_SIGN} from "../utils/Constants";
import {APPFONTS, ETFonts} from "../assets/FontConstants";
import {APPCOLORS, EDColors} from "../assets/Colors";

//import Image from "react-native-fast-image"

const {width, height} = Dimensions.get('window');
const itemWidth = (width - 60);

export default class PopularRestaurantCard extends React.PureComponent {
    state = {
        restObjModel: this.props.restObjModel,
        currentAvailableDay: this.props.restObjModel.current_delivery_available,
    };
    itemPress = () => {
        if (this.props.itemPress) {
            this.props.itemPress(
                "Restaurant",
                {
                    //refresh: this.refreshScreen,
                    restId: this.state.restObjModel.id,
                    distant: this.state.restObjModel.distant
                }
            )
        }
    }

    constructor(props) {
        super(props);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({restObjModel: nextProps.restObjModel});
    }

    notAvailableMessageBox(deliveryObject) {
        return (
            <View style={{
                position: "absolute",
                top: 0,
                left: 0,
                width: "100%",
                height: 120,
                // backgroundColor: "rgba(1,1,1,0.6)",
                alignItems: "center",
                alignContent: "center",
                justifyContent: "center",
                zIndex: 9,
            }}
            >
                <View style={styles.unavailableMessage}>
                    <Text style={styles.unavailableMessageTextBox}>{"Delivery Hours"}</Text>
                    <Text style={styles.unavailableMessageTextBox}>{deliveryObject ?
                        "" + deliveryObject.start_time + " - " + deliveryObject.end_time
                        : "00:00am - 00:00pm"}</Text>
                </View>

            </View>);
    }

    showPriceLevel(price_level) {
        let views = [];
        price_level = price_level == 0 || !price_level ? 1 : price_level;
        for (let i = 0; i < 3; i++) {
            views.push(
                // <Icon name={"dollar"} color={price_level > i ? "#ff963d" : "#b2b2b2"} size={14}/>
                <Text key={i}>{"$"}</Text>
            );
        }

        return (
            <View style={{flexDirection: "row", marginTop: 2}}>
                {views.map((view) => {
                    return view;
                })}
                <Text>{" "}</Text>
            </View>);
    }

    showDistant(distant) {
        return (
            <View
                style={{flexDirection: "row", flex:1}}
            >
                {/*<Icon name={"map-marker"} size={15} color={"gray"} style={{marginRight: 5}}/>*/}
                <Text>{"?"}</Text>
                <Text
                    numberOfLines={1}
                    style={styles.distant}
                >
                    {parseFloat(distant).toFixed(2).toString() + "km"}
                </Text>

            </View>)
    }

    showDeliveryCharge(delivery_charges) {
        return (
            <View
                style={{
                    flexDirection: "row",
                    flex:1,
                    alignItems:"flex-end",
                    alignContent:"flex-end",
                    justifyContent:"flex-end"
                }}
            >
                {/*<Icon name={"motorcycle"} size={15} color={"gray"} style={{marginRight: 5}}/>*/}
                <Text>{"?"}</Text>
                <Text
                    numberOfLines={1}
                    style={{
                        fontSize: 12,
                        fontFamily:APPFONTS.light
                    }}
                >
                    {"Delivery fee : " + INR_SHORT_SIGN + delivery_charges}
                </Text>
            </View>);
    }

    showMinimumCharge(minumCharge) {

        return (
            <View
                style={{
                    flexDirection: "row",
                    flex:1,
                    alignItems:"flex-end",
                    alignContent:"flex-end",
                    justifyContent:"flex-end"
                }}
            >
                {/*<Icon name={"dollar"} size={15} color={"gray"} style={{marginLeft: 5, marginRight: 5}}/>*/}
                <Text>{"?"}</Text>
                <Text
                    numberOfLines={1}
                    style={styles.minimumCharge}
                >
                    {"Minimum Order : " + INR_SHORT_SIGN + minumCharge}
                </Text>
            </View>);
    }

    showDeliveryTime(min, max) {
        return (
            <View
                style={{flexDirection: "row", flex:1}}
            >
                {/*<Icon name={"clock-o"} size={15} color={"gray"} style={{marginRight: 2}}/>*/}
                <Text>{"?"}</Text>
                <Text
                    numberOfLines={1}
                    style={styles.deliveryTime}
                >
                    {min + " - " + max + " min"}
                </Text>

            </View>);
    }

    showAvailable() {

        if (this.state.restObjModel.temporary_close) {
            return (
                <View style={styles.temporarilyCloseOverlay}>
                    <View style={styles.temporarilyCloseMessage}>
                        <Text style={styles.unavailableMessageTextBox}>{"Temporarily"}</Text>
                        <Text style={styles.unavailableMessageTextBox}>{"Closed"}</Text>
                    </View>
                </View>
            );
        }

        if (!this.state.currentAvailableDay) {
            return this.notAvailableMessageBox(null)
        }

        if (!this.state.currentAvailableDay.delivery_available) {
            return this.notAvailableMessageBox(this.state.currentAvailableDay)
        }
    }

    dot(nospace){
        return (
          <Text style={{paddingHorizontal: nospace ? 2 : 4,marginTop:-3,fontWeight: "bold",color:APPCOLORS.group2Right2}}>{"."}</Text>
        );
    }

    render() {
        return (
            <TouchableOpacity
                onPress={this.itemPress}
            >
                <View style={[styles.container, this.props.horizontal ? {marginTop:5,marginRight: 5,marginLeft: 5,width:itemWidth} : {marginRight: 0},
                this.props.style || {}]}>

                    <View style={styles.restaurantBannerBox}>
                        <Image
                            source={this.state.restObjModel.image ? {uri: BASE_URL + this.state.restObjModel.image} : null}
                            style={[styles.restaurantBanner, /*{opacity: 0.6}*/]}
                            resizeMode={"cover"}
                        />
                    </View>

                    {/*this.showAvailable()*/}
                    <View
                        style={styles.restaurantLogoBox}
                    >

                        <Image
                            source={this.state.restObjModel.logo ? {uri:BASE_URL + this.state.restObjModel.logo} : null}
                            style={[styles.restaurantLogo,
                                this.state.currentAvailableDay ? (this.state.currentAvailableDay.delivery_available ? {opacity:1} : {opacity:1}) : {opacity:1}
                            ]}
                            resizeMode={"contain"}
                        />

                        {/*<View style={*/}
                        {/*(this.state.currentAvailableDay ? (*/}
                        {/*this.state.currentAvailableDay.delivery_available ? styles.available : styles.unavailable*/}
                        {/*)*/}
                        {/*: styles.unavailable)*/}
                        {/*}/>*/}
                    </View>

                    {/*<View*/}
                        {/*style={styles.promotion}>*/}
                        {/*/!*<Text*!/*/}
                        {/*/!*style={[styles.rightBannerOverlay,styles.discountOverlay,this.props.offerBannerStyle || {}]}*!/*/}
                        {/*/!*>{"60% OFF"}</Text>*!/*/}

                        {/*{this.state.restObjModel.distant < 1 && this.state.restObjModel.distant > 0.00 ?*/}
                            {/*<Text*/}
                                {/*style={[styles.rightBannerOverlay,styles.freeshipOverlay,this.props.freeshipBannerStyle || {}]}*/}
                            {/*>{"FREESHIP"}</Text>*/}
                            {/*: null}*/}
                    {/*</View>*/}

                    {/*{this.state.restObjModel.distant < 1 && this.state.restObjModel.distant > 0.00 ?*/}
                    <View style={{
                        position:"absolute",
                        top:-3,
                        left:-9
                    }}>
                        <ImageBackground source={Assets.leftTopRibbon}
                                         resizeMode={"contain"}
                                         style={{
                                             justifyContent: "center",
                                             width:85,
                                             height:45,
                                             // top:-3,
                                             // right:-2
                                         }}>

                                <Text
                                    style={[styles.rightBannerOverlay,styles.freeshipOverlay,this.props.freeshipBannerStyle || {}]}
                                >{"FREESHIP"}</Text>

                        </ImageBackground>
                    </View>
                    {/*: null}*/}

                    <View style={{
                        position:"absolute",
                        top:0,
                        right:0
                    }}>
                        <ImageBackground
                        source={Assets.rightTopRibbon}
                        resizeMode={"contain"}
                        style={{
                            justifyContent: "center",
                            width:45,
                            height:45,
                            top:-3,
                            right:-2
                        }}>
                            <Text
                                numberOfLines={1}
                                style={{
                                    position:"relative",
                                    top:-8,
                                    right:-8,
                                    fontSize:11,
                                    color:"#ffF",
                                    textAlign:"center",
                                    transform:[{rotate:'45deg'}],
                                    fontFamily:APPFONTS.regular
                                }}
                            >{"Promo"}</Text>
                        </ImageBackground>
                    </View>

                    <View style={styles.restaurantInfoRowBox}>

                        <View
                            style={{flexDirection: "row", marginTop: 0, marginBottom: 0}}
                        >
                            {/* Restaurant Name */}
                            <View
                                style={{flex:6,flexDirection: "row", width: "100%"}}
                            >
                                {/*<Image*/}
                                {/*source={Assets.verified} style={styles.verifiedIcon}*/}
                                {/*resizeMode={"contain"}*/}
                                {/*/>*/}
                                <Text
                                    style={styles.restaurantName}
                                    numberOfLines={1}
                                >
                                    {this.state.restObjModel.name}
                                </Text>
                            </View>

                            {/* Rate */}
                            <View
                                style={styles.ratingRight}
                            >
                                {/*<Icon name={"star"} size={15} color={"#ff963d"} style={{marginRight: 5}}/>*/}
                                <Text>{"?"}</Text>
                                <Text
                                    numberOfLines={1}
                                    style={styles.ratingNumber}
                                >
                                    {parseFloat(this.state.restObjModel.rating).toFixed(1).toString()}
                                </Text>

                            </View>

                        </View>

                        <View style={{flexDirection: "row",overflow:"hidden", width: "100%"}}>

                            <View style={{flexDirection:"row",paddingVertical:0,position:"relative",top:1,justifyContent:"center"}}>
                                {this.showPriceLevel(this.state.restObjModel.price_level)}
                            </View>
                            <Text
                                numberOfLines={1}
                                style={styles.itemDescription}
                            >
                                {"Lorem ipsum dolor sit amet, consectetur adipiscing elit"}
                            </Text>
                        </View>

                        <View style={{flexDirection:"row",
                            // borderWidth:1,borderColor:"red",
                            marginBottom:5}}>

                            <View style={{
                                flex: 1,
                                flexDirection: "row",
                                alignContent: "flex-start",
                                alignItems: "flex-start",
                                justifyContent: "flex-start",
                                flexWrap:"nowrap"
                                // borderWidth:1,borderColor:"blue"
                            }}>
                                {/*<FIcon name={"motorcycle"} size={20} color={APPCOLORS.group2Left2} style={{marginRight:5,position:"absolute"}}/>*/}
                                <Text>{"?"}</Text>
                                {this.props.horizontal ?
                                    <View style={{
                                        flexDirection: "row",
                                        alignContent: "flex-start",
                                        alignItems: "flex-start",
                                        justifyContent: "flex-start",
                                        flexWrap:"nowrap",
                                        paddingLeft:20,
                                        // borderWidth:1,borderColor:"blue"
                                    }}>
                                        <Text numberOfLines={1} style={[styles.deliveryChargeRowText]}>{"Delivery "}</Text>
                                        <Text numberOfLines={1} style={[styles.deliveryChargeRowText]}>{INR_SHORT_SIGN +
                                        parseFloat(this.state.restObjModel.delivery_charges || 0).toFixed(1)}</Text>
                                    </View>:

                                    <Text numberOfLines={1} style={[styles.deliveryChargeRowText,{paddingLeft:20}]}>
                                        {"Delivery " +INR_SHORT_SIGN + parseFloat(this.state.restObjModel.delivery_charges || 0).toFixed(1)}
                                    </Text>}
                                {/*{this.dot()}*/}

                            </View>
                            <View style={{
                                flex:1,
                                flexDirection:"row",
                                alignContent:"center",
                                alignItems:"center",
                                justifyContent:"center",
                                flexWrap:"nowrap",
                                // borderWidth:1,borderColor:"yellow"
                            }}>
                                <Text style={styles.deliveryTimeText}>{"30—45 min"}</Text>

                            </View>

                            <View style={{
                                flex:1,
                                flexDirection:"row",
                                alignContent:"flex-end",
                                alignItems:"flex-end",
                                justifyContent:"flex-end",
                                flexWrap:"nowrap",
                                // borderWidth:1,borderColor:"yellow"
                            }}>
                                <Text
                                    style={styles.minOrderText}>{"Min.Order " + INR_SHORT_SIGN + global.minimumOrder}</Text>
                            </View>
                        </View>

                        <View style={{
                            flexDirection:"row",
                            borderWidth: 1,
                            borderRadius: 5,
                            borderStyle: 'dashed',
                            borderColor: "#ffc3c6",
                            backgroundColor: "#fff9f3",
                            paddingVertical: 5,
                            paddingHorizontal: 10,
                        }}>
                            <Text style={[{fontFamily:APPFONTS.bold,color:APPCOLORS.group1Main}]}>{"Special day! "}</Text>
                            <Text style={[{fontFamily:APPFONTS.light}]}>{" free Delivery and Discount 30%"}</Text>
                        </View>

                    </View>
                </View>

            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        marginBottom: 13,
        flex: 1,
        backgroundColor: "white",
        borderRadius: 5,
        paddingLeft: 0,
        paddingRight: 0,
        paddingBottom: 5,

        // borderWidth: 8,
        // borderColor:"transparent",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 1,
        shadowRadius: 9,

        elevation: 2,

    },
    restaurantBannerBox:
    {
        width: "100%",
        height: 120,
        backgroundColor: "#000",
        borderTopLeftRadius:5,
        borderTopRightRadius:5,

    },
    restaurantBanner: {
        width: "100%",
        height: 120,
        // marginTop: 5,
        alignSelf: "center",
        // borderRadius: 5,

        borderTopLeftRadius:5,
        borderTopRightRadius:5,
    },
    restaurantLogoBox: {
        width: 60,
        height: 60,
        alignSelf: "center",
        position: "absolute",
        left: 5,
        top: 55,
        overflow:"hidden",
        borderRadius: 40,
        borderColor: "#FFF",
        borderWidth: 1,
        backgroundColor: "#000",// "rgba(0,0,0,0.3)",

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.9,
        shadowRadius: 9,

        elevation: 15,
    },
    restaurantLogo: {
        width: "100%",
        height: "100%",
        alignSelf: "center",
    },
    rightBannerOverlay: {
        // width:78,
        // paddingHorizontal: 5,
        // paddingVertical:2,
        color: "white",
        // marginBottom: 5,
        fontSize:12,
        paddingLeft:15,
        // borderBottomRightRadius: 10,
        // borderTopRightRadius:10,
        fontFamily:APPFONTS.regular,
        // position:"relative",
        bottom:13,
        justifyContent:"flex-start"

    },
    discountOverlay:{
        backgroundColor: APPCOLORS.group1Main,
    },
    freeshipOverlay: {
        // backgroundColor: "#ff37fc",
    },

    restaurantName: {
        fontFamily: APPFONTS.bold,
        fontSize: 16,
    },
    ratingRight: {
        flexDirection: "row",
        // width: "auto",
        // position: "absolute",
        // right: 0,
        // top: 4
        flex:1,
        paddingVertical:4,
        justifyContent:"flex-end",
        alignItems:"flex-end",
        alignContent:"flex-end",
    },
    restaurantInfoRowBox: {
        marginLeft: 5,
        marginRight: 5,
        marginTop: 5
    },
    verifiedIcon: {
        width: 20, height: 20, position: "relative", marginTop: 0, marginLeft: -5
    },
    restaurantInfoRowOne: {
        flexDirection: "row",
        marginTop: 5,
        marginBottom: 0
    },
    restaurantInfoRowTwo: {
        flexDirection: "row",
        marginTop: 5,
        marginBottom: 5,
        alignContent: "center"
    },

    ratingNumber: {
        // marginRight: 10,
        fontSize: 12,
        marginLeft: 3,
        fontFamily:APPFONTS.light,
        // alignSelf: "flex-end"
    },
    distant: {
        marginRight: 10,
        fontSize: 12,
        marginLeft: 3,
        fontFamily:APPFONTS.light
    },
    deliveryCharge: {
        marginRight: 10,
        fontSize: 12,
        marginLeft: 3,
        fontFamily:APPFONTS.light,
        textAlign:"right",
        borderWidth:1
    },
    deliveryTime: {
        marginRight: 10,
        fontSize: 12,
        marginLeft: 3,
        fontFamily:APPFONTS.light
    },
    minimumCharge: {
        // marginRight: 10,
        fontSize: 12,
        marginLeft: 3,
        fontFamily:APPFONTS.light
    },
    promotion: {
        position: "absolute",
        // left: -10,
        left: 5,
        top: 5,
        zIndex: 1,
        // borderColor:"red",
        // borderWidth:1,
        // backgroundColor:"white",
        height: 35,
        width: "auto",
        alignItems: "center",
    },
    available: {
        width: 15,
        height: 15,
        backgroundColor: "#6aff2e",
        position: "absolute",
        borderRadius: 100,
        borderWidth: 2,
        borderColor: "#5ebf29",
        zIndex: 9,
        bottom: 5,
        right: 0
    },
    unavailable: {
        width: 15,
        height: 15,
        backgroundColor: "red",
        position: "absolute",
        borderRadius: 100,
        borderWidth: 2,
        borderColor: "red",
        zIndex: 9,
        bottom: 5,
        right: 0
    },

    unavailableMessage: {
        width: 140,
        alignContent: "center",
        alignItems: "center",
        borderTopWidth: 1,
        borderTopColor: "white",
        borderBottomWidth: 1,
        borderBottomColor: "white",
        paddingTop: 6,
        paddingBottom: 6,
    },
    temporarilyCloseMessage: {
        width: 110,
        alignContent: "center",
        alignItems: "center",
        borderTopWidth: 1,
        borderTopColor: "white",
        borderBottomWidth: 1,
        borderBottomColor: "white",
        paddingTop: 6,
        paddingBottom: 6,
    },
    unavailableMessageTextBox: {
        color: "white",
        fontSize: 16,
        fontFamily:APPFONTS.light
    },
    temporarilyCloseOverlay: {
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: 120,
        backgroundColor: "rgba(1,1,1,0.6)",
        alignItems: "center",
        alignContent: "center",
        justifyContent: "center",
        zIndex: 9,
    },
    deliveryChargeRowText:{
        flexWrap:"nowrap",
        // position:"relative",
        // top:-1,
        fontFamily:APPFONTS.light
    },
    itemDescription:{
        fontSize: 14,
        fontFamily:APPFONTS.light
    },
    deliveryTimeText:{
        textAlign:"center",
        letterSpacing:0,
        fontFamily:APPFONTS.light
    },
    minOrderText:{
        marginRight: 0,
        fontFamily:APPFONTS.light
    },

});


