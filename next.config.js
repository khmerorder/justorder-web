module.exports = {
    webpack: (config,options) => {

        const {isServer} = options;

        config.resolve.alias = {
            ...(config.resolve.alias || {}),
            // Transform all direct `react-native` imports to `react-native-web`
            'react-native$': 'react-native-web',
        }
        config.resolve.extensions = [
            '.web.js',
            '.web.ts',
            '.web.tsx',
            ...config.resolve.extensions,
        ]

        config.module.rules.push({
            test: /\.(jpe?g|png|gif|svg)$/i,
            use: [
                {
                    loader: require.resolve('file-loader'),
                    options: {
                        name: '[name].[ext]',
                        publicPath: (url) => {
                            return `/_next/static/image/${url}`;
                        },
                        outputPath: `${isServer ? '../' : ''}static/image/`,
                        esModule: false,
                    },
                },
            ],
        })

        return config
    },
}
