module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./pages/app/api/ServiceManager.js":
/*!*****************************************!*\
  !*** ./pages/app/api/ServiceManager.js ***!
  \*****************************************/
/*! exports provided: apiPost, apiGet, apiDelete, apiPostQs */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "apiPost", function() { return apiPost; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "apiGet", function() { return apiGet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "apiDelete", function() { return apiDelete; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "apiPostQs", function() { return apiPostQs; });
/**
 * Genric function to make api calls with method post
 * @param {string} url  API end point to call
 * @param {apiPost} responseSuccess  Call-back function to get success response from api call
 * @param {apiPost} responseErr  Call-back function to get error response from api call
 * @param {apiPost} requestHeader  Request header to be send to api
 * @param {apiPost} body data to be send through api
 */
async function apiPost(url, body, responseSuccess, responseErr, requestHeader = {
  "Content-Type": "application/json"
}) {
  console.log("request", JSON.stringify(body));
  return fetch(url, {
    method: "POST",
    headers: requestHeader,
    body: JSON.stringify(body)
  }).then(errorHandler).then(response => response.json()).then(json => responseSuccess(json)).catch(err => responseErr(err));
}
/**
 * Genric function to make api calls with method get
 * @param {apiGet} url  API end point to call
 * @param {apiGet} responseSuccess  Call-back function to get success response from api call
 * @param {apiGet} responseErr  Call-back function to get error response from api call
 * @param {apiGet} requestHeader  Request header to be send to api
 */

async function apiGet(url, responseSuccess, responseErr, requestHeader = {
  "Content-Type": "application/json"
}) {
  return fetch(url, {
    method: "GET",
    headers: requestHeader
  }).then(errorHandler).then(response => response.json()).then(json => responseSuccess(json)).catch(err => responseErr(err));
}
/**
 * Genric function to make api calls with method delete
 * @param {apiDelete} url  API end point to call
 * @param {apiDelete} responseSuccess  Call-back function to get success response from api call
 * @param {apiDelete} responseErr  Call-back function to get error response from api call
 * @param {apiDelete} requestHeader  Request header to be send to api
 */

function apiDelete(url, responseSuccess, responseErr, requestHeader = {
  "Content-Type": "application/json"
}) {
  fetch(url, {
    method: "DELETE",
    headers: requestHeader
  }).then(errorHandler).then(response => response.status == 204 ? response : response.json()).then(json => responseSuccess(json)).catch(err => responseErr(err));
} //Error Handler

/**
 *
 * @param {errorHandler} response Generic function to handle error occur in api
 */

const errorHandler = response => {
  console.log("Response ==>", response);

  if (response.status >= 200 && response.status < 300 || response.status == 401 || response.status == 400) {
    if (response.status == 204) {
      response.body = {
        success: "Saved"
      };
    }

    return Promise.resolve(response);
  } else {
    var error = new Error(response.statusText || response.status);
    error.response = response;
    return Promise.reject(error);
  }
};

async function apiPostQs(url, body, responseSuccess, responseErr, requestHeader = {}) {
  console.log("request", JSON.stringify(body));
  fetch(url, {
    method: "POST",
    body: body,
    headers: {
      Accept: "application/json",
      "Content-Type": "multipart/form-data"
    }
  }).then(errorHandler).then(response => response.json()).then(json => responseSuccess(json)).catch(err => responseErr(err));
}

/***/ }),

/***/ "./pages/app/assets/Colors.js":
/*!************************************!*\
  !*** ./pages/app/assets/Colors.js ***!
  \************************************/
/*! exports provided: EDColors, APPCOLORS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EDColors", function() { return EDColors; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APPCOLORS", function() { return APPCOLORS; });
const EDColors = {
  primary: "#e5006c",
  primaryDisabled: "#b0739f",
  LPrimary: "#f9daec",
  secondary: "#233646",
  secondaryButtons: "#2c6fae",
  background: "#F7F8F9",
  borderColor: 'rgb(229,227,228)',
  backgroundDark: "#eee",
  backgroundLight: 'rgb(240,240,240)',
  placeholder: "#BABAC4",
  text: "gray",
  darkText: "darkgray",
  activeTabColor: "#2F4050",
  inactiveTabColor: "#BABAC4",
  error: "red",
  themeGreen: 'rgb(31,191,193)',
  black: "black",
  white: "white",
  offWhite: 'rgb(247,247,247)',
  buttonReserve: 'rgb(24,121,191)',
  buttonUnreserve: 'rgba(255, 179, 0, 0.3)',
  buttonViewDocuments: 'rgb(25,170,137)',
  paleYellow: 'rgb(252,247,223)',
  paleBlue: 'rgb(212,234,246)',
  shadow: '#ddd',
  DARKGRAY: '#5f5f5a',
  LGRAY: '#efefea',
  CGRAY: '#c6c6c1',
  GRAY: '#b3b3ae',
  WHITE: '#fff',
  LIGHTGRAY: '#d0d0cb'
};
const APPCOLORS = {
  group1Left5: "#ffe5ec",
  group1Left4: "#ffd1cc",
  group1Left3: "#ffadc3",
  group1Left2: "#FF87A5",
  group1Left1: "#FF5781",
  group1Main: "#FF2B60",

  /* Main Primary color */
  group1Right1: "#FF0040",
  group1Right2: "#FE0040",
  group2Left5: "#c7c4ac",
  group2Left4: "#ffe1a5",
  group2Left3: "#ffd4a1",
  group2Left2: "#FFAE88",
  group2Left1: "#FF8D57",
  group2Main: "#FF702B",

  /* Main Secondary color (1) */
  group2Right1: "#FF5300",
  group2Right2: "#FF5300",
  group3Left2: "#08FCA6",
  group3Left1: "#00FFA5",
  group3Main: "#00CB84",

  /* Main Secondary color (2) */
  group3Right1: "#009C65",
  group3Right2: "#007A4F",
  group4Left3: "#9efe5c",
  group4Left2: "#64FE00",
  group4Left1: "#60F300",
  group4Main: "#55D900",

  /* Main Complement color */
  group4Right1: "#45AF00",
  group4Right2: "#348500",

  /* As RGBa codes */
  rgbaPrimary0: "rgba(255, 43, 96,1)",

  /* Main Primary color */
  rgbaPrimary1: "rgba(255,135,165,1)",
  rgbaPrimary2: "rgba(255, 87,129,1)",
  rgbaPrimary3: "rgba(255,  0, 64,1)",
  rgbaPrimary4: "rgba(254,  0, 64,1)",
  rgbaSecondary10: "rgba(255,112, 43,1)",

  /* Main Secondary color (1) */
  rgbaSecondary11: "rgba(255,174,136,1)",
  rgbaSecondary12: "rgba(255,141, 87,1)",
  rgbaSecondary13: "rgba(255, 83,  0,1)",
  rgbaSecondary14: "rgba(255, 83,  0,1)",
  rgbaSecondary20: "rgba(  0,203,132,1)",

  /* Main Secondary color (2) */
  rgbaSecondary21: "rgba(  8,252,166,1)",
  rgbaSecondary22: "rgba(  0,255,165,1)",
  rgbaSecondary23: "rgba(  0,156,101,1)",
  rgbaSecondary24: "rgba(  0,122, 79,1)",
  rgbaComplement20: "rgba( 85,217,  0,1)",

  /* Main Complement color */
  rgbaComplement21: "rgba(100,254,  0,1)",
  rgbaComplement22: "rgba( 96,243,  0,1)",
  rgbaComplement23: "rgba( 69,175,  0,1)",
  rgbaComplement24: "rgba( 52,133,  0,1)"
};

/***/ }),

/***/ "./pages/app/assets/FontConstants.js":
/*!*******************************************!*\
  !*** ./pages/app/assets/FontConstants.js ***!
  \*******************************************/
/*! exports provided: ETFonts, APPFONTS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ETFonts", function() { return ETFonts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APPFONTS", function() { return APPFONTS; });
const ETFonts = {
  // bold: "Lato-Bold",
  // black: "Lato-Black",
  // regular: "Lato-Regular",
  // light: "Lato-Light",
  // hairline: "Lato-Hairline",
  // satisfy : "Satisfy-Regular",
  // iconFont: "iconfont",
  bold: "System",
  black: "System",
  regular: "System",
  light: "System",
  hairline: "System",
  satisfy: "System",
  iconFont: "System"
};
const APPFONTS = {
  bold: "Roboto-Bold",
  black: "Robot-Black",
  boldItalic: "Roboto-BoldItalic",
  italic: "Roboto-Italic",
  light: "Roboto-Light",
  lightItalic: "Roboto-LightItalic",
  medium: "Roboto-Medium",
  meduimItalic: "Roboto-MediumItalic",
  regular: "Roboto-Regular",
  thin: "Roboto-Thin",
  thinItalic: "Roboto-ThinItalic"
};

/***/ }),

/***/ "./pages/app/assets/image/about_us.png":
/*!*********************************************!*\
  !*** ./pages/app/assets/image/about_us.png ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/about_us.png";

/***/ }),

/***/ "./pages/app/assets/image/aboutus_selected.png":
/*!*****************************************************!*\
  !*** ./pages/app/assets/image/aboutus_selected.png ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/aboutus_selected.png";

/***/ }),

/***/ "./pages/app/assets/image/aboutusdeselected.png":
/*!******************************************************!*\
  !*** ./pages/app/assets/image/aboutusdeselected.png ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/aboutusdeselected.png";

/***/ }),

/***/ "./pages/app/assets/image/add.png":
/*!****************************************!*\
  !*** ./pages/app/assets/image/add.png ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/add.png";

/***/ }),

/***/ "./pages/app/assets/image/address.png":
/*!********************************************!*\
  !*** ./pages/app/assets/image/address.png ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/address.png";

/***/ }),

/***/ "./pages/app/assets/image/back.png":
/*!*****************************************!*\
  !*** ./pages/app/assets/image/back.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/back.png";

/***/ }),

/***/ "./pages/app/assets/image/banner-restaurant.jpg":
/*!******************************************************!*\
  !*** ./pages/app/assets/image/banner-restaurant.jpg ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/banner-restaurant.jpg";

/***/ }),

/***/ "./pages/app/assets/image/banner_placeholder.png":
/*!*******************************************************!*\
  !*** ./pages/app/assets/image/banner_placeholder.png ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/banner_placeholder.png";

/***/ }),

/***/ "./pages/app/assets/image/bg_login.png":
/*!*********************************************!*\
  !*** ./pages/app/assets/image/bg_login.png ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/bg_login.png";

/***/ }),

/***/ "./pages/app/assets/image/bg_splash.png":
/*!**********************************************!*\
  !*** ./pages/app/assets/image/bg_splash.png ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/bg_splash.png";

/***/ }),

/***/ "./pages/app/assets/image/bookingavailable.png":
/*!*****************************************************!*\
  !*** ./pages/app/assets/image/bookingavailable.png ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/bookingavailable.png";

/***/ }),

/***/ "./pages/app/assets/image/bookingnotavailable.png":
/*!********************************************************!*\
  !*** ./pages/app/assets/image/bookingnotavailable.png ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/bookingnotavailable.png";

/***/ }),

/***/ "./pages/app/assets/image/brand-icon.png":
/*!***********************************************!*\
  !*** ./pages/app/assets/image/brand-icon.png ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/brand-icon.png";

/***/ }),

/***/ "./pages/app/assets/image/business_icons/alcohol.png":
/*!***********************************************************!*\
  !*** ./pages/app/assets/image/business_icons/alcohol.png ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/alcohol.png";

/***/ }),

/***/ "./pages/app/assets/image/business_icons/express.png":
/*!***********************************************************!*\
  !*** ./pages/app/assets/image/business_icons/express.png ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/express.png";

/***/ }),

/***/ "./pages/app/assets/image/business_icons/food.png":
/*!********************************************************!*\
  !*** ./pages/app/assets/image/business_icons/food.png ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/food.png";

/***/ }),

/***/ "./pages/app/assets/image/business_icons/grocery.png":
/*!***********************************************************!*\
  !*** ./pages/app/assets/image/business_icons/grocery.png ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/grocery.png";

/***/ }),

/***/ "./pages/app/assets/image/calendar.png":
/*!*********************************************!*\
  !*** ./pages/app/assets/image/calendar.png ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/calendar.png";

/***/ }),

/***/ "./pages/app/assets/image/calendarwhite.png":
/*!**************************************************!*\
  !*** ./pages/app/assets/image/calendarwhite.png ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/calendarwhite.png";

/***/ }),

/***/ "./pages/app/assets/image/call.png":
/*!*****************************************!*\
  !*** ./pages/app/assets/image/call.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/call.png";

/***/ }),

/***/ "./pages/app/assets/image/call_order.png":
/*!***********************************************!*\
  !*** ./pages/app/assets/image/call_order.png ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/call_order.png";

/***/ }),

/***/ "./pages/app/assets/image/camera_white.png":
/*!*************************************************!*\
  !*** ./pages/app/assets/image/camera_white.png ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/camera_white.png";

/***/ }),

/***/ "./pages/app/assets/image/cart.png":
/*!*****************************************!*\
  !*** ./pages/app/assets/image/cart.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/cart.png";

/***/ }),

/***/ "./pages/app/assets/image/cart_black.png":
/*!***********************************************!*\
  !*** ./pages/app/assets/image/cart_black.png ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/cart_black.png";

/***/ }),

/***/ "./pages/app/assets/image/category-icon.png":
/*!**************************************************!*\
  !*** ./pages/app/assets/image/category-icon.png ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/category-icon.png";

/***/ }),

/***/ "./pages/app/assets/image/clock_white.png":
/*!************************************************!*\
  !*** ./pages/app/assets/image/clock_white.png ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/clock_white.png";

/***/ }),

/***/ "./pages/app/assets/image/confirm_background.png":
/*!*******************************************************!*\
  !*** ./pages/app/assets/image/confirm_background.png ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/confirm_background.png";

/***/ }),

/***/ "./pages/app/assets/image/confirm_thumb.png":
/*!**************************************************!*\
  !*** ./pages/app/assets/image/confirm_thumb.png ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/confirm_thumb.png";

/***/ }),

/***/ "./pages/app/assets/image/contactus_deselected.png":
/*!*********************************************************!*\
  !*** ./pages/app/assets/image/contactus_deselected.png ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/contactus_deselected.png";

/***/ }),

/***/ "./pages/app/assets/image/contactus_selected.png":
/*!*******************************************************!*\
  !*** ./pages/app/assets/image/contactus_selected.png ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/contactus_selected.png";

/***/ }),

/***/ "./pages/app/assets/image/coundown.jpg":
/*!*********************************************!*\
  !*** ./pages/app/assets/image/coundown.jpg ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/coundown.jpg";

/***/ }),

/***/ "./pages/app/assets/image/cuisines_examples/Coffee.jpg":
/*!*************************************************************!*\
  !*** ./pages/app/assets/image/cuisines_examples/Coffee.jpg ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/Coffee.jpg";

/***/ }),

/***/ "./pages/app/assets/image/cuisines_examples/Japanese.jpg":
/*!***************************************************************!*\
  !*** ./pages/app/assets/image/cuisines_examples/Japanese.jpg ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/Japanese.jpg";

/***/ }),

/***/ "./pages/app/assets/image/cuisines_examples/Milk Tea.jpg":
/*!***************************************************************!*\
  !*** ./pages/app/assets/image/cuisines_examples/Milk Tea.jpg ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/Milk Tea.jpg";

/***/ }),

/***/ "./pages/app/assets/image/cuisines_examples/Western.jpg":
/*!**************************************************************!*\
  !*** ./pages/app/assets/image/cuisines_examples/Western.jpg ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/Western.jpg";

/***/ }),

/***/ "./pages/app/assets/image/currency.png":
/*!*********************************************!*\
  !*** ./pages/app/assets/image/currency.png ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/currency.png";

/***/ }),

/***/ "./pages/app/assets/image/delete.png":
/*!*******************************************!*\
  !*** ./pages/app/assets/image/delete.png ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/delete.png";

/***/ }),

/***/ "./pages/app/assets/image/delete_White.png":
/*!*************************************************!*\
  !*** ./pages/app/assets/image/delete_White.png ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/delete_White.png";

/***/ }),

/***/ "./pages/app/assets/image/delete_gray.png":
/*!************************************************!*\
  !*** ./pages/app/assets/image/delete_gray.png ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/delete_gray.png";

/***/ }),

/***/ "./pages/app/assets/image/delivereddeselected.png":
/*!********************************************************!*\
  !*** ./pages/app/assets/image/delivereddeselected.png ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/delivereddeselected.png";

/***/ }),

/***/ "./pages/app/assets/image/deliveredselected.png":
/*!******************************************************!*\
  !*** ./pages/app/assets/image/deliveredselected.png ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/deliveredselected.png";

/***/ }),

/***/ "./pages/app/assets/image/discount_stick.png":
/*!***************************************************!*\
  !*** ./pages/app/assets/image/discount_stick.png ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/discount_stick.png";

/***/ }),

/***/ "./pages/app/assets/image/down.png":
/*!*****************************************!*\
  !*** ./pages/app/assets/image/down.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/down.png";

/***/ }),

/***/ "./pages/app/assets/image/driver.png":
/*!*******************************************!*\
  !*** ./pages/app/assets/image/driver.png ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/driver.png";

/***/ }),

/***/ "./pages/app/assets/image/edit_profile.png":
/*!*************************************************!*\
  !*** ./pages/app/assets/image/edit_profile.png ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/edit_profile.png";

/***/ }),

/***/ "./pages/app/assets/image/empty-cart.png":
/*!***********************************************!*\
  !*** ./pages/app/assets/image/empty-cart.png ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/empty-cart.png";

/***/ }),

/***/ "./pages/app/assets/image/eventbooking_deselected.png":
/*!************************************************************!*\
  !*** ./pages/app/assets/image/eventbooking_deselected.png ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/eventbooking_deselected.png";

/***/ }),

/***/ "./pages/app/assets/image/eventbooking_selected.png":
/*!**********************************************************!*\
  !*** ./pages/app/assets/image/eventbooking_selected.png ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/eventbooking_selected.png";

/***/ }),

/***/ "./pages/app/assets/image/filter.png":
/*!*******************************************!*\
  !*** ./pages/app/assets/image/filter.png ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/filter.png";

/***/ }),

/***/ "./pages/app/assets/image/flags/kh.png":
/*!*********************************************!*\
  !*** ./pages/app/assets/image/flags/kh.png ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/kh.png";

/***/ }),

/***/ "./pages/app/assets/image/food_icons/all_cuisines.png":
/*!************************************************************!*\
  !*** ./pages/app/assets/image/food_icons/all_cuisines.png ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/all_cuisines.png";

/***/ }),

/***/ "./pages/app/assets/image/food_icons/asian.png":
/*!*****************************************************!*\
  !*** ./pages/app/assets/image/food_icons/asian.png ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/asian.png";

/***/ }),

/***/ "./pages/app/assets/image/food_icons/bbq.png":
/*!***************************************************!*\
  !*** ./pages/app/assets/image/food_icons/bbq.png ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/bbq.png";

/***/ }),

/***/ "./pages/app/assets/image/food_icons/chinese.png":
/*!*******************************************************!*\
  !*** ./pages/app/assets/image/food_icons/chinese.png ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/chinese.png";

/***/ }),

/***/ "./pages/app/assets/image/food_icons/coffee.png":
/*!******************************************************!*\
  !*** ./pages/app/assets/image/food_icons/coffee.png ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/coffee.png";

/***/ }),

/***/ "./pages/app/assets/image/food_icons/italian.png":
/*!*******************************************************!*\
  !*** ./pages/app/assets/image/food_icons/italian.png ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/italian.png";

/***/ }),

/***/ "./pages/app/assets/image/food_icons/japanese.png":
/*!********************************************************!*\
  !*** ./pages/app/assets/image/food_icons/japanese.png ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/japanese.png";

/***/ }),

/***/ "./pages/app/assets/image/food_icons/offers.png":
/*!******************************************************!*\
  !*** ./pages/app/assets/image/food_icons/offers.png ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/offers.png";

/***/ }),

/***/ "./pages/app/assets/image/food_icons/soup.png":
/*!****************************************************!*\
  !*** ./pages/app/assets/image/food_icons/soup.png ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/soup.png";

/***/ }),

/***/ "./pages/app/assets/image/food_icons/view_all.png":
/*!********************************************************!*\
  !*** ./pages/app/assets/image/food_icons/view_all.png ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/view_all.png";

/***/ }),

/***/ "./pages/app/assets/image/headline.png":
/*!*********************************************!*\
  !*** ./pages/app/assets/image/headline.png ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/headline.png";

/***/ }),

/***/ "./pages/app/assets/image/home-icon.png":
/*!**********************************************!*\
  !*** ./pages/app/assets/image/home-icon.png ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/home-icon.png";

/***/ }),

/***/ "./pages/app/assets/image/home_deselected.png":
/*!****************************************************!*\
  !*** ./pages/app/assets/image/home_deselected.png ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/home_deselected.png";

/***/ }),

/***/ "./pages/app/assets/image/home_functions/category.png":
/*!************************************************************!*\
  !*** ./pages/app/assets/image/home_functions/category.png ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/category.png";

/***/ }),

/***/ "./pages/app/assets/image/home_functions/community.png":
/*!*************************************************************!*\
  !*** ./pages/app/assets/image/home_functions/community.png ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/community.png";

/***/ }),

/***/ "./pages/app/assets/image/home_functions/coupon_deals.png":
/*!****************************************************************!*\
  !*** ./pages/app/assets/image/home_functions/coupon_deals.png ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/coupon_deals.png";

/***/ }),

/***/ "./pages/app/assets/image/home_functions/flash_sale.png":
/*!**************************************************************!*\
  !*** ./pages/app/assets/image/home_functions/flash_sale.png ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/flash_sale.png";

/***/ }),

/***/ "./pages/app/assets/image/home_functions/hot.png":
/*!*******************************************************!*\
  !*** ./pages/app/assets/image/home_functions/hot.png ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/hot.png";

/***/ }),

/***/ "./pages/app/assets/image/home_functions/new.png":
/*!*******************************************************!*\
  !*** ./pages/app/assets/image/home_functions/new.png ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/new.png";

/***/ }),

/***/ "./pages/app/assets/image/home_functions/share_gifts.png":
/*!***************************************************************!*\
  !*** ./pages/app/assets/image/home_functions/share_gifts.png ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/share_gifts.png";

/***/ }),

/***/ "./pages/app/assets/image/home_functions/under10.png":
/*!***********************************************************!*\
  !*** ./pages/app/assets/image/home_functions/under10.png ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/under10.png";

/***/ }),

/***/ "./pages/app/assets/image/home_selected.png":
/*!**************************************************!*\
  !*** ./pages/app/assets/image/home_selected.png ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/home_selected.png";

/***/ }),

/***/ "./pages/app/assets/image/hot-icon.png":
/*!*********************************************!*\
  !*** ./pages/app/assets/image/hot-icon.png ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/hot-icon.png";

/***/ }),

/***/ "./pages/app/assets/image/ic_back_reservation.png":
/*!********************************************************!*\
  !*** ./pages/app/assets/image/ic_back_reservation.png ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/ic_back_reservation.png";

/***/ }),

/***/ "./pages/app/assets/image/ic_close.png":
/*!*********************************************!*\
  !*** ./pages/app/assets/image/ic_close.png ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/ic_close.png";

/***/ }),

/***/ "./pages/app/assets/image/itemNotFound.png":
/*!*************************************************!*\
  !*** ./pages/app/assets/image/itemNotFound.png ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/itemNotFound.png";

/***/ }),

/***/ "./pages/app/assets/image/lang.png":
/*!*****************************************!*\
  !*** ./pages/app/assets/image/lang.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/lang.png";

/***/ }),

/***/ "./pages/app/assets/image/leftTopRibbon.png":
/*!**************************************************!*\
  !*** ./pages/app/assets/image/leftTopRibbon.png ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/leftTopRibbon.png";

/***/ }),

/***/ "./pages/app/assets/image/location.png":
/*!*********************************************!*\
  !*** ./pages/app/assets/image/location.png ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/location.png";

/***/ }),

/***/ "./pages/app/assets/image/location_grey.png":
/*!**************************************************!*\
  !*** ./pages/app/assets/image/location_grey.png ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/location_grey.png";

/***/ }),

/***/ "./pages/app/assets/image/logo.png":
/*!*****************************************!*\
  !*** ./pages/app/assets/image/logo.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/logo.png";

/***/ }),

/***/ "./pages/app/assets/image/map_markers/customer.png":
/*!*********************************************************!*\
  !*** ./pages/app/assets/image/map_markers/customer.png ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/customer.png";

/***/ }),

/***/ "./pages/app/assets/image/map_markers/map-cropped.jpg":
/*!************************************************************!*\
  !*** ./pages/app/assets/image/map_markers/map-cropped.jpg ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/map-cropped.jpg";

/***/ }),

/***/ "./pages/app/assets/image/menu.png":
/*!*****************************************!*\
  !*** ./pages/app/assets/image/menu.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/menu.png";

/***/ }),

/***/ "./pages/app/assets/image/merchan-ads.png":
/*!************************************************!*\
  !*** ./pages/app/assets/image/merchan-ads.png ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/merchan-ads.png";

/***/ }),

/***/ "./pages/app/assets/image/minus_round.png":
/*!************************************************!*\
  !*** ./pages/app/assets/image/minus_round.png ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/minus_round.png";

/***/ }),

/***/ "./pages/app/assets/image/minusqty.png":
/*!*********************************************!*\
  !*** ./pages/app/assets/image/minusqty.png ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/minusqty.png";

/***/ }),

/***/ "./pages/app/assets/image/more.png":
/*!*****************************************!*\
  !*** ./pages/app/assets/image/more.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/more.png";

/***/ }),

/***/ "./pages/app/assets/image/moto.png":
/*!*****************************************!*\
  !*** ./pages/app/assets/image/moto.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/moto.png";

/***/ }),

/***/ "./pages/app/assets/image/mybooking_deselected.png":
/*!*********************************************************!*\
  !*** ./pages/app/assets/image/mybooking_deselected.png ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/mybooking_deselected.png";

/***/ }),

/***/ "./pages/app/assets/image/mybooking_selected.png":
/*!*******************************************************!*\
  !*** ./pages/app/assets/image/mybooking_selected.png ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/mybooking_selected.png";

/***/ }),

/***/ "./pages/app/assets/image/name.png":
/*!*****************************************!*\
  !*** ./pages/app/assets/image/name.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/name.png";

/***/ }),

/***/ "./pages/app/assets/image/noproducts.jpg":
/*!***********************************************!*\
  !*** ./pages/app/assets/image/noproducts.jpg ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/noproducts.jpg";

/***/ }),

/***/ "./pages/app/assets/image/notification_deselected.png":
/*!************************************************************!*\
  !*** ./pages/app/assets/image/notification_deselected.png ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/notification_deselected.png";

/***/ }),

/***/ "./pages/app/assets/image/notification_selected.png":
/*!**********************************************************!*\
  !*** ./pages/app/assets/image/notification_selected.png ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/notification_selected.png";

/***/ }),

/***/ "./pages/app/assets/image/onthewaydeselected.png":
/*!*******************************************************!*\
  !*** ./pages/app/assets/image/onthewaydeselected.png ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/onthewaydeselected.png";

/***/ }),

/***/ "./pages/app/assets/image/onthewayselected.png":
/*!*****************************************************!*\
  !*** ./pages/app/assets/image/onthewayselected.png ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/onthewayselected.png";

/***/ }),

/***/ "./pages/app/assets/image/order_deselected.png":
/*!*****************************************************!*\
  !*** ./pages/app/assets/image/order_deselected.png ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/order_deselected.png";

/***/ }),

/***/ "./pages/app/assets/image/order_selected.png":
/*!***************************************************!*\
  !*** ./pages/app/assets/image/order_selected.png ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/order_selected.png";

/***/ }),

/***/ "./pages/app/assets/image/orderplacedselected.png":
/*!********************************************************!*\
  !*** ./pages/app/assets/image/orderplacedselected.png ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/orderplacedselected.png";

/***/ }),

/***/ "./pages/app/assets/image/password.png":
/*!*********************************************!*\
  !*** ./pages/app/assets/image/password.png ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/password.png";

/***/ }),

/***/ "./pages/app/assets/image/payment_gateways/abapay.png":
/*!************************************************************!*\
  !*** ./pages/app/assets/image/payment_gateways/abapay.png ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/abapay.png";

/***/ }),

/***/ "./pages/app/assets/image/payment_gateways/card.png":
/*!**********************************************************!*\
  !*** ./pages/app/assets/image/payment_gateways/card.png ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/card.png";

/***/ }),

/***/ "./pages/app/assets/image/payment_gateways/visa_card.png":
/*!***************************************************************!*\
  !*** ./pages/app/assets/image/payment_gateways/visa_card.png ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/visa_card.png";

/***/ }),

/***/ "./pages/app/assets/image/payment_gateways/wing.png":
/*!**********************************************************!*\
  !*** ./pages/app/assets/image/payment_gateways/wing.png ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/wing.png";

/***/ }),

/***/ "./pages/app/assets/image/people.png":
/*!*******************************************!*\
  !*** ./pages/app/assets/image/people.png ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/people.png";

/***/ }),

/***/ "./pages/app/assets/image/pizza.jpg":
/*!******************************************!*\
  !*** ./pages/app/assets/image/pizza.jpg ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/pizza.jpg";

/***/ }),

/***/ "./pages/app/assets/image/plus.png":
/*!*****************************************!*\
  !*** ./pages/app/assets/image/plus.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/plus.png";

/***/ }),

/***/ "./pages/app/assets/image/plus_round.png":
/*!***********************************************!*\
  !*** ./pages/app/assets/image/plus_round.png ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/plus_round.png";

/***/ }),

/***/ "./pages/app/assets/image/plusqty.png":
/*!********************************************!*\
  !*** ./pages/app/assets/image/plusqty.png ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/plusqty.png";

/***/ }),

/***/ "./pages/app/assets/image/preparingdeselected.png":
/*!********************************************************!*\
  !*** ./pages/app/assets/image/preparingdeselected.png ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/preparingdeselected.png";

/***/ }),

/***/ "./pages/app/assets/image/preparingselected.png":
/*!******************************************************!*\
  !*** ./pages/app/assets/image/preparingselected.png ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/preparingselected.png";

/***/ }),

/***/ "./pages/app/assets/image/privacy_policy.png":
/*!***************************************************!*\
  !*** ./pages/app/assets/image/privacy_policy.png ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/privacy_policy.png";

/***/ }),

/***/ "./pages/app/assets/image/privacypolicy_deselected.png":
/*!*************************************************************!*\
  !*** ./pages/app/assets/image/privacypolicy_deselected.png ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/privacypolicy_deselected.png";

/***/ }),

/***/ "./pages/app/assets/image/privacypolicy_selected.png":
/*!***********************************************************!*\
  !*** ./pages/app/assets/image/privacypolicy_selected.png ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/privacypolicy_selected.png";

/***/ }),

/***/ "./pages/app/assets/image/profile_address.png":
/*!****************************************************!*\
  !*** ./pages/app/assets/image/profile_address.png ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/profile_address.png";

/***/ }),

/***/ "./pages/app/assets/image/rateus_deselected.png":
/*!******************************************************!*\
  !*** ./pages/app/assets/image/rateus_deselected.png ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/rateus_deselected.png";

/***/ }),

/***/ "./pages/app/assets/image/rateus_selected.png":
/*!****************************************************!*\
  !*** ./pages/app/assets/image/rateus_selected.png ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/rateus_selected.png";

/***/ }),

/***/ "./pages/app/assets/image/recipe_deselected.png":
/*!******************************************************!*\
  !*** ./pages/app/assets/image/recipe_deselected.png ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/recipe_deselected.png";

/***/ }),

/***/ "./pages/app/assets/image/recipe_search.png":
/*!**************************************************!*\
  !*** ./pages/app/assets/image/recipe_search.png ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/recipe_search.png";

/***/ }),

/***/ "./pages/app/assets/image/recipe_selected.png":
/*!****************************************************!*\
  !*** ./pages/app/assets/image/recipe_selected.png ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/recipe_selected.png";

/***/ }),

/***/ "./pages/app/assets/image/rest_logo.png":
/*!**********************************************!*\
  !*** ./pages/app/assets/image/rest_logo.png ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/rest_logo.png";

/***/ }),

/***/ "./pages/app/assets/image/restaurant_menu.png":
/*!****************************************************!*\
  !*** ./pages/app/assets/image/restaurant_menu.png ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/restaurant_menu.png";

/***/ }),

/***/ "./pages/app/assets/image/right-arrow.png":
/*!************************************************!*\
  !*** ./pages/app/assets/image/right-arrow.png ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/right-arrow.png";

/***/ }),

/***/ "./pages/app/assets/image/rightTopRibbon.png":
/*!***************************************************!*\
  !*** ./pages/app/assets/image/rightTopRibbon.png ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/rightTopRibbon.png";

/***/ }),

/***/ "./pages/app/assets/image/search.png":
/*!*******************************************!*\
  !*** ./pages/app/assets/image/search.png ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/search.png";

/***/ }),

/***/ "./pages/app/assets/image/settings_selected.png":
/*!******************************************************!*\
  !*** ./pages/app/assets/image/settings_selected.png ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/settings_selected.png";

/***/ }),

/***/ "./pages/app/assets/image/shareus_deselected.png":
/*!*******************************************************!*\
  !*** ./pages/app/assets/image/shareus_deselected.png ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/shareus_deselected.png";

/***/ }),

/***/ "./pages/app/assets/image/shareus_selected.png":
/*!*****************************************************!*\
  !*** ./pages/app/assets/image/shareus_selected.png ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/shareus_selected.png";

/***/ }),

/***/ "./pages/app/assets/image/sign_up.png":
/*!********************************************!*\
  !*** ./pages/app/assets/image/sign_up.png ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/sign_up.png";

/***/ }),

/***/ "./pages/app/assets/image/signout_deselected.png":
/*!*******************************************************!*\
  !*** ./pages/app/assets/image/signout_deselected.png ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/signout_deselected.png";

/***/ }),

/***/ "./pages/app/assets/image/signout_selected.png":
/*!*****************************************************!*\
  !*** ./pages/app/assets/image/signout_selected.png ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/signout_selected.png";

/***/ }),

/***/ "./pages/app/assets/image/slideshow_example/1.png":
/*!********************************************************!*\
  !*** ./pages/app/assets/image/slideshow_example/1.png ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/1.png";

/***/ }),

/***/ "./pages/app/assets/image/slideshow_example/2.png":
/*!********************************************************!*\
  !*** ./pages/app/assets/image/slideshow_example/2.png ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/2.png";

/***/ }),

/***/ "./pages/app/assets/image/slideshow_example/3.png":
/*!********************************************************!*\
  !*** ./pages/app/assets/image/slideshow_example/3.png ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/3.png";

/***/ }),

/***/ "./pages/app/assets/image/slideshow_example/4.png":
/*!********************************************************!*\
  !*** ./pages/app/assets/image/slideshow_example/4.png ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/4.png";

/***/ }),

/***/ "./pages/app/assets/image/star.png":
/*!*****************************************!*\
  !*** ./pages/app/assets/image/star.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/star.png";

/***/ }),

/***/ "./pages/app/assets/image/star_white.png":
/*!***********************************************!*\
  !*** ./pages/app/assets/image/star_white.png ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/star_white.png";

/***/ }),

/***/ "./pages/app/assets/image/super_deals-icon.png":
/*!*****************************************************!*\
  !*** ./pages/app/assets/image/super_deals-icon.png ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/super_deals-icon.png";

/***/ }),

/***/ "./pages/app/assets/image/tick.png":
/*!*****************************************!*\
  !*** ./pages/app/assets/image/tick.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/tick.png";

/***/ }),

/***/ "./pages/app/assets/image/time.png":
/*!*****************************************!*\
  !*** ./pages/app/assets/image/time.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/time.png";

/***/ }),

/***/ "./pages/app/assets/image/up_arrow.png":
/*!*********************************************!*\
  !*** ./pages/app/assets/image/up_arrow.png ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/up_arrow.png";

/***/ }),

/***/ "./pages/app/assets/image/user_placeholder.png":
/*!*****************************************************!*\
  !*** ./pages/app/assets/image/user_placeholder.png ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/user_placeholder.png";

/***/ }),

/***/ "./pages/app/assets/image/vacancy-ads.png":
/*!************************************************!*\
  !*** ./pages/app/assets/image/vacancy-ads.png ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/vacancy-ads.png";

/***/ }),

/***/ "./pages/app/assets/image/verified-shield.png":
/*!****************************************************!*\
  !*** ./pages/app/assets/image/verified-shield.png ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/_next/static/image/verified-shield.png";

/***/ }),

/***/ "./pages/app/assets/index.js":
/*!***********************************!*\
  !*** ./pages/app/assets/index.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);


const Assets = {
  bgHome: __webpack_require__(/*! ./image/bg_splash.png */ "./pages/app/assets/image/bg_splash.png"),
  // bgSignup:require("./image/bg_signup.png"),
  bgSignup: __webpack_require__(/*! ./image/sign_up.png */ "./pages/app/assets/image/sign_up.png"),
  setting: __webpack_require__(/*! ./image/settings_selected.png */ "./pages/app/assets/image/settings_selected.png"),
  backWhite: __webpack_require__(/*! ./image/back.png */ "./pages/app/assets/image/back.png"),
  menu: __webpack_require__(/*! ./image/menu.png */ "./pages/app/assets/image/menu.png"),
  more: __webpack_require__(/*! ./image/more.png */ "./pages/app/assets/image/more.png"),
  add: __webpack_require__(/*! ./image/add.png */ "./pages/app/assets/image/add.png"),
  logo: __webpack_require__(/*! ./image/logo.png */ "./pages/app/assets/image/logo.png"),
  driver: __webpack_require__(/*! ./image/driver.png */ "./pages/app/assets/image/driver.png"),
  call_order: __webpack_require__(/*! ./image/call_order.png */ "./pages/app/assets/image/call_order.png"),
  refresh: './image/refresh.png',
  //Eatance
  // bg_login:require("./image/sign_in.png"),
  bg_login: __webpack_require__(/*! ./image/bg_login.png */ "./pages/app/assets/image/bg_login.png"),
  ic_location: __webpack_require__(/*! ./image/location.png */ "./pages/app/assets/image/location.png"),
  rating: __webpack_require__(/*! ./image/rateus_selected.png */ "./pages/app/assets/image/rateus_selected.png"),
  time: __webpack_require__(/*! ./image/time.png */ "./pages/app/assets/image/time.png"),
  restMenu: __webpack_require__(/*! ./image/restaurant_menu.png */ "./pages/app/assets/image/restaurant_menu.png"),
  address: __webpack_require__(/*! ./image/address.png */ "./pages/app/assets/image/address.png"),
  ic_search: __webpack_require__(/*! ./image/search.png */ "./pages/app/assets/image/search.png"),
  recipe_search: __webpack_require__(/*! ./image/recipe_search.png */ "./pages/app/assets/image/recipe_search.png"),
  ic_location_grey: __webpack_require__(/*! ./image/location_grey.png */ "./pages/app/assets/image/location_grey.png"),
  ic_plus: __webpack_require__(/*! ./image/plusqty.png */ "./pages/app/assets/image/plusqty.png"),
  ic_minus: __webpack_require__(/*! ./image/minusqty.png */ "./pages/app/assets/image/minusqty.png"),
  ic_reservation_back: __webpack_require__(/*! ./image/ic_back_reservation.png */ "./pages/app/assets/image/ic_back_reservation.png"),
  ic_plus_yellow: __webpack_require__(/*! ./image/plus.png */ "./pages/app/assets/image/plus.png"),
  ic_filter: __webpack_require__(/*! ./image/filter.png */ "./pages/app/assets/image/filter.png"),
  ic_cart: __webpack_require__(/*! ./image/cart.png */ "./pages/app/assets/image/cart.png"),
  bl_cart: __webpack_require__(/*! ./image/cart_black.png */ "./pages/app/assets/image/cart_black.png"),
  ic_up_arrow: __webpack_require__(/*! ./image/up_arrow.png */ "./pages/app/assets/image/up_arrow.png"),
  ic_down_arrow: __webpack_require__(/*! ./image/down.png */ "./pages/app/assets/image/down.png"),
  currency: __webpack_require__(/*! ./image/currency.png */ "./pages/app/assets/image/currency.png"),
  sample_rest_banner: __webpack_require__(/*! ./image/banner-restaurant.jpg */ "./pages/app/assets/image/banner-restaurant.jpg"),
  sample_rest_logo: __webpack_require__(/*! ./image/rest_logo.png */ "./pages/app/assets/image/rest_logo.png"),
  star: __webpack_require__(/*! ./image/star.png */ "./pages/app/assets/image/star.png"),
  verified: __webpack_require__(/*! ./image/verified-shield.png */ "./pages/app/assets/image/verified-shield.png"),
  // deselected icons
  home_deselect: __webpack_require__(/*! ./image/home_deselected.png */ "./pages/app/assets/image/home_deselected.png"),
  recipe_deselect: __webpack_require__(/*! ./image/recipe_deselected.png */ "./pages/app/assets/image/recipe_deselected.png"),
  order_deselect: __webpack_require__(/*! ./image/order_deselected.png */ "./pages/app/assets/image/order_deselected.png"),
  eventbooking_deselect: __webpack_require__(/*! ./image/eventbooking_deselected.png */ "./pages/app/assets/image/eventbooking_deselected.png"),
  mybooking_deselect: __webpack_require__(/*! ./image/mybooking_deselected.png */ "./pages/app/assets/image/mybooking_deselected.png"),
  notification_deselect: __webpack_require__(/*! ./image/notification_deselected.png */ "./pages/app/assets/image/notification_deselected.png"),
  rateus_deselect: __webpack_require__(/*! ./image/rateus_deselected.png */ "./pages/app/assets/image/rateus_deselected.png"),
  shareus_deselect: __webpack_require__(/*! ./image/shareus_deselected.png */ "./pages/app/assets/image/shareus_deselected.png"),
  privacypolicy_deselect: __webpack_require__(/*! ./image/privacypolicy_deselected.png */ "./pages/app/assets/image/privacypolicy_deselected.png"),
  aboutus_deselect: __webpack_require__(/*! ./image/aboutusdeselected.png */ "./pages/app/assets/image/aboutusdeselected.png"),
  contactus_deselect: __webpack_require__(/*! ./image/contactus_deselected.png */ "./pages/app/assets/image/contactus_deselected.png"),
  signout_deselect: __webpack_require__(/*! ./image/signout_deselected.png */ "./pages/app/assets/image/signout_deselected.png"),
  // selected icons
  home_select: __webpack_require__(/*! ./image/home_selected.png */ "./pages/app/assets/image/home_selected.png"),
  recipe_select: __webpack_require__(/*! ./image/recipe_selected.png */ "./pages/app/assets/image/recipe_selected.png"),
  order_select: __webpack_require__(/*! ./image/order_selected.png */ "./pages/app/assets/image/order_selected.png"),
  eventbooking_select: __webpack_require__(/*! ./image/eventbooking_selected.png */ "./pages/app/assets/image/eventbooking_selected.png"),
  mybooking_select: __webpack_require__(/*! ./image/mybooking_selected.png */ "./pages/app/assets/image/mybooking_selected.png"),
  notification_select: __webpack_require__(/*! ./image/notification_selected.png */ "./pages/app/assets/image/notification_selected.png"),
  rateus_select: __webpack_require__(/*! ./image/rateus_selected.png */ "./pages/app/assets/image/rateus_selected.png"),
  shareus_select: __webpack_require__(/*! ./image/shareus_selected.png */ "./pages/app/assets/image/shareus_selected.png"),
  privacypolicy_select: __webpack_require__(/*! ./image/privacypolicy_selected.png */ "./pages/app/assets/image/privacypolicy_selected.png"),
  aboutus_select: __webpack_require__(/*! ./image/aboutus_selected.png */ "./pages/app/assets/image/aboutus_selected.png"),
  contactus_select: __webpack_require__(/*! ./image/contactus_selected.png */ "./pages/app/assets/image/contactus_selected.png"),
  signout_select: __webpack_require__(/*! ./image/signout_selected.png */ "./pages/app/assets/image/signout_selected.png"),
  deliveredselected: __webpack_require__(/*! ./image/deliveredselected.png */ "./pages/app/assets/image/deliveredselected.png"),
  delivereddeselected: __webpack_require__(/*! ./image/delivereddeselected.png */ "./pages/app/assets/image/delivereddeselected.png"),
  orderplacedselected: __webpack_require__(/*! ./image/orderplacedselected.png */ "./pages/app/assets/image/orderplacedselected.png"),
  onthewayselected: __webpack_require__(/*! ./image/onthewayselected.png */ "./pages/app/assets/image/onthewayselected.png"),
  onthewaydeselected: __webpack_require__(/*! ./image/onthewaydeselected.png */ "./pages/app/assets/image/onthewaydeselected.png"),
  preparingdeselected: __webpack_require__(/*! ./image/preparingdeselected.png */ "./pages/app/assets/image/preparingdeselected.png"),
  preparingselected: __webpack_require__(/*! ./image/preparingselected.png */ "./pages/app/assets/image/preparingselected.png"),
  bookingavailable: __webpack_require__(/*! ./image/bookingavailable.png */ "./pages/app/assets/image/bookingavailable.png"),
  bookingnotavailable: __webpack_require__(/*! ./image/bookingnotavailable.png */ "./pages/app/assets/image/bookingnotavailable.png"),
  delete_cart: __webpack_require__(/*! ./image/delete.png */ "./pages/app/assets/image/delete.png"),
  ic_close: __webpack_require__(/*! ./image/ic_close.png */ "./pages/app/assets/image/ic_close.png"),
  about_us: __webpack_require__(/*! ./image/about_us.png */ "./pages/app/assets/image/about_us.png"),
  privacy_policy: __webpack_require__(/*! ./image/privacy_policy.png */ "./pages/app/assets/image/privacy_policy.png"),
  confirm_background: __webpack_require__(/*! ./image/confirm_background.png */ "./pages/app/assets/image/confirm_background.png"),
  confirm_thumb: __webpack_require__(/*! ./image/confirm_thumb.png */ "./pages/app/assets/image/confirm_thumb.png"),
  header_placeholder: __webpack_require__(/*! ./image/banner_placeholder.png */ "./pages/app/assets/image/banner_placeholder.png"),
  delete_White: __webpack_require__(/*! ./image/delete_White.png */ "./pages/app/assets/image/delete_White.png"),
  //profile screen
  call: __webpack_require__(/*! ./image/call.png */ "./pages/app/assets/image/call.png"),
  our_address: __webpack_require__(/*! ./image/profile_address.png */ "./pages/app/assets/image/profile_address.png"),
  name: __webpack_require__(/*! ./image/name.png */ "./pages/app/assets/image/name.png"),
  password: __webpack_require__(/*! ./image/password.png */ "./pages/app/assets/image/password.png"),
  notification: __webpack_require__(/*! ./image/notification_selected.png */ "./pages/app/assets/image/notification_selected.png"),
  edit: __webpack_require__(/*! ./image/edit_profile.png */ "./pages/app/assets/image/edit_profile.png"),
  rating_white: __webpack_require__(/*! ./image/star_white.png */ "./pages/app/assets/image/star_white.png"),
  clock_white: __webpack_require__(/*! ./image/clock_white.png */ "./pages/app/assets/image/clock_white.png"),
  star_white: __webpack_require__(/*! ./image/star_white.png */ "./pages/app/assets/image/star_white.png"),
  people_white: __webpack_require__(/*! ./image/people.png */ "./pages/app/assets/image/people.png"),
  calender_white: __webpack_require__(/*! ./image/calendarwhite.png */ "./pages/app/assets/image/calendarwhite.png"),
  calender: __webpack_require__(/*! ./image/calendar.png */ "./pages/app/assets/image/calendar.png"),
  user_placeholder: __webpack_require__(/*! ./image/user_placeholder.png */ "./pages/app/assets/image/user_placeholder.png"),
  plus_round: __webpack_require__(/*! ./image/plus_round.png */ "./pages/app/assets/image/plus_round.png"),
  minus_round: __webpack_require__(/*! ./image/minus_round.png */ "./pages/app/assets/image/minus_round.png"),
  delete_gray: __webpack_require__(/*! ./image/delete_gray.png */ "./pages/app/assets/image/delete_gray.png"),
  tick: __webpack_require__(/*! ./image/tick.png */ "./pages/app/assets/image/tick.png"),
  camera_white: __webpack_require__(/*! ./image/camera_white.png */ "./pages/app/assets/image/camera_white.png"),
  discount_stick: __webpack_require__(/*! ./image/discount_stick.png */ "./pages/app/assets/image/discount_stick.png"),
  // Custom
  lang: __webpack_require__(/*! ./image/lang.png */ "./pages/app/assets/image/lang.png"),
  lang_2: __webpack_require__(/*! ./image/lang.png */ "./pages/app/assets/image/lang.png"),
  noproduct: __webpack_require__(/*! ./image/noproducts.jpg */ "./pages/app/assets/image/noproducts.jpg"),
  supper_deal_icon: __webpack_require__(/*! ./image/super_deals-icon.png */ "./pages/app/assets/image/super_deals-icon.png"),
  arrow_right: __webpack_require__(/*! ./image/right-arrow.png */ "./pages/app/assets/image/right-arrow.png"),
  headline: __webpack_require__(/*! ./image/headline.png */ "./pages/app/assets/image/headline.png"),
  home_icon: __webpack_require__(/*! ./image/home-icon.png */ "./pages/app/assets/image/home-icon.png"),
  hot_icon: __webpack_require__(/*! ./image/hot-icon.png */ "./pages/app/assets/image/hot-icon.png"),
  brand_icon: __webpack_require__(/*! ./image/brand-icon.png */ "./pages/app/assets/image/brand-icon.png"),
  category_icon: __webpack_require__(/*! ./image/category-icon.png */ "./pages/app/assets/image/category-icon.png"),
  //home functions
  fn_category: __webpack_require__(/*! ./image/home_functions/category.png */ "./pages/app/assets/image/home_functions/category.png"),
  fn_community: __webpack_require__(/*! ./image/home_functions/community.png */ "./pages/app/assets/image/home_functions/community.png"),
  fn_coupon_deals: __webpack_require__(/*! ./image/home_functions/coupon_deals.png */ "./pages/app/assets/image/home_functions/coupon_deals.png"),
  fn_flash_sale: __webpack_require__(/*! ./image/home_functions/flash_sale.png */ "./pages/app/assets/image/home_functions/flash_sale.png"),
  fn_hot: __webpack_require__(/*! ./image/home_functions/hot.png */ "./pages/app/assets/image/home_functions/hot.png"),
  fn_new: __webpack_require__(/*! ./image/home_functions/new.png */ "./pages/app/assets/image/home_functions/new.png"),
  fn_uner10: __webpack_require__(/*! ./image/home_functions/under10.png */ "./pages/app/assets/image/home_functions/under10.png"),
  fn_share_gifts: __webpack_require__(/*! ./image/home_functions/share_gifts.png */ "./pages/app/assets/image/home_functions/share_gifts.png"),
  //Home Top Icons
  icon_food: __webpack_require__(/*! ./image/business_icons/food.png */ "./pages/app/assets/image/business_icons/food.png"),
  icon_alcohol: __webpack_require__(/*! ./image/business_icons/alcohol.png */ "./pages/app/assets/image/business_icons/alcohol.png"),
  icon_grocery: __webpack_require__(/*! ./image/business_icons/grocery.png */ "./pages/app/assets/image/business_icons/grocery.png"),
  icon_express: __webpack_require__(/*! ./image/business_icons/express.png */ "./pages/app/assets/image/business_icons/express.png"),
  //Marker
  marker_customer: __webpack_require__(/*! ./image/map_markers/customer.png */ "./pages/app/assets/image/map_markers/customer.png"),
  map_cropped: __webpack_require__(/*! ./image/map_markers/map-cropped.jpg */ "./pages/app/assets/image/map_markers/map-cropped.jpg"),
  pizza: __webpack_require__(/*! ./image/pizza.jpg */ "./pages/app/assets/image/pizza.jpg"),
  countdown: __webpack_require__(/*! ./image/coundown.jpg */ "./pages/app/assets/image/coundown.jpg"),
  moto: __webpack_require__(/*! ./image/moto.png */ "./pages/app/assets/image/moto.png"),
  cuisines_example1: __webpack_require__(/*! ./image/cuisines_examples/Coffee.jpg */ "./pages/app/assets/image/cuisines_examples/Coffee.jpg"),
  cuisines_example2: __webpack_require__(/*! ./image/cuisines_examples/Japanese.jpg */ "./pages/app/assets/image/cuisines_examples/Japanese.jpg"),
  cuisines_example3: __webpack_require__(/*! ./image/cuisines_examples/Milk Tea.jpg */ "./pages/app/assets/image/cuisines_examples/Milk Tea.jpg"),
  cuisines_example4: __webpack_require__(/*! ./image/cuisines_examples/Western.jpg */ "./pages/app/assets/image/cuisines_examples/Western.jpg"),
  icon_food_offers: __webpack_require__(/*! ./image/food_icons/offers.png */ "./pages/app/assets/image/food_icons/offers.png"),
  icon_food_view_all: __webpack_require__(/*! ./image/food_icons/view_all.png */ "./pages/app/assets/image/food_icons/view_all.png"),
  icon_food_all_cuisines: __webpack_require__(/*! ./image/food_icons/all_cuisines.png */ "./pages/app/assets/image/food_icons/all_cuisines.png"),
  icon_food_bbq: __webpack_require__(/*! ./image/food_icons/bbq.png */ "./pages/app/assets/image/food_icons/bbq.png"),
  icon_food_chinese: __webpack_require__(/*! ./image/food_icons/chinese.png */ "./pages/app/assets/image/food_icons/chinese.png"),
  icon_food_coffee: __webpack_require__(/*! ./image/food_icons/coffee.png */ "./pages/app/assets/image/food_icons/coffee.png"),
  icon_food_italian: __webpack_require__(/*! ./image/food_icons/italian.png */ "./pages/app/assets/image/food_icons/italian.png"),
  icon_food_japanese: __webpack_require__(/*! ./image/food_icons/japanese.png */ "./pages/app/assets/image/food_icons/japanese.png"),
  icon_food_pizza: __webpack_require__(/*! ./image/food_icons/bbq.png */ "./pages/app/assets/image/food_icons/bbq.png"),
  icon_food_soup: __webpack_require__(/*! ./image/food_icons/soup.png */ "./pages/app/assets/image/food_icons/soup.png"),
  icon_food_asian: __webpack_require__(/*! ./image/food_icons/asian.png */ "./pages/app/assets/image/food_icons/asian.png"),
  // slideshow example
  slide_1: __webpack_require__(/*! ./image/slideshow_example/1.png */ "./pages/app/assets/image/slideshow_example/1.png"),
  slide_2: __webpack_require__(/*! ./image/slideshow_example/2.png */ "./pages/app/assets/image/slideshow_example/2.png"),
  slide_3: __webpack_require__(/*! ./image/slideshow_example/3.png */ "./pages/app/assets/image/slideshow_example/3.png"),
  slide_4: __webpack_require__(/*! ./image/slideshow_example/4.png */ "./pages/app/assets/image/slideshow_example/4.png"),
  flag_kh: __webpack_require__(/*! ./image/flags/kh.png */ "./pages/app/assets/image/flags/kh.png"),
  leftTopRibbon: __webpack_require__(/*! ./image/leftTopRibbon.png */ "./pages/app/assets/image/leftTopRibbon.png"),
  rightTopRibbon: __webpack_require__(/*! ./image/rightTopRibbon.png */ "./pages/app/assets/image/rightTopRibbon.png"),
  vacancyAds: __webpack_require__(/*! ./image/vacancy-ads.png */ "./pages/app/assets/image/vacancy-ads.png"),
  merchantAds: __webpack_require__(/*! ./image/merchan-ads.png */ "./pages/app/assets/image/merchan-ads.png"),
  cash_icon: __webpack_require__(/*! ./image/payment_gateways/card.png */ "./pages/app/assets/image/payment_gateways/card.png"),
  wing_icon: __webpack_require__(/*! ./image/payment_gateways/wing.png */ "./pages/app/assets/image/payment_gateways/wing.png"),
  abapay_icon: __webpack_require__(/*! ./image/payment_gateways/abapay.png */ "./pages/app/assets/image/payment_gateways/abapay.png"),
  visacard_icon: __webpack_require__(/*! ./image/payment_gateways/visa_card.png */ "./pages/app/assets/image/payment_gateways/visa_card.png"),
  empty_cart: __webpack_require__(/*! ./image/empty-cart.png */ "./pages/app/assets/image/empty-cart.png"),
  itemNotFound: __webpack_require__(/*! ./image/itemNotFound.png */ "./pages/app/assets/image/itemNotFound.png")
};
/* harmony default export */ __webpack_exports__["default"] = (Assets);

/***/ }),

/***/ "./pages/app/components/ETextViewNormalLabel.js":
/*!******************************************************!*\
  !*** ./pages/app/components/ETextViewNormalLabel.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ETextViewNormalLabel; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/StyleSheet */ "react-native-web/dist/cjs/exports/StyleSheet");
/* harmony import */ var react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/Text */ "react-native-web/dist/cjs/exports/Text");
/* harmony import */ var react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _assets_FontConstants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../assets/FontConstants */ "./pages/app/assets/FontConstants.js");
var _jsxFileName = "G:\\JustOrderWeb\\pages\\app\\components\\ETextViewNormalLabel.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




class ETextViewNormalLabel extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  render() {
    return __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_2___default.a, {
      style: [stylesLable.textLable, this.props.style || {}],
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 7,
        columnNumber: 16
      }
    }, this.props.text);
  }

}
const stylesLable = react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_1___default.a.create({
  textLable: {
    marginStart: 10,
    marginEnd: 10,
    color: "#000",
    fontSize: 16,
    marginTop: 8,
    marginLeft: 20,
    fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_3__["APPFONTS"].regular
  }
});

/***/ }),

/***/ "./pages/app/components/PopularRestaurantCard.js":
/*!*******************************************************!*\
  !*** ./pages/app/components/PopularRestaurantCard.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PopularRestaurantCard; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_native_web_dist_cjs_exports_Dimensions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/Dimensions */ "react-native-web/dist/cjs/exports/Dimensions");
/* harmony import */ var react_native_web_dist_cjs_exports_Dimensions__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_Dimensions__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/StyleSheet */ "react-native-web/dist/cjs/exports/StyleSheet");
/* harmony import */ var react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/Text */ "react-native-web/dist/cjs/exports/Text");
/* harmony import */ var react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/TouchableOpacity */ "react-native-web/dist/cjs/exports/TouchableOpacity");
/* harmony import */ var react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/ImageBackground */ "react-native-web/dist/cjs/exports/ImageBackground");
/* harmony import */ var react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/View */ "react-native-web/dist/cjs/exports/View");
/* harmony import */ var react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_native_web_dist_cjs_exports_Image__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/Image */ "react-native-web/dist/cjs/exports/Image");
/* harmony import */ var react_native_web_dist_cjs_exports_Image__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_Image__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _assets__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../assets */ "./pages/app/assets/index.js");
/* harmony import */ var _utils_Constants__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../utils/Constants */ "./pages/app/utils/Constants.js");
/* harmony import */ var _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../assets/FontConstants */ "./pages/app/assets/FontConstants.js");
/* harmony import */ var _assets_Colors__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../assets/Colors */ "./pages/app/assets/Colors.js");
var _jsxFileName = "G:\\JustOrderWeb\\pages\\app\\components\\PopularRestaurantCard.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }









 //import Icon from 'react-native-vector-icons/FontAwesome';
//import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
//import FIcon from 'react-native-vector-icons/Fontisto';
//import AntIcon  from 'react-native-vector-icons/AntDesign';



 //import Image from "react-native-fast-image"

const {
  width,
  height
} = react_native_web_dist_cjs_exports_Dimensions__WEBPACK_IMPORTED_MODULE_1___default.a.get('window');
const itemWidth = width - 60;
class PopularRestaurantCard extends react__WEBPACK_IMPORTED_MODULE_0___default.a.PureComponent {
  constructor(props) {
    super(props);

    _defineProperty(this, "state", {
      restObjModel: this.props.restObjModel,
      currentAvailableDay: this.props.restObjModel.current_delivery_available
    });

    _defineProperty(this, "itemPress", () => {
      if (this.props.itemPress) {
        this.props.itemPress("Restaurant", {
          //refresh: this.refreshScreen,
          restId: this.state.restObjModel.id,
          distant: this.state.restObjModel.distant
        });
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      restObjModel: nextProps.restObjModel
    });
  }

  notAvailableMessageBox(deliveryObject) {
    return __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: 120,
        // backgroundColor: "rgba(1,1,1,0.6)",
        alignItems: "center",
        alignContent: "center",
        justifyContent: "center",
        zIndex: 9
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46,
        columnNumber: 13
      }
    }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: styles.unavailableMessage,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 59,
        columnNumber: 17
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: styles.unavailableMessageTextBox,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 21
      }
    }, "Delivery Hours"), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: styles.unavailableMessageTextBox,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 61,
        columnNumber: 21
      }
    }, deliveryObject ? "" + deliveryObject.start_time + " - " + deliveryObject.end_time : "00:00am - 00:00pm")));
  }

  showPriceLevel(price_level) {
    let views = [];
    price_level = price_level == 0 || !price_level ? 1 : price_level;

    for (let i = 0; i < 3; i++) {
      views.push( // <Icon name={"dollar"} color={price_level > i ? "#ff963d" : "#b2b2b2"} size={14}/>
      __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
        key: i,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 75,
          columnNumber: 17
        }
      }, "$"));
    }

    return __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        flexDirection: "row",
        marginTop: 2
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 80,
        columnNumber: 13
      }
    }, views.map(view => {
      return view;
    }), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 84,
        columnNumber: 17
      }
    }, " "));
  }

  showDistant(distant) {
    return __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        flexDirection: "row",
        flex: 1
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 90,
        columnNumber: 13
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 94,
        columnNumber: 17
      }
    }, "?"), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      numberOfLines: 1,
      style: styles.distant,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 95,
        columnNumber: 17
      }
    }, parseFloat(distant).toFixed(2).toString() + "km"));
  }

  showDeliveryCharge(delivery_charges) {
    return __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        flexDirection: "row",
        flex: 1,
        alignItems: "flex-end",
        alignContent: "flex-end",
        justifyContent: "flex-end"
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 107,
        columnNumber: 13
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 117,
        columnNumber: 17
      }
    }, "?"), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      numberOfLines: 1,
      style: {
        fontSize: 12,
        fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].light
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 118,
        columnNumber: 17
      }
    }, "Delivery fee : " + _utils_Constants__WEBPACK_IMPORTED_MODULE_9__["INR_SHORT_SIGN"] + delivery_charges));
  }

  showMinimumCharge(minumCharge) {
    return __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        flexDirection: "row",
        flex: 1,
        alignItems: "flex-end",
        alignContent: "flex-end",
        justifyContent: "flex-end"
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 133,
        columnNumber: 13
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 143,
        columnNumber: 17
      }
    }, "?"), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      numberOfLines: 1,
      style: styles.minimumCharge,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 144,
        columnNumber: 17
      }
    }, "Minimum Order : " + _utils_Constants__WEBPACK_IMPORTED_MODULE_9__["INR_SHORT_SIGN"] + minumCharge));
  }

  showDeliveryTime(min, max) {
    return __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        flexDirection: "row",
        flex: 1
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 155,
        columnNumber: 13
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 159,
        columnNumber: 17
      }
    }, "?"), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      numberOfLines: 1,
      style: styles.deliveryTime,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 160,
        columnNumber: 17
      }
    }, min + " - " + max + " min"));
  }

  showAvailable() {
    if (this.state.restObjModel.temporary_close) {
      return __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
        style: styles.temporarilyCloseOverlay,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 174,
          columnNumber: 17
        }
      }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
        style: styles.temporarilyCloseMessage,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 175,
          columnNumber: 21
        }
      }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
        style: styles.unavailableMessageTextBox,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 176,
          columnNumber: 25
        }
      }, "Temporarily"), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
        style: styles.unavailableMessageTextBox,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 177,
          columnNumber: 25
        }
      }, "Closed")));
    }

    if (!this.state.currentAvailableDay) {
      return this.notAvailableMessageBox(null);
    }

    if (!this.state.currentAvailableDay.delivery_available) {
      return this.notAvailableMessageBox(this.state.currentAvailableDay);
    }
  }

  dot(nospace) {
    return __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: {
        paddingHorizontal: nospace ? 2 : 4,
        marginTop: -3,
        fontWeight: "bold",
        color: _assets_Colors__WEBPACK_IMPORTED_MODULE_11__["APPCOLORS"].group2Right2
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 194,
        columnNumber: 11
      }
    }, ".");
  }

  render() {
    return __jsx(react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_4___default.a, {
      onPress: this.itemPress,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 200,
        columnNumber: 13
      }
    }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: [styles.container, this.props.horizontal ? {
        marginTop: 5,
        marginRight: 5,
        marginLeft: 5,
        width: itemWidth
      } : {
        marginRight: 0
      }, this.props.style || {}],
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 203,
        columnNumber: 17
      }
    }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: styles.restaurantBannerBox,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 206,
        columnNumber: 21
      }
    }, __jsx(react_native_web_dist_cjs_exports_Image__WEBPACK_IMPORTED_MODULE_7___default.a, {
      source: this.state.restObjModel.image ? {
        uri: _utils_Constants__WEBPACK_IMPORTED_MODULE_9__["BASE_URL"] + this.state.restObjModel.image
      } : null,
      style: [styles.restaurantBanner
      /*{opacity: 0.6}*/
      ],
      resizeMode: "cover",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 207,
        columnNumber: 25
      }
    })), __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: styles.restaurantLogoBox,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 215,
        columnNumber: 21
      }
    }, __jsx(react_native_web_dist_cjs_exports_Image__WEBPACK_IMPORTED_MODULE_7___default.a, {
      source: this.state.restObjModel.logo ? {
        uri: _utils_Constants__WEBPACK_IMPORTED_MODULE_9__["BASE_URL"] + this.state.restObjModel.logo
      } : null,
      style: [styles.restaurantLogo, this.state.currentAvailableDay ? this.state.currentAvailableDay.delivery_available ? {
        opacity: 1
      } : {
        opacity: 1
      } : {
        opacity: 1
      }],
      resizeMode: "contain",
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 219,
        columnNumber: 25
      }
    })), __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        position: "absolute",
        top: -3,
        left: -9
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 249,
        columnNumber: 21
      }
    }, __jsx(react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_5___default.a, {
      source: _assets__WEBPACK_IMPORTED_MODULE_8__["default"].leftTopRibbon,
      resizeMode: "contain",
      style: {
        justifyContent: "center",
        width: 85,
        height: 45 // top:-3,
        // right:-2

      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 254,
        columnNumber: 25
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: [styles.rightBannerOverlay, styles.freeshipOverlay, this.props.freeshipBannerStyle || {}],
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 264,
        columnNumber: 33
      }
    }, "FREESHIP"))), __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        position: "absolute",
        top: 0,
        right: 0
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 272,
        columnNumber: 21
      }
    }, __jsx(react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_5___default.a, {
      source: _assets__WEBPACK_IMPORTED_MODULE_8__["default"].rightTopRibbon,
      resizeMode: "contain",
      style: {
        justifyContent: "center",
        width: 45,
        height: 45,
        top: -3,
        right: -2
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 277,
        columnNumber: 25
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      numberOfLines: 1,
      style: {
        position: "relative",
        top: -8,
        right: -8,
        fontSize: 11,
        color: "#ffF",
        textAlign: "center",
        transform: [{
          rotate: '45deg'
        }],
        fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].regular
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 287,
        columnNumber: 29
      }
    }, "Promo"))), __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: styles.restaurantInfoRowBox,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 303,
        columnNumber: 21
      }
    }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        flexDirection: "row",
        marginTop: 0,
        marginBottom: 0
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 305,
        columnNumber: 25
      }
    }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        flex: 6,
        flexDirection: "row",
        width: "100%"
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 309,
        columnNumber: 29
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: styles.restaurantName,
      numberOfLines: 1,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 316,
        columnNumber: 33
      }
    }, this.state.restObjModel.name)), __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: styles.ratingRight,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 325,
        columnNumber: 29
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 329,
        columnNumber: 33
      }
    }, "?"), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      numberOfLines: 1,
      style: styles.ratingNumber,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 330,
        columnNumber: 33
      }
    }, parseFloat(this.state.restObjModel.rating).toFixed(1).toString()))), __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        flexDirection: "row",
        overflow: "hidden",
        width: "100%"
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 341,
        columnNumber: 25
      }
    }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        flexDirection: "row",
        paddingVertical: 0,
        position: "relative",
        top: 1,
        justifyContent: "center"
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 343,
        columnNumber: 29
      }
    }, this.showPriceLevel(this.state.restObjModel.price_level)), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      numberOfLines: 1,
      style: styles.itemDescription,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 346,
        columnNumber: 29
      }
    }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit")), __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        flexDirection: "row",
        // borderWidth:1,borderColor:"red",
        marginBottom: 5
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 354,
        columnNumber: 25
      }
    }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        flex: 1,
        flexDirection: "row",
        alignContent: "flex-start",
        alignItems: "flex-start",
        justifyContent: "flex-start",
        flexWrap: "nowrap" // borderWidth:1,borderColor:"blue"

      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 358,
        columnNumber: 29
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 368,
        columnNumber: 33
      }
    }, "?"), this.props.horizontal ? __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        flexDirection: "row",
        alignContent: "flex-start",
        alignItems: "flex-start",
        justifyContent: "flex-start",
        flexWrap: "nowrap",
        paddingLeft: 20 // borderWidth:1,borderColor:"blue"

      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 370,
        columnNumber: 37
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      numberOfLines: 1,
      style: [styles.deliveryChargeRowText],
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 379,
        columnNumber: 41
      }
    }, "Delivery "), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      numberOfLines: 1,
      style: [styles.deliveryChargeRowText],
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 380,
        columnNumber: 41
      }
    }, _utils_Constants__WEBPACK_IMPORTED_MODULE_9__["INR_SHORT_SIGN"] + parseFloat(this.state.restObjModel.delivery_charges || 0).toFixed(1))) : __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      numberOfLines: 1,
      style: [styles.deliveryChargeRowText, {
        paddingLeft: 20
      }],
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 384,
        columnNumber: 37
      }
    }, "Delivery " + _utils_Constants__WEBPACK_IMPORTED_MODULE_9__["INR_SHORT_SIGN"] + parseFloat(this.state.restObjModel.delivery_charges || 0).toFixed(1))), __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        flex: 1,
        flexDirection: "row",
        alignContent: "center",
        alignItems: "center",
        justifyContent: "center",
        flexWrap: "nowrap" // borderWidth:1,borderColor:"yellow"

      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 390,
        columnNumber: 29
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: styles.deliveryTimeText,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 399,
        columnNumber: 33
      }
    }, "30—45 min")), __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        flex: 1,
        flexDirection: "row",
        alignContent: "flex-end",
        alignItems: "flex-end",
        justifyContent: "flex-end",
        flexWrap: "nowrap" // borderWidth:1,borderColor:"yellow"

      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 403,
        columnNumber: 29
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: styles.minOrderText,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 412,
        columnNumber: 33
      }
    }, "Min.Order " + _utils_Constants__WEBPACK_IMPORTED_MODULE_9__["INR_SHORT_SIGN"] + global.minimumOrder))), __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_6___default.a, {
      style: {
        flexDirection: "row",
        borderWidth: 1,
        borderRadius: 5,
        borderStyle: 'dashed',
        borderColor: "#ffc3c6",
        backgroundColor: "#fff9f3",
        paddingVertical: 5,
        paddingHorizontal: 10
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 417,
        columnNumber: 25
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: [{
        fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].bold,
        color: _assets_Colors__WEBPACK_IMPORTED_MODULE_11__["APPCOLORS"].group1Main
      }],
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 427,
        columnNumber: 29
      }
    }, "Special day! "), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: [{
        fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].light
      }],
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 428,
        columnNumber: 29
      }
    }, " free Delivery and Discount 30%")))));
  }

}
const styles = react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_2___default.a.create({
  container: {
    marginBottom: 13,
    flex: 1,
    backgroundColor: "white",
    borderRadius: 5,
    paddingLeft: 0,
    paddingRight: 0,
    paddingBottom: 5,
    // borderWidth: 8,
    // borderColor:"transparent",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowOpacity: 1,
    shadowRadius: 9,
    elevation: 2
  },
  restaurantBannerBox: {
    width: "100%",
    height: 120,
    backgroundColor: "#000",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5
  },
  restaurantBanner: {
    width: "100%",
    height: 120,
    // marginTop: 5,
    alignSelf: "center",
    // borderRadius: 5,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5
  },
  restaurantLogoBox: {
    width: 60,
    height: 60,
    alignSelf: "center",
    position: "absolute",
    left: 5,
    top: 55,
    overflow: "hidden",
    borderRadius: 40,
    borderColor: "#FFF",
    borderWidth: 1,
    backgroundColor: "#000",
    // "rgba(0,0,0,0.3)",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 7
    },
    shadowOpacity: 0.9,
    shadowRadius: 9,
    elevation: 15
  },
  restaurantLogo: {
    width: "100%",
    height: "100%",
    alignSelf: "center"
  },
  rightBannerOverlay: {
    // width:78,
    // paddingHorizontal: 5,
    // paddingVertical:2,
    color: "white",
    // marginBottom: 5,
    fontSize: 12,
    paddingLeft: 15,
    // borderBottomRightRadius: 10,
    // borderTopRightRadius:10,
    fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].regular,
    // position:"relative",
    bottom: 13,
    justifyContent: "flex-start"
  },
  discountOverlay: {
    backgroundColor: _assets_Colors__WEBPACK_IMPORTED_MODULE_11__["APPCOLORS"].group1Main
  },
  freeshipOverlay: {// backgroundColor: "#ff37fc",
  },
  restaurantName: {
    fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].bold,
    fontSize: 16
  },
  ratingRight: {
    flexDirection: "row",
    // width: "auto",
    // position: "absolute",
    // right: 0,
    // top: 4
    flex: 1,
    paddingVertical: 4,
    justifyContent: "flex-end",
    alignItems: "flex-end",
    alignContent: "flex-end"
  },
  restaurantInfoRowBox: {
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5
  },
  verifiedIcon: {
    width: 20,
    height: 20,
    position: "relative",
    marginTop: 0,
    marginLeft: -5
  },
  restaurantInfoRowOne: {
    flexDirection: "row",
    marginTop: 5,
    marginBottom: 0
  },
  restaurantInfoRowTwo: {
    flexDirection: "row",
    marginTop: 5,
    marginBottom: 5,
    alignContent: "center"
  },
  ratingNumber: {
    // marginRight: 10,
    fontSize: 12,
    marginLeft: 3,
    fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].light // alignSelf: "flex-end"

  },
  distant: {
    marginRight: 10,
    fontSize: 12,
    marginLeft: 3,
    fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].light
  },
  deliveryCharge: {
    marginRight: 10,
    fontSize: 12,
    marginLeft: 3,
    fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].light,
    textAlign: "right",
    borderWidth: 1
  },
  deliveryTime: {
    marginRight: 10,
    fontSize: 12,
    marginLeft: 3,
    fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].light
  },
  minimumCharge: {
    // marginRight: 10,
    fontSize: 12,
    marginLeft: 3,
    fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].light
  },
  promotion: {
    position: "absolute",
    // left: -10,
    left: 5,
    top: 5,
    zIndex: 1,
    // borderColor:"red",
    // borderWidth:1,
    // backgroundColor:"white",
    height: 35,
    width: "auto",
    alignItems: "center"
  },
  available: {
    width: 15,
    height: 15,
    backgroundColor: "#6aff2e",
    position: "absolute",
    borderRadius: 100,
    borderWidth: 2,
    borderColor: "#5ebf29",
    zIndex: 9,
    bottom: 5,
    right: 0
  },
  unavailable: {
    width: 15,
    height: 15,
    backgroundColor: "red",
    position: "absolute",
    borderRadius: 100,
    borderWidth: 2,
    borderColor: "red",
    zIndex: 9,
    bottom: 5,
    right: 0
  },
  unavailableMessage: {
    width: 140,
    alignContent: "center",
    alignItems: "center",
    borderTopWidth: 1,
    borderTopColor: "white",
    borderBottomWidth: 1,
    borderBottomColor: "white",
    paddingTop: 6,
    paddingBottom: 6
  },
  temporarilyCloseMessage: {
    width: 110,
    alignContent: "center",
    alignItems: "center",
    borderTopWidth: 1,
    borderTopColor: "white",
    borderBottomWidth: 1,
    borderBottomColor: "white",
    paddingTop: 6,
    paddingBottom: 6
  },
  unavailableMessageTextBox: {
    color: "white",
    fontSize: 16,
    fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].light
  },
  temporarilyCloseOverlay: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: 120,
    backgroundColor: "rgba(1,1,1,0.6)",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
    zIndex: 9
  },
  deliveryChargeRowText: {
    flexWrap: "nowrap",
    // position:"relative",
    // top:-1,
    fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].light
  },
  itemDescription: {
    fontSize: 14,
    fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].light
  },
  deliveryTimeText: {
    textAlign: "center",
    letterSpacing: 0,
    fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].light
  },
  minOrderText: {
    marginRight: 0,
    fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].light
  }
});

/***/ }),

/***/ "./pages/app/components/PopularRestaurants.js":
/*!****************************************************!*\
  !*** ./pages/app/components/PopularRestaurants.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PopularRestaurants; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/FlatList */ "react-native-web/dist/cjs/exports/FlatList");
/* harmony import */ var react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/StyleSheet */ "react-native-web/dist/cjs/exports/StyleSheet");
/* harmony import */ var react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/View */ "react-native-web/dist/cjs/exports/View");
/* harmony import */ var react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_native_web_dist_cjs_exports_ActivityIndicator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/ActivityIndicator */ "react-native-web/dist/cjs/exports/ActivityIndicator");
/* harmony import */ var react_native_web_dist_cjs_exports_ActivityIndicator__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_ActivityIndicator__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _PopularRestaurantCard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./PopularRestaurantCard */ "./pages/app/components/PopularRestaurantCard.js");
var _jsxFileName = "G:\\JustOrderWeb\\pages\\app\\components\\PopularRestaurants.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







class PopularRestaurants extends react__WEBPACK_IMPORTED_MODULE_0___default.a.PureComponent {
  constructor(props) {
    super(props); //this.renderFooter = this.renderFooter.bind(this);

    _defineProperty(this, "state", {
      arrayRestaurants: this.props.restaurants,
      showFilter: this.props.show,
      isLoading: this.props.isLoading
    });

    _defineProperty(this, "nextPage", () => {//if(this.props.nextPage){
      //    this.props.nextPage()
      //}
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      arrayRestaurants: nextProps.restaurants,
      isLoading: nextProps.isLoading
    });
  } // shouldComponentUpdate() {
  //     return false
  // }


  renderFooter() {
    return this.state.isLoading ? __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: style.footer,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34,
        columnNumber: 17
      }
    }, __jsx(react_native_web_dist_cjs_exports_ActivityIndicator__WEBPACK_IMPORTED_MODULE_4___default.a, {
      color: "black",
      size: 35,
      style: {
        margin: 15
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 17
      }
    })) : null;
  }

  render() {
    if (!this.state.arrayRestaurants) return null;
    return __jsx(react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_1___default.a, {
      onScroll: this.props.onScroll,
      disableVirtualization: true,
      showsHorizontalScrollIndicator: false,
      showsVerticalScrollIndicator: false,
      horizontal: this.props.horizontal // initialNumToRender={!this.props.horizontal ? 20 : 10}
      // maxToRenderPerBatch={!this.props.horizontal ? 20 : 10}
      // removeClippedSubviews={true}
      ,
      onEndReachedThreshold: 3 //onEndReached={this.nextPage}
      ,
      style: [this.props.horizontal ? {
        marginLeft: 5
      } : {
        marginHorizontal: 10
      }, this.props.style || {}],
      data: this.state.arrayRestaurants,
      extraData: this.state,
      keyExtractor: (item, index) => String(index),
      renderItem: ({
        item,
        index
      }) => {
        return __jsx(_PopularRestaurantCard__WEBPACK_IMPORTED_MODULE_5__["default"] //offerBannerStyle={this.props.offerBannerStyle}
        , {
          horizontal: this.props.horizontal,
          restObjModel: item //itemPress={this.props.itemPress}
          //style={this.props.itemStyle}
          ,
          index: index //freeshipBannerStyle={this.props.freeshipBannerStyle}
          ,
          __self: this,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 67,
            columnNumber: 25
          }
        });
      },
      ListFooterComponent: () => this.renderFooter() // getItemLayout={(data, index) => (
      //     {length: 350, offset: 350 * index, index}
      // )}
      ,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46,
        columnNumber: 13
      }
    });
  }

}
const style = react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_2___default.a.create({
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  }
});

/***/ }),

/***/ "./pages/app/containers/BaseContainer.js":
/*!***********************************************!*\
  !*** ./pages/app/containers/BaseContainer.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return BaseContainer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/View */ "react-native-web/dist/cjs/exports/View");
/* harmony import */ var react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _assets_Colors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../assets/Colors */ "./pages/app/assets/Colors.js");
var _jsxFileName = "G:\\JustOrderWeb\\pages\\app\\containers\\BaseContainer.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
 //import {Container} from "native-base";

 //import NavBar from "./NavBar";
//import BottomMenu from "../components/BottomMenu";
//import ProgressLoader from "../components/ProgressLoader";
//import MainLoader from "../components/MainLoader";

 //import {netStatusEvent} from "../utils/NetworkStatusConnection";

class BaseContainer extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  // constructor(props) {
  //     super(props);
  // }
  //
  // componentDidMount() {
  //     netStatusEvent(status => {
  //
  //     });
  // }
  render() {
    return __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_1___default.a, {
      style: {
        overflow: "scroll",
        height: "100%"
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24,
        columnNumber: 13
      }
    }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_1___default.a, {
      style: {
        backgroundColor: _assets_Colors__WEBPACK_IMPORTED_MODULE_2__["EDColors"].backgroundDark
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 47,
        columnNumber: 17
      }
    }, this.props.children));
  }

}

/***/ }),

/***/ "./pages/app/containers/MainContainer.js":
/*!***********************************************!*\
  !*** ./pages/app/containers/MainContainer.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MainContainer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/FlatList */ "react-native-web/dist/cjs/exports/FlatList");
/* harmony import */ var react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/StyleSheet */ "react-native-web/dist/cjs/exports/StyleSheet");
/* harmony import */ var react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/Text */ "react-native-web/dist/cjs/exports/Text");
/* harmony import */ var react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/TouchableOpacity */ "react-native-web/dist/cjs/exports/TouchableOpacity");
/* harmony import */ var react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/View */ "react-native-web/dist/cjs/exports/View");
/* harmony import */ var react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/ImageBackground */ "react-native-web/dist/cjs/exports/ImageBackground");
/* harmony import */ var react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_native_web_dist_cjs_exports_Image__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/Image */ "react-native-web/dist/cjs/exports/Image");
/* harmony import */ var react_native_web_dist_cjs_exports_Image__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_Image__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _BaseContainer__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./BaseContainer */ "./pages/app/containers/BaseContainer.js");
/* harmony import */ var _assets__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../assets */ "./pages/app/assets/index.js");
/* harmony import */ var _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../assets/FontConstants */ "./pages/app/assets/FontConstants.js");
/* harmony import */ var _api_ServiceManager__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../api/ServiceManager */ "./pages/app/api/ServiceManager.js");
/* harmony import */ var _utils_Constants__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../utils/Constants */ "./pages/app/utils/Constants.js");
/* harmony import */ var _components_PopularRestaurants__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../components/PopularRestaurants */ "./pages/app/components/PopularRestaurants.js");
/* harmony import */ var _assets_Colors__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../assets/Colors */ "./pages/app/assets/Colors.js");
/* harmony import */ var _components_ETextViewNormalLabel__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../components/ETextViewNormalLabel */ "./pages/app/components/ETextViewNormalLabel.js");
var _jsxFileName = "G:\\JustOrderWeb\\pages\\app\\containers\\MainContainer.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }












 //import {netStatus} from "../utils/NetworkStatusConnection"





class MainContainer extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      arrayRestaurants: undefined
    });

    _defineProperty(this, "allRestaurantRows", () => {
      let popularRestaurants = this.state.arrayRestaurants != undefined && this.state.arrayRestaurants != null && this.state.arrayRestaurants.length > 0 ? this.showRestaurantRows() : this.state.arrayRestaurants != undefined && this.state.arrayRestaurants != null && this.state.arrayRestaurants.length == 0 ? null : this.state.isLoading ? null : __jsx(react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_4___default.a, {
        style: {
          alignSelf: 'center',
          backgroundColor: _assets_Colors__WEBPACK_IMPORTED_MODULE_14__["APPCOLORS"].group1Main,
          padding: 10,
          marginVertical: 100
        } // onPress={() => this.refreshScreen}
        ,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59,
          columnNumber: 21
        }
      }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
        style: {
          color: _assets_Colors__WEBPACK_IMPORTED_MODULE_14__["EDColors"].white
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68,
          columnNumber: 25
        }
      }, "Reload"));
      return __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_5___default.a, {
        style: {
          flex: 1,
          minHeight: 200
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76,
          columnNumber: 13
        }
      }, __jsx(_components_ETextViewNormalLabel__WEBPACK_IMPORTED_MODULE_15__["default"], {
        style: {
          marginLeft: 10,
          marginBottom: 10
        },
        text: "Featured",
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77,
          columnNumber: 17
        }
      }), popularRestaurants);
    });
  }

  showRestaurantRows() {
    return __jsx(_components_PopularRestaurants__WEBPACK_IMPORTED_MODULE_13__["default"] // onScrollEndDrag={(e)=>{
    //     //this.setState({scolledOffsetY:e.nativeEvent.contentOffset.y});
    //     // console.log(e.nativeEvent.contentOffset.y);
    //     //this.setState({showScrollUpButton: e.nativeEvent.contentOffset.y >= height})
    //
    // }}
    , {
      restaurants: this.state.arrayRestaurants //itemPress={this.gotoRestaurant}
      //refreshScreen={this.refreshScreen}
      //nextPage={this.nextRestaurantPage}
      //isLoading={this.state.loadingRestaurant}
      ,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 32,
        columnNumber: 13
      }
    });
  }

  getData() {
    const param = {};
    Object(_api_ServiceManager__WEBPACK_IMPORTED_MODULE_11__["apiPost"])(_utils_Constants__WEBPACK_IMPORTED_MODULE_12__["REGISTRATION_HOME"], param, resp => {
      if (resp != undefined) {
        if (resp.status == _utils_Constants__WEBPACK_IMPORTED_MODULE_12__["RESPONSE_SUCCESS"]) {
          let restaurants = resp.restaurants; //.splice(0,10);

          console.log("restaurants ::::: ", restaurants);
          this.setState({
            arrayRestaurants: undefined
          }, () => {
            this.setState({
              arrayRestaurants: restaurants
            });
          });
          this.last_page = resp.last_page;
          this.current_page = 1;
          console.log("current_page : ", this.current_page);
        }
      }

      this.setState({
        isLoading: false
      });

      if (this.state.showSplashLoader) {
        setTimeout(() => {
          this.setState({
            showSplashLoader: false
          });
        }, 3000);
      }
    }, err => {
      this.setState({
        isLoading: false
      });
    });
  }

  allMainRow() {
    let rows = []; // rows.push(this.slider());
    // rows.push(this.businessIcons());
    // rows.push(this.anouncementRow());

    rows.push(__jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_5___default.a, {
      style: {
        flexDirection: "row",
        height: 100,
        marginHorizontal: 7,
        marginVertical: 10
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 134,
        columnNumber: 13
      }
    }, __jsx(react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_4___default.a, {
      onPress: () => {// if(this.merchantModal){
        //     this.merchantModal.show();
        // }
      },
      style: {
        flex: 1,
        paddingHorizontal: 3
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 135,
        columnNumber: 17
      }
    }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_5___default.a, {
      style: {
        width: "100%",
        backgroundColor: "#7d3e1e",
        height: "100%",
        overflow: "hidden",
        borderRadius: 5
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 143,
        columnNumber: 21
      }
    }, __jsx(react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_6___default.a, {
      source: _assets__WEBPACK_IMPORTED_MODULE_9__["default"].merchantAds,
      resizeMode: "contain",
      style: {
        width: "100%",
        height: "100%"
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 144,
        columnNumber: 25
      }
    }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_5___default.a, {
      style: {
        paddingHorizontal: 5,
        paddingVertical: 0
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 145,
        columnNumber: 29
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: {
        fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].regular,
        fontSize: 14,
        letterSpacing: -0.2
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 146,
        columnNumber: 33
      }
    }, "Become a Merchant Partner"), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: {
        fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].regular,
        fontSize: 12
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 147,
        columnNumber: 33
      }
    }, "Help you to Increase Revenue"))), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: styles.joinNow,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 150,
        columnNumber: 25
      }
    }, "Join Now"))), __jsx(react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_4___default.a, {
      onPress: () => {// if(this.jobVacancyModal){
        //     this.jobVacancyModal.show();
        // }
      },
      style: {
        flex: 1,
        paddingHorizontal: 3
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 156,
        columnNumber: 17
      }
    }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_5___default.a, {
      style: {
        width: "100%",
        backgroundColor: "#7d3e1e",
        height: "100%",
        overflow: "hidden",
        borderRadius: 5
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 163,
        columnNumber: 21
      }
    }, __jsx(react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_6___default.a, {
      source: _assets__WEBPACK_IMPORTED_MODULE_9__["default"].vacancyAds,
      resizeMode: "contain",
      style: {
        width: "100%",
        height: "100%"
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 164,
        columnNumber: 25
      }
    }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_5___default.a, {
      style: {
        paddingHorizontal: 5,
        paddingVertical: 0
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 165,
        columnNumber: 29
      }
    }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: {
        fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].regular,
        fontSize: 14,
        letterSpacing: -1
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 166,
        columnNumber: 33
      }
    }, "Job Vacancy"), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: {
        fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_10__["APPFONTS"].regular,
        fontSize: 12
      },
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 167,
        columnNumber: 33
      }
    }, "New job is available now"))), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_3___default.a, {
      style: styles.joinNow,
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 170,
        columnNumber: 25
      }
    }, "Apply Now"))))); // Loading

    rows.push(this.allRestaurantRows());
    return rows;
  }

  async componentDidMount() {
    this.getData();
  }

  render() {
    return __jsx(_BaseContainer__WEBPACK_IMPORTED_MODULE_8__["default"], {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 189,
        columnNumber: 13
      }
    }, __jsx(react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_1___default.a // onScroll={this._onListScroll}
    , {
      ref: ref => this.mainFlatList = ref // refreshControl={
      //     <RefreshControl
      //         refreshing={this.state.refreshing}
      //         onRefresh={this._onRefresh}
      //     />
      // }
      ,
      data: this.allMainRow(),
      renderItem: ({
        item
      }) => item,
      keyExtractor: (item, index) => String(index),
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 190,
        columnNumber: 17
      }
    }));
  }

}
const styles = react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_2___default.a.create({
  scrollUpButton: {
    zIndex: 999,
    right: 10,
    bottom: 60,
    // shadowColor: "#000",
    // shadowOffset: {
    //     width: 20,
    //     height: 20,
    // },
    // shadowOpacity: 1,
    // shadowRadius: 9,
    // elevation: 9,
    backgroundColor: "rgba(0,0,0,0.6)"
  },
  discount_stick: {
    position: "absolute",
    right: 0,
    top: 0,
    zIndex: 1,
    // borderColor:"red",
    // borderWidth:1,
    // backgroundColor:"white",
    height: 35,
    width: 30,
    alignItems: "center",
    borderRadius: 2
  },
  itemImage: {
    width: "100%",
    height: "100%",
    alignSelf: "center",
    resizeMode: 'cover'
  },
  spinner: {
    flex: 1,
    alignSelf: "center",
    zIndex: 1000
  },
  anouncementRow: {
    marginTop: 10,
    marginStart: 10,
    marginEnd: 10,
    backgroundColor: "white",
    borderRadius: 5
  },
  joinNow: {
    position: "absolute",
    textAlign: "center",
    textAlignVertical: "center",
    // justifyContent:"center",
    // alignSelf:"center",
    flex: 1,
    bottom: 10,
    left: 5,
    backgroundColor: "yellow",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "yellow",
    width: 70,
    height: 25,
    fontSize: 12
  }
});

/***/ }),

/***/ "./pages/app/redux/actions/Checkout.js":
/*!*********************************************!*\
  !*** ./pages/app/redux/actions/Checkout.js ***!
  \*********************************************/
/*! exports provided: TYPE_SAVE_CHECKOUT_DETAILS, SAVE_CART_COUNT, SAVE_CART_TOTAL, saveCheckoutDetails, saveCartCount, saveCartTotal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPE_SAVE_CHECKOUT_DETAILS", function() { return TYPE_SAVE_CHECKOUT_DETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SAVE_CART_COUNT", function() { return SAVE_CART_COUNT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SAVE_CART_TOTAL", function() { return SAVE_CART_TOTAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveCheckoutDetails", function() { return saveCheckoutDetails; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveCartCount", function() { return saveCartCount; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveCartTotal", function() { return saveCartTotal; });
const TYPE_SAVE_CHECKOUT_DETAILS = "TYPE_SAVE_CHECKOUT_DETAILS";
const SAVE_CART_COUNT = "SAVE_CART_COUNT";
const SAVE_CART_TOTAL = "SAVE_CART_TOTAL";
function saveCheckoutDetails(data) {
  return {
    type: TYPE_SAVE_CHECKOUT_DETAILS,
    value: data
  };
}
function saveCartCount(data) {
  return {
    type: SAVE_CART_COUNT,
    value: data
  };
}
function saveCartTotal(data) {
  return {
    type: SAVE_CART_TOTAL,
    value: data
  };
}

/***/ }),

/***/ "./pages/app/redux/actions/Navigation.js":
/*!***********************************************!*\
  !*** ./pages/app/redux/actions/Navigation.js ***!
  \***********************************************/
/*! exports provided: TYPE_SAVE_NAVIGATION_SELECTION, saveNavigationSelection */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPE_SAVE_NAVIGATION_SELECTION", function() { return TYPE_SAVE_NAVIGATION_SELECTION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveNavigationSelection", function() { return saveNavigationSelection; });
const TYPE_SAVE_NAVIGATION_SELECTION = "TYPE_SAVE_NAVIGATION_SELECTION";
function saveNavigationSelection(data) {
  return {
    type: TYPE_SAVE_NAVIGATION_SELECTION,
    value: data
  };
}

/***/ }),

/***/ "./pages/app/redux/actions/User.js":
/*!*****************************************!*\
  !*** ./pages/app/redux/actions/User.js ***!
  \*****************************************/
/*! exports provided: TYPE_SAVE_LOGIN_DETAILS, saveUserDetailsInRedux, TYPE_SAVE_USER_ADDRESSES, saveUserAddressesInRedux, TYPE_SAVE_CART_ADDRESSES, saveCartAddressesInRedux, TYPE_SAVE_LOGIN_FCM, saveUserFCMInRedux, TYPE_SAVE_COUNTRY_CODE, saveCountryCodeInRedux, TYPE_SAVE_CURRENT_ADDRESS, saveCurrentAddressInRedux, TYPE_SAVE_CURRENT_RESTAURANT_ID, saveCurrentRestaurantIdInRedux, TYPE_SAVE_FAVORITE_ITEMS, saveFavoriteItemsInRedux, TYPE_SAVE_FAVORITE_SHOPS, saveFavoriteShopsInRedux */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPE_SAVE_LOGIN_DETAILS", function() { return TYPE_SAVE_LOGIN_DETAILS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveUserDetailsInRedux", function() { return saveUserDetailsInRedux; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPE_SAVE_USER_ADDRESSES", function() { return TYPE_SAVE_USER_ADDRESSES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveUserAddressesInRedux", function() { return saveUserAddressesInRedux; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPE_SAVE_CART_ADDRESSES", function() { return TYPE_SAVE_CART_ADDRESSES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveCartAddressesInRedux", function() { return saveCartAddressesInRedux; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPE_SAVE_LOGIN_FCM", function() { return TYPE_SAVE_LOGIN_FCM; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveUserFCMInRedux", function() { return saveUserFCMInRedux; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPE_SAVE_COUNTRY_CODE", function() { return TYPE_SAVE_COUNTRY_CODE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveCountryCodeInRedux", function() { return saveCountryCodeInRedux; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPE_SAVE_CURRENT_ADDRESS", function() { return TYPE_SAVE_CURRENT_ADDRESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveCurrentAddressInRedux", function() { return saveCurrentAddressInRedux; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPE_SAVE_CURRENT_RESTAURANT_ID", function() { return TYPE_SAVE_CURRENT_RESTAURANT_ID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveCurrentRestaurantIdInRedux", function() { return saveCurrentRestaurantIdInRedux; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPE_SAVE_FAVORITE_ITEMS", function() { return TYPE_SAVE_FAVORITE_ITEMS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveFavoriteItemsInRedux", function() { return saveFavoriteItemsInRedux; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TYPE_SAVE_FAVORITE_SHOPS", function() { return TYPE_SAVE_FAVORITE_SHOPS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "saveFavoriteShopsInRedux", function() { return saveFavoriteShopsInRedux; });
const TYPE_SAVE_LOGIN_DETAILS = "TYPE_SAVE_LOGIN_DETAILS";
function saveUserDetailsInRedux(details) {
  return {
    type: TYPE_SAVE_LOGIN_DETAILS,
    value: details
  };
}
const TYPE_SAVE_USER_ADDRESSES = "TYPE_SAVE_USER_ADDRESSES";
function saveUserAddressesInRedux(details) {
  return {
    type: TYPE_SAVE_USER_ADDRESSES,
    value: details
  };
}
const TYPE_SAVE_CART_ADDRESSES = "TYPE_SAVE_CART_ADDRESSES";
function saveCartAddressesInRedux(details) {
  return {
    type: TYPE_SAVE_CART_ADDRESSES,
    value: details
  };
}
const TYPE_SAVE_LOGIN_FCM = "TYPE_SAVE_LOGIN_FCM";
function saveUserFCMInRedux(details) {
  return {
    type: TYPE_SAVE_LOGIN_FCM,
    value: details
  };
}
const TYPE_SAVE_COUNTRY_CODE = "TYPE_SAVE_COUNTRY_CODE";
function saveCountryCodeInRedux(details) {
  return {
    type: TYPE_SAVE_COUNTRY_CODE,
    value: details
  };
}
const TYPE_SAVE_CURRENT_ADDRESS = "TYPE_SAVE_CURRENT_ADDRESS";
function saveCurrentAddressInRedux(details) {
  //console.log(details);
  return {
    type: TYPE_SAVE_CURRENT_ADDRESS,
    value: details
  };
}
const TYPE_SAVE_CURRENT_RESTAURANT_ID = "TYPE_SAVE_CURRENT_RESTAURANT_ID";
function saveCurrentRestaurantIdInRedux(details) {
  return {
    type: TYPE_SAVE_CURRENT_RESTAURANT_ID,
    value: details
  };
}
const TYPE_SAVE_FAVORITE_ITEMS = "TYPE_SAVE_FAVORITE_ITEMS";
function saveFavoriteItemsInRedux(details) {
  return {
    type: TYPE_SAVE_FAVORITE_ITEMS,
    value: details
  };
}
const TYPE_SAVE_FAVORITE_SHOPS = "TYPE_SAVE_FAVORITE_SHOPS";
function saveFavoriteShopsInRedux(details) {
  return {
    type: TYPE_SAVE_FAVORITE_SHOPS,
    value: details
  };
}

/***/ }),

/***/ "./pages/app/redux/reducers/CheckoutReducer.js":
/*!*****************************************************!*\
  !*** ./pages/app/redux/reducers/CheckoutReducer.js ***!
  \*****************************************************/
/*! exports provided: checkoutDetailOperation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "checkoutDetailOperation", function() { return checkoutDetailOperation; });
/* harmony import */ var _actions_Checkout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/Checkout */ "./pages/app/redux/actions/Checkout.js");

const initalState = {
  checkoutDetail: {},
  cartCount: 0,
  cartTotal: 0
};
function checkoutDetailOperation(state = initalState, action) {
  switch (action.type) {
    case _actions_Checkout__WEBPACK_IMPORTED_MODULE_0__["TYPE_SAVE_CHECKOUT_DETAILS"]:
      {
        return Object.assign({}, state, {
          checkoutDetail: action.value // address_id: action.value.address_id,
          // subtotal: action.value.subtotal,
          // items: action.value.items,
          // coupon_id: action.value.coupon_id,
          // coupon_type: action.value.coupon_type,
          // coupon_amount: action.value.coupon_amount,
          // user_id: action.value.user_id,
          // restaurant_id: action.value.resId

        });
      }

    case _actions_Checkout__WEBPACK_IMPORTED_MODULE_0__["SAVE_CART_COUNT"]:
      {
        return Object.assign({}, state, {
          cartCount: action.value
        });
      }

    case _actions_Checkout__WEBPACK_IMPORTED_MODULE_0__["SAVE_CART_TOTAL"]:
      {
        return Object.assign({}, state, {
          cartTotal: action.value
        });
      }

    default:
      return state;
  }
}

/***/ }),

/***/ "./pages/app/redux/reducers/NavigationReducer.js":
/*!*******************************************************!*\
  !*** ./pages/app/redux/reducers/NavigationReducer.js ***!
  \*******************************************************/
/*! exports provided: navigationOperation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "navigationOperation", function() { return navigationOperation; });
/* harmony import */ var _actions_Navigation__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/Navigation */ "./pages/app/redux/actions/Navigation.js");

const initalState = {
  selectedItem: "Home"
};
function navigationOperation(state = initalState, action) {
  switch (action.type) {
    case _actions_Navigation__WEBPACK_IMPORTED_MODULE_0__["TYPE_SAVE_NAVIGATION_SELECTION"]:
      {
        return Object.assign({}, state, {
          selectedItem: action.value
        });
      }

    default:
      return state;
  }
}

/***/ }),

/***/ "./pages/app/redux/reducers/UserFavorite.js":
/*!**************************************************!*\
  !*** ./pages/app/redux/reducers/UserFavorite.js ***!
  \**************************************************/
/*! exports provided: UserFavorite */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserFavorite", function() { return UserFavorite; });
/* harmony import */ var _actions_User__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/User */ "./pages/app/redux/actions/User.js");

const initialFavorite = {
  favoriteItems: {
    items: []
  },
  favoriteShops: {
    shops: []
  }
};
function UserFavorite(state = initialFavorite, action) {
  switch (action.type) {
    case _actions_User__WEBPACK_IMPORTED_MODULE_0__["TYPE_SAVE_FAVORITE_ITEMS"]:
      {
        return Object.assign({}, state, {
          favoriteItems: action.value
        });
      }

    case _actions_User__WEBPACK_IMPORTED_MODULE_0__["TYPE_SAVE_FAVORITE_SHOPS"]:
      {
        return Object.assign({}, state, {
          favoriteShops: action.value
        });
      }

    default:
      return state;
  }
}

/***/ }),

/***/ "./pages/app/redux/reducers/UserReducer.js":
/*!*************************************************!*\
  !*** ./pages/app/redux/reducers/UserReducer.js ***!
  \*************************************************/
/*! exports provided: userOperations */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userOperations", function() { return userOperations; });
/* harmony import */ var _actions_User__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../actions/User */ "./pages/app/redux/actions/User.js");

const initialStateUser = {
  // LOGIN DETAILS
  phoneNumberInRedux: undefined,
  userIdInRedux: undefined,
  currentRestaurantId: undefined,
  currentAddress: {
    landmark: undefined,
    address: undefined,
    latitude: undefined,
    longitude: undefined
  },
  savedCartAddress: undefined
};
function userOperations(state = initialStateUser, action) {
  switch (action.type) {
    case _actions_User__WEBPACK_IMPORTED_MODULE_0__["TYPE_SAVE_LOGIN_DETAILS"]:
      {
        return Object.assign({}, state, {
          phoneNumberInRedux: action.value.PhoneNumber,
          userIdInRedux: action.value.UserID // savedAddresses: action.value.savedAddresses

        });
      }

    case _actions_User__WEBPACK_IMPORTED_MODULE_0__["TYPE_SAVE_USER_ADDRESSES"]:
      {
        return Object.assign({}, state, {
          savedAddresses: action.value
        });
      }

    case _actions_User__WEBPACK_IMPORTED_MODULE_0__["TYPE_SAVE_CART_ADDRESSES"]:
      {
        return Object.assign({}, state, {
          savedCartAddress: action.value
        });
      }

    case _actions_User__WEBPACK_IMPORTED_MODULE_0__["TYPE_SAVE_LOGIN_FCM"]:
      {
        return Object.assign({}, state, {
          token: action.value
        });
      }

    case _actions_User__WEBPACK_IMPORTED_MODULE_0__["TYPE_SAVE_COUNTRY_CODE"]:
      {
        return Object.assign({}, state, {
          code: action.value
        });
      }

    case _actions_User__WEBPACK_IMPORTED_MODULE_0__["TYPE_SAVE_CURRENT_ADDRESS"]:
      {
        return Object.assign({}, state, {
          currentAddress: action.value
        });
      }

    case _actions_User__WEBPACK_IMPORTED_MODULE_0__["TYPE_SAVE_CURRENT_RESTAURANT_ID"]:
      {
        return Object.assign({}, state, {
          currentRestaurantId: action.value
        });
      }
    // case SAVE_CART_COUNT: {
    //   return Object.assign({}, state, {
    //     cartCount: action.value
    //   });
    // }

    default:
      return state;
  }
}

/***/ }),

/***/ "./pages/app/utils/Constants.js":
/*!**************************************!*\
  !*** ./pages/app/utils/Constants.js ***!
  \**************************************/
/*! exports provided: BASE_URL, BASE_URL_API, REGISTRATION_URL, LOGIN_URL, OTP_VERIFY, OTP_RESEND, PRODUCTS_BY_CATE, NEW_PRODUCTS, RECENT_PRODUCTS, PRODUCT_DETAIL, ADD_TO_CART, ADD_ADDRESS, GET_ADDRESS, DELETE_ADDRESS, ORDER_LISTING, LOGOUT_URL, COUNTRY_CODE_URL, DRIVER_TRACKING, CHANGE_TOKEN, CHECK_ORDER_URL, CHECK_ORDER_DELIVERED_URL, GET_RESTAURANT_DETAIL, GET_NOTIFICATION, ADD_REVIEW, ADD_ORDER, CMS_PAGE, PROMO_CODE_LIST, APPLY_PROMO_CODE, GET_RECIPE_LIST, REGISTRATION_HOME, GET_TAGS, GET_POPULAR_TAGS, GET_MORE_PRODUCTS, GET_ITEM_CATEGORIES, SEARCH_RESTAURANTS, CHECK_BOOKING_AVAIL, BOOKING_EVENT, BOOKING_HISTORY, DELETE_EVENT, UPDATE_PROFILE, RESET_PASSWORD_REQ_URL, FORGOT_PASSWORD, VERIFY_OTP, INR_SIGN, INR_SHORT_SIGN, RESERVE_STATIC, DOCUMENTS_STATIC, APP_NAME, DEFAULT_ALERT_TITLE, AlertButtons, RequestKeys, LoanTypes, DEF_LATITUDE, DEF_LONGITUDE, StorageKeys, ACCESS_TOKEN, RESPONSE_FAIL, RESPONSE_SUCCESS, COUPON_ERROR, GOOGLE_API_KEY, ORDER_TYPE, NOTIFICATION_TYPE, DEFAULT_TYPE, CART_PENDING_ITEMS, SEARCH_PLACEHOLDER, ABOUT_US, CONTACT_US, PRIVACY_POLICY, funGetTime, checkNull, funGetDate, funGetTomorrowDate, funGetDateStr, funGetTimeStr, removeChars, capitalize, capiString */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BASE_URL", function() { return BASE_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BASE_URL_API", function() { return BASE_URL_API; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "REGISTRATION_URL", function() { return REGISTRATION_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LOGIN_URL", function() { return LOGIN_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OTP_VERIFY", function() { return OTP_VERIFY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OTP_RESEND", function() { return OTP_RESEND; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PRODUCTS_BY_CATE", function() { return PRODUCTS_BY_CATE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NEW_PRODUCTS", function() { return NEW_PRODUCTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RECENT_PRODUCTS", function() { return RECENT_PRODUCTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PRODUCT_DETAIL", function() { return PRODUCT_DETAIL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_TO_CART", function() { return ADD_TO_CART; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_ADDRESS", function() { return ADD_ADDRESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_ADDRESS", function() { return GET_ADDRESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETE_ADDRESS", function() { return DELETE_ADDRESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ORDER_LISTING", function() { return ORDER_LISTING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LOGOUT_URL", function() { return LOGOUT_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "COUNTRY_CODE_URL", function() { return COUNTRY_CODE_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DRIVER_TRACKING", function() { return DRIVER_TRACKING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHANGE_TOKEN", function() { return CHANGE_TOKEN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHECK_ORDER_URL", function() { return CHECK_ORDER_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHECK_ORDER_DELIVERED_URL", function() { return CHECK_ORDER_DELIVERED_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_RESTAURANT_DETAIL", function() { return GET_RESTAURANT_DETAIL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_NOTIFICATION", function() { return GET_NOTIFICATION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_REVIEW", function() { return ADD_REVIEW; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ADD_ORDER", function() { return ADD_ORDER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CMS_PAGE", function() { return CMS_PAGE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PROMO_CODE_LIST", function() { return PROMO_CODE_LIST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APPLY_PROMO_CODE", function() { return APPLY_PROMO_CODE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_RECIPE_LIST", function() { return GET_RECIPE_LIST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "REGISTRATION_HOME", function() { return REGISTRATION_HOME; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_TAGS", function() { return GET_TAGS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_POPULAR_TAGS", function() { return GET_POPULAR_TAGS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_MORE_PRODUCTS", function() { return GET_MORE_PRODUCTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GET_ITEM_CATEGORIES", function() { return GET_ITEM_CATEGORIES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SEARCH_RESTAURANTS", function() { return SEARCH_RESTAURANTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHECK_BOOKING_AVAIL", function() { return CHECK_BOOKING_AVAIL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BOOKING_EVENT", function() { return BOOKING_EVENT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BOOKING_HISTORY", function() { return BOOKING_HISTORY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DELETE_EVENT", function() { return DELETE_EVENT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATE_PROFILE", function() { return UPDATE_PROFILE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RESET_PASSWORD_REQ_URL", function() { return RESET_PASSWORD_REQ_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FORGOT_PASSWORD", function() { return FORGOT_PASSWORD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VERIFY_OTP", function() { return VERIFY_OTP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "INR_SIGN", function() { return INR_SIGN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "INR_SHORT_SIGN", function() { return INR_SHORT_SIGN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RESERVE_STATIC", function() { return RESERVE_STATIC; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DOCUMENTS_STATIC", function() { return DOCUMENTS_STATIC; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APP_NAME", function() { return APP_NAME; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DEFAULT_ALERT_TITLE", function() { return DEFAULT_ALERT_TITLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertButtons", function() { return AlertButtons; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestKeys", function() { return RequestKeys; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoanTypes", function() { return LoanTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DEF_LATITUDE", function() { return DEF_LATITUDE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DEF_LONGITUDE", function() { return DEF_LONGITUDE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StorageKeys", function() { return StorageKeys; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACCESS_TOKEN", function() { return ACCESS_TOKEN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RESPONSE_FAIL", function() { return RESPONSE_FAIL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RESPONSE_SUCCESS", function() { return RESPONSE_SUCCESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "COUPON_ERROR", function() { return COUPON_ERROR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GOOGLE_API_KEY", function() { return GOOGLE_API_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ORDER_TYPE", function() { return ORDER_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NOTIFICATION_TYPE", function() { return NOTIFICATION_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DEFAULT_TYPE", function() { return DEFAULT_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CART_PENDING_ITEMS", function() { return CART_PENDING_ITEMS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SEARCH_PLACEHOLDER", function() { return SEARCH_PLACEHOLDER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ABOUT_US", function() { return ABOUT_US; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CONTACT_US", function() { return CONTACT_US; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PRIVACY_POLICY", function() { return PRIVACY_POLICY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "funGetTime", function() { return funGetTime; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "checkNull", function() { return checkNull; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "funGetDate", function() { return funGetDate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "funGetTomorrowDate", function() { return funGetTomorrowDate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "funGetDateStr", function() { return funGetDateStr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "funGetTimeStr", function() { return funGetTimeStr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeChars", function() { return removeChars; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "capitalize", function() { return capitalize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "capiString", function() { return capiString; });
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "moment");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
 // API URL CONSTANTS
// export const BASE_URL_API = "http://27.54.170.187/~restaura/v1/api/";
// export const BASE_URL_API = "http://eatance.evincedev.com/v1/api/";
// export const BASE_URL_API = "http://192.168.1.146/fooddeli/v1/api/";
//export const BASE_URL = "http://192.168.0.31";

const BASE_URL = "https://foods.khmerorder.com";
const BASE_URL_API = BASE_URL + "/public/api/";
const REGISTRATION_URL = BASE_URL_API + "app_register";
const LOGIN_URL = BASE_URL_API + "app_login";
const OTP_VERIFY = BASE_URL_API + "app_otp_verify";
const OTP_RESEND = BASE_URL_API + "app_otp_resend";
const PRODUCTS_BY_CATE = BASE_URL_API + "app_get_product_by_cate";
const NEW_PRODUCTS = BASE_URL_API + "app_get_products";
const RECENT_PRODUCTS = BASE_URL_API + "app_get_recent_products";
const PRODUCT_DETAIL = BASE_URL_API + "app_get_product_detail";
const ADD_TO_CART = BASE_URL_API + "app_addtocart";
const ADD_ADDRESS = BASE_URL_API + "app_save_address";
const GET_ADDRESS = BASE_URL_API + "app_get_addresses";
const DELETE_ADDRESS = BASE_URL_API + "app_delete_address";
const ORDER_LISTING = BASE_URL_API + "app_get_orders";
const LOGOUT_URL = BASE_URL_API + "logout";
const COUNTRY_CODE_URL = BASE_URL_API + "getCountryPhoneCode";
const DRIVER_TRACKING = BASE_URL_API + "delivery-guy-order-tracking";
const CHANGE_TOKEN = BASE_URL_API + "changeToken";
const CHECK_ORDER_URL = BASE_URL_API + "checkOrderDelivery";
const CHECK_ORDER_DELIVERED_URL = BASE_URL_API + "checkOrderDelivered";
const GET_RESTAURANT_DETAIL = BASE_URL_API + "get-restaurant-info-by-id";
const GET_NOTIFICATION = BASE_URL_API + "getNotification";
const ADD_REVIEW = BASE_URL_API + "addReview";
const ADD_ORDER = BASE_URL_API + "placeorderapi";
const CMS_PAGE = BASE_URL_API + "getCMSPage";
const PROMO_CODE_LIST = BASE_URL_API + "couponList";
const APPLY_PROMO_CODE = BASE_URL_API + "checkPromocode";
const GET_RECIPE_LIST = BASE_URL_API + "getReceipe";
const REGISTRATION_HOME = BASE_URL_API + "getHome";
const GET_TAGS = BASE_URL_API + "getTags";
const GET_POPULAR_TAGS = BASE_URL_API + "getPopularTags";
const GET_MORE_PRODUCTS = BASE_URL_API + "app_get_products";
const GET_ITEM_CATEGORIES = BASE_URL_API + "getCategories";
const SEARCH_RESTAURANTS = BASE_URL_API + "search-restaurants";
const CHECK_BOOKING_AVAIL = BASE_URL_API + "bookingAvailable";
const BOOKING_EVENT = BASE_URL_API + "bookEvent";
const BOOKING_HISTORY = BASE_URL_API + "getBooking";
const DELETE_EVENT = BASE_URL_API + "deleteBooking";
const UPDATE_PROFILE = BASE_URL_API + "editProfile";
const RESET_PASSWORD_REQ_URL = BASE_URL_API + "changePassword";
const FORGOT_PASSWORD = BASE_URL_API + "forgotpassword";
const VERIFY_OTP = BASE_URL_API + "verifyOTP";
const INR_SIGN = "USD \u0024";
const INR_SHORT_SIGN = "\u0024"; // ALERT CONSTANTS

const RESERVE_STATIC = "/reserve";
const DOCUMENTS_STATIC = "/documents";
const APP_NAME = "Just Order";
const DEFAULT_ALERT_TITLE = APP_NAME;
const AlertButtons = {
  ok: "OK",
  cancel: "Cancel",
  notNow: "Not now",
  yes: "Yes",
  no: "No"
}; // REQUESTS CONSTANTS

const RequestKeys = {
  contentType: "Content-Type",
  json: "application/json",
  authorization: "Authorization",
  bearer: "Bearer"
}; // LOAN TYPES CONSTANTS

const LoanTypes = {
  available: "available",
  reserved: "reserved",
  closed: "closed"
}; // LOCATION LATITUDE LONGITUDE

const DEF_LATITUDE = 11.5605504;
const DEF_LONGITUDE = 104.8838144; // STORAGE CONSTANTS

const StorageKeys = {
  user_details: "UserDetails"
}; // REDUX CONSTANTS

const ACCESS_TOKEN = "ACCESS_TOKEN";
const RESPONSE_FAIL = 0;
const RESPONSE_SUCCESS = 1;
const COUPON_ERROR = 2;
const GOOGLE_API_KEY = "AIzaSyCAzKH0AVRyXkKrP6XEcK2i9bGMHwr771c"; //NOTIFICATION_TYPE

const ORDER_TYPE = "orderNotification";
const NOTIFICATION_TYPE = "notification";
const DEFAULT_TYPE = "default"; //MESSAGES

const CART_PENDING_ITEMS = "You have pending items in cart from another restaurant";
const SEARCH_PLACEHOLDER = "Search for restaurant, cuisine or dish"; //CMS PAGE

const ABOUT_US = 1;
const CONTACT_US = 2;
const PRIVACY_POLICY = 3;
const funGetTime = date => {
  var d = new Date(date);
  return moment__WEBPACK_IMPORTED_MODULE_0___default()(d).format("LT");
};
const checkNull = data => {
  if (data != undefined && data != "") {
    return data;
  } else {
    return;
  }
};
const funGetDate = date => {
  var d = new Date(date);
  return moment__WEBPACK_IMPORTED_MODULE_0___default()(d).format("DD-MM-YYYY");
};
const funGetTomorrowDate = () => {
  var d = new Date();
  var newDate = moment__WEBPACK_IMPORTED_MODULE_0___default()(d).add(1, "day");
  return new Date(newDate);
};
function funGetDateStr(date, formats) {
  if (formats == undefined) {
    formats = "DD-MM-YYYY";
  }

  moment__WEBPACK_IMPORTED_MODULE_0___default.a.locale("en");
  var d = new Date("" + date.replaceAll("-", "/"));
  return moment__WEBPACK_IMPORTED_MODULE_0___default()(d).format(formats);
}
function funGetTimeStr(date) {
  moment__WEBPACK_IMPORTED_MODULE_0___default.a.locale("en");
  var d = new Date("" + date.replaceAll("-", "/"));
  return moment__WEBPACK_IMPORTED_MODULE_0___default()(d).format("LT");
}

String.prototype.replaceAll = function (search, replacement) {
  var target = this;
  return target.split(search).join(replacement);
};

function removeChars(str) {
  var target = "";
  var strArray = str.split(",");
  const rowLen = strArray.length;

  try {
    if (strArray != undefined) strArray.map((data, i) => {
      if (data.trim() === "" || data.trim() === undefined) {} else {
        if (rowLen === i + 1) {
          target = target + data;
        } else {
          target = target + data + ",";
        }
      }
    });
  } catch (e) {}

  return target;
}
function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
function capiString(str) {
  var splitStr = str.toLowerCase().split(" ");

  for (var i = 0; i < splitStr.length; i++) {
    // You do not need to check if i is larger than splitStr length, as your for does that for you
    // Assign it back to the array
    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  } // Directly return the joined string


  return splitStr.join(" ");
}

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return App; });
/* harmony import */ var react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/StyleSheet */ "react-native-web/dist/cjs/exports/StyleSheet");
/* harmony import */ var react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/Text */ "react-native-web/dist/cjs/exports/Text");
/* harmony import */ var react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/View */ "react-native-web/dist/cjs/exports/View");
/* harmony import */ var react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _app_redux_reducers_UserReducer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app/redux/reducers/UserReducer */ "./pages/app/redux/reducers/UserReducer.js");
/* harmony import */ var _app_redux_reducers_UserFavorite__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app/redux/reducers/UserFavorite */ "./pages/app/redux/reducers/UserFavorite.js");
/* harmony import */ var _app_redux_reducers_NavigationReducer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app/redux/reducers/NavigationReducer */ "./pages/app/redux/reducers/NavigationReducer.js");
/* harmony import */ var _app_redux_reducers_CheckoutReducer__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app/redux/reducers/CheckoutReducer */ "./pages/app/redux/reducers/CheckoutReducer.js");
/* harmony import */ var _app_containers_MainContainer__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app/containers/MainContainer */ "./pages/app/containers/MainContainer.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_10__);
var _jsxFileName = "G:\\JustOrderWeb\\pages\\index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement;











const rootReducer = Object(redux__WEBPACK_IMPORTED_MODULE_3__["combineReducers"])({
  userOperations: _app_redux_reducers_UserReducer__WEBPACK_IMPORTED_MODULE_5__["userOperations"],
  navigationReducer: _app_redux_reducers_NavigationReducer__WEBPACK_IMPORTED_MODULE_7__["navigationOperation"],
  checkoutReducer: _app_redux_reducers_CheckoutReducer__WEBPACK_IMPORTED_MODULE_8__["checkoutDetailOperation"],
  userFavorite: _app_redux_reducers_UserFavorite__WEBPACK_IMPORTED_MODULE_6__["UserFavorite"]
});
const JustOrderGlobalStore = Object(redux__WEBPACK_IMPORTED_MODULE_3__["createStore"])(rootReducer);
function App(props) {
  return __jsx(react_redux__WEBPACK_IMPORTED_MODULE_4__["Provider"], {
    store: JustOrderGlobalStore,
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 7
    }
  }, __jsx(_app_containers_MainContainer__WEBPACK_IMPORTED_MODULE_9__["default"], {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 9
    }
  }));
}
const styles = react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_0___default.a.create({
  container: {
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center'
  },
  link: {
    color: 'blue'
  },
  textContainer: {
    alignItems: 'center',
    marginTop: 16
  },
  text: {
    alignItems: 'center',
    fontSize: 24,
    marginBottom: 24
  }
});

/***/ }),

/***/ "moment":
/*!*************************!*\
  !*** external "moment" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-native-web/dist/cjs/exports/ActivityIndicator":
/*!**********************************************************************!*\
  !*** external "react-native-web/dist/cjs/exports/ActivityIndicator" ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-native-web/dist/cjs/exports/ActivityIndicator");

/***/ }),

/***/ "react-native-web/dist/cjs/exports/Dimensions":
/*!***************************************************************!*\
  !*** external "react-native-web/dist/cjs/exports/Dimensions" ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-native-web/dist/cjs/exports/Dimensions");

/***/ }),

/***/ "react-native-web/dist/cjs/exports/FlatList":
/*!*************************************************************!*\
  !*** external "react-native-web/dist/cjs/exports/FlatList" ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-native-web/dist/cjs/exports/FlatList");

/***/ }),

/***/ "react-native-web/dist/cjs/exports/Image":
/*!**********************************************************!*\
  !*** external "react-native-web/dist/cjs/exports/Image" ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-native-web/dist/cjs/exports/Image");

/***/ }),

/***/ "react-native-web/dist/cjs/exports/ImageBackground":
/*!********************************************************************!*\
  !*** external "react-native-web/dist/cjs/exports/ImageBackground" ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-native-web/dist/cjs/exports/ImageBackground");

/***/ }),

/***/ "react-native-web/dist/cjs/exports/StyleSheet":
/*!***************************************************************!*\
  !*** external "react-native-web/dist/cjs/exports/StyleSheet" ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-native-web/dist/cjs/exports/StyleSheet");

/***/ }),

/***/ "react-native-web/dist/cjs/exports/Text":
/*!*********************************************************!*\
  !*** external "react-native-web/dist/cjs/exports/Text" ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-native-web/dist/cjs/exports/Text");

/***/ }),

/***/ "react-native-web/dist/cjs/exports/TouchableOpacity":
/*!*********************************************************************!*\
  !*** external "react-native-web/dist/cjs/exports/TouchableOpacity" ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-native-web/dist/cjs/exports/TouchableOpacity");

/***/ }),

/***/ "react-native-web/dist/cjs/exports/View":
/*!*********************************************************!*\
  !*** external "react-native-web/dist/cjs/exports/View" ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-native-web/dist/cjs/exports/View");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2FwaS9TZXJ2aWNlTWFuYWdlci5qcyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL0NvbG9ycy5qcyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL0ZvbnRDb25zdGFudHMuanMiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9hYm91dF91cy5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9hYm91dHVzX3NlbGVjdGVkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2Fib3V0dXNkZXNlbGVjdGVkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2FkZC5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9hZGRyZXNzLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2JhY2sucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvYmFubmVyLXJlc3RhdXJhbnQuanBnIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvYmFubmVyX3BsYWNlaG9sZGVyLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2JnX2xvZ2luLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2JnX3NwbGFzaC5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9ib29raW5nYXZhaWxhYmxlLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2Jvb2tpbmdub3RhdmFpbGFibGUucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvYnJhbmQtaWNvbi5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9idXNpbmVzc19pY29ucy9hbGNvaG9sLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2J1c2luZXNzX2ljb25zL2V4cHJlc3MucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvYnVzaW5lc3NfaWNvbnMvZm9vZC5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9idXNpbmVzc19pY29ucy9ncm9jZXJ5LnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2NhbGVuZGFyLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2NhbGVuZGFyd2hpdGUucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvY2FsbC5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9jYWxsX29yZGVyLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2NhbWVyYV93aGl0ZS5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9jYXJ0LnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2NhcnRfYmxhY2sucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvY2F0ZWdvcnktaWNvbi5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9jbG9ja193aGl0ZS5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9jb25maXJtX2JhY2tncm91bmQucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvY29uZmlybV90aHVtYi5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9jb250YWN0dXNfZGVzZWxlY3RlZC5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9jb250YWN0dXNfc2VsZWN0ZWQucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvY291bmRvd24uanBnIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvY3Vpc2luZXNfZXhhbXBsZXMvQ29mZmVlLmpwZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2N1aXNpbmVzX2V4YW1wbGVzL0phcGFuZXNlLmpwZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2N1aXNpbmVzX2V4YW1wbGVzL01pbGsgVGVhLmpwZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2N1aXNpbmVzX2V4YW1wbGVzL1dlc3Rlcm4uanBnIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvY3VycmVuY3kucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvZGVsZXRlLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2RlbGV0ZV9XaGl0ZS5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9kZWxldGVfZ3JheS5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9kZWxpdmVyZWRkZXNlbGVjdGVkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2RlbGl2ZXJlZHNlbGVjdGVkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2Rpc2NvdW50X3N0aWNrLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2Rvd24ucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvZHJpdmVyLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2VkaXRfcHJvZmlsZS5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9lbXB0eS1jYXJ0LnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2V2ZW50Ym9va2luZ19kZXNlbGVjdGVkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2V2ZW50Ym9va2luZ19zZWxlY3RlZC5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9maWx0ZXIucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvZmxhZ3Mva2gucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvZm9vZF9pY29ucy9hbGxfY3Vpc2luZXMucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvZm9vZF9pY29ucy9hc2lhbi5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9mb29kX2ljb25zL2JicS5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9mb29kX2ljb25zL2NoaW5lc2UucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvZm9vZF9pY29ucy9jb2ZmZWUucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvZm9vZF9pY29ucy9pdGFsaWFuLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2Zvb2RfaWNvbnMvamFwYW5lc2UucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvZm9vZF9pY29ucy9vZmZlcnMucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvZm9vZF9pY29ucy9zb3VwLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2Zvb2RfaWNvbnMvdmlld19hbGwucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvaGVhZGxpbmUucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvaG9tZS1pY29uLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2hvbWVfZGVzZWxlY3RlZC5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9ob21lX2Z1bmN0aW9ucy9jYXRlZ29yeS5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9ob21lX2Z1bmN0aW9ucy9jb21tdW5pdHkucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvaG9tZV9mdW5jdGlvbnMvY291cG9uX2RlYWxzLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2hvbWVfZnVuY3Rpb25zL2ZsYXNoX3NhbGUucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvaG9tZV9mdW5jdGlvbnMvaG90LnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2hvbWVfZnVuY3Rpb25zL25ldy5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9ob21lX2Z1bmN0aW9ucy9zaGFyZV9naWZ0cy5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9ob21lX2Z1bmN0aW9ucy91bmRlcjEwLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2hvbWVfc2VsZWN0ZWQucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvaG90LWljb24ucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvaWNfYmFja19yZXNlcnZhdGlvbi5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9pY19jbG9zZS5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9pdGVtTm90Rm91bmQucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvbGFuZy5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9sZWZ0VG9wUmliYm9uLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2xvY2F0aW9uLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL2xvY2F0aW9uX2dyZXkucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvbG9nby5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9tYXBfbWFya2Vycy9jdXN0b21lci5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9tYXBfbWFya2Vycy9tYXAtY3JvcHBlZC5qcGciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9tZW51LnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL21lcmNoYW4tYWRzLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL21pbnVzX3JvdW5kLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL21pbnVzcXR5LnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL21vcmUucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvbW90by5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9teWJvb2tpbmdfZGVzZWxlY3RlZC5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9teWJvb2tpbmdfc2VsZWN0ZWQucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvbmFtZS5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9ub3Byb2R1Y3RzLmpwZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL25vdGlmaWNhdGlvbl9kZXNlbGVjdGVkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL25vdGlmaWNhdGlvbl9zZWxlY3RlZC5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9vbnRoZXdheWRlc2VsZWN0ZWQucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2Uvb250aGV3YXlzZWxlY3RlZC5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9vcmRlcl9kZXNlbGVjdGVkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL29yZGVyX3NlbGVjdGVkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL29yZGVycGxhY2Vkc2VsZWN0ZWQucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvcGFzc3dvcmQucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvcGF5bWVudF9nYXRld2F5cy9hYmFwYXkucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvcGF5bWVudF9nYXRld2F5cy9jYXJkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL3BheW1lbnRfZ2F0ZXdheXMvdmlzYV9jYXJkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL3BheW1lbnRfZ2F0ZXdheXMvd2luZy5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9wZW9wbGUucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvcGl6emEuanBnIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvcGx1cy5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9wbHVzX3JvdW5kLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL3BsdXNxdHkucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvcHJlcGFyaW5nZGVzZWxlY3RlZC5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9wcmVwYXJpbmdzZWxlY3RlZC5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9wcml2YWN5X3BvbGljeS5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9wcml2YWN5cG9saWN5X2Rlc2VsZWN0ZWQucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvcHJpdmFjeXBvbGljeV9zZWxlY3RlZC5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9wcm9maWxlX2FkZHJlc3MucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvcmF0ZXVzX2Rlc2VsZWN0ZWQucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvcmF0ZXVzX3NlbGVjdGVkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL3JlY2lwZV9kZXNlbGVjdGVkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL3JlY2lwZV9zZWFyY2gucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvcmVjaXBlX3NlbGVjdGVkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL3Jlc3RfbG9nby5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9yZXN0YXVyYW50X21lbnUucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvcmlnaHQtYXJyb3cucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvcmlnaHRUb3BSaWJib24ucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2Uvc2VhcmNoLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL3NldHRpbmdzX3NlbGVjdGVkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL3NoYXJldXNfZGVzZWxlY3RlZC5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9zaGFyZXVzX3NlbGVjdGVkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL3NpZ25fdXAucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2Uvc2lnbm91dF9kZXNlbGVjdGVkLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL3NpZ25vdXRfc2VsZWN0ZWQucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2Uvc2xpZGVzaG93X2V4YW1wbGUvMS5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9zbGlkZXNob3dfZXhhbXBsZS8yLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL3NsaWRlc2hvd19leGFtcGxlLzMucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2Uvc2xpZGVzaG93X2V4YW1wbGUvNC5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS9zdGFyLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL3N0YXJfd2hpdGUucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2Uvc3VwZXJfZGVhbHMtaWNvbi5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS90aWNrLnBuZyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvYXNzZXRzL2ltYWdlL3RpbWUucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvdXBfYXJyb3cucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW1hZ2UvdXNlcl9wbGFjZWhvbGRlci5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS92YWNhbmN5LWFkcy5wbmciLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2Fzc2V0cy9pbWFnZS92ZXJpZmllZC1zaGllbGQucG5nIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9hc3NldHMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2NvbXBvbmVudHMvRVRleHRWaWV3Tm9ybWFsTGFiZWwuanMiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL2NvbXBvbmVudHMvUG9wdWxhclJlc3RhdXJhbnRDYXJkLmpzIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9jb21wb25lbnRzL1BvcHVsYXJSZXN0YXVyYW50cy5qcyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvY29udGFpbmVycy9CYXNlQ29udGFpbmVyLmpzIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9jb250YWluZXJzL01haW5Db250YWluZXIuanMiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL3JlZHV4L2FjdGlvbnMvQ2hlY2tvdXQuanMiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL3JlZHV4L2FjdGlvbnMvTmF2aWdhdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcHAvcmVkdXgvYWN0aW9ucy9Vc2VyLmpzIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9yZWR1eC9yZWR1Y2Vycy9DaGVja291dFJlZHVjZXIuanMiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL3JlZHV4L3JlZHVjZXJzL05hdmlnYXRpb25SZWR1Y2VyLmpzIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC9yZWR1eC9yZWR1Y2Vycy9Vc2VyRmF2b3JpdGUuanMiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBwL3JlZHV4L3JlZHVjZXJzL1VzZXJSZWR1Y2VyLmpzIiwid2VicGFjazovLy8uL3BhZ2VzL2FwcC91dGlscy9Db25zdGFudHMuanMiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibW9tZW50XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3RcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1uYXRpdmUtd2ViL2Rpc3QvY2pzL2V4cG9ydHMvQWN0aXZpdHlJbmRpY2F0b3JcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1uYXRpdmUtd2ViL2Rpc3QvY2pzL2V4cG9ydHMvRGltZW5zaW9uc1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LW5hdGl2ZS13ZWIvZGlzdC9janMvZXhwb3J0cy9GbGF0TGlzdFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LW5hdGl2ZS13ZWIvZGlzdC9janMvZXhwb3J0cy9JbWFnZVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LW5hdGl2ZS13ZWIvZGlzdC9janMvZXhwb3J0cy9JbWFnZUJhY2tncm91bmRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1uYXRpdmUtd2ViL2Rpc3QvY2pzL2V4cG9ydHMvU3R5bGVTaGVldFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LW5hdGl2ZS13ZWIvZGlzdC9janMvZXhwb3J0cy9UZXh0XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QtbmF0aXZlLXdlYi9kaXN0L2Nqcy9leHBvcnRzL1RvdWNoYWJsZU9wYWNpdHlcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1uYXRpdmUtd2ViL2Rpc3QvY2pzL2V4cG9ydHMvVmlld1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LXJlZHV4XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVkdXhcIiJdLCJuYW1lcyI6WyJhcGlQb3N0IiwidXJsIiwiYm9keSIsInJlc3BvbnNlU3VjY2VzcyIsInJlc3BvbnNlRXJyIiwicmVxdWVzdEhlYWRlciIsImNvbnNvbGUiLCJsb2ciLCJKU09OIiwic3RyaW5naWZ5IiwiZmV0Y2giLCJtZXRob2QiLCJoZWFkZXJzIiwidGhlbiIsImVycm9ySGFuZGxlciIsInJlc3BvbnNlIiwianNvbiIsImNhdGNoIiwiZXJyIiwiYXBpR2V0IiwiYXBpRGVsZXRlIiwic3RhdHVzIiwic3VjY2VzcyIsIlByb21pc2UiLCJyZXNvbHZlIiwiZXJyb3IiLCJFcnJvciIsInN0YXR1c1RleHQiLCJyZWplY3QiLCJhcGlQb3N0UXMiLCJBY2NlcHQiLCJFRENvbG9ycyIsInByaW1hcnkiLCJwcmltYXJ5RGlzYWJsZWQiLCJMUHJpbWFyeSIsInNlY29uZGFyeSIsInNlY29uZGFyeUJ1dHRvbnMiLCJiYWNrZ3JvdW5kIiwiYm9yZGVyQ29sb3IiLCJiYWNrZ3JvdW5kRGFyayIsImJhY2tncm91bmRMaWdodCIsInBsYWNlaG9sZGVyIiwidGV4dCIsImRhcmtUZXh0IiwiYWN0aXZlVGFiQ29sb3IiLCJpbmFjdGl2ZVRhYkNvbG9yIiwidGhlbWVHcmVlbiIsImJsYWNrIiwid2hpdGUiLCJvZmZXaGl0ZSIsImJ1dHRvblJlc2VydmUiLCJidXR0b25VbnJlc2VydmUiLCJidXR0b25WaWV3RG9jdW1lbnRzIiwicGFsZVllbGxvdyIsInBhbGVCbHVlIiwic2hhZG93IiwiREFSS0dSQVkiLCJMR1JBWSIsIkNHUkFZIiwiR1JBWSIsIldISVRFIiwiTElHSFRHUkFZIiwiQVBQQ09MT1JTIiwiZ3JvdXAxTGVmdDUiLCJncm91cDFMZWZ0NCIsImdyb3VwMUxlZnQzIiwiZ3JvdXAxTGVmdDIiLCJncm91cDFMZWZ0MSIsImdyb3VwMU1haW4iLCJncm91cDFSaWdodDEiLCJncm91cDFSaWdodDIiLCJncm91cDJMZWZ0NSIsImdyb3VwMkxlZnQ0IiwiZ3JvdXAyTGVmdDMiLCJncm91cDJMZWZ0MiIsImdyb3VwMkxlZnQxIiwiZ3JvdXAyTWFpbiIsImdyb3VwMlJpZ2h0MSIsImdyb3VwMlJpZ2h0MiIsImdyb3VwM0xlZnQyIiwiZ3JvdXAzTGVmdDEiLCJncm91cDNNYWluIiwiZ3JvdXAzUmlnaHQxIiwiZ3JvdXAzUmlnaHQyIiwiZ3JvdXA0TGVmdDMiLCJncm91cDRMZWZ0MiIsImdyb3VwNExlZnQxIiwiZ3JvdXA0TWFpbiIsImdyb3VwNFJpZ2h0MSIsImdyb3VwNFJpZ2h0MiIsInJnYmFQcmltYXJ5MCIsInJnYmFQcmltYXJ5MSIsInJnYmFQcmltYXJ5MiIsInJnYmFQcmltYXJ5MyIsInJnYmFQcmltYXJ5NCIsInJnYmFTZWNvbmRhcnkxMCIsInJnYmFTZWNvbmRhcnkxMSIsInJnYmFTZWNvbmRhcnkxMiIsInJnYmFTZWNvbmRhcnkxMyIsInJnYmFTZWNvbmRhcnkxNCIsInJnYmFTZWNvbmRhcnkyMCIsInJnYmFTZWNvbmRhcnkyMSIsInJnYmFTZWNvbmRhcnkyMiIsInJnYmFTZWNvbmRhcnkyMyIsInJnYmFTZWNvbmRhcnkyNCIsInJnYmFDb21wbGVtZW50MjAiLCJyZ2JhQ29tcGxlbWVudDIxIiwicmdiYUNvbXBsZW1lbnQyMiIsInJnYmFDb21wbGVtZW50MjMiLCJyZ2JhQ29tcGxlbWVudDI0IiwiRVRGb250cyIsImJvbGQiLCJyZWd1bGFyIiwibGlnaHQiLCJoYWlybGluZSIsInNhdGlzZnkiLCJpY29uRm9udCIsIkFQUEZPTlRTIiwiYm9sZEl0YWxpYyIsIml0YWxpYyIsImxpZ2h0SXRhbGljIiwibWVkaXVtIiwibWVkdWltSXRhbGljIiwidGhpbiIsInRoaW5JdGFsaWMiLCJBc3NldHMiLCJiZ0hvbWUiLCJyZXF1aXJlIiwiYmdTaWdudXAiLCJzZXR0aW5nIiwiYmFja1doaXRlIiwibWVudSIsIm1vcmUiLCJhZGQiLCJsb2dvIiwiZHJpdmVyIiwiY2FsbF9vcmRlciIsInJlZnJlc2giLCJiZ19sb2dpbiIsImljX2xvY2F0aW9uIiwicmF0aW5nIiwidGltZSIsInJlc3RNZW51IiwiYWRkcmVzcyIsImljX3NlYXJjaCIsInJlY2lwZV9zZWFyY2giLCJpY19sb2NhdGlvbl9ncmV5IiwiaWNfcGx1cyIsImljX21pbnVzIiwiaWNfcmVzZXJ2YXRpb25fYmFjayIsImljX3BsdXNfeWVsbG93IiwiaWNfZmlsdGVyIiwiaWNfY2FydCIsImJsX2NhcnQiLCJpY191cF9hcnJvdyIsImljX2Rvd25fYXJyb3ciLCJjdXJyZW5jeSIsInNhbXBsZV9yZXN0X2Jhbm5lciIsInNhbXBsZV9yZXN0X2xvZ28iLCJzdGFyIiwidmVyaWZpZWQiLCJob21lX2Rlc2VsZWN0IiwicmVjaXBlX2Rlc2VsZWN0Iiwib3JkZXJfZGVzZWxlY3QiLCJldmVudGJvb2tpbmdfZGVzZWxlY3QiLCJteWJvb2tpbmdfZGVzZWxlY3QiLCJub3RpZmljYXRpb25fZGVzZWxlY3QiLCJyYXRldXNfZGVzZWxlY3QiLCJzaGFyZXVzX2Rlc2VsZWN0IiwicHJpdmFjeXBvbGljeV9kZXNlbGVjdCIsImFib3V0dXNfZGVzZWxlY3QiLCJjb250YWN0dXNfZGVzZWxlY3QiLCJzaWdub3V0X2Rlc2VsZWN0IiwiaG9tZV9zZWxlY3QiLCJyZWNpcGVfc2VsZWN0Iiwib3JkZXJfc2VsZWN0IiwiZXZlbnRib29raW5nX3NlbGVjdCIsIm15Ym9va2luZ19zZWxlY3QiLCJub3RpZmljYXRpb25fc2VsZWN0IiwicmF0ZXVzX3NlbGVjdCIsInNoYXJldXNfc2VsZWN0IiwicHJpdmFjeXBvbGljeV9zZWxlY3QiLCJhYm91dHVzX3NlbGVjdCIsImNvbnRhY3R1c19zZWxlY3QiLCJzaWdub3V0X3NlbGVjdCIsImRlbGl2ZXJlZHNlbGVjdGVkIiwiZGVsaXZlcmVkZGVzZWxlY3RlZCIsIm9yZGVycGxhY2Vkc2VsZWN0ZWQiLCJvbnRoZXdheXNlbGVjdGVkIiwib250aGV3YXlkZXNlbGVjdGVkIiwicHJlcGFyaW5nZGVzZWxlY3RlZCIsInByZXBhcmluZ3NlbGVjdGVkIiwiYm9va2luZ2F2YWlsYWJsZSIsImJvb2tpbmdub3RhdmFpbGFibGUiLCJkZWxldGVfY2FydCIsImljX2Nsb3NlIiwiYWJvdXRfdXMiLCJwcml2YWN5X3BvbGljeSIsImNvbmZpcm1fYmFja2dyb3VuZCIsImNvbmZpcm1fdGh1bWIiLCJoZWFkZXJfcGxhY2Vob2xkZXIiLCJkZWxldGVfV2hpdGUiLCJjYWxsIiwib3VyX2FkZHJlc3MiLCJuYW1lIiwicGFzc3dvcmQiLCJub3RpZmljYXRpb24iLCJlZGl0IiwicmF0aW5nX3doaXRlIiwiY2xvY2tfd2hpdGUiLCJzdGFyX3doaXRlIiwicGVvcGxlX3doaXRlIiwiY2FsZW5kZXJfd2hpdGUiLCJjYWxlbmRlciIsInVzZXJfcGxhY2Vob2xkZXIiLCJwbHVzX3JvdW5kIiwibWludXNfcm91bmQiLCJkZWxldGVfZ3JheSIsInRpY2siLCJjYW1lcmFfd2hpdGUiLCJkaXNjb3VudF9zdGljayIsImxhbmciLCJsYW5nXzIiLCJub3Byb2R1Y3QiLCJzdXBwZXJfZGVhbF9pY29uIiwiYXJyb3dfcmlnaHQiLCJoZWFkbGluZSIsImhvbWVfaWNvbiIsImhvdF9pY29uIiwiYnJhbmRfaWNvbiIsImNhdGVnb3J5X2ljb24iLCJmbl9jYXRlZ29yeSIsImZuX2NvbW11bml0eSIsImZuX2NvdXBvbl9kZWFscyIsImZuX2ZsYXNoX3NhbGUiLCJmbl9ob3QiLCJmbl9uZXciLCJmbl91bmVyMTAiLCJmbl9zaGFyZV9naWZ0cyIsImljb25fZm9vZCIsImljb25fYWxjb2hvbCIsImljb25fZ3JvY2VyeSIsImljb25fZXhwcmVzcyIsIm1hcmtlcl9jdXN0b21lciIsIm1hcF9jcm9wcGVkIiwicGl6emEiLCJjb3VudGRvd24iLCJtb3RvIiwiY3Vpc2luZXNfZXhhbXBsZTEiLCJjdWlzaW5lc19leGFtcGxlMiIsImN1aXNpbmVzX2V4YW1wbGUzIiwiY3Vpc2luZXNfZXhhbXBsZTQiLCJpY29uX2Zvb2Rfb2ZmZXJzIiwiaWNvbl9mb29kX3ZpZXdfYWxsIiwiaWNvbl9mb29kX2FsbF9jdWlzaW5lcyIsImljb25fZm9vZF9iYnEiLCJpY29uX2Zvb2RfY2hpbmVzZSIsImljb25fZm9vZF9jb2ZmZWUiLCJpY29uX2Zvb2RfaXRhbGlhbiIsImljb25fZm9vZF9qYXBhbmVzZSIsImljb25fZm9vZF9waXp6YSIsImljb25fZm9vZF9zb3VwIiwiaWNvbl9mb29kX2FzaWFuIiwic2xpZGVfMSIsInNsaWRlXzIiLCJzbGlkZV8zIiwic2xpZGVfNCIsImZsYWdfa2giLCJsZWZ0VG9wUmliYm9uIiwicmlnaHRUb3BSaWJib24iLCJ2YWNhbmN5QWRzIiwibWVyY2hhbnRBZHMiLCJjYXNoX2ljb24iLCJ3aW5nX2ljb24iLCJhYmFwYXlfaWNvbiIsInZpc2FjYXJkX2ljb24iLCJlbXB0eV9jYXJ0IiwiaXRlbU5vdEZvdW5kIiwiRVRleHRWaWV3Tm9ybWFsTGFiZWwiLCJSZWFjdCIsIkNvbXBvbmVudCIsInJlbmRlciIsInN0eWxlc0xhYmxlIiwidGV4dExhYmxlIiwicHJvcHMiLCJzdHlsZSIsIlN0eWxlU2hlZXQiLCJjcmVhdGUiLCJtYXJnaW5TdGFydCIsIm1hcmdpbkVuZCIsImNvbG9yIiwiZm9udFNpemUiLCJtYXJnaW5Ub3AiLCJtYXJnaW5MZWZ0IiwiZm9udEZhbWlseSIsIndpZHRoIiwiaGVpZ2h0IiwiRGltZW5zaW9ucyIsImdldCIsIml0ZW1XaWR0aCIsIlBvcHVsYXJSZXN0YXVyYW50Q2FyZCIsIlB1cmVDb21wb25lbnQiLCJjb25zdHJ1Y3RvciIsInJlc3RPYmpNb2RlbCIsImN1cnJlbnRBdmFpbGFibGVEYXkiLCJjdXJyZW50X2RlbGl2ZXJ5X2F2YWlsYWJsZSIsIml0ZW1QcmVzcyIsInJlc3RJZCIsInN0YXRlIiwiaWQiLCJkaXN0YW50IiwiY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyIsIm5leHRQcm9wcyIsInNldFN0YXRlIiwibm90QXZhaWxhYmxlTWVzc2FnZUJveCIsImRlbGl2ZXJ5T2JqZWN0IiwicG9zaXRpb24iLCJ0b3AiLCJsZWZ0IiwiYWxpZ25JdGVtcyIsImFsaWduQ29udGVudCIsImp1c3RpZnlDb250ZW50IiwiekluZGV4Iiwic3R5bGVzIiwidW5hdmFpbGFibGVNZXNzYWdlIiwidW5hdmFpbGFibGVNZXNzYWdlVGV4dEJveCIsInN0YXJ0X3RpbWUiLCJlbmRfdGltZSIsInNob3dQcmljZUxldmVsIiwicHJpY2VfbGV2ZWwiLCJ2aWV3cyIsImkiLCJwdXNoIiwiZmxleERpcmVjdGlvbiIsIm1hcCIsInZpZXciLCJzaG93RGlzdGFudCIsImZsZXgiLCJwYXJzZUZsb2F0IiwidG9GaXhlZCIsInRvU3RyaW5nIiwic2hvd0RlbGl2ZXJ5Q2hhcmdlIiwiZGVsaXZlcnlfY2hhcmdlcyIsIklOUl9TSE9SVF9TSUdOIiwic2hvd01pbmltdW1DaGFyZ2UiLCJtaW51bUNoYXJnZSIsIm1pbmltdW1DaGFyZ2UiLCJzaG93RGVsaXZlcnlUaW1lIiwibWluIiwibWF4IiwiZGVsaXZlcnlUaW1lIiwic2hvd0F2YWlsYWJsZSIsInRlbXBvcmFyeV9jbG9zZSIsInRlbXBvcmFyaWx5Q2xvc2VPdmVybGF5IiwidGVtcG9yYXJpbHlDbG9zZU1lc3NhZ2UiLCJkZWxpdmVyeV9hdmFpbGFibGUiLCJkb3QiLCJub3NwYWNlIiwicGFkZGluZ0hvcml6b250YWwiLCJmb250V2VpZ2h0IiwiY29udGFpbmVyIiwiaG9yaXpvbnRhbCIsIm1hcmdpblJpZ2h0IiwicmVzdGF1cmFudEJhbm5lckJveCIsImltYWdlIiwidXJpIiwiQkFTRV9VUkwiLCJyZXN0YXVyYW50QmFubmVyIiwicmVzdGF1cmFudExvZ29Cb3giLCJyZXN0YXVyYW50TG9nbyIsIm9wYWNpdHkiLCJyaWdodEJhbm5lck92ZXJsYXkiLCJmcmVlc2hpcE92ZXJsYXkiLCJmcmVlc2hpcEJhbm5lclN0eWxlIiwicmlnaHQiLCJ0ZXh0QWxpZ24iLCJ0cmFuc2Zvcm0iLCJyb3RhdGUiLCJyZXN0YXVyYW50SW5mb1Jvd0JveCIsIm1hcmdpbkJvdHRvbSIsInJlc3RhdXJhbnROYW1lIiwicmF0aW5nUmlnaHQiLCJyYXRpbmdOdW1iZXIiLCJvdmVyZmxvdyIsInBhZGRpbmdWZXJ0aWNhbCIsIml0ZW1EZXNjcmlwdGlvbiIsImZsZXhXcmFwIiwicGFkZGluZ0xlZnQiLCJkZWxpdmVyeUNoYXJnZVJvd1RleHQiLCJkZWxpdmVyeVRpbWVUZXh0IiwibWluT3JkZXJUZXh0IiwiZ2xvYmFsIiwibWluaW11bU9yZGVyIiwiYm9yZGVyV2lkdGgiLCJib3JkZXJSYWRpdXMiLCJib3JkZXJTdHlsZSIsImJhY2tncm91bmRDb2xvciIsInBhZGRpbmdSaWdodCIsInBhZGRpbmdCb3R0b20iLCJzaGFkb3dDb2xvciIsInNoYWRvd09mZnNldCIsInNoYWRvd09wYWNpdHkiLCJzaGFkb3dSYWRpdXMiLCJlbGV2YXRpb24iLCJib3JkZXJUb3BMZWZ0UmFkaXVzIiwiYm9yZGVyVG9wUmlnaHRSYWRpdXMiLCJhbGlnblNlbGYiLCJib3R0b20iLCJkaXNjb3VudE92ZXJsYXkiLCJ2ZXJpZmllZEljb24iLCJyZXN0YXVyYW50SW5mb1Jvd09uZSIsInJlc3RhdXJhbnRJbmZvUm93VHdvIiwiZGVsaXZlcnlDaGFyZ2UiLCJwcm9tb3Rpb24iLCJhdmFpbGFibGUiLCJ1bmF2YWlsYWJsZSIsImJvcmRlclRvcFdpZHRoIiwiYm9yZGVyVG9wQ29sb3IiLCJib3JkZXJCb3R0b21XaWR0aCIsImJvcmRlckJvdHRvbUNvbG9yIiwicGFkZGluZ1RvcCIsImxldHRlclNwYWNpbmciLCJQb3B1bGFyUmVzdGF1cmFudHMiLCJhcnJheVJlc3RhdXJhbnRzIiwicmVzdGF1cmFudHMiLCJzaG93RmlsdGVyIiwic2hvdyIsImlzTG9hZGluZyIsInJlbmRlckZvb3RlciIsImZvb3RlciIsIm1hcmdpbiIsIm9uU2Nyb2xsIiwibWFyZ2luSG9yaXpvbnRhbCIsIml0ZW0iLCJpbmRleCIsIlN0cmluZyIsInBhZGRpbmciLCJCYXNlQ29udGFpbmVyIiwiY2hpbGRyZW4iLCJNYWluQ29udGFpbmVyIiwidW5kZWZpbmVkIiwicG9wdWxhclJlc3RhdXJhbnRzIiwibGVuZ3RoIiwic2hvd1Jlc3RhdXJhbnRSb3dzIiwibWFyZ2luVmVydGljYWwiLCJtaW5IZWlnaHQiLCJnZXREYXRhIiwicGFyYW0iLCJSRUdJU1RSQVRJT05fSE9NRSIsInJlc3AiLCJSRVNQT05TRV9TVUNDRVNTIiwibGFzdF9wYWdlIiwiY3VycmVudF9wYWdlIiwic2hvd1NwbGFzaExvYWRlciIsInNldFRpbWVvdXQiLCJhbGxNYWluUm93Iiwicm93cyIsImpvaW5Ob3ciLCJhbGxSZXN0YXVyYW50Um93cyIsImNvbXBvbmVudERpZE1vdW50IiwicmVmIiwibWFpbkZsYXRMaXN0Iiwic2Nyb2xsVXBCdXR0b24iLCJpdGVtSW1hZ2UiLCJyZXNpemVNb2RlIiwic3Bpbm5lciIsImFub3VuY2VtZW50Um93IiwidGV4dEFsaWduVmVydGljYWwiLCJUWVBFX1NBVkVfQ0hFQ0tPVVRfREVUQUlMUyIsIlNBVkVfQ0FSVF9DT1VOVCIsIlNBVkVfQ0FSVF9UT1RBTCIsInNhdmVDaGVja291dERldGFpbHMiLCJkYXRhIiwidHlwZSIsInZhbHVlIiwic2F2ZUNhcnRDb3VudCIsInNhdmVDYXJ0VG90YWwiLCJUWVBFX1NBVkVfTkFWSUdBVElPTl9TRUxFQ1RJT04iLCJzYXZlTmF2aWdhdGlvblNlbGVjdGlvbiIsIlRZUEVfU0FWRV9MT0dJTl9ERVRBSUxTIiwic2F2ZVVzZXJEZXRhaWxzSW5SZWR1eCIsImRldGFpbHMiLCJUWVBFX1NBVkVfVVNFUl9BRERSRVNTRVMiLCJzYXZlVXNlckFkZHJlc3Nlc0luUmVkdXgiLCJUWVBFX1NBVkVfQ0FSVF9BRERSRVNTRVMiLCJzYXZlQ2FydEFkZHJlc3Nlc0luUmVkdXgiLCJUWVBFX1NBVkVfTE9HSU5fRkNNIiwic2F2ZVVzZXJGQ01JblJlZHV4IiwiVFlQRV9TQVZFX0NPVU5UUllfQ09ERSIsInNhdmVDb3VudHJ5Q29kZUluUmVkdXgiLCJUWVBFX1NBVkVfQ1VSUkVOVF9BRERSRVNTIiwic2F2ZUN1cnJlbnRBZGRyZXNzSW5SZWR1eCIsIlRZUEVfU0FWRV9DVVJSRU5UX1JFU1RBVVJBTlRfSUQiLCJzYXZlQ3VycmVudFJlc3RhdXJhbnRJZEluUmVkdXgiLCJUWVBFX1NBVkVfRkFWT1JJVEVfSVRFTVMiLCJzYXZlRmF2b3JpdGVJdGVtc0luUmVkdXgiLCJUWVBFX1NBVkVfRkFWT1JJVEVfU0hPUFMiLCJzYXZlRmF2b3JpdGVTaG9wc0luUmVkdXgiLCJpbml0YWxTdGF0ZSIsImNoZWNrb3V0RGV0YWlsIiwiY2FydENvdW50IiwiY2FydFRvdGFsIiwiY2hlY2tvdXREZXRhaWxPcGVyYXRpb24iLCJhY3Rpb24iLCJPYmplY3QiLCJhc3NpZ24iLCJzZWxlY3RlZEl0ZW0iLCJuYXZpZ2F0aW9uT3BlcmF0aW9uIiwiaW5pdGlhbEZhdm9yaXRlIiwiZmF2b3JpdGVJdGVtcyIsIml0ZW1zIiwiZmF2b3JpdGVTaG9wcyIsInNob3BzIiwiVXNlckZhdm9yaXRlIiwiaW5pdGlhbFN0YXRlVXNlciIsInBob25lTnVtYmVySW5SZWR1eCIsInVzZXJJZEluUmVkdXgiLCJjdXJyZW50UmVzdGF1cmFudElkIiwiY3VycmVudEFkZHJlc3MiLCJsYW5kbWFyayIsImxhdGl0dWRlIiwibG9uZ2l0dWRlIiwic2F2ZWRDYXJ0QWRkcmVzcyIsInVzZXJPcGVyYXRpb25zIiwiUGhvbmVOdW1iZXIiLCJVc2VySUQiLCJzYXZlZEFkZHJlc3NlcyIsInRva2VuIiwiY29kZSIsIkJBU0VfVVJMX0FQSSIsIlJFR0lTVFJBVElPTl9VUkwiLCJMT0dJTl9VUkwiLCJPVFBfVkVSSUZZIiwiT1RQX1JFU0VORCIsIlBST0RVQ1RTX0JZX0NBVEUiLCJORVdfUFJPRFVDVFMiLCJSRUNFTlRfUFJPRFVDVFMiLCJQUk9EVUNUX0RFVEFJTCIsIkFERF9UT19DQVJUIiwiQUREX0FERFJFU1MiLCJHRVRfQUREUkVTUyIsIkRFTEVURV9BRERSRVNTIiwiT1JERVJfTElTVElORyIsIkxPR09VVF9VUkwiLCJDT1VOVFJZX0NPREVfVVJMIiwiRFJJVkVSX1RSQUNLSU5HIiwiQ0hBTkdFX1RPS0VOIiwiQ0hFQ0tfT1JERVJfVVJMIiwiQ0hFQ0tfT1JERVJfREVMSVZFUkVEX1VSTCIsIkdFVF9SRVNUQVVSQU5UX0RFVEFJTCIsIkdFVF9OT1RJRklDQVRJT04iLCJBRERfUkVWSUVXIiwiQUREX09SREVSIiwiQ01TX1BBR0UiLCJQUk9NT19DT0RFX0xJU1QiLCJBUFBMWV9QUk9NT19DT0RFIiwiR0VUX1JFQ0lQRV9MSVNUIiwiR0VUX1RBR1MiLCJHRVRfUE9QVUxBUl9UQUdTIiwiR0VUX01PUkVfUFJPRFVDVFMiLCJHRVRfSVRFTV9DQVRFR09SSUVTIiwiU0VBUkNIX1JFU1RBVVJBTlRTIiwiQ0hFQ0tfQk9PS0lOR19BVkFJTCIsIkJPT0tJTkdfRVZFTlQiLCJCT09LSU5HX0hJU1RPUlkiLCJERUxFVEVfRVZFTlQiLCJVUERBVEVfUFJPRklMRSIsIlJFU0VUX1BBU1NXT1JEX1JFUV9VUkwiLCJGT1JHT1RfUEFTU1dPUkQiLCJWRVJJRllfT1RQIiwiSU5SX1NJR04iLCJSRVNFUlZFX1NUQVRJQyIsIkRPQ1VNRU5UU19TVEFUSUMiLCJBUFBfTkFNRSIsIkRFRkFVTFRfQUxFUlRfVElUTEUiLCJBbGVydEJ1dHRvbnMiLCJvayIsImNhbmNlbCIsIm5vdE5vdyIsInllcyIsIm5vIiwiUmVxdWVzdEtleXMiLCJjb250ZW50VHlwZSIsImF1dGhvcml6YXRpb24iLCJiZWFyZXIiLCJMb2FuVHlwZXMiLCJyZXNlcnZlZCIsImNsb3NlZCIsIkRFRl9MQVRJVFVERSIsIkRFRl9MT05HSVRVREUiLCJTdG9yYWdlS2V5cyIsInVzZXJfZGV0YWlscyIsIkFDQ0VTU19UT0tFTiIsIlJFU1BPTlNFX0ZBSUwiLCJDT1VQT05fRVJST1IiLCJHT09HTEVfQVBJX0tFWSIsIk9SREVSX1RZUEUiLCJOT1RJRklDQVRJT05fVFlQRSIsIkRFRkFVTFRfVFlQRSIsIkNBUlRfUEVORElOR19JVEVNUyIsIlNFQVJDSF9QTEFDRUhPTERFUiIsIkFCT1VUX1VTIiwiQ09OVEFDVF9VUyIsIlBSSVZBQ1lfUE9MSUNZIiwiZnVuR2V0VGltZSIsImRhdGUiLCJkIiwiRGF0ZSIsIk1vbWVudCIsImZvcm1hdCIsImNoZWNrTnVsbCIsImZ1bkdldERhdGUiLCJmdW5HZXRUb21vcnJvd0RhdGUiLCJuZXdEYXRlIiwiZnVuR2V0RGF0ZVN0ciIsImZvcm1hdHMiLCJsb2NhbGUiLCJyZXBsYWNlQWxsIiwiZnVuR2V0VGltZVN0ciIsInByb3RvdHlwZSIsInNlYXJjaCIsInJlcGxhY2VtZW50IiwidGFyZ2V0Iiwic3BsaXQiLCJqb2luIiwicmVtb3ZlQ2hhcnMiLCJzdHIiLCJzdHJBcnJheSIsInJvd0xlbiIsInRyaW0iLCJlIiwiY2FwaXRhbGl6ZSIsInN0cmluZyIsImNoYXJBdCIsInRvVXBwZXJDYXNlIiwic2xpY2UiLCJjYXBpU3RyaW5nIiwic3BsaXRTdHIiLCJ0b0xvd2VyQ2FzZSIsInN1YnN0cmluZyIsInJvb3RSZWR1Y2VyIiwiY29tYmluZVJlZHVjZXJzIiwibmF2aWdhdGlvblJlZHVjZXIiLCJjaGVja291dFJlZHVjZXIiLCJ1c2VyRmF2b3JpdGUiLCJKdXN0T3JkZXJHbG9iYWxTdG9yZSIsImNyZWF0ZVN0b3JlIiwiQXBwIiwiZmxleEdyb3ciLCJsaW5rIiwidGV4dENvbnRhaW5lciJdLCJtYXBwaW5ncyI6Ijs7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLElBQUk7UUFDSjtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7OztBQ3hGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7O0FBUU8sZUFBZUEsT0FBZixDQUNMQyxHQURLLEVBRUxDLElBRkssRUFHTEMsZUFISyxFQUlMQyxXQUpLLEVBS0xDLGFBQWEsR0FBRztBQUNkLGtCQUFnQjtBQURGLENBTFgsRUFRTDtBQUNBQyxTQUFPLENBQUNDLEdBQVIsQ0FBWSxTQUFaLEVBQXVCQyxJQUFJLENBQUNDLFNBQUwsQ0FBZVAsSUFBZixDQUF2QjtBQUNBLFNBQU9RLEtBQUssQ0FBQ1QsR0FBRCxFQUFNO0FBQ2hCVSxVQUFNLEVBQUUsTUFEUTtBQUVoQkMsV0FBTyxFQUFFUCxhQUZPO0FBR2hCSCxRQUFJLEVBQUVNLElBQUksQ0FBQ0MsU0FBTCxDQUFlUCxJQUFmO0FBSFUsR0FBTixDQUFMLENBS0pXLElBTEksQ0FLQ0MsWUFMRCxFQU1KRCxJQU5JLENBTUNFLFFBQVEsSUFBSUEsUUFBUSxDQUFDQyxJQUFULEVBTmIsRUFPSkgsSUFQSSxDQU9DRyxJQUFJLElBQUliLGVBQWUsQ0FBQ2EsSUFBRCxDQVB4QixFQVFKQyxLQVJJLENBUUVDLEdBQUcsSUFBSWQsV0FBVyxDQUFDYyxHQUFELENBUnBCLENBQVA7QUFTRDtBQUVEOzs7Ozs7OztBQU9PLGVBQWVDLE1BQWYsQ0FDTGxCLEdBREssRUFFTEUsZUFGSyxFQUdMQyxXQUhLLEVBSUxDLGFBQWEsR0FBRztBQUNkLGtCQUFnQjtBQURGLENBSlgsRUFPTDtBQUNBLFNBQU9LLEtBQUssQ0FBQ1QsR0FBRCxFQUFNO0FBQ2hCVSxVQUFNLEVBQUUsS0FEUTtBQUVoQkMsV0FBTyxFQUFFUDtBQUZPLEdBQU4sQ0FBTCxDQUlKUSxJQUpJLENBSUNDLFlBSkQsRUFLSkQsSUFMSSxDQUtDRSxRQUFRLElBQUlBLFFBQVEsQ0FBQ0MsSUFBVCxFQUxiLEVBTUpILElBTkksQ0FNQ0csSUFBSSxJQUFJYixlQUFlLENBQUNhLElBQUQsQ0FOeEIsRUFPSkMsS0FQSSxDQU9FQyxHQUFHLElBQUlkLFdBQVcsQ0FBQ2MsR0FBRCxDQVBwQixDQUFQO0FBUUQ7QUFFRDs7Ozs7Ozs7QUFPTyxTQUFTRSxTQUFULENBQ0xuQixHQURLLEVBRUxFLGVBRkssRUFHTEMsV0FISyxFQUlMQyxhQUFhLEdBQUc7QUFDZCxrQkFBZ0I7QUFERixDQUpYLEVBT0w7QUFDQUssT0FBSyxDQUFDVCxHQUFELEVBQU07QUFDVFUsVUFBTSxFQUFFLFFBREM7QUFFVEMsV0FBTyxFQUFFUDtBQUZBLEdBQU4sQ0FBTCxDQUlHUSxJQUpILENBSVFDLFlBSlIsRUFLR0QsSUFMSCxDQUtRRSxRQUFRLElBQUtBLFFBQVEsQ0FBQ00sTUFBVCxJQUFtQixHQUFuQixHQUF5Qk4sUUFBekIsR0FBb0NBLFFBQVEsQ0FBQ0MsSUFBVCxFQUx6RCxFQU1HSCxJQU5ILENBTVFHLElBQUksSUFBSWIsZUFBZSxDQUFDYSxJQUFELENBTi9CLEVBT0dDLEtBUEgsQ0FPU0MsR0FBRyxJQUFJZCxXQUFXLENBQUNjLEdBQUQsQ0FQM0I7QUFRRCxDLENBRUQ7O0FBQ0E7Ozs7O0FBSUEsTUFBTUosWUFBWSxHQUFHQyxRQUFRLElBQUk7QUFDL0JULFNBQU8sQ0FBQ0MsR0FBUixDQUFZLGNBQVosRUFBMkJRLFFBQTNCOztBQUNBLE1BQ0dBLFFBQVEsQ0FBQ00sTUFBVCxJQUFtQixHQUFuQixJQUEwQk4sUUFBUSxDQUFDTSxNQUFULEdBQWtCLEdBQTdDLElBQ0FOLFFBQVEsQ0FBQ00sTUFBVCxJQUFtQixHQURuQixJQUVBTixRQUFRLENBQUNNLE1BQVQsSUFBbUIsR0FIckIsRUFJRTtBQUNBLFFBQUlOLFFBQVEsQ0FBQ00sTUFBVCxJQUFtQixHQUF2QixFQUE0QjtBQUMxQk4sY0FBUSxDQUFDYixJQUFULEdBQWdCO0FBQUVvQixlQUFPLEVBQUU7QUFBWCxPQUFoQjtBQUNEOztBQUNELFdBQU9DLE9BQU8sQ0FBQ0MsT0FBUixDQUFnQlQsUUFBaEIsQ0FBUDtBQUNELEdBVEQsTUFTTztBQUVMLFFBQUlVLEtBQUssR0FBRyxJQUFJQyxLQUFKLENBQVVYLFFBQVEsQ0FBQ1ksVUFBVCxJQUF1QlosUUFBUSxDQUFDTSxNQUExQyxDQUFaO0FBQ0FJLFNBQUssQ0FBQ1YsUUFBTixHQUFpQkEsUUFBakI7QUFDQSxXQUFPUSxPQUFPLENBQUNLLE1BQVIsQ0FBZUgsS0FBZixDQUFQO0FBQ0Q7QUFDRixDQWpCRDs7QUFtQk8sZUFBZUksU0FBZixDQUNMNUIsR0FESyxFQUVMQyxJQUZLLEVBR0xDLGVBSEssRUFJTEMsV0FKSyxFQUtMQyxhQUFhLEdBQUcsRUFMWCxFQU1MO0FBQ0FDLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLFNBQVosRUFBdUJDLElBQUksQ0FBQ0MsU0FBTCxDQUFlUCxJQUFmLENBQXZCO0FBQ0FRLE9BQUssQ0FBQ1QsR0FBRCxFQUFNO0FBQ1RVLFVBQU0sRUFBRSxNQURDO0FBRVRULFFBQUksRUFBRUEsSUFGRztBQUdUVSxXQUFPLEVBQUU7QUFDUGtCLFlBQU0sRUFBRSxrQkFERDtBQUVQLHNCQUFnQjtBQUZUO0FBSEEsR0FBTixDQUFMLENBUUdqQixJQVJILENBUVFDLFlBUlIsRUFTR0QsSUFUSCxDQVNRRSxRQUFRLElBQUlBLFFBQVEsQ0FBQ0MsSUFBVCxFQVRwQixFQVVHSCxJQVZILENBVVFHLElBQUksSUFBSWIsZUFBZSxDQUFDYSxJQUFELENBVi9CLEVBV0dDLEtBWEgsQ0FXU0MsR0FBRyxJQUFJZCxXQUFXLENBQUNjLEdBQUQsQ0FYM0I7QUFZRCxDOzs7Ozs7Ozs7Ozs7QUMzSEQ7QUFBQTtBQUFBO0FBQU8sTUFBTWEsUUFBUSxHQUFHO0FBQ3RCQyxTQUFPLEVBQUUsU0FEYTtBQUV0QkMsaUJBQWUsRUFBRSxTQUZLO0FBR3RCQyxVQUFRLEVBQUUsU0FIWTtBQUl0QkMsV0FBUyxFQUFFLFNBSlc7QUFLdEJDLGtCQUFnQixFQUFFLFNBTEk7QUFNdEJDLFlBQVUsRUFBRSxTQU5VO0FBT3RCQyxhQUFXLEVBQUUsa0JBUFM7QUFRdEJDLGdCQUFjLEVBQUUsTUFSTTtBQVN0QkMsaUJBQWUsRUFBRSxrQkFUSztBQVV0QkMsYUFBVyxFQUFFLFNBVlM7QUFXdEJDLE1BQUksRUFBRSxNQVhnQjtBQVl0QkMsVUFBUSxFQUFFLFVBWlk7QUFhdEJDLGdCQUFjLEVBQUMsU0FiTztBQWN0QkMsa0JBQWdCLEVBQUMsU0FkSztBQWV0QnBCLE9BQUssRUFBQyxLQWZnQjtBQWdCdEJxQixZQUFVLEVBQUUsaUJBaEJVO0FBaUJ0QkMsT0FBSyxFQUFFLE9BakJlO0FBa0J0QkMsT0FBSyxFQUFFLE9BbEJlO0FBbUJ0QkMsVUFBUSxFQUFDLGtCQW5CYTtBQW9CdEJDLGVBQWEsRUFBRSxpQkFwQk87QUFxQnRCQyxpQkFBZSxFQUFFLHdCQXJCSztBQXNCdEJDLHFCQUFtQixFQUFFLGlCQXRCQztBQXVCdEJDLFlBQVUsRUFBRSxrQkF2QlU7QUF3QnRCQyxVQUFRLEVBQUUsa0JBeEJZO0FBeUJ0QkMsUUFBTSxFQUFDLE1BekJlO0FBMEJ0QkMsVUFBUSxFQUFDLFNBMUJhO0FBMkJ0QkMsT0FBSyxFQUFDLFNBM0JnQjtBQTRCdEJDLE9BQUssRUFBQyxTQTVCZ0I7QUE2QnRCQyxNQUFJLEVBQUMsU0E3QmlCO0FBOEJ0QkMsT0FBSyxFQUFDLE1BOUJnQjtBQStCdEJDLFdBQVMsRUFBQztBQS9CWSxDQUFqQjtBQWtDQSxNQUFNQyxTQUFTLEdBQUc7QUFDekJDLGFBQVcsRUFBQyxTQURhO0FBRXpCQyxhQUFXLEVBQUMsU0FGYTtBQUd6QkMsYUFBVyxFQUFDLFNBSGE7QUFJekJDLGFBQVcsRUFBQyxTQUphO0FBS3pCQyxhQUFXLEVBQUMsU0FMYTtBQU16QkMsWUFBVSxFQUFDLFNBTmM7O0FBTUg7QUFDdEJDLGNBQVksRUFBQyxTQVBZO0FBUXpCQyxjQUFZLEVBQUMsU0FSWTtBQVV6QkMsYUFBVyxFQUFDLFNBVmE7QUFXekJDLGFBQVcsRUFBQyxTQVhhO0FBWXpCQyxhQUFXLEVBQUMsU0FaYTtBQWF6QkMsYUFBVyxFQUFDLFNBYmE7QUFjekJDLGFBQVcsRUFBQyxTQWRhO0FBZXpCQyxZQUFVLEVBQUMsU0FmYzs7QUFlSDtBQUN0QkMsY0FBWSxFQUFDLFNBaEJZO0FBaUJ6QkMsY0FBWSxFQUFDLFNBakJZO0FBb0J6QkMsYUFBVyxFQUFDLFNBcEJhO0FBcUJ6QkMsYUFBVyxFQUFDLFNBckJhO0FBc0J6QkMsWUFBVSxFQUFDLFNBdEJjOztBQXNCSDtBQUN0QkMsY0FBWSxFQUFDLFNBdkJZO0FBd0J6QkMsY0FBWSxFQUFDLFNBeEJZO0FBMEJ6QkMsYUFBVyxFQUFDLFNBMUJhO0FBMkJ6QkMsYUFBVyxFQUFDLFNBM0JhO0FBNEJ6QkMsYUFBVyxFQUFDLFNBNUJhO0FBNkJ6QkMsWUFBVSxFQUFDLFNBN0JjOztBQTZCSDtBQUN0QkMsY0FBWSxFQUFDLFNBOUJZO0FBK0J6QkMsY0FBWSxFQUFDLFNBL0JZOztBQW1DekI7QUFFQUMsY0FBWSxFQUFDLHFCQXJDWTs7QUFxQ1c7QUFDcENDLGNBQVksRUFBQyxxQkF0Q1k7QUF1Q3pCQyxjQUFZLEVBQUMscUJBdkNZO0FBd0N6QkMsY0FBWSxFQUFDLHFCQXhDWTtBQXlDekJDLGNBQVksRUFBQyxxQkF6Q1k7QUEyQ3pCQyxpQkFBZSxFQUFDLHFCQTNDUzs7QUEyQ2M7QUFDdkNDLGlCQUFlLEVBQUMscUJBNUNTO0FBNkN6QkMsaUJBQWUsRUFBQyxxQkE3Q1M7QUE4Q3pCQyxpQkFBZSxFQUFDLHFCQTlDUztBQStDekJDLGlCQUFlLEVBQUMscUJBL0NTO0FBaUR6QkMsaUJBQWUsRUFBQyxxQkFqRFM7O0FBaURjO0FBQ3ZDQyxpQkFBZSxFQUFDLHFCQWxEUztBQW1EekJDLGlCQUFlLEVBQUMscUJBbkRTO0FBb0R6QkMsaUJBQWUsRUFBQyxxQkFwRFM7QUFxRHpCQyxpQkFBZSxFQUFDLHFCQXJEUztBQXVEekJDLGtCQUFnQixFQUFDLHFCQXZEUTs7QUF1RGU7QUFDeENDLGtCQUFnQixFQUFDLHFCQXhEUTtBQXlEekJDLGtCQUFnQixFQUFDLHFCQXpEUTtBQTBEekJDLGtCQUFnQixFQUFDLHFCQTFEUTtBQTJEekJDLGtCQUFnQixFQUFDO0FBM0RRLENBQWxCLEM7Ozs7Ozs7Ozs7OztBQ2xDUDtBQUFBO0FBQUE7QUFBTyxNQUFNQyxPQUFPLEdBQUc7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFRUMsTUFBSSxFQUFFLFFBVGE7QUFVbkJoRSxPQUFLLEVBQUUsUUFWWTtBQVduQmlFLFNBQU8sRUFBRSxRQVhVO0FBWW5CQyxPQUFLLEVBQUUsUUFaWTtBQWFuQkMsVUFBUSxFQUFFLFFBYlM7QUFjbkJDLFNBQU8sRUFBRyxRQWRTO0FBZW5CQyxVQUFRLEVBQUU7QUFmUyxDQUFoQjtBQWtCQSxNQUFNQyxRQUFRLEdBQUc7QUFDcEJOLE1BQUksRUFBQyxhQURlO0FBRXBCaEUsT0FBSyxFQUFDLGFBRmM7QUFHcEJ1RSxZQUFVLEVBQUMsbUJBSFM7QUFJcEJDLFFBQU0sRUFBQyxlQUphO0FBS3BCTixPQUFLLEVBQUMsY0FMYztBQU1wQk8sYUFBVyxFQUFDLG9CQU5RO0FBT3BCQyxRQUFNLEVBQUUsZUFQWTtBQVFwQkMsY0FBWSxFQUFDLHFCQVJPO0FBU3BCVixTQUFPLEVBQUUsZ0JBVFc7QUFVcEJXLE1BQUksRUFBQyxhQVZlO0FBV3BCQyxZQUFVLEVBQUM7QUFYUyxDQUFqQixDOzs7Ozs7Ozs7OztBQ2xCUCxvRDs7Ozs7Ozs7Ozs7QUNBQSw0RDs7Ozs7Ozs7Ozs7QUNBQSw2RDs7Ozs7Ozs7Ozs7QUNBQSwrQzs7Ozs7Ozs7Ozs7QUNBQSxtRDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSw2RDs7Ozs7Ozs7Ozs7QUNBQSw4RDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSxxRDs7Ozs7Ozs7Ozs7QUNBQSw0RDs7Ozs7Ozs7Ozs7QUNBQSwrRDs7Ozs7Ozs7Ozs7QUNBQSxzRDs7Ozs7Ozs7Ozs7QUNBQSxtRDs7Ozs7Ozs7Ozs7QUNBQSxtRDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSxtRDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSx5RDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSxzRDs7Ozs7Ozs7Ozs7QUNBQSx3RDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSxzRDs7Ozs7Ozs7Ozs7QUNBQSx5RDs7Ozs7Ozs7Ozs7QUNBQSx1RDs7Ozs7Ozs7Ozs7QUNBQSw4RDs7Ozs7Ozs7Ozs7QUNBQSx5RDs7Ozs7Ozs7Ozs7QUNBQSxnRTs7Ozs7Ozs7Ozs7QUNBQSw4RDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSxrRDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSxtRDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSxrRDs7Ozs7Ozs7Ozs7QUNBQSx3RDs7Ozs7Ozs7Ozs7QUNBQSx1RDs7Ozs7Ozs7Ozs7QUNBQSwrRDs7Ozs7Ozs7Ozs7QUNBQSw2RDs7Ozs7Ozs7Ozs7QUNBQSwwRDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSxrRDs7Ozs7Ozs7Ozs7QUNBQSx3RDs7Ozs7Ozs7Ozs7QUNBQSxzRDs7Ozs7Ozs7Ozs7QUNBQSxtRTs7Ozs7Ozs7Ozs7QUNBQSxpRTs7Ozs7Ozs7Ozs7QUNBQSxrRDs7Ozs7Ozs7Ozs7QUNBQSw4Qzs7Ozs7Ozs7Ozs7QUNBQSx3RDs7Ozs7Ozs7Ozs7QUNBQSxpRDs7Ozs7Ozs7Ozs7QUNBQSwrQzs7Ozs7Ozs7Ozs7QUNBQSxtRDs7Ozs7Ozs7Ozs7QUNBQSxrRDs7Ozs7Ozs7Ozs7QUNBQSxtRDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSxrRDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSxxRDs7Ozs7Ozs7Ozs7QUNBQSwyRDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSxxRDs7Ozs7Ozs7Ozs7QUNBQSx3RDs7Ozs7Ozs7Ozs7QUNBQSxzRDs7Ozs7Ozs7Ozs7QUNBQSwrQzs7Ozs7Ozs7Ozs7QUNBQSwrQzs7Ozs7Ozs7Ozs7QUNBQSx1RDs7Ozs7Ozs7Ozs7QUNBQSxtRDs7Ozs7Ozs7Ozs7QUNBQSx5RDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSwrRDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSx3RDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSx5RDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSx5RDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSx1RDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSx1RDs7Ozs7Ozs7Ozs7QUNBQSx1RDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSxnRTs7Ozs7Ozs7Ozs7QUNBQSw4RDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSxzRDs7Ozs7Ozs7Ozs7QUNBQSxtRTs7Ozs7Ozs7Ozs7QUNBQSxpRTs7Ozs7Ozs7Ozs7QUNBQSw4RDs7Ozs7Ozs7Ozs7QUNBQSw0RDs7Ozs7Ozs7Ozs7QUNBQSw0RDs7Ozs7Ozs7Ozs7QUNBQSwwRDs7Ozs7Ozs7Ozs7QUNBQSwrRDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSxrRDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSxxRDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSxrRDs7Ozs7Ozs7Ozs7QUNBQSxpRDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSxzRDs7Ozs7Ozs7Ozs7QUNBQSxtRDs7Ozs7Ozs7Ozs7QUNBQSwrRDs7Ozs7Ozs7Ozs7QUNBQSw2RDs7Ozs7Ozs7Ozs7QUNBQSwwRDs7Ozs7Ozs7Ozs7QUNBQSxvRTs7Ozs7Ozs7Ozs7QUNBQSxrRTs7Ozs7Ozs7Ozs7QUNBQSwyRDs7Ozs7Ozs7Ozs7QUNBQSw2RDs7Ozs7Ozs7Ozs7QUNBQSwyRDs7Ozs7Ozs7Ozs7QUNBQSw2RDs7Ozs7Ozs7Ozs7QUNBQSx5RDs7Ozs7Ozs7Ozs7QUNBQSwyRDs7Ozs7Ozs7Ozs7QUNBQSxxRDs7Ozs7Ozs7Ozs7QUNBQSwyRDs7Ozs7Ozs7Ozs7QUNBQSx1RDs7Ozs7Ozs7Ozs7QUNBQSwwRDs7Ozs7Ozs7Ozs7QUNBQSxrRDs7Ozs7Ozs7Ozs7QUNBQSw2RDs7Ozs7Ozs7Ozs7QUNBQSw4RDs7Ozs7Ozs7Ozs7QUNBQSw0RDs7Ozs7Ozs7Ozs7QUNBQSxtRDs7Ozs7Ozs7Ozs7QUNBQSw4RDs7Ozs7Ozs7Ozs7QUNBQSw0RDs7Ozs7Ozs7Ozs7QUNBQSw2Qzs7Ozs7Ozs7Ozs7QUNBQSw2Qzs7Ozs7Ozs7Ozs7QUNBQSw2Qzs7Ozs7Ozs7Ozs7QUNBQSw2Qzs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSxzRDs7Ozs7Ozs7Ozs7QUNBQSw0RDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSxnRDs7Ozs7Ozs7Ozs7QUNBQSxvRDs7Ozs7Ozs7Ozs7QUNBQSw0RDs7Ozs7Ozs7Ozs7QUNBQSx1RDs7Ozs7Ozs7Ozs7QUNBQSwyRDs7Ozs7Ozs7Ozs7O0FDQUE7QUFBYTs7QUFFYixNQUFNQyxNQUFNLEdBQUc7QUFDWEMsUUFBTSxFQUFFQyxtQkFBTyxDQUFDLHFFQUFELENBREo7QUFFWDtBQUNBQyxVQUFRLEVBQUVELG1CQUFPLENBQUMsaUVBQUQsQ0FITjtBQUlYRSxTQUFPLEVBQUVGLG1CQUFPLENBQUMscUZBQUQsQ0FKTDtBQUtYRyxXQUFTLEVBQUVILG1CQUFPLENBQUMsMkRBQUQsQ0FMUDtBQU1YSSxNQUFJLEVBQUVKLG1CQUFPLENBQUMsMkRBQUQsQ0FORjtBQU9YSyxNQUFJLEVBQUVMLG1CQUFPLENBQUMsMkRBQUQsQ0FQRjtBQVFYTSxLQUFHLEVBQUVOLG1CQUFPLENBQUMseURBQUQsQ0FSRDtBQVNYTyxNQUFJLEVBQUVQLG1CQUFPLENBQUMsMkRBQUQsQ0FURjtBQVVYUSxRQUFNLEVBQUVSLG1CQUFPLENBQUMsK0RBQUQsQ0FWSjtBQVdYUyxZQUFVLEVBQUVULG1CQUFPLENBQUMsdUVBQUQsQ0FYUjtBQVlYVSxTQUFPLEVBQUUscUJBWkU7QUFjWDtBQUNBO0FBQ0FDLFVBQVEsRUFBRVgsbUJBQU8sQ0FBQyxtRUFBRCxDQWhCTjtBQWlCWFksYUFBVyxFQUFFWixtQkFBTyxDQUFDLG1FQUFELENBakJUO0FBbUJYYSxRQUFNLEVBQUViLG1CQUFPLENBQUMsaUZBQUQsQ0FuQko7QUFvQlhjLE1BQUksRUFBRWQsbUJBQU8sQ0FBQywyREFBRCxDQXBCRjtBQXFCWGUsVUFBUSxFQUFFZixtQkFBTyxDQUFDLGlGQUFELENBckJOO0FBc0JYZ0IsU0FBTyxFQUFFaEIsbUJBQU8sQ0FBQyxpRUFBRCxDQXRCTDtBQXVCWGlCLFdBQVMsRUFBRWpCLG1CQUFPLENBQUMsK0RBQUQsQ0F2QlA7QUF3QlhrQixlQUFhLEVBQUVsQixtQkFBTyxDQUFDLDZFQUFELENBeEJYO0FBeUJYbUIsa0JBQWdCLEVBQUVuQixtQkFBTyxDQUFDLDZFQUFELENBekJkO0FBMEJYb0IsU0FBTyxFQUFFcEIsbUJBQU8sQ0FBQyxpRUFBRCxDQTFCTDtBQTJCWHFCLFVBQVEsRUFBRXJCLG1CQUFPLENBQUMsbUVBQUQsQ0EzQk47QUE0QlhzQixxQkFBbUIsRUFBRXRCLG1CQUFPLENBQUMseUZBQUQsQ0E1QmpCO0FBNkJYdUIsZ0JBQWMsRUFBRXZCLG1CQUFPLENBQUMsMkRBQUQsQ0E3Qlo7QUE4Qlh3QixXQUFTLEVBQUV4QixtQkFBTyxDQUFDLCtEQUFELENBOUJQO0FBK0JYeUIsU0FBTyxFQUFFekIsbUJBQU8sQ0FBQywyREFBRCxDQS9CTDtBQWdDWDBCLFNBQU8sRUFBRTFCLG1CQUFPLENBQUMsdUVBQUQsQ0FoQ0w7QUFpQ1gyQixhQUFXLEVBQUUzQixtQkFBTyxDQUFDLG1FQUFELENBakNUO0FBa0NYNEIsZUFBYSxFQUFFNUIsbUJBQU8sQ0FBQywyREFBRCxDQWxDWDtBQW9DWDZCLFVBQVEsRUFBRTdCLG1CQUFPLENBQUMsbUVBQUQsQ0FwQ047QUFzQ1g4QixvQkFBa0IsRUFBRTlCLG1CQUFPLENBQUMscUZBQUQsQ0F0Q2hCO0FBdUNYK0Isa0JBQWdCLEVBQUUvQixtQkFBTyxDQUFDLHFFQUFELENBdkNkO0FBd0NYZ0MsTUFBSSxFQUFFaEMsbUJBQU8sQ0FBQywyREFBRCxDQXhDRjtBQTBDWGlDLFVBQVEsRUFBRWpDLG1CQUFPLENBQUMsaUZBQUQsQ0ExQ047QUE0Q1g7QUFDQWtDLGVBQWEsRUFBRWxDLG1CQUFPLENBQUMsaUZBQUQsQ0E3Q1g7QUE4Q1htQyxpQkFBZSxFQUFFbkMsbUJBQU8sQ0FBQyxxRkFBRCxDQTlDYjtBQStDWG9DLGdCQUFjLEVBQUVwQyxtQkFBTyxDQUFDLG1GQUFELENBL0NaO0FBZ0RYcUMsdUJBQXFCLEVBQUVyQyxtQkFBTyxDQUFDLGlHQUFELENBaERuQjtBQWlEWHNDLG9CQUFrQixFQUFFdEMsbUJBQU8sQ0FBQywyRkFBRCxDQWpEaEI7QUFrRFh1Qyx1QkFBcUIsRUFBRXZDLG1CQUFPLENBQUMsaUdBQUQsQ0FsRG5CO0FBbURYd0MsaUJBQWUsRUFBRXhDLG1CQUFPLENBQUMscUZBQUQsQ0FuRGI7QUFvRFh5QyxrQkFBZ0IsRUFBRXpDLG1CQUFPLENBQUMsdUZBQUQsQ0FwRGQ7QUFxRFgwQyx3QkFBc0IsRUFBRTFDLG1CQUFPLENBQUMsbUdBQUQsQ0FyRHBCO0FBc0RYMkMsa0JBQWdCLEVBQUUzQyxtQkFBTyxDQUFDLHFGQUFELENBdERkO0FBdURYNEMsb0JBQWtCLEVBQUU1QyxtQkFBTyxDQUFDLDJGQUFELENBdkRoQjtBQXdEWDZDLGtCQUFnQixFQUFFN0MsbUJBQU8sQ0FBQyx1RkFBRCxDQXhEZDtBQTBEWDtBQUNBOEMsYUFBVyxFQUFFOUMsbUJBQU8sQ0FBQyw2RUFBRCxDQTNEVDtBQTREWCtDLGVBQWEsRUFBRS9DLG1CQUFPLENBQUMsaUZBQUQsQ0E1RFg7QUE2RFhnRCxjQUFZLEVBQUVoRCxtQkFBTyxDQUFDLCtFQUFELENBN0RWO0FBOERYaUQscUJBQW1CLEVBQUVqRCxtQkFBTyxDQUFDLDZGQUFELENBOURqQjtBQStEWGtELGtCQUFnQixFQUFFbEQsbUJBQU8sQ0FBQyx1RkFBRCxDQS9EZDtBQWdFWG1ELHFCQUFtQixFQUFFbkQsbUJBQU8sQ0FBQyw2RkFBRCxDQWhFakI7QUFpRVhvRCxlQUFhLEVBQUVwRCxtQkFBTyxDQUFDLGlGQUFELENBakVYO0FBa0VYcUQsZ0JBQWMsRUFBRXJELG1CQUFPLENBQUMsbUZBQUQsQ0FsRVo7QUFtRVhzRCxzQkFBb0IsRUFBRXRELG1CQUFPLENBQUMsK0ZBQUQsQ0FuRWxCO0FBb0VYdUQsZ0JBQWMsRUFBRXZELG1CQUFPLENBQUMsbUZBQUQsQ0FwRVo7QUFxRVh3RCxrQkFBZ0IsRUFBRXhELG1CQUFPLENBQUMsdUZBQUQsQ0FyRWQ7QUFzRVh5RCxnQkFBYyxFQUFFekQsbUJBQU8sQ0FBQyxtRkFBRCxDQXRFWjtBQXdFWDBELG1CQUFpQixFQUFFMUQsbUJBQU8sQ0FBQyxxRkFBRCxDQXhFZjtBQXlFWDJELHFCQUFtQixFQUFFM0QsbUJBQU8sQ0FBQyx5RkFBRCxDQXpFakI7QUEwRVg0RCxxQkFBbUIsRUFBRTVELG1CQUFPLENBQUMseUZBQUQsQ0ExRWpCO0FBMkVYNkQsa0JBQWdCLEVBQUU3RCxtQkFBTyxDQUFDLG1GQUFELENBM0VkO0FBNEVYOEQsb0JBQWtCLEVBQUU5RCxtQkFBTyxDQUFDLHVGQUFELENBNUVoQjtBQTZFWCtELHFCQUFtQixFQUFFL0QsbUJBQU8sQ0FBQyx5RkFBRCxDQTdFakI7QUE4RVhnRSxtQkFBaUIsRUFBRWhFLG1CQUFPLENBQUMscUZBQUQsQ0E5RWY7QUErRVhpRSxrQkFBZ0IsRUFBRWpFLG1CQUFPLENBQUMsbUZBQUQsQ0EvRWQ7QUFnRlhrRSxxQkFBbUIsRUFBRWxFLG1CQUFPLENBQUMseUZBQUQsQ0FoRmpCO0FBaUZYbUUsYUFBVyxFQUFFbkUsbUJBQU8sQ0FBQywrREFBRCxDQWpGVDtBQWtGWG9FLFVBQVEsRUFBRXBFLG1CQUFPLENBQUMsbUVBQUQsQ0FsRk47QUFtRlhxRSxVQUFRLEVBQUVyRSxtQkFBTyxDQUFDLG1FQUFELENBbkZOO0FBb0ZYc0UsZ0JBQWMsRUFBRXRFLG1CQUFPLENBQUMsK0VBQUQsQ0FwRlo7QUFxRlh1RSxvQkFBa0IsRUFBRXZFLG1CQUFPLENBQUMsdUZBQUQsQ0FyRmhCO0FBc0ZYd0UsZUFBYSxFQUFFeEUsbUJBQU8sQ0FBQyw2RUFBRCxDQXRGWDtBQXVGWHlFLG9CQUFrQixFQUFFekUsbUJBQU8sQ0FBQyx1RkFBRCxDQXZGaEI7QUF3RlgwRSxjQUFZLEVBQUUxRSxtQkFBTyxDQUFDLDJFQUFELENBeEZWO0FBeUZYO0FBQ0EyRSxNQUFJLEVBQUUzRSxtQkFBTyxDQUFDLDJEQUFELENBMUZGO0FBMkZYNEUsYUFBVyxFQUFFNUUsbUJBQU8sQ0FBQyxpRkFBRCxDQTNGVDtBQTRGWDZFLE1BQUksRUFBRTdFLG1CQUFPLENBQUMsMkRBQUQsQ0E1RkY7QUE2Rlg4RSxVQUFRLEVBQUU5RSxtQkFBTyxDQUFDLG1FQUFELENBN0ZOO0FBOEZYK0UsY0FBWSxFQUFFL0UsbUJBQU8sQ0FBQyw2RkFBRCxDQTlGVjtBQStGWGdGLE1BQUksRUFBRWhGLG1CQUFPLENBQUMsMkVBQUQsQ0EvRkY7QUFpR1hpRixjQUFZLEVBQUVqRixtQkFBTyxDQUFDLHVFQUFELENBakdWO0FBa0dYa0YsYUFBVyxFQUFFbEYsbUJBQU8sQ0FBQyx5RUFBRCxDQWxHVDtBQW1HWG1GLFlBQVUsRUFBRW5GLG1CQUFPLENBQUMsdUVBQUQsQ0FuR1I7QUFvR1hvRixjQUFZLEVBQUVwRixtQkFBTyxDQUFDLCtEQUFELENBcEdWO0FBcUdYcUYsZ0JBQWMsRUFBRXJGLG1CQUFPLENBQUMsNkVBQUQsQ0FyR1o7QUFzR1hzRixVQUFRLEVBQUV0RixtQkFBTyxDQUFDLG1FQUFELENBdEdOO0FBdUdYdUYsa0JBQWdCLEVBQUV2RixtQkFBTyxDQUFDLG1GQUFELENBdkdkO0FBd0dYd0YsWUFBVSxFQUFFeEYsbUJBQU8sQ0FBQyx1RUFBRCxDQXhHUjtBQXlHWHlGLGFBQVcsRUFBRXpGLG1CQUFPLENBQUMseUVBQUQsQ0F6R1Q7QUEwR1gwRixhQUFXLEVBQUUxRixtQkFBTyxDQUFDLHlFQUFELENBMUdUO0FBMkdYMkYsTUFBSSxFQUFFM0YsbUJBQU8sQ0FBQywyREFBRCxDQTNHRjtBQTRHWDRGLGNBQVksRUFBRTVGLG1CQUFPLENBQUMsMkVBQUQsQ0E1R1Y7QUE4R1g2RixnQkFBYyxFQUFFN0YsbUJBQU8sQ0FBQywrRUFBRCxDQTlHWjtBQWdIWDtBQUNBOEYsTUFBSSxFQUFFOUYsbUJBQU8sQ0FBQywyREFBRCxDQWpIRjtBQWtIWCtGLFFBQU0sRUFBRS9GLG1CQUFPLENBQUMsMkRBQUQsQ0FsSEo7QUFtSFhnRyxXQUFTLEVBQUVoRyxtQkFBTyxDQUFDLHVFQUFELENBbkhQO0FBb0hYaUcsa0JBQWdCLEVBQUVqRyxtQkFBTyxDQUFDLG1GQUFELENBcEhkO0FBcUhYa0csYUFBVyxFQUFFbEcsbUJBQU8sQ0FBQyx5RUFBRCxDQXJIVDtBQXNIWG1HLFVBQVEsRUFBRW5HLG1CQUFPLENBQUMsbUVBQUQsQ0F0SE47QUF1SFhvRyxXQUFTLEVBQUVwRyxtQkFBTyxDQUFDLHFFQUFELENBdkhQO0FBd0hYcUcsVUFBUSxFQUFFckcsbUJBQU8sQ0FBQyxtRUFBRCxDQXhITjtBQXlIWHNHLFlBQVUsRUFBRXRHLG1CQUFPLENBQUMsdUVBQUQsQ0F6SFI7QUEwSFh1RyxlQUFhLEVBQUV2RyxtQkFBTyxDQUFDLDZFQUFELENBMUhYO0FBNEhYO0FBQ0F3RyxhQUFXLEVBQUV4RyxtQkFBTyxDQUFDLGlHQUFELENBN0hUO0FBOEhYeUcsY0FBWSxFQUFFekcsbUJBQU8sQ0FBQyxtR0FBRCxDQTlIVjtBQStIWDBHLGlCQUFlLEVBQUUxRyxtQkFBTyxDQUFDLHlHQUFELENBL0hiO0FBZ0lYMkcsZUFBYSxFQUFFM0csbUJBQU8sQ0FBQyxxR0FBRCxDQWhJWDtBQWlJWDRHLFFBQU0sRUFBRTVHLG1CQUFPLENBQUMsdUZBQUQsQ0FqSUo7QUFrSVg2RyxRQUFNLEVBQUU3RyxtQkFBTyxDQUFDLHVGQUFELENBbElKO0FBbUlYOEcsV0FBUyxFQUFFOUcsbUJBQU8sQ0FBQywrRkFBRCxDQW5JUDtBQW9JWCtHLGdCQUFjLEVBQUUvRyxtQkFBTyxDQUFDLHVHQUFELENBcElaO0FBc0lYO0FBQ0FnSCxXQUFTLEVBQUVoSCxtQkFBTyxDQUFDLHlGQUFELENBdklQO0FBd0lYaUgsY0FBWSxFQUFFakgsbUJBQU8sQ0FBQywrRkFBRCxDQXhJVjtBQXlJWGtILGNBQVksRUFBRWxILG1CQUFPLENBQUMsK0ZBQUQsQ0F6SVY7QUEwSVhtSCxjQUFZLEVBQUVuSCxtQkFBTyxDQUFDLCtGQUFELENBMUlWO0FBNElYO0FBQ0FvSCxpQkFBZSxFQUFFcEgsbUJBQU8sQ0FBQywyRkFBRCxDQTdJYjtBQThJWHFILGFBQVcsRUFBRXJILG1CQUFPLENBQUMsaUdBQUQsQ0E5SVQ7QUFnSlhzSCxPQUFLLEVBQUV0SCxtQkFBTyxDQUFDLDZEQUFELENBaEpIO0FBa0pYdUgsV0FBUyxFQUFFdkgsbUJBQU8sQ0FBQyxtRUFBRCxDQWxKUDtBQW9KWHdILE1BQUksRUFBRXhILG1CQUFPLENBQUMsMkRBQUQsQ0FwSkY7QUFzSlh5SCxtQkFBaUIsRUFBRXpILG1CQUFPLENBQUMsbUdBQUQsQ0F0SmY7QUF1SlgwSCxtQkFBaUIsRUFBRTFILG1CQUFPLENBQUMsdUdBQUQsQ0F2SmY7QUF3SlgySCxtQkFBaUIsRUFBRTNILG1CQUFPLENBQUMsdUdBQUQsQ0F4SmY7QUF5Slg0SCxtQkFBaUIsRUFBRTVILG1CQUFPLENBQUMscUdBQUQsQ0F6SmY7QUEySlg2SCxrQkFBZ0IsRUFBRTdILG1CQUFPLENBQUMscUZBQUQsQ0EzSmQ7QUE0Slg4SCxvQkFBa0IsRUFBRTlILG1CQUFPLENBQUMseUZBQUQsQ0E1SmhCO0FBOEpYK0gsd0JBQXNCLEVBQUUvSCxtQkFBTyxDQUFDLGlHQUFELENBOUpwQjtBQStKWGdJLGVBQWEsRUFBRWhJLG1CQUFPLENBQUMsK0VBQUQsQ0EvSlg7QUFnS1hpSSxtQkFBaUIsRUFBRWpJLG1CQUFPLENBQUMsdUZBQUQsQ0FoS2Y7QUFpS1hrSSxrQkFBZ0IsRUFBRWxJLG1CQUFPLENBQUMscUZBQUQsQ0FqS2Q7QUFrS1htSSxtQkFBaUIsRUFBRW5JLG1CQUFPLENBQUMsdUZBQUQsQ0FsS2Y7QUFtS1hvSSxvQkFBa0IsRUFBRXBJLG1CQUFPLENBQUMseUZBQUQsQ0FuS2hCO0FBb0tYcUksaUJBQWUsRUFBRXJJLG1CQUFPLENBQUMsK0VBQUQsQ0FwS2I7QUFxS1hzSSxnQkFBYyxFQUFFdEksbUJBQU8sQ0FBQyxpRkFBRCxDQXJLWjtBQXNLWHVJLGlCQUFlLEVBQUV2SSxtQkFBTyxDQUFDLG1GQUFELENBdEtiO0FBd0tYO0FBRUF3SSxTQUFPLEVBQUV4SSxtQkFBTyxDQUFDLHlGQUFELENBMUtMO0FBMktYeUksU0FBTyxFQUFFekksbUJBQU8sQ0FBQyx5RkFBRCxDQTNLTDtBQTRLWDBJLFNBQU8sRUFBRTFJLG1CQUFPLENBQUMseUZBQUQsQ0E1S0w7QUE2S1gySSxTQUFPLEVBQUUzSSxtQkFBTyxDQUFDLHlGQUFELENBN0tMO0FBK0tYNEksU0FBTyxFQUFFNUksbUJBQU8sQ0FBQyxtRUFBRCxDQS9LTDtBQWlMWDZJLGVBQWEsRUFBRTdJLG1CQUFPLENBQUMsNkVBQUQsQ0FqTFg7QUFtTFg4SSxnQkFBYyxFQUFFOUksbUJBQU8sQ0FBQywrRUFBRCxDQW5MWjtBQXFMWCtJLFlBQVUsRUFBRS9JLG1CQUFPLENBQUMseUVBQUQsQ0FyTFI7QUFzTFhnSixhQUFXLEVBQUVoSixtQkFBTyxDQUFDLHlFQUFELENBdExUO0FBd0xYaUosV0FBUyxFQUFFakosbUJBQU8sQ0FBQyw2RkFBRCxDQXhMUDtBQXlMWGtKLFdBQVMsRUFBRWxKLG1CQUFPLENBQUMsNkZBQUQsQ0F6TFA7QUEwTFhtSixhQUFXLEVBQUVuSixtQkFBTyxDQUFDLGlHQUFELENBMUxUO0FBMkxYb0osZUFBYSxFQUFFcEosbUJBQU8sQ0FBQyx1R0FBRCxDQTNMWDtBQTZMWHFKLFlBQVUsRUFBRXJKLG1CQUFPLENBQUMsdUVBQUQsQ0E3TFI7QUErTFhzSixjQUFZLEVBQUV0SixtQkFBTyxDQUFDLDJFQUFEO0FBL0xWLENBQWY7QUFvTWVGLHFFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdE1BOzs7QUFFQTtBQUVlLE1BQU15SixvQkFBTixTQUFtQ0MsNENBQUssQ0FBQ0MsU0FBekMsQ0FBbUQ7QUFDOURDLFFBQU0sR0FBRztBQUNMLFdBQU8sTUFBQyw2RUFBRDtBQUFNLFdBQUssRUFBRSxDQUFDQyxXQUFXLENBQUNDLFNBQWIsRUFBd0IsS0FBS0MsS0FBTCxDQUFXQyxLQUFYLElBQW9CLEVBQTVDLENBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUErRCxLQUFLRCxLQUFMLENBQVdsUCxJQUExRSxDQUFQO0FBQ0g7O0FBSDZEO0FBS2xFLE1BQU1nUCxXQUFXLEdBQUdJLG1GQUFVLENBQUNDLE1BQVgsQ0FBa0I7QUFDbENKLFdBQVMsRUFBRTtBQUNQSyxlQUFXLEVBQUUsRUFETjtBQUVQQyxhQUFTLEVBQUUsRUFGSjtBQUdQQyxTQUFLLEVBQUUsTUFIQTtBQUlQQyxZQUFRLEVBQUUsRUFKSDtBQUtQQyxhQUFTLEVBQUUsQ0FMSjtBQU1QQyxjQUFVLEVBQUUsRUFOTDtBQU9QQyxjQUFVLEVBQUVqTCw4REFBUSxDQUFDTDtBQVBkO0FBRHVCLENBQWxCLENBQXBCLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1RBOzs7Ozs7OztDQUdBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7Q0FHQTs7QUFFQSxNQUFNO0FBQUN1TCxPQUFEO0FBQVFDO0FBQVIsSUFBa0JDLG1GQUFVLENBQUNDLEdBQVgsQ0FBZSxRQUFmLENBQXhCO0FBQ0EsTUFBTUMsU0FBUyxHQUFJSixLQUFLLEdBQUcsRUFBM0I7QUFFZSxNQUFNSyxxQkFBTixTQUFvQ3JCLDRDQUFLLENBQUNzQixhQUExQyxDQUF3RDtBQWtCbkVDLGFBQVcsQ0FBQ2xCLEtBQUQsRUFBUTtBQUNmLFVBQU1BLEtBQU47O0FBRGUsbUNBakJYO0FBQ0ptQixrQkFBWSxFQUFFLEtBQUtuQixLQUFMLENBQVdtQixZQURyQjtBQUVKQyx5QkFBbUIsRUFBRSxLQUFLcEIsS0FBTCxDQUFXbUIsWUFBWCxDQUF3QkU7QUFGekMsS0FpQlc7O0FBQUEsdUNBYlAsTUFBTTtBQUNkLFVBQUksS0FBS3JCLEtBQUwsQ0FBV3NCLFNBQWYsRUFBMEI7QUFDdEIsYUFBS3RCLEtBQUwsQ0FBV3NCLFNBQVgsQ0FDSSxZQURKLEVBRUk7QUFDSTtBQUNBQyxnQkFBTSxFQUFFLEtBQUtDLEtBQUwsQ0FBV0wsWUFBWCxDQUF3Qk0sRUFGcEM7QUFHSUMsaUJBQU8sRUFBRSxLQUFLRixLQUFMLENBQVdMLFlBQVgsQ0FBd0JPO0FBSHJDLFNBRko7QUFRSDtBQUNKLEtBRWtCO0FBRWxCOztBQUVEQywyQkFBeUIsQ0FBQ0MsU0FBRCxFQUFZO0FBQ2pDLFNBQUtDLFFBQUwsQ0FBYztBQUFDVixrQkFBWSxFQUFFUyxTQUFTLENBQUNUO0FBQXpCLEtBQWQ7QUFDSDs7QUFFRFcsd0JBQXNCLENBQUNDLGNBQUQsRUFBaUI7QUFDbkMsV0FDSSxNQUFDLDZFQUFEO0FBQU0sV0FBSyxFQUFFO0FBQ1RDLGdCQUFRLEVBQUUsVUFERDtBQUVUQyxXQUFHLEVBQUUsQ0FGSTtBQUdUQyxZQUFJLEVBQUUsQ0FIRztBQUlUdkIsYUFBSyxFQUFFLE1BSkU7QUFLVEMsY0FBTSxFQUFFLEdBTEM7QUFNVDtBQUNBdUIsa0JBQVUsRUFBRSxRQVBIO0FBUVRDLG9CQUFZLEVBQUUsUUFSTDtBQVNUQyxzQkFBYyxFQUFFLFFBVFA7QUFVVEMsY0FBTSxFQUFFO0FBVkMsT0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BYUksTUFBQyw2RUFBRDtBQUFNLFdBQUssRUFBRUMsTUFBTSxDQUFDQyxrQkFBcEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUNJLE1BQUMsNkVBQUQ7QUFBTSxXQUFLLEVBQUVELE1BQU0sQ0FBQ0UseUJBQXBCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FBZ0QsZ0JBQWhELENBREosRUFFSSxNQUFDLDZFQUFEO0FBQU0sV0FBSyxFQUFFRixNQUFNLENBQUNFLHlCQUFwQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQWdEVixjQUFjLEdBQzFELEtBQUtBLGNBQWMsQ0FBQ1csVUFBcEIsR0FBaUMsS0FBakMsR0FBeUNYLGNBQWMsQ0FBQ1ksUUFERSxHQUV4RCxtQkFGTixDQUZKLENBYkosQ0FESjtBQXNCSDs7QUFFREMsZ0JBQWMsQ0FBQ0MsV0FBRCxFQUFjO0FBQ3hCLFFBQUlDLEtBQUssR0FBRyxFQUFaO0FBQ0FELGVBQVcsR0FBR0EsV0FBVyxJQUFJLENBQWYsSUFBb0IsQ0FBQ0EsV0FBckIsR0FBbUMsQ0FBbkMsR0FBdUNBLFdBQXJEOztBQUNBLFNBQUssSUFBSUUsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBRyxDQUFwQixFQUF1QkEsQ0FBQyxFQUF4QixFQUE0QjtBQUN4QkQsV0FBSyxDQUFDRSxJQUFOLEVBQ0k7QUFDQSxZQUFDLDZFQUFEO0FBQU0sV0FBRyxFQUFFRCxDQUFYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FBZSxHQUFmLENBRko7QUFJSDs7QUFFRCxXQUNJLE1BQUMsNkVBQUQ7QUFBTSxXQUFLLEVBQUU7QUFBQ0UscUJBQWEsRUFBRSxLQUFoQjtBQUF1QnpDLGlCQUFTLEVBQUU7QUFBbEMsT0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQ0tzQyxLQUFLLENBQUNJLEdBQU4sQ0FBV0MsSUFBRCxJQUFVO0FBQ2pCLGFBQU9BLElBQVA7QUFDSCxLQUZBLENBREwsRUFJSSxNQUFDLDZFQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FBTyxHQUFQLENBSkosQ0FESjtBQU9IOztBQUVEQyxhQUFXLENBQUMxQixPQUFELEVBQVU7QUFDakIsV0FDSSxNQUFDLDZFQUFEO0FBQ0ksV0FBSyxFQUFFO0FBQUN1QixxQkFBYSxFQUFFLEtBQWhCO0FBQXVCSSxZQUFJLEVBQUM7QUFBNUIsT0FEWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BSUksTUFBQyw2RUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQU8sR0FBUCxDQUpKLEVBS0ksTUFBQyw2RUFBRDtBQUNJLG1CQUFhLEVBQUUsQ0FEbkI7QUFFSSxXQUFLLEVBQUVkLE1BQU0sQ0FBQ2IsT0FGbEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUlLNEIsVUFBVSxDQUFDNUIsT0FBRCxDQUFWLENBQW9CNkIsT0FBcEIsQ0FBNEIsQ0FBNUIsRUFBK0JDLFFBQS9CLEtBQTRDLElBSmpELENBTEosQ0FESjtBQWNIOztBQUVEQyxvQkFBa0IsQ0FBQ0MsZ0JBQUQsRUFBbUI7QUFDakMsV0FDSSxNQUFDLDZFQUFEO0FBQ0ksV0FBSyxFQUFFO0FBQ0hULHFCQUFhLEVBQUUsS0FEWjtBQUVISSxZQUFJLEVBQUMsQ0FGRjtBQUdIbEIsa0JBQVUsRUFBQyxVQUhSO0FBSUhDLG9CQUFZLEVBQUMsVUFKVjtBQUtIQyxzQkFBYyxFQUFDO0FBTFosT0FEWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BVUksTUFBQyw2RUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQU8sR0FBUCxDQVZKLEVBV0ksTUFBQyw2RUFBRDtBQUNJLG1CQUFhLEVBQUUsQ0FEbkI7QUFFSSxXQUFLLEVBQUU7QUFDSDlCLGdCQUFRLEVBQUUsRUFEUDtBQUVIRyxrQkFBVSxFQUFDakwsK0RBQVEsQ0FBQ0o7QUFGakIsT0FGWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BT0ssb0JBQW9Cc08sK0RBQXBCLEdBQXFDRCxnQkFQMUMsQ0FYSixDQURKO0FBc0JIOztBQUVERSxtQkFBaUIsQ0FBQ0MsV0FBRCxFQUFjO0FBRTNCLFdBQ0ksTUFBQyw2RUFBRDtBQUNJLFdBQUssRUFBRTtBQUNIWixxQkFBYSxFQUFFLEtBRFo7QUFFSEksWUFBSSxFQUFDLENBRkY7QUFHSGxCLGtCQUFVLEVBQUMsVUFIUjtBQUlIQyxvQkFBWSxFQUFDLFVBSlY7QUFLSEMsc0JBQWMsRUFBQztBQUxaLE9BRFg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQVVJLE1BQUMsNkVBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUFPLEdBQVAsQ0FWSixFQVdJLE1BQUMsNkVBQUQ7QUFDSSxtQkFBYSxFQUFFLENBRG5CO0FBRUksV0FBSyxFQUFFRSxNQUFNLENBQUN1QixhQUZsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BSUsscUJBQXFCSCwrREFBckIsR0FBc0NFLFdBSjNDLENBWEosQ0FESjtBQW1CSDs7QUFFREUsa0JBQWdCLENBQUNDLEdBQUQsRUFBTUMsR0FBTixFQUFXO0FBQ3ZCLFdBQ0ksTUFBQyw2RUFBRDtBQUNJLFdBQUssRUFBRTtBQUFDaEIscUJBQWEsRUFBRSxLQUFoQjtBQUF1QkksWUFBSSxFQUFDO0FBQTVCLE9BRFg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUlJLE1BQUMsNkVBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUFPLEdBQVAsQ0FKSixFQUtJLE1BQUMsNkVBQUQ7QUFDSSxtQkFBYSxFQUFFLENBRG5CO0FBRUksV0FBSyxFQUFFZCxNQUFNLENBQUMyQixZQUZsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BSUtGLEdBQUcsR0FBRyxLQUFOLEdBQWNDLEdBQWQsR0FBb0IsTUFKekIsQ0FMSixDQURKO0FBY0g7O0FBRURFLGVBQWEsR0FBRztBQUVaLFFBQUksS0FBSzNDLEtBQUwsQ0FBV0wsWUFBWCxDQUF3QmlELGVBQTVCLEVBQTZDO0FBQ3pDLGFBQ0ksTUFBQyw2RUFBRDtBQUFNLGFBQUssRUFBRTdCLE1BQU0sQ0FBQzhCLHVCQUFwQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0ksTUFBQyw2RUFBRDtBQUFNLGFBQUssRUFBRTlCLE1BQU0sQ0FBQytCLHVCQUFwQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0ksTUFBQyw2RUFBRDtBQUFNLGFBQUssRUFBRS9CLE1BQU0sQ0FBQ0UseUJBQXBCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FBZ0QsYUFBaEQsQ0FESixFQUVJLE1BQUMsNkVBQUQ7QUFBTSxhQUFLLEVBQUVGLE1BQU0sQ0FBQ0UseUJBQXBCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FBZ0QsUUFBaEQsQ0FGSixDQURKLENBREo7QUFRSDs7QUFFRCxRQUFJLENBQUMsS0FBS2pCLEtBQUwsQ0FBV0osbUJBQWhCLEVBQXFDO0FBQ2pDLGFBQU8sS0FBS1Usc0JBQUwsQ0FBNEIsSUFBNUIsQ0FBUDtBQUNIOztBQUVELFFBQUksQ0FBQyxLQUFLTixLQUFMLENBQVdKLG1CQUFYLENBQStCbUQsa0JBQXBDLEVBQXdEO0FBQ3BELGFBQU8sS0FBS3pDLHNCQUFMLENBQTRCLEtBQUtOLEtBQUwsQ0FBV0osbUJBQXZDLENBQVA7QUFDSDtBQUNKOztBQUVEb0QsS0FBRyxDQUFDQyxPQUFELEVBQVM7QUFDUixXQUNFLE1BQUMsNkVBQUQ7QUFBTSxXQUFLLEVBQUU7QUFBQ0MseUJBQWlCLEVBQUVELE9BQU8sR0FBRyxDQUFILEdBQU8sQ0FBbEM7QUFBb0NqRSxpQkFBUyxFQUFDLENBQUMsQ0FBL0M7QUFBaURtRSxrQkFBVSxFQUFFLE1BQTdEO0FBQW9FckUsYUFBSyxFQUFDcE8seURBQVMsQ0FBQ2dCO0FBQXBGLE9BQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUFpSCxHQUFqSCxDQURGO0FBR0g7O0FBRUQyTSxRQUFNLEdBQUc7QUFDTCxXQUNJLE1BQUMseUZBQUQ7QUFDSSxhQUFPLEVBQUUsS0FBS3lCLFNBRGxCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FHSSxNQUFDLDZFQUFEO0FBQU0sV0FBSyxFQUFFLENBQUNpQixNQUFNLENBQUNxQyxTQUFSLEVBQW1CLEtBQUs1RSxLQUFMLENBQVc2RSxVQUFYLEdBQXdCO0FBQUNyRSxpQkFBUyxFQUFDLENBQVg7QUFBYXNFLG1CQUFXLEVBQUUsQ0FBMUI7QUFBNEJyRSxrQkFBVSxFQUFFLENBQXhDO0FBQTBDRSxhQUFLLEVBQUNJO0FBQWhELE9BQXhCLEdBQXFGO0FBQUMrRCxtQkFBVyxFQUFFO0FBQWQsT0FBeEcsRUFDYixLQUFLOUUsS0FBTCxDQUFXQyxLQUFYLElBQW9CLEVBRFAsQ0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BR0ksTUFBQyw2RUFBRDtBQUFNLFdBQUssRUFBRXNDLE1BQU0sQ0FBQ3dDLG1CQUFwQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQ0ksTUFBQyw4RUFBRDtBQUNJLFlBQU0sRUFBRSxLQUFLdkQsS0FBTCxDQUFXTCxZQUFYLENBQXdCNkQsS0FBeEIsR0FBZ0M7QUFBQ0MsV0FBRyxFQUFFQyx5REFBUSxHQUFHLEtBQUsxRCxLQUFMLENBQVdMLFlBQVgsQ0FBd0I2RDtBQUF6QyxPQUFoQyxHQUFrRixJQUQ5RjtBQUVJLFdBQUssRUFBRSxDQUFDekMsTUFBTSxDQUFDNEM7QUFBa0I7QUFBMUIsT0FGWDtBQUdJLGdCQUFVLEVBQUUsT0FIaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxNQURKLENBSEosRUFZSSxNQUFDLDZFQUFEO0FBQ0ksV0FBSyxFQUFFNUMsTUFBTSxDQUFDNkMsaUJBRGxCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FJSSxNQUFDLDhFQUFEO0FBQ0ksWUFBTSxFQUFFLEtBQUs1RCxLQUFMLENBQVdMLFlBQVgsQ0FBd0J6SyxJQUF4QixHQUErQjtBQUFDdU8sV0FBRyxFQUFDQyx5REFBUSxHQUFHLEtBQUsxRCxLQUFMLENBQVdMLFlBQVgsQ0FBd0J6SztBQUF4QyxPQUEvQixHQUErRSxJQUQzRjtBQUVJLFdBQUssRUFBRSxDQUFDNkwsTUFBTSxDQUFDOEMsY0FBUixFQUNILEtBQUs3RCxLQUFMLENBQVdKLG1CQUFYLEdBQWtDLEtBQUtJLEtBQUwsQ0FBV0osbUJBQVgsQ0FBK0JtRCxrQkFBL0IsR0FBb0Q7QUFBQ2UsZUFBTyxFQUFDO0FBQVQsT0FBcEQsR0FBa0U7QUFBQ0EsZUFBTyxFQUFDO0FBQVQsT0FBcEcsR0FBbUg7QUFBQ0EsZUFBTyxFQUFDO0FBQVQsT0FEaEgsQ0FGWDtBQUtJLGdCQUFVLEVBQUUsU0FMaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxNQUpKLENBWkosRUE4Q0ksTUFBQyw2RUFBRDtBQUFNLFdBQUssRUFBRTtBQUNUdEQsZ0JBQVEsRUFBQyxVQURBO0FBRVRDLFdBQUcsRUFBQyxDQUFDLENBRkk7QUFHVEMsWUFBSSxFQUFDLENBQUM7QUFIRyxPQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FLSSxNQUFDLHdGQUFEO0FBQWlCLFlBQU0sRUFBRWpNLCtDQUFNLENBQUMrSSxhQUFoQztBQUNpQixnQkFBVSxFQUFFLFNBRDdCO0FBRWlCLFdBQUssRUFBRTtBQUNIcUQsc0JBQWMsRUFBRSxRQURiO0FBRUgxQixhQUFLLEVBQUMsRUFGSDtBQUdIQyxjQUFNLEVBQUMsRUFISixDQUlIO0FBQ0E7O0FBTEcsT0FGeEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQVVRLE1BQUMsNkVBQUQ7QUFDSSxXQUFLLEVBQUUsQ0FBQzJCLE1BQU0sQ0FBQ2dELGtCQUFSLEVBQTJCaEQsTUFBTSxDQUFDaUQsZUFBbEMsRUFBa0QsS0FBS3hGLEtBQUwsQ0FBV3lGLG1CQUFYLElBQWtDLEVBQXBGLENBRFg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUVFLFVBRkYsQ0FWUixDQUxKLENBOUNKLEVBcUVJLE1BQUMsNkVBQUQ7QUFBTSxXQUFLLEVBQUU7QUFDVHpELGdCQUFRLEVBQUMsVUFEQTtBQUVUQyxXQUFHLEVBQUMsQ0FGSztBQUdUeUQsYUFBSyxFQUFDO0FBSEcsT0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BS0ksTUFBQyx3RkFBRDtBQUNBLFlBQU0sRUFBRXpQLCtDQUFNLENBQUNnSixjQURmO0FBRUEsZ0JBQVUsRUFBRSxTQUZaO0FBR0EsV0FBSyxFQUFFO0FBQ0hvRCxzQkFBYyxFQUFFLFFBRGI7QUFFSDFCLGFBQUssRUFBQyxFQUZIO0FBR0hDLGNBQU0sRUFBQyxFQUhKO0FBSUhxQixXQUFHLEVBQUMsQ0FBQyxDQUpGO0FBS0h5RCxhQUFLLEVBQUMsQ0FBQztBQUxKLE9BSFA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQVVJLE1BQUMsNkVBQUQ7QUFDSSxtQkFBYSxFQUFFLENBRG5CO0FBRUksV0FBSyxFQUFFO0FBQ0gxRCxnQkFBUSxFQUFDLFVBRE47QUFFSEMsV0FBRyxFQUFDLENBQUMsQ0FGRjtBQUdIeUQsYUFBSyxFQUFDLENBQUMsQ0FISjtBQUlIbkYsZ0JBQVEsRUFBQyxFQUpOO0FBS0hELGFBQUssRUFBQyxNQUxIO0FBTUhxRixpQkFBUyxFQUFDLFFBTlA7QUFPSEMsaUJBQVMsRUFBQyxDQUFDO0FBQUNDLGdCQUFNLEVBQUM7QUFBUixTQUFELENBUFA7QUFRSG5GLGtCQUFVLEVBQUNqTCwrREFBUSxDQUFDTDtBQVJqQixPQUZYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FZRSxPQVpGLENBVkosQ0FMSixDQXJFSixFQW9HSSxNQUFDLDZFQUFEO0FBQU0sV0FBSyxFQUFFbU4sTUFBTSxDQUFDdUQsb0JBQXBCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FFSSxNQUFDLDZFQUFEO0FBQ0ksV0FBSyxFQUFFO0FBQUM3QyxxQkFBYSxFQUFFLEtBQWhCO0FBQXVCekMsaUJBQVMsRUFBRSxDQUFsQztBQUFxQ3VGLG9CQUFZLEVBQUU7QUFBbkQsT0FEWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BSUksTUFBQyw2RUFBRDtBQUNJLFdBQUssRUFBRTtBQUFDMUMsWUFBSSxFQUFDLENBQU47QUFBUUoscUJBQWEsRUFBRSxLQUF2QjtBQUE4QnRDLGFBQUssRUFBRTtBQUFyQyxPQURYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FPSSxNQUFDLDZFQUFEO0FBQ0ksV0FBSyxFQUFFNEIsTUFBTSxDQUFDeUQsY0FEbEI7QUFFSSxtQkFBYSxFQUFFLENBRm5CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FJSyxLQUFLeEUsS0FBTCxDQUFXTCxZQUFYLENBQXdCbkcsSUFKN0IsQ0FQSixDQUpKLEVBb0JJLE1BQUMsNkVBQUQ7QUFDSSxXQUFLLEVBQUV1SCxNQUFNLENBQUMwRCxXQURsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BSUksTUFBQyw2RUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQU8sR0FBUCxDQUpKLEVBS0ksTUFBQyw2RUFBRDtBQUNJLG1CQUFhLEVBQUUsQ0FEbkI7QUFFSSxXQUFLLEVBQUUxRCxNQUFNLENBQUMyRCxZQUZsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BSUs1QyxVQUFVLENBQUMsS0FBSzlCLEtBQUwsQ0FBV0wsWUFBWCxDQUF3Qm5LLE1BQXpCLENBQVYsQ0FBMkN1TSxPQUEzQyxDQUFtRCxDQUFuRCxFQUFzREMsUUFBdEQsRUFKTCxDQUxKLENBcEJKLENBRkosRUFzQ0ksTUFBQyw2RUFBRDtBQUFNLFdBQUssRUFBRTtBQUFDUCxxQkFBYSxFQUFFLEtBQWhCO0FBQXNCa0QsZ0JBQVEsRUFBQyxRQUEvQjtBQUF5Q3hGLGFBQUssRUFBRTtBQUFoRCxPQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FFSSxNQUFDLDZFQUFEO0FBQU0sV0FBSyxFQUFFO0FBQUNzQyxxQkFBYSxFQUFDLEtBQWY7QUFBcUJtRCx1QkFBZSxFQUFDLENBQXJDO0FBQXVDcEUsZ0JBQVEsRUFBQyxVQUFoRDtBQUEyREMsV0FBRyxFQUFDLENBQS9EO0FBQWlFSSxzQkFBYyxFQUFDO0FBQWhGLE9BQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUNLLEtBQUtPLGNBQUwsQ0FBb0IsS0FBS3BCLEtBQUwsQ0FBV0wsWUFBWCxDQUF3QjBCLFdBQTVDLENBREwsQ0FGSixFQUtJLE1BQUMsNkVBQUQ7QUFDSSxtQkFBYSxFQUFFLENBRG5CO0FBRUksV0FBSyxFQUFFTixNQUFNLENBQUM4RCxlQUZsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BSUsseURBSkwsQ0FMSixDQXRDSixFQW1ESSxNQUFDLDZFQUFEO0FBQU0sV0FBSyxFQUFFO0FBQUNwRCxxQkFBYSxFQUFDLEtBQWY7QUFDVDtBQUNBOEMsb0JBQVksRUFBQztBQUZKLE9BQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUlJLE1BQUMsNkVBQUQ7QUFBTSxXQUFLLEVBQUU7QUFDVDFDLFlBQUksRUFBRSxDQURHO0FBRVRKLHFCQUFhLEVBQUUsS0FGTjtBQUdUYixvQkFBWSxFQUFFLFlBSEw7QUFJVEQsa0JBQVUsRUFBRSxZQUpIO0FBS1RFLHNCQUFjLEVBQUUsWUFMUDtBQU1UaUUsZ0JBQVEsRUFBQyxRQU5BLENBT1Q7O0FBUFMsT0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BVUksTUFBQyw2RUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQU8sR0FBUCxDQVZKLEVBV0ssS0FBS3RHLEtBQUwsQ0FBVzZFLFVBQVgsR0FDRyxNQUFDLDZFQUFEO0FBQU0sV0FBSyxFQUFFO0FBQ1Q1QixxQkFBYSxFQUFFLEtBRE47QUFFVGIsb0JBQVksRUFBRSxZQUZMO0FBR1RELGtCQUFVLEVBQUUsWUFISDtBQUlURSxzQkFBYyxFQUFFLFlBSlA7QUFLVGlFLGdCQUFRLEVBQUMsUUFMQTtBQU1UQyxtQkFBVyxFQUFDLEVBTkgsQ0FPVDs7QUFQUyxPQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FTSSxNQUFDLDZFQUFEO0FBQU0sbUJBQWEsRUFBRSxDQUFyQjtBQUF3QixXQUFLLEVBQUUsQ0FBQ2hFLE1BQU0sQ0FBQ2lFLHFCQUFSLENBQS9CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FBZ0UsV0FBaEUsQ0FUSixFQVVJLE1BQUMsNkVBQUQ7QUFBTSxtQkFBYSxFQUFFLENBQXJCO0FBQXdCLFdBQUssRUFBRSxDQUFDakUsTUFBTSxDQUFDaUUscUJBQVIsQ0FBL0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUFnRTdDLCtEQUFjLEdBQzlFTCxVQUFVLENBQUMsS0FBSzlCLEtBQUwsQ0FBV0wsWUFBWCxDQUF3QnVDLGdCQUF4QixJQUE0QyxDQUE3QyxDQUFWLENBQTBESCxPQUExRCxDQUFrRSxDQUFsRSxDQURBLENBVkosQ0FESCxHQWVHLE1BQUMsNkVBQUQ7QUFBTSxtQkFBYSxFQUFFLENBQXJCO0FBQXdCLFdBQUssRUFBRSxDQUFDaEIsTUFBTSxDQUFDaUUscUJBQVIsRUFBOEI7QUFBQ0QsbUJBQVcsRUFBQztBQUFiLE9BQTlCLENBQS9CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FDSyxjQUFhNUMsK0RBQWIsR0FBOEJMLFVBQVUsQ0FBQyxLQUFLOUIsS0FBTCxDQUFXTCxZQUFYLENBQXdCdUMsZ0JBQXhCLElBQTRDLENBQTdDLENBQVYsQ0FBMERILE9BQTFELENBQWtFLENBQWxFLENBRG5DLENBMUJSLENBSkosRUFvQ0ksTUFBQyw2RUFBRDtBQUFNLFdBQUssRUFBRTtBQUNURixZQUFJLEVBQUMsQ0FESTtBQUVUSixxQkFBYSxFQUFDLEtBRkw7QUFHVGIsb0JBQVksRUFBQyxRQUhKO0FBSVRELGtCQUFVLEVBQUMsUUFKRjtBQUtURSxzQkFBYyxFQUFDLFFBTE47QUFNVGlFLGdCQUFRLEVBQUMsUUFOQSxDQU9UOztBQVBTLE9BQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQVNJLE1BQUMsNkVBQUQ7QUFBTSxXQUFLLEVBQUUvRCxNQUFNLENBQUNrRSxnQkFBcEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUF1QyxXQUF2QyxDQVRKLENBcENKLEVBaURJLE1BQUMsNkVBQUQ7QUFBTSxXQUFLLEVBQUU7QUFDVHBELFlBQUksRUFBQyxDQURJO0FBRVRKLHFCQUFhLEVBQUMsS0FGTDtBQUdUYixvQkFBWSxFQUFDLFVBSEo7QUFJVEQsa0JBQVUsRUFBQyxVQUpGO0FBS1RFLHNCQUFjLEVBQUMsVUFMTjtBQU1UaUUsZ0JBQVEsRUFBQyxRQU5BLENBT1Q7O0FBUFMsT0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BU0ksTUFBQyw2RUFBRDtBQUNJLFdBQUssRUFBRS9ELE1BQU0sQ0FBQ21FLFlBRGxCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FDaUMsZUFBZS9DLCtEQUFmLEdBQWdDZ0QsTUFBTSxDQUFDQyxZQUR4RSxDQVRKLENBakRKLENBbkRKLEVBa0hJLE1BQUMsNkVBQUQ7QUFBTSxXQUFLLEVBQUU7QUFDVDNELHFCQUFhLEVBQUMsS0FETDtBQUVUNEQsbUJBQVcsRUFBRSxDQUZKO0FBR1RDLG9CQUFZLEVBQUUsQ0FITDtBQUlUQyxtQkFBVyxFQUFFLFFBSko7QUFLVHJXLG1CQUFXLEVBQUUsU0FMSjtBQU1Uc1csdUJBQWUsRUFBRSxTQU5SO0FBT1RaLHVCQUFlLEVBQUUsQ0FQUjtBQVFUMUIseUJBQWlCLEVBQUU7QUFSVixPQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FVSSxNQUFDLDZFQUFEO0FBQU0sV0FBSyxFQUFFLENBQUM7QUFBQ2hFLGtCQUFVLEVBQUNqTCwrREFBUSxDQUFDTixJQUFyQjtBQUEwQm1MLGFBQUssRUFBQ3BPLHlEQUFTLENBQUNNO0FBQTFDLE9BQUQsQ0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQXVFLGVBQXZFLENBVkosRUFXSSxNQUFDLDZFQUFEO0FBQU0sV0FBSyxFQUFFLENBQUM7QUFBQ2tPLGtCQUFVLEVBQUNqTCwrREFBUSxDQUFDSjtBQUFyQixPQUFELENBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUE2QyxpQ0FBN0MsQ0FYSixDQWxISixDQXBHSixDQUhKLENBREo7QUE2T0g7O0FBbGFrRTtBQXFhdkUsTUFBTWtOLE1BQU0sR0FBR3JDLG1GQUFVLENBQUNDLE1BQVgsQ0FBa0I7QUFFN0J5RSxXQUFTLEVBQUU7QUFDUG1CLGdCQUFZLEVBQUUsRUFEUDtBQUVQMUMsUUFBSSxFQUFFLENBRkM7QUFHUDJELG1CQUFlLEVBQUUsT0FIVjtBQUlQRixnQkFBWSxFQUFFLENBSlA7QUFLUFAsZUFBVyxFQUFFLENBTE47QUFNUFUsZ0JBQVksRUFBRSxDQU5QO0FBT1BDLGlCQUFhLEVBQUUsQ0FQUjtBQVNQO0FBQ0E7QUFDQUMsZUFBVyxFQUFFLE1BWE47QUFZUEMsZ0JBQVksRUFBRTtBQUNWekcsV0FBSyxFQUFFLENBREc7QUFFVkMsWUFBTSxFQUFFO0FBRkUsS0FaUDtBQWdCUHlHLGlCQUFhLEVBQUUsQ0FoQlI7QUFpQlBDLGdCQUFZLEVBQUUsQ0FqQlA7QUFtQlBDLGFBQVMsRUFBRTtBQW5CSixHQUZrQjtBQXdCN0J4QyxxQkFBbUIsRUFDbkI7QUFDSXBFLFNBQUssRUFBRSxNQURYO0FBRUlDLFVBQU0sRUFBRSxHQUZaO0FBR0lvRyxtQkFBZSxFQUFFLE1BSHJCO0FBSUlRLHVCQUFtQixFQUFDLENBSnhCO0FBS0lDLHdCQUFvQixFQUFDO0FBTHpCLEdBekI2QjtBQWlDN0J0QyxrQkFBZ0IsRUFBRTtBQUNkeEUsU0FBSyxFQUFFLE1BRE87QUFFZEMsVUFBTSxFQUFFLEdBRk07QUFHZDtBQUNBOEcsYUFBUyxFQUFFLFFBSkc7QUFLZDtBQUVBRix1QkFBbUIsRUFBQyxDQVBOO0FBUWRDLHdCQUFvQixFQUFDO0FBUlAsR0FqQ1c7QUEyQzdCckMsbUJBQWlCLEVBQUU7QUFDZnpFLFNBQUssRUFBRSxFQURRO0FBRWZDLFVBQU0sRUFBRSxFQUZPO0FBR2Y4RyxhQUFTLEVBQUUsUUFISTtBQUlmMUYsWUFBUSxFQUFFLFVBSks7QUFLZkUsUUFBSSxFQUFFLENBTFM7QUFNZkQsT0FBRyxFQUFFLEVBTlU7QUFPZmtFLFlBQVEsRUFBQyxRQVBNO0FBUWZXLGdCQUFZLEVBQUUsRUFSQztBQVNmcFcsZUFBVyxFQUFFLE1BVEU7QUFVZm1XLGVBQVcsRUFBRSxDQVZFO0FBV2ZHLG1CQUFlLEVBQUUsTUFYRjtBQVdTO0FBRXhCRyxlQUFXLEVBQUUsTUFiRTtBQWNmQyxnQkFBWSxFQUFFO0FBQ1Z6RyxXQUFLLEVBQUUsQ0FERztBQUVWQyxZQUFNLEVBQUU7QUFGRSxLQWRDO0FBa0JmeUcsaUJBQWEsRUFBRSxHQWxCQTtBQW1CZkMsZ0JBQVksRUFBRSxDQW5CQztBQXFCZkMsYUFBUyxFQUFFO0FBckJJLEdBM0NVO0FBa0U3QmxDLGdCQUFjLEVBQUU7QUFDWjFFLFNBQUssRUFBRSxNQURLO0FBRVpDLFVBQU0sRUFBRSxNQUZJO0FBR1o4RyxhQUFTLEVBQUU7QUFIQyxHQWxFYTtBQXVFN0JuQyxvQkFBa0IsRUFBRTtBQUNoQjtBQUNBO0FBQ0E7QUFDQWpGLFNBQUssRUFBRSxPQUpTO0FBS2hCO0FBQ0FDLFlBQVEsRUFBQyxFQU5PO0FBT2hCZ0csZUFBVyxFQUFDLEVBUEk7QUFRaEI7QUFDQTtBQUNBN0YsY0FBVSxFQUFDakwsK0RBQVEsQ0FBQ0wsT0FWSjtBQVdoQjtBQUNBdVMsVUFBTSxFQUFDLEVBWlM7QUFhaEJ0RixrQkFBYyxFQUFDO0FBYkMsR0F2RVM7QUF1RjdCdUYsaUJBQWUsRUFBQztBQUNaWixtQkFBZSxFQUFFOVUseURBQVMsQ0FBQ007QUFEZixHQXZGYTtBQTBGN0JnVCxpQkFBZSxFQUFFLENBQ2I7QUFEYSxHQTFGWTtBQThGN0JRLGdCQUFjLEVBQUU7QUFDWnRGLGNBQVUsRUFBRWpMLCtEQUFRLENBQUNOLElBRFQ7QUFFWm9MLFlBQVEsRUFBRTtBQUZFLEdBOUZhO0FBa0c3QjBGLGFBQVcsRUFBRTtBQUNUaEQsaUJBQWEsRUFBRSxLQUROO0FBRVQ7QUFDQTtBQUNBO0FBQ0E7QUFDQUksUUFBSSxFQUFDLENBTkk7QUFPVCtDLG1CQUFlLEVBQUMsQ0FQUDtBQVFUL0Qsa0JBQWMsRUFBQyxVQVJOO0FBU1RGLGNBQVUsRUFBQyxVQVRGO0FBVVRDLGdCQUFZLEVBQUM7QUFWSixHQWxHZ0I7QUE4RzdCMEQsc0JBQW9CLEVBQUU7QUFDbEJyRixjQUFVLEVBQUUsQ0FETTtBQUVsQnFFLGVBQVcsRUFBRSxDQUZLO0FBR2xCdEUsYUFBUyxFQUFFO0FBSE8sR0E5R087QUFtSDdCcUgsY0FBWSxFQUFFO0FBQ1ZsSCxTQUFLLEVBQUUsRUFERztBQUNDQyxVQUFNLEVBQUUsRUFEVDtBQUNhb0IsWUFBUSxFQUFFLFVBRHZCO0FBQ21DeEIsYUFBUyxFQUFFLENBRDlDO0FBQ2lEQyxjQUFVLEVBQUUsQ0FBQztBQUQ5RCxHQW5IZTtBQXNIN0JxSCxzQkFBb0IsRUFBRTtBQUNsQjdFLGlCQUFhLEVBQUUsS0FERztBQUVsQnpDLGFBQVMsRUFBRSxDQUZPO0FBR2xCdUYsZ0JBQVksRUFBRTtBQUhJLEdBdEhPO0FBMkg3QmdDLHNCQUFvQixFQUFFO0FBQ2xCOUUsaUJBQWEsRUFBRSxLQURHO0FBRWxCekMsYUFBUyxFQUFFLENBRk87QUFHbEJ1RixnQkFBWSxFQUFFLENBSEk7QUFJbEIzRCxnQkFBWSxFQUFFO0FBSkksR0EzSE87QUFrSTdCOEQsY0FBWSxFQUFFO0FBQ1Y7QUFDQTNGLFlBQVEsRUFBRSxFQUZBO0FBR1ZFLGNBQVUsRUFBRSxDQUhGO0FBSVZDLGNBQVUsRUFBQ2pMLCtEQUFRLENBQUNKLEtBSlYsQ0FLVjs7QUFMVSxHQWxJZTtBQXlJN0JxTSxTQUFPLEVBQUU7QUFDTG9ELGVBQVcsRUFBRSxFQURSO0FBRUx2RSxZQUFRLEVBQUUsRUFGTDtBQUdMRSxjQUFVLEVBQUUsQ0FIUDtBQUlMQyxjQUFVLEVBQUNqTCwrREFBUSxDQUFDSjtBQUpmLEdBeklvQjtBQStJN0IyUyxnQkFBYyxFQUFFO0FBQ1psRCxlQUFXLEVBQUUsRUFERDtBQUVadkUsWUFBUSxFQUFFLEVBRkU7QUFHWkUsY0FBVSxFQUFFLENBSEE7QUFJWkMsY0FBVSxFQUFDakwsK0RBQVEsQ0FBQ0osS0FKUjtBQUtac1EsYUFBUyxFQUFDLE9BTEU7QUFNWmtCLGVBQVcsRUFBQztBQU5BLEdBL0lhO0FBdUo3QjNDLGNBQVksRUFBRTtBQUNWWSxlQUFXLEVBQUUsRUFESDtBQUVWdkUsWUFBUSxFQUFFLEVBRkE7QUFHVkUsY0FBVSxFQUFFLENBSEY7QUFJVkMsY0FBVSxFQUFDakwsK0RBQVEsQ0FBQ0o7QUFKVixHQXZKZTtBQTZKN0J5TyxlQUFhLEVBQUU7QUFDWDtBQUNBdkQsWUFBUSxFQUFFLEVBRkM7QUFHWEUsY0FBVSxFQUFFLENBSEQ7QUFJWEMsY0FBVSxFQUFDakwsK0RBQVEsQ0FBQ0o7QUFKVCxHQTdKYztBQW1LN0I0UyxXQUFTLEVBQUU7QUFDUGpHLFlBQVEsRUFBRSxVQURIO0FBRVA7QUFDQUUsUUFBSSxFQUFFLENBSEM7QUFJUEQsT0FBRyxFQUFFLENBSkU7QUFLUEssVUFBTSxFQUFFLENBTEQ7QUFNUDtBQUNBO0FBQ0E7QUFDQTFCLFVBQU0sRUFBRSxFQVREO0FBVVBELFNBQUssRUFBRSxNQVZBO0FBV1B3QixjQUFVLEVBQUU7QUFYTCxHQW5La0I7QUFnTDdCK0YsV0FBUyxFQUFFO0FBQ1B2SCxTQUFLLEVBQUUsRUFEQTtBQUVQQyxVQUFNLEVBQUUsRUFGRDtBQUdQb0csbUJBQWUsRUFBRSxTQUhWO0FBSVBoRixZQUFRLEVBQUUsVUFKSDtBQUtQOEUsZ0JBQVksRUFBRSxHQUxQO0FBTVBELGVBQVcsRUFBRSxDQU5OO0FBT1BuVyxlQUFXLEVBQUUsU0FQTjtBQVFQNFIsVUFBTSxFQUFFLENBUkQ7QUFTUHFGLFVBQU0sRUFBRSxDQVREO0FBVVBqQyxTQUFLLEVBQUU7QUFWQSxHQWhMa0I7QUE0TDdCeUMsYUFBVyxFQUFFO0FBQ1R4SCxTQUFLLEVBQUUsRUFERTtBQUVUQyxVQUFNLEVBQUUsRUFGQztBQUdUb0csbUJBQWUsRUFBRSxLQUhSO0FBSVRoRixZQUFRLEVBQUUsVUFKRDtBQUtUOEUsZ0JBQVksRUFBRSxHQUxMO0FBTVRELGVBQVcsRUFBRSxDQU5KO0FBT1RuVyxlQUFXLEVBQUUsS0FQSjtBQVFUNFIsVUFBTSxFQUFFLENBUkM7QUFTVHFGLFVBQU0sRUFBRSxDQVRDO0FBVVRqQyxTQUFLLEVBQUU7QUFWRSxHQTVMZ0I7QUF5TTdCbEQsb0JBQWtCLEVBQUU7QUFDaEI3QixTQUFLLEVBQUUsR0FEUztBQUVoQnlCLGdCQUFZLEVBQUUsUUFGRTtBQUdoQkQsY0FBVSxFQUFFLFFBSEk7QUFJaEJpRyxrQkFBYyxFQUFFLENBSkE7QUFLaEJDLGtCQUFjLEVBQUUsT0FMQTtBQU1oQkMscUJBQWlCLEVBQUUsQ0FOSDtBQU9oQkMscUJBQWlCLEVBQUUsT0FQSDtBQVFoQkMsY0FBVSxFQUFFLENBUkk7QUFTaEJ0QixpQkFBYSxFQUFFO0FBVEMsR0F6TVM7QUFvTjdCNUMseUJBQXVCLEVBQUU7QUFDckIzRCxTQUFLLEVBQUUsR0FEYztBQUVyQnlCLGdCQUFZLEVBQUUsUUFGTztBQUdyQkQsY0FBVSxFQUFFLFFBSFM7QUFJckJpRyxrQkFBYyxFQUFFLENBSks7QUFLckJDLGtCQUFjLEVBQUUsT0FMSztBQU1yQkMscUJBQWlCLEVBQUUsQ0FORTtBQU9yQkMscUJBQWlCLEVBQUUsT0FQRTtBQVFyQkMsY0FBVSxFQUFFLENBUlM7QUFTckJ0QixpQkFBYSxFQUFFO0FBVE0sR0FwTkk7QUErTjdCekUsMkJBQXlCLEVBQUU7QUFDdkJuQyxTQUFLLEVBQUUsT0FEZ0I7QUFFdkJDLFlBQVEsRUFBRSxFQUZhO0FBR3ZCRyxjQUFVLEVBQUNqTCwrREFBUSxDQUFDSjtBQUhHLEdBL05FO0FBb083QmdQLHlCQUF1QixFQUFFO0FBQ3JCckMsWUFBUSxFQUFFLFVBRFc7QUFFckJDLE9BQUcsRUFBRSxDQUZnQjtBQUdyQkMsUUFBSSxFQUFFLENBSGU7QUFJckJ2QixTQUFLLEVBQUUsTUFKYztBQUtyQkMsVUFBTSxFQUFFLEdBTGE7QUFNckJvRyxtQkFBZSxFQUFFLGlCQU5JO0FBT3JCN0UsY0FBVSxFQUFFLFFBUFM7QUFRckJDLGdCQUFZLEVBQUUsUUFSTztBQVNyQkMsa0JBQWMsRUFBRSxRQVRLO0FBVXJCQyxVQUFNLEVBQUU7QUFWYSxHQXBPSTtBQWdQN0JrRSx1QkFBcUIsRUFBQztBQUNsQkYsWUFBUSxFQUFDLFFBRFM7QUFFbEI7QUFDQTtBQUNBNUYsY0FBVSxFQUFDakwsK0RBQVEsQ0FBQ0o7QUFKRixHQWhQTztBQXNQN0JnUixpQkFBZSxFQUFDO0FBQ1o5RixZQUFRLEVBQUUsRUFERTtBQUVaRyxjQUFVLEVBQUNqTCwrREFBUSxDQUFDSjtBQUZSLEdBdFBhO0FBMFA3Qm9SLGtCQUFnQixFQUFDO0FBQ2JkLGFBQVMsRUFBQyxRQURHO0FBRWI4QyxpQkFBYSxFQUFDLENBRkQ7QUFHYi9ILGNBQVUsRUFBQ2pMLCtEQUFRLENBQUNKO0FBSFAsR0ExUFk7QUErUDdCcVIsY0FBWSxFQUFDO0FBQ1Q1QixlQUFXLEVBQUUsQ0FESjtBQUVUcEUsY0FBVSxFQUFDakwsK0RBQVEsQ0FBQ0o7QUFGWDtBQS9QZ0IsQ0FBbEIsQ0FBZixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0YkE7Ozs7O0FBRUE7QUFFZSxNQUFNcVQsa0JBQU4sU0FBaUMvSSw0Q0FBSyxDQUFDc0IsYUFBdkMsQ0FBcUQ7QUFFaEVDLGFBQVcsQ0FBQ2xCLEtBQUQsRUFBTztBQUNkLFVBQU1BLEtBQU4sRUFEYyxDQUVkOztBQUZjLG1DQUlWO0FBQ0oySSxzQkFBZ0IsRUFBRSxLQUFLM0ksS0FBTCxDQUFXNEksV0FEekI7QUFFSkMsZ0JBQVUsRUFBRSxLQUFLN0ksS0FBTCxDQUFXOEksSUFGbkI7QUFHSkMsZUFBUyxFQUFDLEtBQUsvSSxLQUFMLENBQVcrSTtBQUhqQixLQUpVOztBQUFBLHNDQWtCUCxNQUFLLENBQ1o7QUFDQTtBQUNBO0FBQ0gsS0F0QmlCO0FBR2pCOztBQU9EcEgsMkJBQXlCLENBQUNDLFNBQUQsRUFBWTtBQUNqQyxTQUFLQyxRQUFMLENBQWM7QUFBQzhHLHNCQUFnQixFQUFFL0csU0FBUyxDQUFDZ0gsV0FBN0I7QUFBeUNHLGVBQVMsRUFBQ25ILFNBQVMsQ0FBQ21IO0FBQTdELEtBQWQ7QUFDSCxHQWQrRCxDQWdCaEU7QUFDQTtBQUNBOzs7QUFRQUMsY0FBWSxHQUFJO0FBQ1osV0FDSSxLQUFLeEgsS0FBTCxDQUFXdUgsU0FBWCxHQUNJLE1BQUMsNkVBQUQ7QUFBTSxXQUFLLEVBQUU5SSxLQUFLLENBQUNnSixNQUFuQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQ0EsTUFBQywwRkFBRDtBQUFtQixXQUFLLEVBQUMsT0FBekI7QUFBaUMsVUFBSSxFQUFFLEVBQXZDO0FBQTJDLFdBQUssRUFBRTtBQUFFQyxjQUFNLEVBQUU7QUFBVixPQUFsRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE1BREEsQ0FESixHQUlJLElBTFI7QUFPSDs7QUFHRHJKLFFBQU0sR0FBRztBQUNMLFFBQUcsQ0FBQyxLQUFLMkIsS0FBTCxDQUFXbUgsZ0JBQWYsRUFBaUMsT0FBTyxJQUFQO0FBRWpDLFdBQ0ksTUFBQyxpRkFBRDtBQUNJLGNBQVEsRUFBRSxLQUFLM0ksS0FBTCxDQUFXbUosUUFEekI7QUFFSSwyQkFBcUIsRUFBRSxJQUYzQjtBQUdJLG9DQUE4QixFQUFFLEtBSHBDO0FBSUksa0NBQTRCLEVBQUUsS0FKbEM7QUFLSSxnQkFBVSxFQUFFLEtBQUtuSixLQUFMLENBQVc2RSxVQUwzQixDQU9JO0FBQ0E7QUFDQTtBQVRKO0FBV0ksMkJBQXFCLEVBQUUsQ0FYM0IsQ0FhSTtBQWJKO0FBZUksV0FBSyxFQUFFLENBQUMsS0FBSzdFLEtBQUwsQ0FBVzZFLFVBQVgsR0FBd0I7QUFBQ3BFLGtCQUFVLEVBQUM7QUFBWixPQUF4QixHQUF5QztBQUFDMkksd0JBQWdCLEVBQUM7QUFBbEIsT0FBMUMsRUFBZ0UsS0FBS3BKLEtBQUwsQ0FBV0MsS0FBWCxJQUFvQixFQUFwRixDQWZYO0FBZ0JJLFVBQUksRUFBRSxLQUFLdUIsS0FBTCxDQUFXbUgsZ0JBaEJyQjtBQWlCSSxlQUFTLEVBQUUsS0FBS25ILEtBakJwQjtBQWtCSSxrQkFBWSxFQUFFLENBQUM2SCxJQUFELEVBQU9DLEtBQVAsS0FBaUJDLE1BQU0sQ0FBQ0QsS0FBRCxDQWxCekM7QUFtQkksZ0JBQVUsRUFBRSxDQUFDO0FBQUNELFlBQUQ7QUFBT0M7QUFBUCxPQUFELEtBQW1CO0FBQzNCLGVBQ0ksTUFBQyw4REFBRCxDQUNJO0FBREo7QUFFSSxvQkFBVSxFQUFFLEtBQUt0SixLQUFMLENBQVc2RSxVQUYzQjtBQUdJLHNCQUFZLEVBQUV3RSxJQUhsQixDQUlJO0FBQ0E7QUFMSjtBQU1JLGVBQUssRUFBRUMsS0FOWCxDQU9JO0FBUEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURKO0FBV0gsT0EvQkw7QUFpQ0kseUJBQW1CLEVBQUUsTUFBSSxLQUFLTixZQUFMLEVBakM3QixDQW1DSTtBQUNBO0FBQ0E7QUFyQ0o7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxNQURKO0FBMENIOztBQWxGK0Q7QUFvRnBFLE1BQU0vSSxLQUFLLEdBQUdDLG1GQUFVLENBQUNDLE1BQVgsQ0FBa0I7QUFDNUI4SSxRQUFNLEVBQUU7QUFDSk8sV0FBTyxFQUFFLEVBREw7QUFFSm5ILGtCQUFjLEVBQUUsUUFGWjtBQUdKRixjQUFVLEVBQUUsUUFIUjtBQUlKYyxpQkFBYSxFQUFFO0FBSlg7QUFEb0IsQ0FBbEIsQ0FBZCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Q0N2RkE7O0NBRUE7QUFDQTtBQUNBO0FBQ0E7O0NBRUE7O0FBRWUsTUFBTXdHLGFBQU4sU0FBNEI5Siw0Q0FBSyxDQUFDQyxTQUFsQyxDQUE0QztBQUN2RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQUMsUUFBTSxHQUFHO0FBQ0wsV0FDSSxNQUFDLDZFQUFEO0FBQU0sV0FBSyxFQUFFO0FBQUNzRyxnQkFBUSxFQUFDLFFBQVY7QUFBbUJ2RixjQUFNLEVBQUM7QUFBMUIsT0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BdUJJLE1BQUMsNkVBQUQ7QUFBTSxXQUFLLEVBQUU7QUFBQ29HLHVCQUFlLEVBQUU3Vyx1REFBUSxDQUFDUTtBQUEzQixPQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FDSyxLQUFLcVAsS0FBTCxDQUFXMEosUUFEaEIsQ0F2QkosQ0FESjtBQXlESDs7QUFyRXNELEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNWM0Q7Ozs7Ozs7O0FBV0E7QUFDQTtBQUNBO0NBR0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFZSxNQUFNQyxhQUFOLFNBQTRCaEssNENBQUssQ0FBQ0MsU0FBbEMsQ0FBNEM7QUFBQTtBQUFBOztBQUFBLG1DQUUvQztBQUNKK0ksc0JBQWdCLEVBQUNpQjtBQURiLEtBRitDOztBQUFBLCtDQXVCbkMsTUFBTTtBQUN0QixVQUFJQyxrQkFBa0IsR0FDbEIsS0FBS3JJLEtBQUwsQ0FBV21ILGdCQUFYLElBQStCaUIsU0FBL0IsSUFDQSxLQUFLcEksS0FBTCxDQUFXbUgsZ0JBQVgsSUFBK0IsSUFEL0IsSUFFQSxLQUFLbkgsS0FBTCxDQUFXbUgsZ0JBQVgsQ0FBNEJtQixNQUE1QixHQUFxQyxDQUZyQyxHQUdJLEtBQUtDLGtCQUFMLEVBSEosR0FLSSxLQUFLdkksS0FBTCxDQUFXbUgsZ0JBQVgsSUFBK0JpQixTQUEvQixJQUNKLEtBQUtwSSxLQUFMLENBQVdtSCxnQkFBWCxJQUErQixJQUQzQixJQUVKLEtBQUtuSCxLQUFMLENBQVdtSCxnQkFBWCxDQUE0Qm1CLE1BQTVCLElBQXNDLENBRmxDLEdBR0EsSUFIQSxHQUlDLEtBQUt0SSxLQUFMLENBQVd1SCxTQUFYLEdBQXVCLElBQXZCLEdBQ0csTUFBQyx5RkFBRDtBQUFrQixhQUFLLEVBQUU7QUFDckJyQixtQkFBUyxFQUFFLFFBRFU7QUFFckJWLHlCQUFlLEVBQUU5VSx5REFBUyxDQUFDTSxVQUZOO0FBR3JCZ1gsaUJBQU8sRUFBRSxFQUhZO0FBSXJCUSx3QkFBYyxFQUFFO0FBSkssU0FBekIsQ0FNQTtBQU5BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FTSSxNQUFDLDZFQUFEO0FBQU0sYUFBSyxFQUFFO0FBQUMxSixlQUFLLEVBQUVuUSx3REFBUSxDQUFDaUI7QUFBakIsU0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQVRKLENBWFo7QUEyQkEsYUFDSSxNQUFDLDZFQUFEO0FBQU0sYUFBSyxFQUFFO0FBQUNpUyxjQUFJLEVBQUUsQ0FBUDtBQUFVNEcsbUJBQVMsRUFBRTtBQUFyQixTQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FDSSxNQUFDLHlFQUFEO0FBQ0ksYUFBSyxFQUFFO0FBQUN4SixvQkFBVSxFQUFFLEVBQWI7QUFBaUJzRixzQkFBWSxFQUFFO0FBQS9CLFNBRFg7QUFFSSxZQUFJLEVBQUMsVUFGVDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFFBREosRUFLSzhELGtCQUxMLENBREo7QUFVSCxLQTdEc0Q7QUFBQTs7QUFNdkRFLG9CQUFrQixHQUFHO0FBQ2pCLFdBQ0ksTUFBQyx1RUFBRCxDQUNJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5KO0FBT0ksaUJBQVcsRUFBRSxLQUFLdkksS0FBTCxDQUFXbUgsZ0JBUDVCLENBUUk7QUFDQTtBQUNBO0FBQ0E7QUFYSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE1BREo7QUFlSDs7QUF5Q0R1QixTQUFPLEdBQUU7QUFDTCxVQUFNQyxLQUFLLEdBQUcsRUFBZDtBQUNBL2Isd0VBQU8sQ0FDSGdjLG1FQURHLEVBRUhELEtBRkcsRUFHSEUsSUFBSSxJQUFJO0FBQ0osVUFBSUEsSUFBSSxJQUFJVCxTQUFaLEVBQXVCO0FBRW5CLFlBQUlTLElBQUksQ0FBQzVhLE1BQUwsSUFBZTZhLGtFQUFuQixFQUFxQztBQUVqQyxjQUFJMUIsV0FBVyxHQUFHeUIsSUFBSSxDQUFDekIsV0FBdkIsQ0FGaUMsQ0FFRTs7QUFFbkNsYSxpQkFBTyxDQUFDQyxHQUFSLENBQVksb0JBQVosRUFBa0NpYSxXQUFsQztBQUVBLGVBQUsvRyxRQUFMLENBQWM7QUFBQzhHLDRCQUFnQixFQUFFaUI7QUFBbkIsV0FBZCxFQUE2QyxNQUFNO0FBQy9DLGlCQUFLL0gsUUFBTCxDQUFjO0FBQUM4Ryw4QkFBZ0IsRUFBRUM7QUFBbkIsYUFBZDtBQUNILFdBRkQ7QUFJQSxlQUFLMkIsU0FBTCxHQUFpQkYsSUFBSSxDQUFDRSxTQUF0QjtBQUNBLGVBQUtDLFlBQUwsR0FBb0IsQ0FBcEI7QUFDQTliLGlCQUFPLENBQUNDLEdBQVIsQ0FBWSxpQkFBWixFQUErQixLQUFLNmIsWUFBcEM7QUFFSDtBQUNKOztBQUVELFdBQUszSSxRQUFMLENBQWM7QUFBQ2tILGlCQUFTLEVBQUU7QUFBWixPQUFkOztBQUVBLFVBQUksS0FBS3ZILEtBQUwsQ0FBV2lKLGdCQUFmLEVBQWlDO0FBQzdCQyxrQkFBVSxDQUFDLE1BQU07QUFDYixlQUFLN0ksUUFBTCxDQUFjO0FBQUM0SSw0QkFBZ0IsRUFBRTtBQUFuQixXQUFkO0FBQ0gsU0FGUyxFQUVQLElBRk8sQ0FBVjtBQUdIO0FBQ0osS0E5QkUsRUErQkhuYixHQUFHLElBQUk7QUFDSCxXQUFLdVMsUUFBTCxDQUFjO0FBQUNrSCxpQkFBUyxFQUFFO0FBQVosT0FBZDtBQUNILEtBakNFLENBQVA7QUFtQ0g7O0FBRUQ0QixZQUFVLEdBQUU7QUFDUixRQUFJQyxJQUFJLEdBQUcsRUFBWCxDQURRLENBR1I7QUFDQTtBQUNBOztBQUVBQSxRQUFJLENBQUM1SCxJQUFMLENBQ0ksTUFBQyw2RUFBRDtBQUFNLFdBQUssRUFBRTtBQUFDQyxxQkFBYSxFQUFDLEtBQWY7QUFBcUJyQyxjQUFNLEVBQUMsR0FBNUI7QUFBZ0N3SSx3QkFBZ0IsRUFBQyxDQUFqRDtBQUFtRFksc0JBQWMsRUFBRTtBQUFuRSxPQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FDSSxNQUFDLHlGQUFEO0FBQ0ksYUFBTyxFQUFFLE1BQUksQ0FDVDtBQUNBO0FBQ0E7QUFDSCxPQUxMO0FBTUksV0FBSyxFQUFFO0FBQUMzRyxZQUFJLEVBQUMsQ0FBTjtBQUFRcUIseUJBQWlCLEVBQUM7QUFBMUIsT0FOWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BUUksTUFBQyw2RUFBRDtBQUFNLFdBQUssRUFBRTtBQUFDL0QsYUFBSyxFQUFDLE1BQVA7QUFBY3FHLHVCQUFlLEVBQUMsU0FBOUI7QUFBd0NwRyxjQUFNLEVBQUMsTUFBL0M7QUFBc0R1RixnQkFBUSxFQUFDLFFBQS9EO0FBQXdFVyxvQkFBWSxFQUFDO0FBQXJGLE9BQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUNJLE1BQUMsd0ZBQUQ7QUFBaUIsWUFBTSxFQUFFN1EsK0NBQU0sQ0FBQ2tKLFdBQWhDO0FBQTZDLGdCQUFVLEVBQUUsU0FBekQ7QUFBb0UsV0FBSyxFQUFFO0FBQUN3QixhQUFLLEVBQUMsTUFBUDtBQUFjQyxjQUFNLEVBQUM7QUFBckIsT0FBM0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUNJLE1BQUMsNkVBQUQ7QUFBTSxXQUFLLEVBQUU7QUFBQzhELHlCQUFpQixFQUFDLENBQW5CO0FBQXFCMEIsdUJBQWUsRUFBQztBQUFyQyxPQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FDSSxNQUFDLDZFQUFEO0FBQU0sV0FBSyxFQUFFO0FBQUMxRixrQkFBVSxFQUFDakwsK0RBQVEsQ0FBQ0wsT0FBckI7QUFBNkJtTCxnQkFBUSxFQUFDLEVBQXRDO0FBQXlDa0kscUJBQWEsRUFBQyxDQUFDO0FBQXhELE9BQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUE0RSwyQkFBNUUsQ0FESixFQUVJLE1BQUMsNkVBQUQ7QUFBTSxXQUFLLEVBQUU7QUFBQy9ILGtCQUFVLEVBQUNqTCwrREFBUSxDQUFDTCxPQUFyQjtBQUE2Qm1MLGdCQUFRLEVBQUM7QUFBdEMsT0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQXlELDhCQUF6RCxDQUZKLENBREosQ0FESixFQU9JLE1BQUMsNkVBQUQ7QUFBTSxXQUFLLEVBQUVnQyxNQUFNLENBQUNzSSxPQUFwQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BQ0ssVUFETCxDQVBKLENBUkosQ0FESixFQXNCSSxNQUFDLHlGQUFEO0FBQ0ksYUFBTyxFQUFFLE1BQUksQ0FDVDtBQUNBO0FBQ0E7QUFDSCxPQUxMO0FBTUksV0FBSyxFQUFFO0FBQUN4SCxZQUFJLEVBQUMsQ0FBTjtBQUFRcUIseUJBQWlCLEVBQUM7QUFBMUIsT0FOWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BT0ksTUFBQyw2RUFBRDtBQUFNLFdBQUssRUFBRTtBQUFDL0QsYUFBSyxFQUFDLE1BQVA7QUFBY3FHLHVCQUFlLEVBQUMsU0FBOUI7QUFBd0NwRyxjQUFNLEVBQUMsTUFBL0M7QUFBc0R1RixnQkFBUSxFQUFDLFFBQS9EO0FBQXdFVyxvQkFBWSxFQUFDO0FBQXJGLE9BQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUNJLE1BQUMsd0ZBQUQ7QUFBaUIsWUFBTSxFQUFFN1EsK0NBQU0sQ0FBQ2lKLFVBQWhDO0FBQTRDLGdCQUFVLEVBQUUsU0FBeEQ7QUFBbUUsV0FBSyxFQUFFO0FBQUN5QixhQUFLLEVBQUMsTUFBUDtBQUFjQyxjQUFNLEVBQUM7QUFBckIsT0FBMUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUNJLE1BQUMsNkVBQUQ7QUFBTSxXQUFLLEVBQUU7QUFBQzhELHlCQUFpQixFQUFDLENBQW5CO0FBQXFCMEIsdUJBQWUsRUFBQztBQUFyQyxPQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FDSSxNQUFDLDZFQUFEO0FBQU0sV0FBSyxFQUFFO0FBQUMxRixrQkFBVSxFQUFDakwsK0RBQVEsQ0FBQ0wsT0FBckI7QUFBNkJtTCxnQkFBUSxFQUFDLEVBQXRDO0FBQXlDa0kscUJBQWEsRUFBRSxDQUFDO0FBQXpELE9BQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUEyRSxhQUEzRSxDQURKLEVBRUksTUFBQyw2RUFBRDtBQUFNLFdBQUssRUFBRTtBQUFDL0gsa0JBQVUsRUFBQ2pMLCtEQUFRLENBQUNMLE9BQXJCO0FBQTZCbUwsZ0JBQVEsRUFBQztBQUF0QyxPQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FBeUQsMEJBQXpELENBRkosQ0FESixDQURKLEVBT0ksTUFBQyw2RUFBRDtBQUFNLFdBQUssRUFBRWdDLE1BQU0sQ0FBQ3NJLE9BQXBCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FDSyxXQURMLENBUEosQ0FQSixDQXRCSixDQURKLEVBUFEsQ0FtRFI7O0FBQ0FELFFBQUksQ0FBQzVILElBQUwsQ0FBVSxLQUFLOEgsaUJBQUwsRUFBVjtBQUVBLFdBQU9GLElBQVA7QUFDSDs7QUFFRCxRQUFNRyxpQkFBTixHQUEwQjtBQUN0QixTQUFLYixPQUFMO0FBQ0g7O0FBRURySyxRQUFNLEdBQUc7QUFDTCxXQUNJLE1BQUMsc0RBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQUNJLE1BQUMsaUZBQUQsQ0FDSTtBQURKO0FBR0ksU0FBRyxFQUFHbUwsR0FBRCxJQUFPLEtBQUtDLFlBQUwsR0FBb0JELEdBSHBDLENBSUk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEo7QUFVSSxVQUFJLEVBQUUsS0FBS0wsVUFBTCxFQVZWO0FBV0ksZ0JBQVUsRUFBRSxDQUFDO0FBQUN0QjtBQUFELE9BQUQsS0FDUkEsSUFaUjtBQWNJLGtCQUFZLEVBQUUsQ0FBQ0EsSUFBRCxFQUFPQyxLQUFQLEtBQWlCQyxNQUFNLENBQUNELEtBQUQsQ0FkekM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxNQURKLENBREo7QUFvQkg7O0FBeExzRDtBQTJMM0QsTUFBTS9HLE1BQU0sR0FBR3JDLG1GQUFVLENBQUNDLE1BQVgsQ0FBa0I7QUFDN0IrSyxnQkFBYyxFQUFDO0FBQ1g1SSxVQUFNLEVBQUMsR0FESTtBQUVYb0QsU0FBSyxFQUFDLEVBRks7QUFHWGlDLFVBQU0sRUFBQyxFQUhJO0FBSVg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBWCxtQkFBZSxFQUFDO0FBYkwsR0FEYztBQWdCN0JoTCxnQkFBYyxFQUFFO0FBQ1pnRyxZQUFRLEVBQUUsVUFERTtBQUVaMEQsU0FBSyxFQUFFLENBRks7QUFHWnpELE9BQUcsRUFBRSxDQUhPO0FBSVpLLFVBQU0sRUFBRSxDQUpJO0FBS1o7QUFDQTtBQUNBO0FBQ0ExQixVQUFNLEVBQUUsRUFSSTtBQVNaRCxTQUFLLEVBQUUsRUFUSztBQVVad0IsY0FBVSxFQUFFLFFBVkE7QUFXWjJFLGdCQUFZLEVBQUU7QUFYRixHQWhCYTtBQTZCN0JxRSxXQUFTLEVBQUU7QUFDUHhLLFNBQUssRUFBRSxNQURBO0FBRVBDLFVBQU0sRUFBRSxNQUZEO0FBR1A4RyxhQUFTLEVBQUUsUUFISjtBQUlQMEQsY0FBVSxFQUFFO0FBSkwsR0E3QmtCO0FBbUM3QkMsU0FBTyxFQUFFO0FBQ0xoSSxRQUFJLEVBQUUsQ0FERDtBQUVMcUUsYUFBUyxFQUFFLFFBRk47QUFHTHBGLFVBQU0sRUFBRTtBQUhILEdBbkNvQjtBQXdDN0JnSixnQkFBYyxFQUFFO0FBQ1o5SyxhQUFTLEVBQUUsRUFEQztBQUVaSixlQUFXLEVBQUUsRUFGRDtBQUdaQyxhQUFTLEVBQUUsRUFIQztBQUlaMkcsbUJBQWUsRUFBRSxPQUpMO0FBS1pGLGdCQUFZLEVBQUU7QUFMRixHQXhDYTtBQStDN0IrRCxTQUFPLEVBQUM7QUFDSjdJLFlBQVEsRUFBQyxVQURMO0FBRUoyRCxhQUFTLEVBQUUsUUFGUDtBQUdKNEYscUJBQWlCLEVBQUMsUUFIZDtBQUlKO0FBQ0E7QUFDQWxJLFFBQUksRUFBQyxDQU5EO0FBT0pzRSxVQUFNLEVBQUMsRUFQSDtBQVFKekYsUUFBSSxFQUFDLENBUkQ7QUFTSjhFLG1CQUFlLEVBQUMsUUFUWjtBQVVKRixnQkFBWSxFQUFDLENBVlQ7QUFXSkQsZUFBVyxFQUFDLENBWFI7QUFZSm5XLGVBQVcsRUFBQyxRQVpSO0FBYUppUSxTQUFLLEVBQUMsRUFiRjtBQWNKQyxVQUFNLEVBQUMsRUFkSDtBQWVKTCxZQUFRLEVBQUM7QUFmTDtBQS9DcUIsQ0FBbEIsQ0FBZixDOzs7Ozs7Ozs7Ozs7QUNsTkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBTyxNQUFNaUwsMEJBQTBCLEdBQUcsNEJBQW5DO0FBQ0EsTUFBTUMsZUFBZSxHQUFHLGlCQUF4QjtBQUNBLE1BQU1DLGVBQWUsR0FBRyxpQkFBeEI7QUFFQSxTQUFTQyxtQkFBVCxDQUE2QkMsSUFBN0IsRUFBbUM7QUFDdEMsU0FBTztBQUNIQyxRQUFJLEVBQUVMLDBCQURIO0FBRUhNLFNBQUssRUFBRUY7QUFGSixHQUFQO0FBSUg7QUFFTSxTQUFTRyxhQUFULENBQXVCSCxJQUF2QixFQUE2QjtBQUNoQyxTQUFPO0FBQ0hDLFFBQUksRUFBRUosZUFESDtBQUVISyxTQUFLLEVBQUVGO0FBRkosR0FBUDtBQUlIO0FBRU0sU0FBU0ksYUFBVCxDQUF1QkosSUFBdkIsRUFBNkI7QUFDaEMsU0FBTztBQUNIQyxRQUFJLEVBQUVILGVBREg7QUFFSEksU0FBSyxFQUFFRjtBQUZKLEdBQVA7QUFJSCxDOzs7Ozs7Ozs7Ozs7QUN2QkQ7QUFBQTtBQUFBO0FBQU8sTUFBTUssOEJBQThCLEdBQUcsZ0NBQXZDO0FBRUEsU0FBU0MsdUJBQVQsQ0FBaUNOLElBQWpDLEVBQXVDO0FBQzFDLFNBQU87QUFDSEMsUUFBSSxFQUFFSSw4QkFESDtBQUVISCxTQUFLLEVBQUVGO0FBRkosR0FBUDtBQUlILEM7Ozs7Ozs7Ozs7OztBQ1BEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQU8sTUFBTU8sdUJBQXVCLEdBQUcseUJBQWhDO0FBRUEsU0FBU0Msc0JBQVQsQ0FBZ0NDLE9BQWhDLEVBQXlDO0FBQzVDLFNBQU87QUFDSFIsUUFBSSxFQUFFTSx1QkFESDtBQUVITCxTQUFLLEVBQUVPO0FBRkosR0FBUDtBQUlIO0FBRU0sTUFBTUMsd0JBQXdCLEdBQUcsMEJBQWpDO0FBQ0EsU0FBU0Msd0JBQVQsQ0FBa0NGLE9BQWxDLEVBQTJDO0FBQzlDLFNBQU87QUFDSFIsUUFBSSxFQUFFUyx3QkFESDtBQUVIUixTQUFLLEVBQUVPO0FBRkosR0FBUDtBQUlIO0FBRU0sTUFBTUcsd0JBQXdCLEdBQUcsMEJBQWpDO0FBQ0EsU0FBU0Msd0JBQVQsQ0FBa0NKLE9BQWxDLEVBQTJDO0FBQzlDLFNBQU87QUFDSFIsUUFBSSxFQUFFVyx3QkFESDtBQUVIVixTQUFLLEVBQUVPO0FBRkosR0FBUDtBQUlIO0FBRU0sTUFBTUssbUJBQW1CLEdBQUcscUJBQTVCO0FBRUEsU0FBU0Msa0JBQVQsQ0FBNEJOLE9BQTVCLEVBQXFDO0FBQ3hDLFNBQU87QUFDSFIsUUFBSSxFQUFFYSxtQkFESDtBQUVIWixTQUFLLEVBQUVPO0FBRkosR0FBUDtBQUlIO0FBRU0sTUFBTU8sc0JBQXNCLEdBQUcsd0JBQS9CO0FBRUEsU0FBU0Msc0JBQVQsQ0FBZ0NSLE9BQWhDLEVBQXlDO0FBQzVDLFNBQU87QUFDSFIsUUFBSSxFQUFFZSxzQkFESDtBQUVIZCxTQUFLLEVBQUVPO0FBRkosR0FBUDtBQUlIO0FBRU0sTUFBTVMseUJBQXlCLEdBQUcsMkJBQWxDO0FBRUEsU0FBU0MseUJBQVQsQ0FBbUNWLE9BQW5DLEVBQTRDO0FBQy9DO0FBQ0EsU0FBTztBQUNIUixRQUFJLEVBQUVpQix5QkFESDtBQUVIaEIsU0FBSyxFQUFFTztBQUZKLEdBQVA7QUFJSDtBQUVNLE1BQU1XLCtCQUErQixHQUFHLGlDQUF4QztBQUVBLFNBQVNDLDhCQUFULENBQXdDWixPQUF4QyxFQUFpRDtBQUNwRCxTQUFPO0FBQ0hSLFFBQUksRUFBRW1CLCtCQURIO0FBRUhsQixTQUFLLEVBQUVPO0FBRkosR0FBUDtBQUlIO0FBRU0sTUFBTWEsd0JBQXdCLEdBQUcsMEJBQWpDO0FBRUEsU0FBU0Msd0JBQVQsQ0FBa0NkLE9BQWxDLEVBQTJDO0FBQzlDLFNBQU87QUFDSFIsUUFBSSxFQUFFcUIsd0JBREg7QUFFSHBCLFNBQUssRUFBRU87QUFGSixHQUFQO0FBSUg7QUFFTSxNQUFNZSx3QkFBd0IsR0FBRywwQkFBakM7QUFFQSxTQUFTQyx3QkFBVCxDQUFrQ2hCLE9BQWxDLEVBQTJDO0FBQzlDLFNBQU87QUFDSFIsUUFBSSxFQUFFdUIsd0JBREg7QUFFSHRCLFNBQUssRUFBRU87QUFGSixHQUFQO0FBSUgsQzs7Ozs7Ozs7Ozs7O0FDOUVEO0FBQUE7QUFBQTtBQUFBO0FBRUEsTUFBTWlCLFdBQVcsR0FBRztBQUNoQkMsZ0JBQWMsRUFBRSxFQURBO0FBRWhCQyxXQUFTLEVBQUUsQ0FGSztBQUdoQkMsV0FBUyxFQUFFO0FBSEssQ0FBcEI7QUFNTyxTQUFTQyx1QkFBVCxDQUFpQ2xNLEtBQUssR0FBRzhMLFdBQXpDLEVBQXNESyxNQUF0RCxFQUE4RDtBQUNqRSxVQUFRQSxNQUFNLENBQUM5QixJQUFmO0FBQ0ksU0FBS0wsNEVBQUw7QUFBaUM7QUFDN0IsZUFBT29DLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLEVBQWQsRUFBa0JyTSxLQUFsQixFQUF5QjtBQUM1QitMLHdCQUFjLEVBQUVJLE1BQU0sQ0FBQzdCLEtBREssQ0FFNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFUNEIsU0FBekIsQ0FBUDtBQVdIOztBQUNELFNBQUtMLGlFQUFMO0FBQXNCO0FBQ2xCLGVBQU9tQyxNQUFNLENBQUNDLE1BQVAsQ0FBYyxFQUFkLEVBQWtCck0sS0FBbEIsRUFBeUI7QUFDNUJnTSxtQkFBUyxFQUFFRyxNQUFNLENBQUM3QjtBQURVLFNBQXpCLENBQVA7QUFHSDs7QUFDRCxTQUFLSixpRUFBTDtBQUFzQjtBQUNsQixlQUFPa0MsTUFBTSxDQUFDQyxNQUFQLENBQWMsRUFBZCxFQUFrQnJNLEtBQWxCLEVBQXlCO0FBQzVCaU0sbUJBQVMsRUFBRUUsTUFBTSxDQUFDN0I7QUFEVSxTQUF6QixDQUFQO0FBR0g7O0FBQ0Q7QUFDSSxhQUFPdEssS0FBUDtBQXpCUjtBQTJCSCxDOzs7Ozs7Ozs7Ozs7QUNwQ0Q7QUFBQTtBQUFBO0FBQUE7QUFFQSxNQUFNOEwsV0FBVyxHQUFHO0FBQ2hCUSxjQUFZLEVBQUU7QUFERSxDQUFwQjtBQUlPLFNBQVNDLG1CQUFULENBQTZCdk0sS0FBSyxHQUFHOEwsV0FBckMsRUFBa0RLLE1BQWxELEVBQTBEO0FBQzdELFVBQVFBLE1BQU0sQ0FBQzlCLElBQWY7QUFDSSxTQUFLSSxrRkFBTDtBQUFxQztBQUNqQyxlQUFPMkIsTUFBTSxDQUFDQyxNQUFQLENBQWMsRUFBZCxFQUFrQnJNLEtBQWxCLEVBQXlCO0FBQzVCc00sc0JBQVksRUFBRUgsTUFBTSxDQUFDN0I7QUFETyxTQUF6QixDQUFQO0FBR0g7O0FBQ0Q7QUFDSSxhQUFPdEssS0FBUDtBQVBSO0FBU0gsQzs7Ozs7Ozs7Ozs7O0FDaEJEO0FBQUE7QUFBQTtBQUFBO0FBRUEsTUFBTXdNLGVBQWUsR0FBRztBQUNwQkMsZUFBYSxFQUFFO0FBQ1hDLFNBQUssRUFBRTtBQURJLEdBREs7QUFJcEJDLGVBQWEsRUFBRTtBQUNYQyxTQUFLLEVBQUU7QUFESTtBQUpLLENBQXhCO0FBU08sU0FBU0MsWUFBVCxDQUFzQjdNLEtBQUssR0FBR3dNLGVBQTlCLEVBQStDTCxNQUEvQyxFQUF1RDtBQUMxRCxVQUFRQSxNQUFNLENBQUM5QixJQUFmO0FBQ0ksU0FBS3FCLHNFQUFMO0FBQStCO0FBQzNCLGVBQU9VLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLEVBQWQsRUFBa0JyTSxLQUFsQixFQUF5QjtBQUM1QnlNLHVCQUFhLEVBQUVOLE1BQU0sQ0FBQzdCO0FBRE0sU0FBekIsQ0FBUDtBQUdIOztBQUNELFNBQUtzQixzRUFBTDtBQUErQjtBQUMzQixlQUFPUSxNQUFNLENBQUNDLE1BQVAsQ0FBYyxFQUFkLEVBQWtCck0sS0FBbEIsRUFBeUI7QUFDNUIyTSx1QkFBYSxFQUFFUixNQUFNLENBQUM3QjtBQURNLFNBQXpCLENBQVA7QUFHSDs7QUFDRDtBQUNJLGFBQU90SyxLQUFQO0FBWlI7QUFjSCxDOzs7Ozs7Ozs7Ozs7QUMxQkQ7QUFBQTtBQUFBO0FBQUE7QUFVQSxNQUFNOE0sZ0JBQWdCLEdBQUc7QUFDckI7QUFDQUMsb0JBQWtCLEVBQUUzRSxTQUZDO0FBR3JCNEUsZUFBYSxFQUFFNUUsU0FITTtBQUlyQjZFLHFCQUFtQixFQUFDN0UsU0FKQztBQUtyQjhFLGdCQUFjLEVBQUU7QUFDWkMsWUFBUSxFQUFFL0UsU0FERTtBQUVaelMsV0FBTyxFQUFFeVMsU0FGRztBQUdaZ0YsWUFBUSxFQUFFaEYsU0FIRTtBQUlaaUYsYUFBUyxFQUFFakY7QUFKQyxHQUxLO0FBV3JCa0Ysa0JBQWdCLEVBQUNsRjtBQVhJLENBQXpCO0FBY08sU0FBU21GLGNBQVQsQ0FBd0J2TixLQUFLLEdBQUc4TSxnQkFBaEMsRUFBa0RYLE1BQWxELEVBQTBEO0FBQzdELFVBQVFBLE1BQU0sQ0FBQzlCLElBQWY7QUFDSSxTQUFLTSxxRUFBTDtBQUE4QjtBQUMxQixlQUFPeUIsTUFBTSxDQUFDQyxNQUFQLENBQWMsRUFBZCxFQUFrQnJNLEtBQWxCLEVBQXlCO0FBQzVCK00sNEJBQWtCLEVBQUVaLE1BQU0sQ0FBQzdCLEtBQVAsQ0FBYWtELFdBREw7QUFFNUJSLHVCQUFhLEVBQUViLE1BQU0sQ0FBQzdCLEtBQVAsQ0FBYW1ELE1BRkEsQ0FHNUI7O0FBSDRCLFNBQXpCLENBQVA7QUFLSDs7QUFDRCxTQUFLM0Msc0VBQUw7QUFBK0I7QUFDM0IsZUFBT3NCLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLEVBQWQsRUFBa0JyTSxLQUFsQixFQUF5QjtBQUM1QjBOLHdCQUFjLEVBQUV2QixNQUFNLENBQUM3QjtBQURLLFNBQXpCLENBQVA7QUFHSDs7QUFDRCxTQUFLVSxzRUFBTDtBQUErQjtBQUMzQixlQUFPb0IsTUFBTSxDQUFDQyxNQUFQLENBQWMsRUFBZCxFQUFrQnJNLEtBQWxCLEVBQXlCO0FBQzVCc04sMEJBQWdCLEVBQUVuQixNQUFNLENBQUM3QjtBQURHLFNBQXpCLENBQVA7QUFHSDs7QUFFRCxTQUFLWSxpRUFBTDtBQUEwQjtBQUN0QixlQUFPa0IsTUFBTSxDQUFDQyxNQUFQLENBQWMsRUFBZCxFQUFrQnJNLEtBQWxCLEVBQXlCO0FBQzVCMk4sZUFBSyxFQUFFeEIsTUFBTSxDQUFDN0I7QUFEYyxTQUF6QixDQUFQO0FBR0g7O0FBQ0QsU0FBS2Msb0VBQUw7QUFBNkI7QUFDekIsZUFBT2dCLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLEVBQWQsRUFBa0JyTSxLQUFsQixFQUF5QjtBQUM1QjROLGNBQUksRUFBRXpCLE1BQU0sQ0FBQzdCO0FBRGUsU0FBekIsQ0FBUDtBQUdIOztBQUNELFNBQUtnQix1RUFBTDtBQUFnQztBQUM1QixlQUFPYyxNQUFNLENBQUNDLE1BQVAsQ0FBYyxFQUFkLEVBQWtCck0sS0FBbEIsRUFBeUI7QUFDNUJrTix3QkFBYyxFQUFFZixNQUFNLENBQUM3QjtBQURLLFNBQXpCLENBQVA7QUFHSDs7QUFDRCxTQUFLa0IsNkVBQUw7QUFBc0M7QUFDbEMsZUFBT1ksTUFBTSxDQUFDQyxNQUFQLENBQWMsRUFBZCxFQUFrQnJNLEtBQWxCLEVBQXlCO0FBQzVCaU4sNkJBQW1CLEVBQUVkLE1BQU0sQ0FBQzdCO0FBREEsU0FBekIsQ0FBUDtBQUdIO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNJLGFBQU90SyxLQUFQO0FBN0NSO0FBK0NILEM7Ozs7Ozs7Ozs7OztBQ3hFRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0NBRUE7QUFFQTtBQUNBO0FBQ0E7QUFFQTs7QUFFTyxNQUFNMEQsUUFBUSxHQUFHLDhCQUFqQjtBQUVBLE1BQU1tSyxZQUFZLEdBQUduSyxRQUFRLEdBQUcsY0FBaEM7QUFFQSxNQUFNb0ssZ0JBQWdCLEdBQUdELFlBQVksR0FBRyxjQUF4QztBQUVBLE1BQU1FLFNBQVMsR0FBR0YsWUFBWSxHQUFHLFdBQWpDO0FBRUEsTUFBTUcsVUFBVSxHQUFHSCxZQUFZLEdBQUcsZ0JBQWxDO0FBRUEsTUFBTUksVUFBVSxHQUFHSixZQUFZLEdBQUcsZ0JBQWxDO0FBRUEsTUFBTUssZ0JBQWdCLEdBQUdMLFlBQVksR0FBRyx5QkFBeEM7QUFFQSxNQUFNTSxZQUFZLEdBQUdOLFlBQVksR0FBRyxrQkFBcEM7QUFFQSxNQUFNTyxlQUFlLEdBQUdQLFlBQVksR0FBRyx5QkFBdkM7QUFFQSxNQUFNUSxjQUFjLEdBQUdSLFlBQVksR0FBRyx3QkFBdEM7QUFFQSxNQUFNUyxXQUFXLEdBQUdULFlBQVksR0FBRyxlQUFuQztBQUVBLE1BQU1VLFdBQVcsR0FBR1YsWUFBWSxHQUFHLGtCQUFuQztBQUNBLE1BQU1XLFdBQVcsR0FBR1gsWUFBWSxHQUFHLG1CQUFuQztBQUNBLE1BQU1ZLGNBQWMsR0FBR1osWUFBWSxHQUFHLG9CQUF0QztBQUVBLE1BQU1hLGFBQWEsR0FBR2IsWUFBWSxHQUFHLGdCQUFyQztBQUVBLE1BQU1jLFVBQVUsR0FBR2QsWUFBWSxHQUFHLFFBQWxDO0FBQ0EsTUFBTWUsZ0JBQWdCLEdBQUdmLFlBQVksR0FBRyxxQkFBeEM7QUFDQSxNQUFNZ0IsZUFBZSxHQUFHaEIsWUFBWSxHQUFHLDZCQUF2QztBQUNBLE1BQU1pQixZQUFZLEdBQUdqQixZQUFZLEdBQUcsYUFBcEM7QUFDQSxNQUFNa0IsZUFBZSxHQUFHbEIsWUFBWSxHQUFHLG9CQUF2QztBQUNBLE1BQU1tQix5QkFBeUIsR0FBR25CLFlBQVksR0FBRyxxQkFBakQ7QUFDQSxNQUFNb0IscUJBQXFCLEdBQUdwQixZQUFZLEdBQUcsMkJBQTdDO0FBRUEsTUFBTXFCLGdCQUFnQixHQUFHckIsWUFBWSxHQUFHLGlCQUF4QztBQUNBLE1BQU1zQixVQUFVLEdBQUd0QixZQUFZLEdBQUcsV0FBbEM7QUFDQSxNQUFNdUIsU0FBUyxHQUFHdkIsWUFBWSxHQUFHLGVBQWpDO0FBQ0EsTUFBTXdCLFFBQVEsR0FBR3hCLFlBQVksR0FBRyxZQUFoQztBQUNBLE1BQU15QixlQUFlLEdBQUd6QixZQUFZLEdBQUcsWUFBdkM7QUFDQSxNQUFNMEIsZ0JBQWdCLEdBQUcxQixZQUFZLEdBQUcsZ0JBQXhDO0FBQ0EsTUFBTTJCLGVBQWUsR0FBRzNCLFlBQVksR0FBRyxZQUF2QztBQUVBLE1BQU1qRixpQkFBaUIsR0FBR2lGLFlBQVksR0FBRyxTQUF6QztBQUNBLE1BQU00QixRQUFRLEdBQUc1QixZQUFZLEdBQUcsU0FBaEM7QUFFQSxNQUFNNkIsZ0JBQWdCLEdBQUc3QixZQUFZLEdBQUcsZ0JBQXhDO0FBRUEsTUFBTThCLGlCQUFpQixHQUFHOUIsWUFBWSxHQUFHLGtCQUF6QztBQUVBLE1BQU0rQixtQkFBbUIsR0FBRy9CLFlBQVksR0FBRyxlQUEzQztBQUVBLE1BQU1nQyxrQkFBa0IsR0FBR2hDLFlBQVksR0FBRyxvQkFBMUM7QUFFQSxNQUFNaUMsbUJBQW1CLEdBQUdqQyxZQUFZLEdBQUcsa0JBQTNDO0FBQ0EsTUFBTWtDLGFBQWEsR0FBR2xDLFlBQVksR0FBRyxXQUFyQztBQUNBLE1BQU1tQyxlQUFlLEdBQUduQyxZQUFZLEdBQUcsWUFBdkM7QUFDQSxNQUFNb0MsWUFBWSxHQUFHcEMsWUFBWSxHQUFHLGVBQXBDO0FBQ0EsTUFBTXFDLGNBQWMsR0FBR3JDLFlBQVksR0FBRyxhQUF0QztBQUNBLE1BQU1zQyxzQkFBc0IsR0FBR3RDLFlBQVksR0FBRyxnQkFBOUM7QUFDQSxNQUFNdUMsZUFBZSxHQUFHdkMsWUFBWSxHQUFHLGdCQUF2QztBQUNBLE1BQU13QyxVQUFVLEdBQUd4QyxZQUFZLEdBQUcsV0FBbEM7QUFDQSxNQUFNeUMsUUFBUSxHQUFHLFlBQWpCO0FBQ0EsTUFBTW5PLGNBQWMsR0FBRyxRQUF2QixDLENBRVA7O0FBQ08sTUFBTW9PLGNBQWMsR0FBRyxVQUF2QjtBQUNBLE1BQU1DLGdCQUFnQixHQUFHLFlBQXpCO0FBQ0EsTUFBTUMsUUFBUSxHQUFHLFlBQWpCO0FBQ0EsTUFBTUMsbUJBQW1CLEdBQUdELFFBQTVCO0FBQ0EsTUFBTUUsWUFBWSxHQUFHO0FBQ3hCQyxJQUFFLEVBQUUsSUFEb0I7QUFFeEJDLFFBQU0sRUFBRSxRQUZnQjtBQUd4QkMsUUFBTSxFQUFFLFNBSGdCO0FBSXhCQyxLQUFHLEVBQUUsS0FKbUI7QUFLeEJDLElBQUUsRUFBRTtBQUxvQixDQUFyQixDLENBUVA7O0FBQ08sTUFBTUMsV0FBVyxHQUFHO0FBQ3ZCQyxhQUFXLEVBQUUsY0FEVTtBQUV2QnRqQixNQUFJLEVBQUUsa0JBRmlCO0FBR3ZCdWpCLGVBQWEsRUFBRSxlQUhRO0FBSXZCQyxRQUFNLEVBQUU7QUFKZSxDQUFwQixDLENBT1A7O0FBQ08sTUFBTUMsU0FBUyxHQUFHO0FBQ3JCM0ssV0FBUyxFQUFFLFdBRFU7QUFFckI0SyxVQUFRLEVBQUUsVUFGVztBQUdyQkMsUUFBTSxFQUFFO0FBSGEsQ0FBbEIsQyxDQU1QOztBQUNPLE1BQU1DLFlBQVksR0FBRyxVQUFyQjtBQUNBLE1BQU1DLGFBQWEsR0FBRyxXQUF0QixDLENBRVA7O0FBQ08sTUFBTUMsV0FBVyxHQUFHO0FBQ3ZCQyxjQUFZLEVBQUU7QUFEUyxDQUFwQixDLENBSVA7O0FBQ08sTUFBTUMsWUFBWSxHQUFHLGNBQXJCO0FBQ0EsTUFBTUMsYUFBYSxHQUFHLENBQXRCO0FBQ0EsTUFBTS9JLGdCQUFnQixHQUFHLENBQXpCO0FBQ0EsTUFBTWdKLFlBQVksR0FBRyxDQUFyQjtBQUVBLE1BQU1DLGNBQWMsR0FBRyx5Q0FBdkIsQyxDQUdQOztBQUNPLE1BQU1DLFVBQVUsR0FBRyxtQkFBbkI7QUFDQSxNQUFNQyxpQkFBaUIsR0FBRyxjQUExQjtBQUNBLE1BQU1DLFlBQVksR0FBRyxTQUFyQixDLENBRVA7O0FBQ08sTUFBTUMsa0JBQWtCLEdBQzNCLHdEQURHO0FBRUEsTUFBTUMsa0JBQWtCLEdBQUcsd0NBQTNCLEMsQ0FFUDs7QUFDTyxNQUFNQyxRQUFRLEdBQUcsQ0FBakI7QUFDQSxNQUFNQyxVQUFVLEdBQUcsQ0FBbkI7QUFDQSxNQUFNQyxjQUFjLEdBQUcsQ0FBdkI7QUFFQSxNQUFNQyxVQUFVLEdBQUdDLElBQUksSUFBSTtBQUM5QixNQUFJQyxDQUFDLEdBQUcsSUFBSUMsSUFBSixDQUFTRixJQUFULENBQVI7QUFDQSxTQUFPRyw2Q0FBTSxDQUFDRixDQUFELENBQU4sQ0FBVUcsTUFBVixDQUFpQixJQUFqQixDQUFQO0FBQ0gsQ0FITTtBQUtBLE1BQU1DLFNBQVMsR0FBRzFJLElBQUksSUFBSTtBQUM3QixNQUFJQSxJQUFJLElBQUloQyxTQUFSLElBQXFCZ0MsSUFBSSxJQUFJLEVBQWpDLEVBQXFDO0FBQ2pDLFdBQU9BLElBQVA7QUFDSCxHQUZELE1BRU87QUFDSDtBQUNIO0FBQ0osQ0FOTTtBQVFBLE1BQU0ySSxVQUFVLEdBQUdOLElBQUksSUFBSTtBQUM5QixNQUFJQyxDQUFDLEdBQUcsSUFBSUMsSUFBSixDQUFTRixJQUFULENBQVI7QUFDQSxTQUFPRyw2Q0FBTSxDQUFDRixDQUFELENBQU4sQ0FBVUcsTUFBVixDQUFpQixZQUFqQixDQUFQO0FBQ0gsQ0FITTtBQUtBLE1BQU1HLGtCQUFrQixHQUFHLE1BQU07QUFDcEMsTUFBSU4sQ0FBQyxHQUFHLElBQUlDLElBQUosRUFBUjtBQUNBLE1BQUlNLE9BQU8sR0FBR0wsNkNBQU0sQ0FBQ0YsQ0FBRCxDQUFOLENBQVV6ZCxHQUFWLENBQWMsQ0FBZCxFQUFpQixLQUFqQixDQUFkO0FBRUEsU0FBTyxJQUFJMGQsSUFBSixDQUFTTSxPQUFULENBQVA7QUFDSCxDQUxNO0FBT0EsU0FBU0MsYUFBVCxDQUF1QlQsSUFBdkIsRUFBNkJVLE9BQTdCLEVBQXNDO0FBQ3pDLE1BQUlBLE9BQU8sSUFBSS9LLFNBQWYsRUFBMEI7QUFDdEIrSyxXQUFPLEdBQUcsWUFBVjtBQUNIOztBQUNEUCwrQ0FBTSxDQUFDUSxNQUFQLENBQWMsSUFBZDtBQUVBLE1BQUlWLENBQUMsR0FBRyxJQUFJQyxJQUFKLENBQVMsS0FBS0YsSUFBSSxDQUFDWSxVQUFMLENBQWdCLEdBQWhCLEVBQXFCLEdBQXJCLENBQWQsQ0FBUjtBQUVBLFNBQU9ULDZDQUFNLENBQUNGLENBQUQsQ0FBTixDQUFVRyxNQUFWLENBQWlCTSxPQUFqQixDQUFQO0FBQ0g7QUFFTSxTQUFTRyxhQUFULENBQXVCYixJQUF2QixFQUE2QjtBQUNoQ0csK0NBQU0sQ0FBQ1EsTUFBUCxDQUFjLElBQWQ7QUFFQSxNQUFJVixDQUFDLEdBQUcsSUFBSUMsSUFBSixDQUFTLEtBQUtGLElBQUksQ0FBQ1ksVUFBTCxDQUFnQixHQUFoQixFQUFxQixHQUFyQixDQUFkLENBQVI7QUFFQSxTQUFPVCw2Q0FBTSxDQUFDRixDQUFELENBQU4sQ0FBVUcsTUFBVixDQUFpQixJQUFqQixDQUFQO0FBQ0g7O0FBRUQ5SyxNQUFNLENBQUN3TCxTQUFQLENBQWlCRixVQUFqQixHQUE4QixVQUFVRyxNQUFWLEVBQWtCQyxXQUFsQixFQUErQjtBQUN6RCxNQUFJQyxNQUFNLEdBQUcsSUFBYjtBQUNBLFNBQU9BLE1BQU0sQ0FBQ0MsS0FBUCxDQUFhSCxNQUFiLEVBQXFCSSxJQUFyQixDQUEwQkgsV0FBMUIsQ0FBUDtBQUNILENBSEQ7O0FBS08sU0FBU0ksV0FBVCxDQUFxQkMsR0FBckIsRUFBMEI7QUFDN0IsTUFBSUosTUFBTSxHQUFHLEVBQWI7QUFFQSxNQUFJSyxRQUFRLEdBQUdELEdBQUcsQ0FBQ0gsS0FBSixDQUFVLEdBQVYsQ0FBZjtBQUNBLFFBQU1LLE1BQU0sR0FBR0QsUUFBUSxDQUFDekwsTUFBeEI7O0FBQ0EsTUFBSTtBQUNBLFFBQUl5TCxRQUFRLElBQUkzTCxTQUFoQixFQUNJMkwsUUFBUSxDQUFDclMsR0FBVCxDQUFhLENBQUMwSSxJQUFELEVBQU83SSxDQUFQLEtBQWE7QUFDdEIsVUFBSTZJLElBQUksQ0FBQzZKLElBQUwsT0FBZ0IsRUFBaEIsSUFBc0I3SixJQUFJLENBQUM2SixJQUFMLE9BQWdCN0wsU0FBMUMsRUFBcUQsQ0FDcEQsQ0FERCxNQUNPO0FBQ0gsWUFBSTRMLE1BQU0sS0FBS3pTLENBQUMsR0FBRyxDQUFuQixFQUFzQjtBQUNsQm1TLGdCQUFNLEdBQUdBLE1BQU0sR0FBR3RKLElBQWxCO0FBQ0gsU0FGRCxNQUVPO0FBQ0hzSixnQkFBTSxHQUFHQSxNQUFNLEdBQUd0SixJQUFULEdBQWdCLEdBQXpCO0FBQ0g7QUFDSjtBQUNKLEtBVEQ7QUFVUCxHQVpELENBWUUsT0FBTzhKLENBQVAsRUFBVSxDQUNYOztBQUNELFNBQU9SLE1BQVA7QUFDSDtBQUVNLFNBQVNTLFVBQVQsQ0FBb0JDLE1BQXBCLEVBQTRCO0FBQy9CLFNBQU9BLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjLENBQWQsRUFBaUJDLFdBQWpCLEtBQWlDRixNQUFNLENBQUNHLEtBQVAsQ0FBYSxDQUFiLENBQXhDO0FBQ0g7QUFFTSxTQUFTQyxVQUFULENBQW9CVixHQUFwQixFQUF5QjtBQUM1QixNQUFJVyxRQUFRLEdBQUdYLEdBQUcsQ0FBQ1ksV0FBSixHQUFrQmYsS0FBbEIsQ0FBd0IsR0FBeEIsQ0FBZjs7QUFDQSxPQUFLLElBQUlwUyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHa1QsUUFBUSxDQUFDbk0sTUFBN0IsRUFBcUMvRyxDQUFDLEVBQXRDLEVBQTBDO0FBQ3RDO0FBQ0E7QUFDQWtULFlBQVEsQ0FBQ2xULENBQUQsQ0FBUixHQUNJa1QsUUFBUSxDQUFDbFQsQ0FBRCxDQUFSLENBQVk4UyxNQUFaLENBQW1CLENBQW5CLEVBQXNCQyxXQUF0QixLQUFzQ0csUUFBUSxDQUFDbFQsQ0FBRCxDQUFSLENBQVlvVCxTQUFaLENBQXNCLENBQXRCLENBRDFDO0FBRUgsR0FQMkIsQ0FRNUI7OztBQUNBLFNBQU9GLFFBQVEsQ0FBQ2IsSUFBVCxDQUFjLEdBQWQsQ0FBUDtBQUNILEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzVORDtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUEsTUFBTWdCLFdBQVcsR0FBR0MsNkRBQWUsQ0FBQztBQUNsQ3RILGdCQUFjLEVBQUVBLDhFQURrQjtBQUVsQ3VILG1CQUFpQixFQUFFdkkseUZBRmU7QUFHbEN3SSxpQkFBZSxFQUFFN0ksMkZBSGlCO0FBSWxDOEksY0FBWSxFQUFFbkksNkVBQVlBO0FBSlEsQ0FBRCxDQUFuQztBQVNBLE1BQU1vSSxvQkFBb0IsR0FBR0MseURBQVcsQ0FBQ04sV0FBRCxDQUF4QztBQUVlLFNBQVNPLEdBQVQsQ0FBYTNXLEtBQWIsRUFBb0I7QUFDakMsU0FDSSxNQUFDLG9EQUFEO0FBQVUsU0FBSyxFQUFFeVcsb0JBQWpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FDRSxNQUFDLHFFQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsSUFERixDQURKO0FBS0Q7QUFFRCxNQUFNbFUsTUFBTSxHQUFHckMsbUZBQVUsQ0FBQ0MsTUFBWCxDQUFrQjtBQUMvQnlFLFdBQVMsRUFBRTtBQUNUekMsY0FBVSxFQUFFLFFBREg7QUFFVHlVLFlBQVEsRUFBRSxDQUZEO0FBR1R2VSxrQkFBYyxFQUFFO0FBSFAsR0FEb0I7QUFNL0J3VSxNQUFJLEVBQUU7QUFDSnZXLFNBQUssRUFBRTtBQURILEdBTnlCO0FBUy9Cd1csZUFBYSxFQUFFO0FBQ2IzVSxjQUFVLEVBQUUsUUFEQztBQUViM0IsYUFBUyxFQUFFO0FBRkUsR0FUZ0I7QUFhL0IxUCxNQUFJLEVBQUU7QUFDSnFSLGNBQVUsRUFBRSxRQURSO0FBRUo1QixZQUFRLEVBQUUsRUFGTjtBQUdKd0YsZ0JBQVksRUFBRTtBQUhWO0FBYnlCLENBQWxCLENBQWYsQzs7Ozs7Ozs7Ozs7QUMvQkEsbUM7Ozs7Ozs7Ozs7O0FDQUEsa0M7Ozs7Ozs7Ozs7O0FDQUEsZ0Y7Ozs7Ozs7Ozs7O0FDQUEseUU7Ozs7Ozs7Ozs7O0FDQUEsdUU7Ozs7Ozs7Ozs7O0FDQUEsb0U7Ozs7Ozs7Ozs7O0FDQUEsOEU7Ozs7Ozs7Ozs7O0FDQUEseUU7Ozs7Ozs7Ozs7O0FDQUEsbUU7Ozs7Ozs7Ozs7O0FDQUEsK0U7Ozs7Ozs7Ozs7O0FDQUEsbUU7Ozs7Ozs7Ozs7O0FDQUEsd0M7Ozs7Ozs7Ozs7O0FDQUEsa0MiLCJmaWxlIjoicGFnZXMvaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHJlcXVpcmUoJy4uL3Nzci1tb2R1bGUtY2FjaGUuanMnKTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0dmFyIHRocmV3ID0gdHJ1ZTtcbiBcdFx0dHJ5IHtcbiBcdFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcbiBcdFx0XHR0aHJldyA9IGZhbHNlO1xuIFx0XHR9IGZpbmFsbHkge1xuIFx0XHRcdGlmKHRocmV3KSBkZWxldGUgaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdH1cblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3BhZ2VzL2luZGV4LmpzXCIpO1xuIiwiLyoqXHJcbiAqIEdlbnJpYyBmdW5jdGlvbiB0byBtYWtlIGFwaSBjYWxscyB3aXRoIG1ldGhvZCBwb3N0XHJcbiAqIEBwYXJhbSB7c3RyaW5nfSB1cmwgIEFQSSBlbmQgcG9pbnQgdG8gY2FsbFxyXG4gKiBAcGFyYW0ge2FwaVBvc3R9IHJlc3BvbnNlU3VjY2VzcyAgQ2FsbC1iYWNrIGZ1bmN0aW9uIHRvIGdldCBzdWNjZXNzIHJlc3BvbnNlIGZyb20gYXBpIGNhbGxcclxuICogQHBhcmFtIHthcGlQb3N0fSByZXNwb25zZUVyciAgQ2FsbC1iYWNrIGZ1bmN0aW9uIHRvIGdldCBlcnJvciByZXNwb25zZSBmcm9tIGFwaSBjYWxsXHJcbiAqIEBwYXJhbSB7YXBpUG9zdH0gcmVxdWVzdEhlYWRlciAgUmVxdWVzdCBoZWFkZXIgdG8gYmUgc2VuZCB0byBhcGlcclxuICogQHBhcmFtIHthcGlQb3N0fSBib2R5IGRhdGEgdG8gYmUgc2VuZCB0aHJvdWdoIGFwaVxyXG4gKi9cclxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGFwaVBvc3QoXHJcbiAgdXJsLFxyXG4gIGJvZHksXHJcbiAgcmVzcG9uc2VTdWNjZXNzLFxyXG4gIHJlc3BvbnNlRXJyLFxyXG4gIHJlcXVlc3RIZWFkZXIgPSB7XHJcbiAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIlxyXG4gIH1cclxuKSB7XHJcbiAgY29uc29sZS5sb2coXCJyZXF1ZXN0XCIsIEpTT04uc3RyaW5naWZ5KGJvZHkpKTtcclxuICByZXR1cm4gZmV0Y2godXJsLCB7XHJcbiAgICBtZXRob2Q6IFwiUE9TVFwiLFxyXG4gICAgaGVhZGVyczogcmVxdWVzdEhlYWRlcixcclxuICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KGJvZHkpXHJcbiAgfSlcclxuICAgIC50aGVuKGVycm9ySGFuZGxlcilcclxuICAgIC50aGVuKHJlc3BvbnNlID0+IHJlc3BvbnNlLmpzb24oKSlcclxuICAgIC50aGVuKGpzb24gPT4gcmVzcG9uc2VTdWNjZXNzKGpzb24pKVxyXG4gICAgLmNhdGNoKGVyciA9PiByZXNwb25zZUVycihlcnIpKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIEdlbnJpYyBmdW5jdGlvbiB0byBtYWtlIGFwaSBjYWxscyB3aXRoIG1ldGhvZCBnZXRcclxuICogQHBhcmFtIHthcGlHZXR9IHVybCAgQVBJIGVuZCBwb2ludCB0byBjYWxsXHJcbiAqIEBwYXJhbSB7YXBpR2V0fSByZXNwb25zZVN1Y2Nlc3MgIENhbGwtYmFjayBmdW5jdGlvbiB0byBnZXQgc3VjY2VzcyByZXNwb25zZSBmcm9tIGFwaSBjYWxsXHJcbiAqIEBwYXJhbSB7YXBpR2V0fSByZXNwb25zZUVyciAgQ2FsbC1iYWNrIGZ1bmN0aW9uIHRvIGdldCBlcnJvciByZXNwb25zZSBmcm9tIGFwaSBjYWxsXHJcbiAqIEBwYXJhbSB7YXBpR2V0fSByZXF1ZXN0SGVhZGVyICBSZXF1ZXN0IGhlYWRlciB0byBiZSBzZW5kIHRvIGFwaVxyXG4gKi9cclxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGFwaUdldChcclxuICB1cmwsXHJcbiAgcmVzcG9uc2VTdWNjZXNzLFxyXG4gIHJlc3BvbnNlRXJyLFxyXG4gIHJlcXVlc3RIZWFkZXIgPSB7XHJcbiAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIlxyXG4gIH1cclxuKSB7XHJcbiAgcmV0dXJuIGZldGNoKHVybCwge1xyXG4gICAgbWV0aG9kOiBcIkdFVFwiLFxyXG4gICAgaGVhZGVyczogcmVxdWVzdEhlYWRlclxyXG4gIH0pXHJcbiAgICAudGhlbihlcnJvckhhbmRsZXIpXHJcbiAgICAudGhlbihyZXNwb25zZSA9PiByZXNwb25zZS5qc29uKCkpXHJcbiAgICAudGhlbihqc29uID0+IHJlc3BvbnNlU3VjY2Vzcyhqc29uKSlcclxuICAgIC5jYXRjaChlcnIgPT4gcmVzcG9uc2VFcnIoZXJyKSk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBHZW5yaWMgZnVuY3Rpb24gdG8gbWFrZSBhcGkgY2FsbHMgd2l0aCBtZXRob2QgZGVsZXRlXHJcbiAqIEBwYXJhbSB7YXBpRGVsZXRlfSB1cmwgIEFQSSBlbmQgcG9pbnQgdG8gY2FsbFxyXG4gKiBAcGFyYW0ge2FwaURlbGV0ZX0gcmVzcG9uc2VTdWNjZXNzICBDYWxsLWJhY2sgZnVuY3Rpb24gdG8gZ2V0IHN1Y2Nlc3MgcmVzcG9uc2UgZnJvbSBhcGkgY2FsbFxyXG4gKiBAcGFyYW0ge2FwaURlbGV0ZX0gcmVzcG9uc2VFcnIgIENhbGwtYmFjayBmdW5jdGlvbiB0byBnZXQgZXJyb3IgcmVzcG9uc2UgZnJvbSBhcGkgY2FsbFxyXG4gKiBAcGFyYW0ge2FwaURlbGV0ZX0gcmVxdWVzdEhlYWRlciAgUmVxdWVzdCBoZWFkZXIgdG8gYmUgc2VuZCB0byBhcGlcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBhcGlEZWxldGUoXHJcbiAgdXJsLFxyXG4gIHJlc3BvbnNlU3VjY2VzcyxcclxuICByZXNwb25zZUVycixcclxuICByZXF1ZXN0SGVhZGVyID0ge1xyXG4gICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCJcclxuICB9XHJcbikge1xyXG4gIGZldGNoKHVybCwge1xyXG4gICAgbWV0aG9kOiBcIkRFTEVURVwiLFxyXG4gICAgaGVhZGVyczogcmVxdWVzdEhlYWRlclxyXG4gIH0pXHJcbiAgICAudGhlbihlcnJvckhhbmRsZXIpXHJcbiAgICAudGhlbihyZXNwb25zZSA9PiAocmVzcG9uc2Uuc3RhdHVzID09IDIwNCA/IHJlc3BvbnNlIDogcmVzcG9uc2UuanNvbigpKSlcclxuICAgIC50aGVuKGpzb24gPT4gcmVzcG9uc2VTdWNjZXNzKGpzb24pKVxyXG4gICAgLmNhdGNoKGVyciA9PiByZXNwb25zZUVycihlcnIpKTtcclxufVxyXG5cclxuLy9FcnJvciBIYW5kbGVyXHJcbi8qKlxyXG4gKlxyXG4gKiBAcGFyYW0ge2Vycm9ySGFuZGxlcn0gcmVzcG9uc2UgR2VuZXJpYyBmdW5jdGlvbiB0byBoYW5kbGUgZXJyb3Igb2NjdXIgaW4gYXBpXHJcbiAqL1xyXG5jb25zdCBlcnJvckhhbmRsZXIgPSByZXNwb25zZSA9PiB7XHJcbiAgY29uc29sZS5sb2coXCJSZXNwb25zZSA9PT5cIixyZXNwb25zZSk7XHJcbiAgaWYgKFxyXG4gICAgKHJlc3BvbnNlLnN0YXR1cyA+PSAyMDAgJiYgcmVzcG9uc2Uuc3RhdHVzIDwgMzAwKSB8fFxyXG4gICAgcmVzcG9uc2Uuc3RhdHVzID09IDQwMSB8fFxyXG4gICAgcmVzcG9uc2Uuc3RhdHVzID09IDQwMFxyXG4gICkge1xyXG4gICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PSAyMDQpIHtcclxuICAgICAgcmVzcG9uc2UuYm9keSA9IHsgc3VjY2VzczogXCJTYXZlZFwiIH07XHJcbiAgICB9XHJcbiAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKHJlc3BvbnNlKTtcclxuICB9IGVsc2Uge1xyXG5cclxuICAgIHZhciBlcnJvciA9IG5ldyBFcnJvcihyZXNwb25zZS5zdGF0dXNUZXh0IHx8IHJlc3BvbnNlLnN0YXR1cyk7XHJcbiAgICBlcnJvci5yZXNwb25zZSA9IHJlc3BvbnNlO1xyXG4gICAgcmV0dXJuIFByb21pc2UucmVqZWN0KGVycm9yKTtcclxuICB9XHJcbn07XHJcblxyXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gYXBpUG9zdFFzKFxyXG4gIHVybCxcclxuICBib2R5LFxyXG4gIHJlc3BvbnNlU3VjY2VzcyxcclxuICByZXNwb25zZUVycixcclxuICByZXF1ZXN0SGVhZGVyID0ge31cclxuKSB7XHJcbiAgY29uc29sZS5sb2coXCJyZXF1ZXN0XCIsIEpTT04uc3RyaW5naWZ5KGJvZHkpKTtcclxuICBmZXRjaCh1cmwsIHtcclxuICAgIG1ldGhvZDogXCJQT1NUXCIsXHJcbiAgICBib2R5OiBib2R5LFxyXG4gICAgaGVhZGVyczoge1xyXG4gICAgICBBY2NlcHQ6IFwiYXBwbGljYXRpb24vanNvblwiLFxyXG4gICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcIm11bHRpcGFydC9mb3JtLWRhdGFcIlxyXG4gICAgfVxyXG4gIH0pXHJcbiAgICAudGhlbihlcnJvckhhbmRsZXIpXHJcbiAgICAudGhlbihyZXNwb25zZSA9PiByZXNwb25zZS5qc29uKCkpXHJcbiAgICAudGhlbihqc29uID0+IHJlc3BvbnNlU3VjY2Vzcyhqc29uKSlcclxuICAgIC5jYXRjaChlcnIgPT4gcmVzcG9uc2VFcnIoZXJyKSk7XHJcbn1cclxuIiwiZXhwb3J0IGNvbnN0IEVEQ29sb3JzID0ge1xyXG4gIHByaW1hcnk6IFwiI2U1MDA2Y1wiLFxyXG4gIHByaW1hcnlEaXNhYmxlZDogXCIjYjA3MzlmXCIsXHJcbiAgTFByaW1hcnk6IFwiI2Y5ZGFlY1wiLFxyXG4gIHNlY29uZGFyeTogXCIjMjMzNjQ2XCIsXHJcbiAgc2Vjb25kYXJ5QnV0dG9uczogXCIjMmM2ZmFlXCIsXHJcbiAgYmFja2dyb3VuZDogXCIjRjdGOEY5XCIsXHJcbiAgYm9yZGVyQ29sb3I6ICdyZ2IoMjI5LDIyNywyMjgpJyxcclxuICBiYWNrZ3JvdW5kRGFyazogXCIjZWVlXCIsXHJcbiAgYmFja2dyb3VuZExpZ2h0OiAncmdiKDI0MCwyNDAsMjQwKScsXHJcbiAgcGxhY2Vob2xkZXI6IFwiI0JBQkFDNFwiLFxyXG4gIHRleHQ6IFwiZ3JheVwiLFxyXG4gIGRhcmtUZXh0OiBcImRhcmtncmF5XCIsXHJcbiAgYWN0aXZlVGFiQ29sb3I6XCIjMkY0MDUwXCIsXHJcbiAgaW5hY3RpdmVUYWJDb2xvcjpcIiNCQUJBQzRcIixcclxuICBlcnJvcjpcInJlZFwiLFxyXG4gIHRoZW1lR3JlZW46ICdyZ2IoMzEsMTkxLDE5MyknLFxyXG4gIGJsYWNrOiBcImJsYWNrXCIsXHJcbiAgd2hpdGU6IFwid2hpdGVcIixcclxuICBvZmZXaGl0ZToncmdiKDI0NywyNDcsMjQ3KScsXHJcbiAgYnV0dG9uUmVzZXJ2ZTogJ3JnYigyNCwxMjEsMTkxKScsXHJcbiAgYnV0dG9uVW5yZXNlcnZlOiAncmdiYSgyNTUsIDE3OSwgMCwgMC4zKScsXHJcbiAgYnV0dG9uVmlld0RvY3VtZW50czogJ3JnYigyNSwxNzAsMTM3KScsXHJcbiAgcGFsZVllbGxvdzogJ3JnYigyNTIsMjQ3LDIyMyknLFxyXG4gIHBhbGVCbHVlOiAncmdiKDIxMiwyMzQsMjQ2KScsXHJcbiAgc2hhZG93OicjZGRkJyxcclxuICBEQVJLR1JBWTonIzVmNWY1YScsXHJcbiAgTEdSQVk6JyNlZmVmZWEnLFxyXG4gIENHUkFZOicjYzZjNmMxJyxcclxuICBHUkFZOicjYjNiM2FlJyxcclxuICBXSElURTonI2ZmZicsXHJcbiAgTElHSFRHUkFZOicjZDBkMGNiJ1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IEFQUENPTE9SUyA9IHtcclxuZ3JvdXAxTGVmdDU6XCIjZmZlNWVjXCIsXHJcbmdyb3VwMUxlZnQ0OlwiI2ZmZDFjY1wiLFxyXG5ncm91cDFMZWZ0MzpcIiNmZmFkYzNcIixcclxuZ3JvdXAxTGVmdDI6XCIjRkY4N0E1XCIsXHJcbmdyb3VwMUxlZnQxOlwiI0ZGNTc4MVwiLFxyXG5ncm91cDFNYWluOlwiI0ZGMkI2MFwiLFx0LyogTWFpbiBQcmltYXJ5IGNvbG9yICovXHJcbmdyb3VwMVJpZ2h0MTpcIiNGRjAwNDBcIixcclxuZ3JvdXAxUmlnaHQyOlwiI0ZFMDA0MFwiLFxyXG5cclxuZ3JvdXAyTGVmdDU6XCIjYzdjNGFjXCIsXHJcbmdyb3VwMkxlZnQ0OlwiI2ZmZTFhNVwiLFxyXG5ncm91cDJMZWZ0MzpcIiNmZmQ0YTFcIixcclxuZ3JvdXAyTGVmdDI6XCIjRkZBRTg4XCIsXHJcbmdyb3VwMkxlZnQxOlwiI0ZGOEQ1N1wiLFxyXG5ncm91cDJNYWluOlwiI0ZGNzAyQlwiLFx0LyogTWFpbiBTZWNvbmRhcnkgY29sb3IgKDEpICovXHJcbmdyb3VwMlJpZ2h0MTpcIiNGRjUzMDBcIixcclxuZ3JvdXAyUmlnaHQyOlwiI0ZGNTMwMFwiLFxyXG5cclxuXHJcbmdyb3VwM0xlZnQyOlwiIzA4RkNBNlwiLFxyXG5ncm91cDNMZWZ0MTpcIiMwMEZGQTVcIixcclxuZ3JvdXAzTWFpbjpcIiMwMENCODRcIixcdC8qIE1haW4gU2Vjb25kYXJ5IGNvbG9yICgyKSAqL1xyXG5ncm91cDNSaWdodDE6XCIjMDA5QzY1XCIsXHJcbmdyb3VwM1JpZ2h0MjpcIiMwMDdBNEZcIixcclxuXHJcbmdyb3VwNExlZnQzOlwiIzllZmU1Y1wiLFxyXG5ncm91cDRMZWZ0MjpcIiM2NEZFMDBcIixcclxuZ3JvdXA0TGVmdDE6XCIjNjBGMzAwXCIsXHJcbmdyb3VwNE1haW46XCIjNTVEOTAwXCIsXHQvKiBNYWluIENvbXBsZW1lbnQgY29sb3IgKi9cclxuZ3JvdXA0UmlnaHQxOlwiIzQ1QUYwMFwiLFxyXG5ncm91cDRSaWdodDI6XCIjMzQ4NTAwXCIsXHJcblxyXG5cclxuXHJcbi8qIEFzIFJHQmEgY29kZXMgKi9cclxuXHJcbnJnYmFQcmltYXJ5MDpcInJnYmEoMjU1LCA0MywgOTYsMSlcIixcdC8qIE1haW4gUHJpbWFyeSBjb2xvciAqL1xyXG5yZ2JhUHJpbWFyeTE6XCJyZ2JhKDI1NSwxMzUsMTY1LDEpXCIsXHJcbnJnYmFQcmltYXJ5MjpcInJnYmEoMjU1LCA4NywxMjksMSlcIixcclxucmdiYVByaW1hcnkzOlwicmdiYSgyNTUsICAwLCA2NCwxKVwiLFxyXG5yZ2JhUHJpbWFyeTQ6XCJyZ2JhKDI1NCwgIDAsIDY0LDEpXCIsXHJcblxyXG5yZ2JhU2Vjb25kYXJ5MTA6XCJyZ2JhKDI1NSwxMTIsIDQzLDEpXCIsXHQvKiBNYWluIFNlY29uZGFyeSBjb2xvciAoMSkgKi9cclxucmdiYVNlY29uZGFyeTExOlwicmdiYSgyNTUsMTc0LDEzNiwxKVwiLFxyXG5yZ2JhU2Vjb25kYXJ5MTI6XCJyZ2JhKDI1NSwxNDEsIDg3LDEpXCIsXHJcbnJnYmFTZWNvbmRhcnkxMzpcInJnYmEoMjU1LCA4MywgIDAsMSlcIixcclxucmdiYVNlY29uZGFyeTE0OlwicmdiYSgyNTUsIDgzLCAgMCwxKVwiLFxyXG5cclxucmdiYVNlY29uZGFyeTIwOlwicmdiYSggIDAsMjAzLDEzMiwxKVwiLFx0LyogTWFpbiBTZWNvbmRhcnkgY29sb3IgKDIpICovXHJcbnJnYmFTZWNvbmRhcnkyMTpcInJnYmEoICA4LDI1MiwxNjYsMSlcIixcclxucmdiYVNlY29uZGFyeTIyOlwicmdiYSggIDAsMjU1LDE2NSwxKVwiLFxyXG5yZ2JhU2Vjb25kYXJ5MjM6XCJyZ2JhKCAgMCwxNTYsMTAxLDEpXCIsXHJcbnJnYmFTZWNvbmRhcnkyNDpcInJnYmEoICAwLDEyMiwgNzksMSlcIixcclxuXHJcbnJnYmFDb21wbGVtZW50MjA6XCJyZ2JhKCA4NSwyMTcsICAwLDEpXCIsXHQvKiBNYWluIENvbXBsZW1lbnQgY29sb3IgKi9cclxucmdiYUNvbXBsZW1lbnQyMTpcInJnYmEoMTAwLDI1NCwgIDAsMSlcIixcclxucmdiYUNvbXBsZW1lbnQyMjpcInJnYmEoIDk2LDI0MywgIDAsMSlcIixcclxucmdiYUNvbXBsZW1lbnQyMzpcInJnYmEoIDY5LDE3NSwgIDAsMSlcIixcclxucmdiYUNvbXBsZW1lbnQyNDpcInJnYmEoIDUyLDEzMywgIDAsMSlcIixcclxufVxyXG5cclxuXHJcbiIsImV4cG9ydCBjb25zdCBFVEZvbnRzID0ge1xyXG4gIC8vIGJvbGQ6IFwiTGF0by1Cb2xkXCIsXHJcbiAgLy8gYmxhY2s6IFwiTGF0by1CbGFja1wiLFxyXG4gIC8vIHJlZ3VsYXI6IFwiTGF0by1SZWd1bGFyXCIsXHJcbiAgLy8gbGlnaHQ6IFwiTGF0by1MaWdodFwiLFxyXG4gIC8vIGhhaXJsaW5lOiBcIkxhdG8tSGFpcmxpbmVcIixcclxuICAvLyBzYXRpc2Z5IDogXCJTYXRpc2Z5LVJlZ3VsYXJcIixcclxuICAvLyBpY29uRm9udDogXCJpY29uZm9udFwiLFxyXG5cclxuICAgIGJvbGQ6IFwiU3lzdGVtXCIsXHJcbiAgICBibGFjazogXCJTeXN0ZW1cIixcclxuICAgIHJlZ3VsYXI6IFwiU3lzdGVtXCIsXHJcbiAgICBsaWdodDogXCJTeXN0ZW1cIixcclxuICAgIGhhaXJsaW5lOiBcIlN5c3RlbVwiLFxyXG4gICAgc2F0aXNmeSA6IFwiU3lzdGVtXCIsXHJcbiAgICBpY29uRm9udDogXCJTeXN0ZW1cIixcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBBUFBGT05UUyA9IHtcclxuICAgIGJvbGQ6XCJSb2JvdG8tQm9sZFwiLFxyXG4gICAgYmxhY2s6XCJSb2JvdC1CbGFja1wiLFxyXG4gICAgYm9sZEl0YWxpYzpcIlJvYm90by1Cb2xkSXRhbGljXCIsXHJcbiAgICBpdGFsaWM6XCJSb2JvdG8tSXRhbGljXCIsXHJcbiAgICBsaWdodDpcIlJvYm90by1MaWdodFwiLFxyXG4gICAgbGlnaHRJdGFsaWM6XCJSb2JvdG8tTGlnaHRJdGFsaWNcIixcclxuICAgIG1lZGl1bTogXCJSb2JvdG8tTWVkaXVtXCIsXHJcbiAgICBtZWR1aW1JdGFsaWM6XCJSb2JvdG8tTWVkaXVtSXRhbGljXCIsXHJcbiAgICByZWd1bGFyOiBcIlJvYm90by1SZWd1bGFyXCIsXHJcbiAgICB0aGluOlwiUm9ib3RvLVRoaW5cIixcclxuICAgIHRoaW5JdGFsaWM6XCJSb2JvdG8tVGhpbkl0YWxpY1wiLFxyXG59XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2Fib3V0X3VzLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2Fib3V0dXNfc2VsZWN0ZWQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvYWJvdXR1c2Rlc2VsZWN0ZWQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvYWRkLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2FkZHJlc3MucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvYmFjay5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9iYW5uZXItcmVzdGF1cmFudC5qcGdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9iYW5uZXJfcGxhY2Vob2xkZXIucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvYmdfbG9naW4ucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvYmdfc3BsYXNoLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2Jvb2tpbmdhdmFpbGFibGUucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvYm9va2luZ25vdGF2YWlsYWJsZS5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9icmFuZC1pY29uLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2FsY29ob2wucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvZXhwcmVzcy5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9mb29kLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2dyb2NlcnkucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvY2FsZW5kYXIucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvY2FsZW5kYXJ3aGl0ZS5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9jYWxsLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2NhbGxfb3JkZXIucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvY2FtZXJhX3doaXRlLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2NhcnQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvY2FydF9ibGFjay5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9jYXRlZ29yeS1pY29uLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2Nsb2NrX3doaXRlLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2NvbmZpcm1fYmFja2dyb3VuZC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9jb25maXJtX3RodW1iLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2NvbnRhY3R1c19kZXNlbGVjdGVkLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2NvbnRhY3R1c19zZWxlY3RlZC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9jb3VuZG93bi5qcGdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9Db2ZmZWUuanBnXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvSmFwYW5lc2UuanBnXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvTWlsayBUZWEuanBnXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvV2VzdGVybi5qcGdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9jdXJyZW5jeS5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9kZWxldGUucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvZGVsZXRlX1doaXRlLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2RlbGV0ZV9ncmF5LnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2RlbGl2ZXJlZGRlc2VsZWN0ZWQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvZGVsaXZlcmVkc2VsZWN0ZWQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvZGlzY291bnRfc3RpY2sucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvZG93bi5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9kcml2ZXIucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvZWRpdF9wcm9maWxlLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2VtcHR5LWNhcnQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvZXZlbnRib29raW5nX2Rlc2VsZWN0ZWQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvZXZlbnRib29raW5nX3NlbGVjdGVkLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2ZpbHRlci5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9raC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9hbGxfY3Vpc2luZXMucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvYXNpYW4ucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvYmJxLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2NoaW5lc2UucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvY29mZmVlLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2l0YWxpYW4ucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvamFwYW5lc2UucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2Uvb2ZmZXJzLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3NvdXAucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2Uvdmlld19hbGwucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvaGVhZGxpbmUucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvaG9tZS1pY29uLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2hvbWVfZGVzZWxlY3RlZC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9jYXRlZ29yeS5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9jb21tdW5pdHkucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvY291cG9uX2RlYWxzLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2ZsYXNoX3NhbGUucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvaG90LnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL25ldy5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9zaGFyZV9naWZ0cy5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS91bmRlcjEwLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2hvbWVfc2VsZWN0ZWQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvaG90LWljb24ucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvaWNfYmFja19yZXNlcnZhdGlvbi5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9pY19jbG9zZS5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9pdGVtTm90Rm91bmQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvbGFuZy5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9sZWZ0VG9wUmliYm9uLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2xvY2F0aW9uLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2xvY2F0aW9uX2dyZXkucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvbG9nby5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9jdXN0b21lci5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9tYXAtY3JvcHBlZC5qcGdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9tZW51LnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL21lcmNoYW4tYWRzLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL21pbnVzX3JvdW5kLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL21pbnVzcXR5LnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL21vcmUucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvbW90by5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9teWJvb2tpbmdfZGVzZWxlY3RlZC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9teWJvb2tpbmdfc2VsZWN0ZWQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvbmFtZS5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9ub3Byb2R1Y3RzLmpwZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL25vdGlmaWNhdGlvbl9kZXNlbGVjdGVkLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL25vdGlmaWNhdGlvbl9zZWxlY3RlZC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9vbnRoZXdheWRlc2VsZWN0ZWQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2Uvb250aGV3YXlzZWxlY3RlZC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9vcmRlcl9kZXNlbGVjdGVkLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL29yZGVyX3NlbGVjdGVkLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL29yZGVycGxhY2Vkc2VsZWN0ZWQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvcGFzc3dvcmQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvYWJhcGF5LnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL2NhcmQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvdmlzYV9jYXJkLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3dpbmcucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvcGVvcGxlLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3BpenphLmpwZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3BsdXMucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvcGx1c19yb3VuZC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9wbHVzcXR5LnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3ByZXBhcmluZ2Rlc2VsZWN0ZWQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvcHJlcGFyaW5nc2VsZWN0ZWQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvcHJpdmFjeV9wb2xpY3kucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvcHJpdmFjeXBvbGljeV9kZXNlbGVjdGVkLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3ByaXZhY3lwb2xpY3lfc2VsZWN0ZWQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvcHJvZmlsZV9hZGRyZXNzLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3JhdGV1c19kZXNlbGVjdGVkLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3JhdGV1c19zZWxlY3RlZC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9yZWNpcGVfZGVzZWxlY3RlZC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9yZWNpcGVfc2VhcmNoLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3JlY2lwZV9zZWxlY3RlZC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9yZXN0X2xvZ28ucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvcmVzdGF1cmFudF9tZW51LnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3JpZ2h0LWFycm93LnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3JpZ2h0VG9wUmliYm9uLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3NlYXJjaC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9zZXR0aW5nc19zZWxlY3RlZC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9zaGFyZXVzX2Rlc2VsZWN0ZWQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2Uvc2hhcmV1c19zZWxlY3RlZC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9zaWduX3VwLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3NpZ25vdXRfZGVzZWxlY3RlZC5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9zaWdub3V0X3NlbGVjdGVkLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlLzEucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvMi5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS8zLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlLzQucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2Uvc3Rhci5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS9zdGFyX3doaXRlLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3N1cGVyX2RlYWxzLWljb24ucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvdGljay5wbmdcIjsiLCJtb2R1bGUuZXhwb3J0cyA9IFwiL19uZXh0L3N0YXRpYy9pbWFnZS90aW1lLnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3VwX2Fycm93LnBuZ1wiOyIsIm1vZHVsZS5leHBvcnRzID0gXCIvX25leHQvc3RhdGljL2ltYWdlL3VzZXJfcGxhY2Vob2xkZXIucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvdmFjYW5jeS1hZHMucG5nXCI7IiwibW9kdWxlLmV4cG9ydHMgPSBcIi9fbmV4dC9zdGF0aWMvaW1hZ2UvdmVyaWZpZWQtc2hpZWxkLnBuZ1wiOyIsIlwidXNlIHN0cmljdFwiO1xyXG5cclxuY29uc3QgQXNzZXRzID0ge1xyXG4gICAgYmdIb21lOiByZXF1aXJlKFwiLi9pbWFnZS9iZ19zcGxhc2gucG5nXCIpLFxyXG4gICAgLy8gYmdTaWdudXA6cmVxdWlyZShcIi4vaW1hZ2UvYmdfc2lnbnVwLnBuZ1wiKSxcclxuICAgIGJnU2lnbnVwOiByZXF1aXJlKFwiLi9pbWFnZS9zaWduX3VwLnBuZ1wiKSxcclxuICAgIHNldHRpbmc6IHJlcXVpcmUoXCIuL2ltYWdlL3NldHRpbmdzX3NlbGVjdGVkLnBuZ1wiKSxcclxuICAgIGJhY2tXaGl0ZTogcmVxdWlyZShcIi4vaW1hZ2UvYmFjay5wbmdcIiksXHJcbiAgICBtZW51OiByZXF1aXJlKFwiLi9pbWFnZS9tZW51LnBuZ1wiKSxcclxuICAgIG1vcmU6IHJlcXVpcmUoXCIuL2ltYWdlL21vcmUucG5nXCIpLFxyXG4gICAgYWRkOiByZXF1aXJlKFwiLi9pbWFnZS9hZGQucG5nXCIpLFxyXG4gICAgbG9nbzogcmVxdWlyZShcIi4vaW1hZ2UvbG9nby5wbmdcIiksXHJcbiAgICBkcml2ZXI6IHJlcXVpcmUoXCIuL2ltYWdlL2RyaXZlci5wbmdcIiksXHJcbiAgICBjYWxsX29yZGVyOiByZXF1aXJlKFwiLi9pbWFnZS9jYWxsX29yZGVyLnBuZ1wiKSxcclxuICAgIHJlZnJlc2g6ICcuL2ltYWdlL3JlZnJlc2gucG5nJyxcclxuXHJcbiAgICAvL0VhdGFuY2VcclxuICAgIC8vIGJnX2xvZ2luOnJlcXVpcmUoXCIuL2ltYWdlL3NpZ25faW4ucG5nXCIpLFxyXG4gICAgYmdfbG9naW46IHJlcXVpcmUoXCIuL2ltYWdlL2JnX2xvZ2luLnBuZ1wiKSxcclxuICAgIGljX2xvY2F0aW9uOiByZXF1aXJlKFwiLi9pbWFnZS9sb2NhdGlvbi5wbmdcIiksXHJcblxyXG4gICAgcmF0aW5nOiByZXF1aXJlKFwiLi9pbWFnZS9yYXRldXNfc2VsZWN0ZWQucG5nXCIpLFxyXG4gICAgdGltZTogcmVxdWlyZShcIi4vaW1hZ2UvdGltZS5wbmdcIiksXHJcbiAgICByZXN0TWVudTogcmVxdWlyZShcIi4vaW1hZ2UvcmVzdGF1cmFudF9tZW51LnBuZ1wiKSxcclxuICAgIGFkZHJlc3M6IHJlcXVpcmUoXCIuL2ltYWdlL2FkZHJlc3MucG5nXCIpLFxyXG4gICAgaWNfc2VhcmNoOiByZXF1aXJlKFwiLi9pbWFnZS9zZWFyY2gucG5nXCIpLFxyXG4gICAgcmVjaXBlX3NlYXJjaDogcmVxdWlyZShcIi4vaW1hZ2UvcmVjaXBlX3NlYXJjaC5wbmdcIiksXHJcbiAgICBpY19sb2NhdGlvbl9ncmV5OiByZXF1aXJlKFwiLi9pbWFnZS9sb2NhdGlvbl9ncmV5LnBuZ1wiKSxcclxuICAgIGljX3BsdXM6IHJlcXVpcmUoXCIuL2ltYWdlL3BsdXNxdHkucG5nXCIpLFxyXG4gICAgaWNfbWludXM6IHJlcXVpcmUoXCIuL2ltYWdlL21pbnVzcXR5LnBuZ1wiKSxcclxuICAgIGljX3Jlc2VydmF0aW9uX2JhY2s6IHJlcXVpcmUoXCIuL2ltYWdlL2ljX2JhY2tfcmVzZXJ2YXRpb24ucG5nXCIpLFxyXG4gICAgaWNfcGx1c195ZWxsb3c6IHJlcXVpcmUoXCIuL2ltYWdlL3BsdXMucG5nXCIpLFxyXG4gICAgaWNfZmlsdGVyOiByZXF1aXJlKFwiLi9pbWFnZS9maWx0ZXIucG5nXCIpLFxyXG4gICAgaWNfY2FydDogcmVxdWlyZShcIi4vaW1hZ2UvY2FydC5wbmdcIiksXHJcbiAgICBibF9jYXJ0OiByZXF1aXJlKFwiLi9pbWFnZS9jYXJ0X2JsYWNrLnBuZ1wiKSxcclxuICAgIGljX3VwX2Fycm93OiByZXF1aXJlKFwiLi9pbWFnZS91cF9hcnJvdy5wbmdcIiksXHJcbiAgICBpY19kb3duX2Fycm93OiByZXF1aXJlKFwiLi9pbWFnZS9kb3duLnBuZ1wiKSxcclxuXHJcbiAgICBjdXJyZW5jeTogcmVxdWlyZShcIi4vaW1hZ2UvY3VycmVuY3kucG5nXCIpLFxyXG5cclxuICAgIHNhbXBsZV9yZXN0X2Jhbm5lcjogcmVxdWlyZShcIi4vaW1hZ2UvYmFubmVyLXJlc3RhdXJhbnQuanBnXCIpLFxyXG4gICAgc2FtcGxlX3Jlc3RfbG9nbzogcmVxdWlyZShcIi4vaW1hZ2UvcmVzdF9sb2dvLnBuZ1wiKSxcclxuICAgIHN0YXI6IHJlcXVpcmUoXCIuL2ltYWdlL3N0YXIucG5nXCIpLFxyXG5cclxuICAgIHZlcmlmaWVkOiByZXF1aXJlKFwiLi9pbWFnZS92ZXJpZmllZC1zaGllbGQucG5nXCIpLFxyXG5cclxuICAgIC8vIGRlc2VsZWN0ZWQgaWNvbnNcclxuICAgIGhvbWVfZGVzZWxlY3Q6IHJlcXVpcmUoXCIuL2ltYWdlL2hvbWVfZGVzZWxlY3RlZC5wbmdcIiksXHJcbiAgICByZWNpcGVfZGVzZWxlY3Q6IHJlcXVpcmUoXCIuL2ltYWdlL3JlY2lwZV9kZXNlbGVjdGVkLnBuZ1wiKSxcclxuICAgIG9yZGVyX2Rlc2VsZWN0OiByZXF1aXJlKFwiLi9pbWFnZS9vcmRlcl9kZXNlbGVjdGVkLnBuZ1wiKSxcclxuICAgIGV2ZW50Ym9va2luZ19kZXNlbGVjdDogcmVxdWlyZShcIi4vaW1hZ2UvZXZlbnRib29raW5nX2Rlc2VsZWN0ZWQucG5nXCIpLFxyXG4gICAgbXlib29raW5nX2Rlc2VsZWN0OiByZXF1aXJlKFwiLi9pbWFnZS9teWJvb2tpbmdfZGVzZWxlY3RlZC5wbmdcIiksXHJcbiAgICBub3RpZmljYXRpb25fZGVzZWxlY3Q6IHJlcXVpcmUoXCIuL2ltYWdlL25vdGlmaWNhdGlvbl9kZXNlbGVjdGVkLnBuZ1wiKSxcclxuICAgIHJhdGV1c19kZXNlbGVjdDogcmVxdWlyZShcIi4vaW1hZ2UvcmF0ZXVzX2Rlc2VsZWN0ZWQucG5nXCIpLFxyXG4gICAgc2hhcmV1c19kZXNlbGVjdDogcmVxdWlyZShcIi4vaW1hZ2Uvc2hhcmV1c19kZXNlbGVjdGVkLnBuZ1wiKSxcclxuICAgIHByaXZhY3lwb2xpY3lfZGVzZWxlY3Q6IHJlcXVpcmUoXCIuL2ltYWdlL3ByaXZhY3lwb2xpY3lfZGVzZWxlY3RlZC5wbmdcIiksXHJcbiAgICBhYm91dHVzX2Rlc2VsZWN0OiByZXF1aXJlKFwiLi9pbWFnZS9hYm91dHVzZGVzZWxlY3RlZC5wbmdcIiksXHJcbiAgICBjb250YWN0dXNfZGVzZWxlY3Q6IHJlcXVpcmUoXCIuL2ltYWdlL2NvbnRhY3R1c19kZXNlbGVjdGVkLnBuZ1wiKSxcclxuICAgIHNpZ25vdXRfZGVzZWxlY3Q6IHJlcXVpcmUoXCIuL2ltYWdlL3NpZ25vdXRfZGVzZWxlY3RlZC5wbmdcIiksXHJcblxyXG4gICAgLy8gc2VsZWN0ZWQgaWNvbnNcclxuICAgIGhvbWVfc2VsZWN0OiByZXF1aXJlKFwiLi9pbWFnZS9ob21lX3NlbGVjdGVkLnBuZ1wiKSxcclxuICAgIHJlY2lwZV9zZWxlY3Q6IHJlcXVpcmUoXCIuL2ltYWdlL3JlY2lwZV9zZWxlY3RlZC5wbmdcIiksXHJcbiAgICBvcmRlcl9zZWxlY3Q6IHJlcXVpcmUoXCIuL2ltYWdlL29yZGVyX3NlbGVjdGVkLnBuZ1wiKSxcclxuICAgIGV2ZW50Ym9va2luZ19zZWxlY3Q6IHJlcXVpcmUoXCIuL2ltYWdlL2V2ZW50Ym9va2luZ19zZWxlY3RlZC5wbmdcIiksXHJcbiAgICBteWJvb2tpbmdfc2VsZWN0OiByZXF1aXJlKFwiLi9pbWFnZS9teWJvb2tpbmdfc2VsZWN0ZWQucG5nXCIpLFxyXG4gICAgbm90aWZpY2F0aW9uX3NlbGVjdDogcmVxdWlyZShcIi4vaW1hZ2Uvbm90aWZpY2F0aW9uX3NlbGVjdGVkLnBuZ1wiKSxcclxuICAgIHJhdGV1c19zZWxlY3Q6IHJlcXVpcmUoXCIuL2ltYWdlL3JhdGV1c19zZWxlY3RlZC5wbmdcIiksXHJcbiAgICBzaGFyZXVzX3NlbGVjdDogcmVxdWlyZShcIi4vaW1hZ2Uvc2hhcmV1c19zZWxlY3RlZC5wbmdcIiksXHJcbiAgICBwcml2YWN5cG9saWN5X3NlbGVjdDogcmVxdWlyZShcIi4vaW1hZ2UvcHJpdmFjeXBvbGljeV9zZWxlY3RlZC5wbmdcIiksXHJcbiAgICBhYm91dHVzX3NlbGVjdDogcmVxdWlyZShcIi4vaW1hZ2UvYWJvdXR1c19zZWxlY3RlZC5wbmdcIiksXHJcbiAgICBjb250YWN0dXNfc2VsZWN0OiByZXF1aXJlKFwiLi9pbWFnZS9jb250YWN0dXNfc2VsZWN0ZWQucG5nXCIpLFxyXG4gICAgc2lnbm91dF9zZWxlY3Q6IHJlcXVpcmUoXCIuL2ltYWdlL3NpZ25vdXRfc2VsZWN0ZWQucG5nXCIpLFxyXG5cclxuICAgIGRlbGl2ZXJlZHNlbGVjdGVkOiByZXF1aXJlKFwiLi9pbWFnZS9kZWxpdmVyZWRzZWxlY3RlZC5wbmdcIiksXHJcbiAgICBkZWxpdmVyZWRkZXNlbGVjdGVkOiByZXF1aXJlKFwiLi9pbWFnZS9kZWxpdmVyZWRkZXNlbGVjdGVkLnBuZ1wiKSxcclxuICAgIG9yZGVycGxhY2Vkc2VsZWN0ZWQ6IHJlcXVpcmUoXCIuL2ltYWdlL29yZGVycGxhY2Vkc2VsZWN0ZWQucG5nXCIpLFxyXG4gICAgb250aGV3YXlzZWxlY3RlZDogcmVxdWlyZShcIi4vaW1hZ2Uvb250aGV3YXlzZWxlY3RlZC5wbmdcIiksXHJcbiAgICBvbnRoZXdheWRlc2VsZWN0ZWQ6IHJlcXVpcmUoXCIuL2ltYWdlL29udGhld2F5ZGVzZWxlY3RlZC5wbmdcIiksXHJcbiAgICBwcmVwYXJpbmdkZXNlbGVjdGVkOiByZXF1aXJlKFwiLi9pbWFnZS9wcmVwYXJpbmdkZXNlbGVjdGVkLnBuZ1wiKSxcclxuICAgIHByZXBhcmluZ3NlbGVjdGVkOiByZXF1aXJlKFwiLi9pbWFnZS9wcmVwYXJpbmdzZWxlY3RlZC5wbmdcIiksXHJcbiAgICBib29raW5nYXZhaWxhYmxlOiByZXF1aXJlKFwiLi9pbWFnZS9ib29raW5nYXZhaWxhYmxlLnBuZ1wiKSxcclxuICAgIGJvb2tpbmdub3RhdmFpbGFibGU6IHJlcXVpcmUoXCIuL2ltYWdlL2Jvb2tpbmdub3RhdmFpbGFibGUucG5nXCIpLFxyXG4gICAgZGVsZXRlX2NhcnQ6IHJlcXVpcmUoXCIuL2ltYWdlL2RlbGV0ZS5wbmdcIiksXHJcbiAgICBpY19jbG9zZTogcmVxdWlyZShcIi4vaW1hZ2UvaWNfY2xvc2UucG5nXCIpLFxyXG4gICAgYWJvdXRfdXM6IHJlcXVpcmUoXCIuL2ltYWdlL2Fib3V0X3VzLnBuZ1wiKSxcclxuICAgIHByaXZhY3lfcG9saWN5OiByZXF1aXJlKFwiLi9pbWFnZS9wcml2YWN5X3BvbGljeS5wbmdcIiksXHJcbiAgICBjb25maXJtX2JhY2tncm91bmQ6IHJlcXVpcmUoXCIuL2ltYWdlL2NvbmZpcm1fYmFja2dyb3VuZC5wbmdcIiksXHJcbiAgICBjb25maXJtX3RodW1iOiByZXF1aXJlKFwiLi9pbWFnZS9jb25maXJtX3RodW1iLnBuZ1wiKSxcclxuICAgIGhlYWRlcl9wbGFjZWhvbGRlcjogcmVxdWlyZShcIi4vaW1hZ2UvYmFubmVyX3BsYWNlaG9sZGVyLnBuZ1wiKSxcclxuICAgIGRlbGV0ZV9XaGl0ZTogcmVxdWlyZShcIi4vaW1hZ2UvZGVsZXRlX1doaXRlLnBuZ1wiKSxcclxuICAgIC8vcHJvZmlsZSBzY3JlZW5cclxuICAgIGNhbGw6IHJlcXVpcmUoXCIuL2ltYWdlL2NhbGwucG5nXCIpLFxyXG4gICAgb3VyX2FkZHJlc3M6IHJlcXVpcmUoXCIuL2ltYWdlL3Byb2ZpbGVfYWRkcmVzcy5wbmdcIiksXHJcbiAgICBuYW1lOiByZXF1aXJlKFwiLi9pbWFnZS9uYW1lLnBuZ1wiKSxcclxuICAgIHBhc3N3b3JkOiByZXF1aXJlKFwiLi9pbWFnZS9wYXNzd29yZC5wbmdcIiksXHJcbiAgICBub3RpZmljYXRpb246IHJlcXVpcmUoXCIuL2ltYWdlL25vdGlmaWNhdGlvbl9zZWxlY3RlZC5wbmdcIiksXHJcbiAgICBlZGl0OiByZXF1aXJlKFwiLi9pbWFnZS9lZGl0X3Byb2ZpbGUucG5nXCIpLFxyXG5cclxuICAgIHJhdGluZ193aGl0ZTogcmVxdWlyZShcIi4vaW1hZ2Uvc3Rhcl93aGl0ZS5wbmdcIiksXHJcbiAgICBjbG9ja193aGl0ZTogcmVxdWlyZShcIi4vaW1hZ2UvY2xvY2tfd2hpdGUucG5nXCIpLFxyXG4gICAgc3Rhcl93aGl0ZTogcmVxdWlyZShcIi4vaW1hZ2Uvc3Rhcl93aGl0ZS5wbmdcIiksXHJcbiAgICBwZW9wbGVfd2hpdGU6IHJlcXVpcmUoXCIuL2ltYWdlL3Blb3BsZS5wbmdcIiksXHJcbiAgICBjYWxlbmRlcl93aGl0ZTogcmVxdWlyZShcIi4vaW1hZ2UvY2FsZW5kYXJ3aGl0ZS5wbmdcIiksXHJcbiAgICBjYWxlbmRlcjogcmVxdWlyZShcIi4vaW1hZ2UvY2FsZW5kYXIucG5nXCIpLFxyXG4gICAgdXNlcl9wbGFjZWhvbGRlcjogcmVxdWlyZShcIi4vaW1hZ2UvdXNlcl9wbGFjZWhvbGRlci5wbmdcIiksXHJcbiAgICBwbHVzX3JvdW5kOiByZXF1aXJlKFwiLi9pbWFnZS9wbHVzX3JvdW5kLnBuZ1wiKSxcclxuICAgIG1pbnVzX3JvdW5kOiByZXF1aXJlKFwiLi9pbWFnZS9taW51c19yb3VuZC5wbmdcIiksXHJcbiAgICBkZWxldGVfZ3JheTogcmVxdWlyZShcIi4vaW1hZ2UvZGVsZXRlX2dyYXkucG5nXCIpLFxyXG4gICAgdGljazogcmVxdWlyZShcIi4vaW1hZ2UvdGljay5wbmdcIiksXHJcbiAgICBjYW1lcmFfd2hpdGU6IHJlcXVpcmUoXCIuL2ltYWdlL2NhbWVyYV93aGl0ZS5wbmdcIiksXHJcblxyXG4gICAgZGlzY291bnRfc3RpY2s6IHJlcXVpcmUoXCIuL2ltYWdlL2Rpc2NvdW50X3N0aWNrLnBuZ1wiKSxcclxuXHJcbiAgICAvLyBDdXN0b21cclxuICAgIGxhbmc6IHJlcXVpcmUoXCIuL2ltYWdlL2xhbmcucG5nXCIpLFxyXG4gICAgbGFuZ18yOiByZXF1aXJlKFwiLi9pbWFnZS9sYW5nLnBuZ1wiKSxcclxuICAgIG5vcHJvZHVjdDogcmVxdWlyZShcIi4vaW1hZ2Uvbm9wcm9kdWN0cy5qcGdcIiksXHJcbiAgICBzdXBwZXJfZGVhbF9pY29uOiByZXF1aXJlKFwiLi9pbWFnZS9zdXBlcl9kZWFscy1pY29uLnBuZ1wiKSxcclxuICAgIGFycm93X3JpZ2h0OiByZXF1aXJlKFwiLi9pbWFnZS9yaWdodC1hcnJvdy5wbmdcIiksXHJcbiAgICBoZWFkbGluZTogcmVxdWlyZShcIi4vaW1hZ2UvaGVhZGxpbmUucG5nXCIpLFxyXG4gICAgaG9tZV9pY29uOiByZXF1aXJlKFwiLi9pbWFnZS9ob21lLWljb24ucG5nXCIpLFxyXG4gICAgaG90X2ljb246IHJlcXVpcmUoXCIuL2ltYWdlL2hvdC1pY29uLnBuZ1wiKSxcclxuICAgIGJyYW5kX2ljb246IHJlcXVpcmUoXCIuL2ltYWdlL2JyYW5kLWljb24ucG5nXCIpLFxyXG4gICAgY2F0ZWdvcnlfaWNvbjogcmVxdWlyZShcIi4vaW1hZ2UvY2F0ZWdvcnktaWNvbi5wbmdcIiksXHJcblxyXG4gICAgLy9ob21lIGZ1bmN0aW9uc1xyXG4gICAgZm5fY2F0ZWdvcnk6IHJlcXVpcmUoXCIuL2ltYWdlL2hvbWVfZnVuY3Rpb25zL2NhdGVnb3J5LnBuZ1wiKSxcclxuICAgIGZuX2NvbW11bml0eTogcmVxdWlyZShcIi4vaW1hZ2UvaG9tZV9mdW5jdGlvbnMvY29tbXVuaXR5LnBuZ1wiKSxcclxuICAgIGZuX2NvdXBvbl9kZWFsczogcmVxdWlyZShcIi4vaW1hZ2UvaG9tZV9mdW5jdGlvbnMvY291cG9uX2RlYWxzLnBuZ1wiKSxcclxuICAgIGZuX2ZsYXNoX3NhbGU6IHJlcXVpcmUoXCIuL2ltYWdlL2hvbWVfZnVuY3Rpb25zL2ZsYXNoX3NhbGUucG5nXCIpLFxyXG4gICAgZm5faG90OiByZXF1aXJlKFwiLi9pbWFnZS9ob21lX2Z1bmN0aW9ucy9ob3QucG5nXCIpLFxyXG4gICAgZm5fbmV3OiByZXF1aXJlKFwiLi9pbWFnZS9ob21lX2Z1bmN0aW9ucy9uZXcucG5nXCIpLFxyXG4gICAgZm5fdW5lcjEwOiByZXF1aXJlKFwiLi9pbWFnZS9ob21lX2Z1bmN0aW9ucy91bmRlcjEwLnBuZ1wiKSxcclxuICAgIGZuX3NoYXJlX2dpZnRzOiByZXF1aXJlKFwiLi9pbWFnZS9ob21lX2Z1bmN0aW9ucy9zaGFyZV9naWZ0cy5wbmdcIiksXHJcblxyXG4gICAgLy9Ib21lIFRvcCBJY29uc1xyXG4gICAgaWNvbl9mb29kOiByZXF1aXJlKFwiLi9pbWFnZS9idXNpbmVzc19pY29ucy9mb29kLnBuZ1wiKSxcclxuICAgIGljb25fYWxjb2hvbDogcmVxdWlyZShcIi4vaW1hZ2UvYnVzaW5lc3NfaWNvbnMvYWxjb2hvbC5wbmdcIiksXHJcbiAgICBpY29uX2dyb2Nlcnk6IHJlcXVpcmUoXCIuL2ltYWdlL2J1c2luZXNzX2ljb25zL2dyb2NlcnkucG5nXCIpLFxyXG4gICAgaWNvbl9leHByZXNzOiByZXF1aXJlKFwiLi9pbWFnZS9idXNpbmVzc19pY29ucy9leHByZXNzLnBuZ1wiKSxcclxuXHJcbiAgICAvL01hcmtlclxyXG4gICAgbWFya2VyX2N1c3RvbWVyOiByZXF1aXJlKFwiLi9pbWFnZS9tYXBfbWFya2Vycy9jdXN0b21lci5wbmdcIiksXHJcbiAgICBtYXBfY3JvcHBlZDogcmVxdWlyZShcIi4vaW1hZ2UvbWFwX21hcmtlcnMvbWFwLWNyb3BwZWQuanBnXCIpLFxyXG5cclxuICAgIHBpenphOiByZXF1aXJlKFwiLi9pbWFnZS9waXp6YS5qcGdcIiksXHJcblxyXG4gICAgY291bnRkb3duOiByZXF1aXJlKFwiLi9pbWFnZS9jb3VuZG93bi5qcGdcIiksXHJcblxyXG4gICAgbW90bzogcmVxdWlyZShcIi4vaW1hZ2UvbW90by5wbmdcIiksXHJcblxyXG4gICAgY3Vpc2luZXNfZXhhbXBsZTE6IHJlcXVpcmUoXCIuL2ltYWdlL2N1aXNpbmVzX2V4YW1wbGVzL0NvZmZlZS5qcGdcIiksXHJcbiAgICBjdWlzaW5lc19leGFtcGxlMjogcmVxdWlyZShcIi4vaW1hZ2UvY3Vpc2luZXNfZXhhbXBsZXMvSmFwYW5lc2UuanBnXCIpLFxyXG4gICAgY3Vpc2luZXNfZXhhbXBsZTM6IHJlcXVpcmUoXCIuL2ltYWdlL2N1aXNpbmVzX2V4YW1wbGVzL01pbGsgVGVhLmpwZ1wiKSxcclxuICAgIGN1aXNpbmVzX2V4YW1wbGU0OiByZXF1aXJlKFwiLi9pbWFnZS9jdWlzaW5lc19leGFtcGxlcy9XZXN0ZXJuLmpwZ1wiKSxcclxuXHJcbiAgICBpY29uX2Zvb2Rfb2ZmZXJzOiByZXF1aXJlKFwiLi9pbWFnZS9mb29kX2ljb25zL29mZmVycy5wbmdcIiksXHJcbiAgICBpY29uX2Zvb2Rfdmlld19hbGw6IHJlcXVpcmUoXCIuL2ltYWdlL2Zvb2RfaWNvbnMvdmlld19hbGwucG5nXCIpLFxyXG5cclxuICAgIGljb25fZm9vZF9hbGxfY3Vpc2luZXM6IHJlcXVpcmUoXCIuL2ltYWdlL2Zvb2RfaWNvbnMvYWxsX2N1aXNpbmVzLnBuZ1wiKSxcclxuICAgIGljb25fZm9vZF9iYnE6IHJlcXVpcmUoXCIuL2ltYWdlL2Zvb2RfaWNvbnMvYmJxLnBuZ1wiKSxcclxuICAgIGljb25fZm9vZF9jaGluZXNlOiByZXF1aXJlKFwiLi9pbWFnZS9mb29kX2ljb25zL2NoaW5lc2UucG5nXCIpLFxyXG4gICAgaWNvbl9mb29kX2NvZmZlZTogcmVxdWlyZShcIi4vaW1hZ2UvZm9vZF9pY29ucy9jb2ZmZWUucG5nXCIpLFxyXG4gICAgaWNvbl9mb29kX2l0YWxpYW46IHJlcXVpcmUoXCIuL2ltYWdlL2Zvb2RfaWNvbnMvaXRhbGlhbi5wbmdcIiksXHJcbiAgICBpY29uX2Zvb2RfamFwYW5lc2U6IHJlcXVpcmUoXCIuL2ltYWdlL2Zvb2RfaWNvbnMvamFwYW5lc2UucG5nXCIpLFxyXG4gICAgaWNvbl9mb29kX3BpenphOiByZXF1aXJlKFwiLi9pbWFnZS9mb29kX2ljb25zL2JicS5wbmdcIiksXHJcbiAgICBpY29uX2Zvb2Rfc291cDogcmVxdWlyZShcIi4vaW1hZ2UvZm9vZF9pY29ucy9zb3VwLnBuZ1wiKSxcclxuICAgIGljb25fZm9vZF9hc2lhbjogcmVxdWlyZShcIi4vaW1hZ2UvZm9vZF9pY29ucy9hc2lhbi5wbmdcIiksXHJcblxyXG4gICAgLy8gc2xpZGVzaG93IGV4YW1wbGVcclxuXHJcbiAgICBzbGlkZV8xOiByZXF1aXJlKFwiLi9pbWFnZS9zbGlkZXNob3dfZXhhbXBsZS8xLnBuZ1wiKSxcclxuICAgIHNsaWRlXzI6IHJlcXVpcmUoXCIuL2ltYWdlL3NsaWRlc2hvd19leGFtcGxlLzIucG5nXCIpLFxyXG4gICAgc2xpZGVfMzogcmVxdWlyZShcIi4vaW1hZ2Uvc2xpZGVzaG93X2V4YW1wbGUvMy5wbmdcIiksXHJcbiAgICBzbGlkZV80OiByZXF1aXJlKFwiLi9pbWFnZS9zbGlkZXNob3dfZXhhbXBsZS80LnBuZ1wiKSxcclxuXHJcbiAgICBmbGFnX2toOiByZXF1aXJlKFwiLi9pbWFnZS9mbGFncy9raC5wbmdcIiksXHJcblxyXG4gICAgbGVmdFRvcFJpYmJvbjogcmVxdWlyZShcIi4vaW1hZ2UvbGVmdFRvcFJpYmJvbi5wbmdcIiksXHJcblxyXG4gICAgcmlnaHRUb3BSaWJib246IHJlcXVpcmUoXCIuL2ltYWdlL3JpZ2h0VG9wUmliYm9uLnBuZ1wiKSxcclxuXHJcbiAgICB2YWNhbmN5QWRzOiByZXF1aXJlKFwiLi9pbWFnZS92YWNhbmN5LWFkcy5wbmdcIiksXHJcbiAgICBtZXJjaGFudEFkczogcmVxdWlyZShcIi4vaW1hZ2UvbWVyY2hhbi1hZHMucG5nXCIpLFxyXG5cclxuICAgIGNhc2hfaWNvbjogcmVxdWlyZShcIi4vaW1hZ2UvcGF5bWVudF9nYXRld2F5cy9jYXJkLnBuZ1wiKSxcclxuICAgIHdpbmdfaWNvbjogcmVxdWlyZShcIi4vaW1hZ2UvcGF5bWVudF9nYXRld2F5cy93aW5nLnBuZ1wiKSxcclxuICAgIGFiYXBheV9pY29uOiByZXF1aXJlKFwiLi9pbWFnZS9wYXltZW50X2dhdGV3YXlzL2FiYXBheS5wbmdcIiksXHJcbiAgICB2aXNhY2FyZF9pY29uOiByZXF1aXJlKFwiLi9pbWFnZS9wYXltZW50X2dhdGV3YXlzL3Zpc2FfY2FyZC5wbmdcIiksXHJcblxyXG4gICAgZW1wdHlfY2FydDogcmVxdWlyZShcIi4vaW1hZ2UvZW1wdHktY2FydC5wbmdcIiksXHJcblxyXG4gICAgaXRlbU5vdEZvdW5kOiByZXF1aXJlKFwiLi9pbWFnZS9pdGVtTm90Rm91bmQucG5nXCIpLFxyXG5cclxuXHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBBc3NldHM7XHJcbiIsImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHtTdHlsZVNoZWV0LCBUZXh0fSBmcm9tIFwicmVhY3QtbmF0aXZlXCI7XHJcbmltcG9ydCB7QVBQRk9OVFN9IGZyb20gXCIuLi9hc3NldHMvRm9udENvbnN0YW50c1wiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRVRleHRWaWV3Tm9ybWFsTGFiZWwgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIHJldHVybiA8VGV4dCBzdHlsZT17W3N0eWxlc0xhYmxlLnRleHRMYWJsZSwgdGhpcy5wcm9wcy5zdHlsZSB8fCB7fV19Pnt0aGlzLnByb3BzLnRleHR9PC9UZXh0PjtcclxuICAgIH1cclxufVxyXG5jb25zdCBzdHlsZXNMYWJsZSA9IFN0eWxlU2hlZXQuY3JlYXRlKHtcclxuICAgIHRleHRMYWJsZToge1xyXG4gICAgICAgIG1hcmdpblN0YXJ0OiAxMCxcclxuICAgICAgICBtYXJnaW5FbmQ6IDEwLFxyXG4gICAgICAgIGNvbG9yOiBcIiMwMDBcIixcclxuICAgICAgICBmb250U2l6ZTogMTYsXHJcbiAgICAgICAgbWFyZ2luVG9wOiA4LFxyXG4gICAgICAgIG1hcmdpbkxlZnQ6IDIwLFxyXG4gICAgICAgIGZvbnRGYW1pbHk6IEFQUEZPTlRTLnJlZ3VsYXJcclxuICAgIH1cclxufSk7XHJcbiIsImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHtEaW1lbnNpb25zLCBTdHlsZVNoZWV0LCBUZXh0LFRvdWNoYWJsZU9wYWNpdHksIEltYWdlQmFja2dyb3VuZCwgVmlldyxJbWFnZX0gZnJvbSBcInJlYWN0LW5hdGl2ZVwiO1xyXG5pbXBvcnQgQXNzZXRzIGZyb20gXCIuLi9hc3NldHNcIjtcclxuLy9pbXBvcnQgSWNvbiBmcm9tICdyZWFjdC1uYXRpdmUtdmVjdG9yLWljb25zL0ZvbnRBd2Vzb21lJztcclxuLy9pbXBvcnQgTUljb24gZnJvbSAncmVhY3QtbmF0aXZlLXZlY3Rvci1pY29ucy9NYXRlcmlhbENvbW11bml0eUljb25zJztcclxuLy9pbXBvcnQgRkljb24gZnJvbSAncmVhY3QtbmF0aXZlLXZlY3Rvci1pY29ucy9Gb250aXN0byc7XHJcbi8vaW1wb3J0IEFudEljb24gIGZyb20gJ3JlYWN0LW5hdGl2ZS12ZWN0b3ItaWNvbnMvQW50RGVzaWduJztcclxuXHJcbmltcG9ydCB7QkFTRV9VUkwsIElOUl9TSE9SVF9TSUdOfSBmcm9tIFwiLi4vdXRpbHMvQ29uc3RhbnRzXCI7XHJcbmltcG9ydCB7QVBQRk9OVFMsIEVURm9udHN9IGZyb20gXCIuLi9hc3NldHMvRm9udENvbnN0YW50c1wiO1xyXG5pbXBvcnQge0FQUENPTE9SUywgRURDb2xvcnN9IGZyb20gXCIuLi9hc3NldHMvQ29sb3JzXCI7XHJcblxyXG4vL2ltcG9ydCBJbWFnZSBmcm9tIFwicmVhY3QtbmF0aXZlLWZhc3QtaW1hZ2VcIlxyXG5cclxuY29uc3Qge3dpZHRoLCBoZWlnaHR9ID0gRGltZW5zaW9ucy5nZXQoJ3dpbmRvdycpO1xyXG5jb25zdCBpdGVtV2lkdGggPSAod2lkdGggLSA2MCk7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQb3B1bGFyUmVzdGF1cmFudENhcmQgZXh0ZW5kcyBSZWFjdC5QdXJlQ29tcG9uZW50IHtcclxuICAgIHN0YXRlID0ge1xyXG4gICAgICAgIHJlc3RPYmpNb2RlbDogdGhpcy5wcm9wcy5yZXN0T2JqTW9kZWwsXHJcbiAgICAgICAgY3VycmVudEF2YWlsYWJsZURheTogdGhpcy5wcm9wcy5yZXN0T2JqTW9kZWwuY3VycmVudF9kZWxpdmVyeV9hdmFpbGFibGUsXHJcbiAgICB9O1xyXG4gICAgaXRlbVByZXNzID0gKCkgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BzLml0ZW1QcmVzcykge1xyXG4gICAgICAgICAgICB0aGlzLnByb3BzLml0ZW1QcmVzcyhcclxuICAgICAgICAgICAgICAgIFwiUmVzdGF1cmFudFwiLFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vcmVmcmVzaDogdGhpcy5yZWZyZXNoU2NyZWVuLFxyXG4gICAgICAgICAgICAgICAgICAgIHJlc3RJZDogdGhpcy5zdGF0ZS5yZXN0T2JqTW9kZWwuaWQsXHJcbiAgICAgICAgICAgICAgICAgICAgZGlzdGFudDogdGhpcy5zdGF0ZS5yZXN0T2JqTW9kZWwuZGlzdGFudFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAgICAgc3VwZXIocHJvcHMpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMobmV4dFByb3BzKSB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7cmVzdE9iak1vZGVsOiBuZXh0UHJvcHMucmVzdE9iak1vZGVsfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbm90QXZhaWxhYmxlTWVzc2FnZUJveChkZWxpdmVyeU9iamVjdCkge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxWaWV3IHN0eWxlPXt7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogXCJhYnNvbHV0ZVwiLFxyXG4gICAgICAgICAgICAgICAgdG9wOiAwLFxyXG4gICAgICAgICAgICAgICAgbGVmdDogMCxcclxuICAgICAgICAgICAgICAgIHdpZHRoOiBcIjEwMCVcIixcclxuICAgICAgICAgICAgICAgIGhlaWdodDogMTIwLFxyXG4gICAgICAgICAgICAgICAgLy8gYmFja2dyb3VuZENvbG9yOiBcInJnYmEoMSwxLDEsMC42KVwiLFxyXG4gICAgICAgICAgICAgICAgYWxpZ25JdGVtczogXCJjZW50ZXJcIixcclxuICAgICAgICAgICAgICAgIGFsaWduQ29udGVudDogXCJjZW50ZXJcIixcclxuICAgICAgICAgICAgICAgIGp1c3RpZnlDb250ZW50OiBcImNlbnRlclwiLFxyXG4gICAgICAgICAgICAgICAgekluZGV4OiA5LFxyXG4gICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8VmlldyBzdHlsZT17c3R5bGVzLnVuYXZhaWxhYmxlTWVzc2FnZX0+XHJcbiAgICAgICAgICAgICAgICAgICAgPFRleHQgc3R5bGU9e3N0eWxlcy51bmF2YWlsYWJsZU1lc3NhZ2VUZXh0Qm94fT57XCJEZWxpdmVyeSBIb3Vyc1wifTwvVGV4dD5cclxuICAgICAgICAgICAgICAgICAgICA8VGV4dCBzdHlsZT17c3R5bGVzLnVuYXZhaWxhYmxlTWVzc2FnZVRleHRCb3h9PntkZWxpdmVyeU9iamVjdCA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiXCIgKyBkZWxpdmVyeU9iamVjdC5zdGFydF90aW1lICsgXCIgLSBcIiArIGRlbGl2ZXJ5T2JqZWN0LmVuZF90aW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDogXCIwMDowMGFtIC0gMDA6MDBwbVwifTwvVGV4dD5cclxuICAgICAgICAgICAgICAgIDwvVmlldz5cclxuXHJcbiAgICAgICAgICAgIDwvVmlldz4pO1xyXG4gICAgfVxyXG5cclxuICAgIHNob3dQcmljZUxldmVsKHByaWNlX2xldmVsKSB7XHJcbiAgICAgICAgbGV0IHZpZXdzID0gW107XHJcbiAgICAgICAgcHJpY2VfbGV2ZWwgPSBwcmljZV9sZXZlbCA9PSAwIHx8ICFwcmljZV9sZXZlbCA/IDEgOiBwcmljZV9sZXZlbDtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IDM7IGkrKykge1xyXG4gICAgICAgICAgICB2aWV3cy5wdXNoKFxyXG4gICAgICAgICAgICAgICAgLy8gPEljb24gbmFtZT17XCJkb2xsYXJcIn0gY29sb3I9e3ByaWNlX2xldmVsID4gaSA/IFwiI2ZmOTYzZFwiIDogXCIjYjJiMmIyXCJ9IHNpemU9ezE0fS8+XHJcbiAgICAgICAgICAgICAgICA8VGV4dCBrZXk9e2l9PntcIiRcIn08L1RleHQ+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8VmlldyBzdHlsZT17e2ZsZXhEaXJlY3Rpb246IFwicm93XCIsIG1hcmdpblRvcDogMn19PlxyXG4gICAgICAgICAgICAgICAge3ZpZXdzLm1hcCgodmlldykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB2aWV3O1xyXG4gICAgICAgICAgICAgICAgfSl9XHJcbiAgICAgICAgICAgICAgICA8VGV4dD57XCIgXCJ9PC9UZXh0PlxyXG4gICAgICAgICAgICA8L1ZpZXc+KTtcclxuICAgIH1cclxuXHJcbiAgICBzaG93RGlzdGFudChkaXN0YW50KSB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPFZpZXdcclxuICAgICAgICAgICAgICAgIHN0eWxlPXt7ZmxleERpcmVjdGlvbjogXCJyb3dcIiwgZmxleDoxfX1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgey8qPEljb24gbmFtZT17XCJtYXAtbWFya2VyXCJ9IHNpemU9ezE1fSBjb2xvcj17XCJncmF5XCJ9IHN0eWxlPXt7bWFyZ2luUmlnaHQ6IDV9fS8+Ki99XHJcbiAgICAgICAgICAgICAgICA8VGV4dD57XCI/XCJ9PC9UZXh0PlxyXG4gICAgICAgICAgICAgICAgPFRleHRcclxuICAgICAgICAgICAgICAgICAgICBudW1iZXJPZkxpbmVzPXsxfVxyXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlPXtzdHlsZXMuZGlzdGFudH1cclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICB7cGFyc2VGbG9hdChkaXN0YW50KS50b0ZpeGVkKDIpLnRvU3RyaW5nKCkgKyBcImttXCJ9XHJcbiAgICAgICAgICAgICAgICA8L1RleHQ+XHJcblxyXG4gICAgICAgICAgICA8L1ZpZXc+KVxyXG4gICAgfVxyXG5cclxuICAgIHNob3dEZWxpdmVyeUNoYXJnZShkZWxpdmVyeV9jaGFyZ2VzKSB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPFZpZXdcclxuICAgICAgICAgICAgICAgIHN0eWxlPXt7XHJcbiAgICAgICAgICAgICAgICAgICAgZmxleERpcmVjdGlvbjogXCJyb3dcIixcclxuICAgICAgICAgICAgICAgICAgICBmbGV4OjEsXHJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ25JdGVtczpcImZsZXgtZW5kXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgYWxpZ25Db250ZW50OlwiZmxleC1lbmRcIixcclxuICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5Q29udGVudDpcImZsZXgtZW5kXCJcclxuICAgICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIHsvKjxJY29uIG5hbWU9e1wibW90b3JjeWNsZVwifSBzaXplPXsxNX0gY29sb3I9e1wiZ3JheVwifSBzdHlsZT17e21hcmdpblJpZ2h0OiA1fX0vPiovfVxyXG4gICAgICAgICAgICAgICAgPFRleHQ+e1wiP1wifTwvVGV4dD5cclxuICAgICAgICAgICAgICAgIDxUZXh0XHJcbiAgICAgICAgICAgICAgICAgICAgbnVtYmVyT2ZMaW5lcz17MX1cclxuICAgICAgICAgICAgICAgICAgICBzdHlsZT17e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb250U2l6ZTogMTIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnRGYW1pbHk6QVBQRk9OVFMubGlnaHRcclxuICAgICAgICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIHtcIkRlbGl2ZXJ5IGZlZSA6IFwiICsgSU5SX1NIT1JUX1NJR04gKyBkZWxpdmVyeV9jaGFyZ2VzfVxyXG4gICAgICAgICAgICAgICAgPC9UZXh0PlxyXG4gICAgICAgICAgICA8L1ZpZXc+KTtcclxuICAgIH1cclxuXHJcbiAgICBzaG93TWluaW11bUNoYXJnZShtaW51bUNoYXJnZSkge1xyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8Vmlld1xyXG4gICAgICAgICAgICAgICAgc3R5bGU9e3tcclxuICAgICAgICAgICAgICAgICAgICBmbGV4RGlyZWN0aW9uOiBcInJvd1wiLFxyXG4gICAgICAgICAgICAgICAgICAgIGZsZXg6MSxcclxuICAgICAgICAgICAgICAgICAgICBhbGlnbkl0ZW1zOlwiZmxleC1lbmRcIixcclxuICAgICAgICAgICAgICAgICAgICBhbGlnbkNvbnRlbnQ6XCJmbGV4LWVuZFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIGp1c3RpZnlDb250ZW50OlwiZmxleC1lbmRcIlxyXG4gICAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgey8qPEljb24gbmFtZT17XCJkb2xsYXJcIn0gc2l6ZT17MTV9IGNvbG9yPXtcImdyYXlcIn0gc3R5bGU9e3ttYXJnaW5MZWZ0OiA1LCBtYXJnaW5SaWdodDogNX19Lz4qL31cclxuICAgICAgICAgICAgICAgIDxUZXh0PntcIj9cIn08L1RleHQ+XHJcbiAgICAgICAgICAgICAgICA8VGV4dFxyXG4gICAgICAgICAgICAgICAgICAgIG51bWJlck9mTGluZXM9ezF9XHJcbiAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3N0eWxlcy5taW5pbXVtQ2hhcmdlfVxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIHtcIk1pbmltdW0gT3JkZXIgOiBcIiArIElOUl9TSE9SVF9TSUdOICsgbWludW1DaGFyZ2V9XHJcbiAgICAgICAgICAgICAgICA8L1RleHQ+XHJcbiAgICAgICAgICAgIDwvVmlldz4pO1xyXG4gICAgfVxyXG5cclxuICAgIHNob3dEZWxpdmVyeVRpbWUobWluLCBtYXgpIHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8Vmlld1xyXG4gICAgICAgICAgICAgICAgc3R5bGU9e3tmbGV4RGlyZWN0aW9uOiBcInJvd1wiLCBmbGV4OjF9fVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICB7Lyo8SWNvbiBuYW1lPXtcImNsb2NrLW9cIn0gc2l6ZT17MTV9IGNvbG9yPXtcImdyYXlcIn0gc3R5bGU9e3ttYXJnaW5SaWdodDogMn19Lz4qL31cclxuICAgICAgICAgICAgICAgIDxUZXh0PntcIj9cIn08L1RleHQ+XHJcbiAgICAgICAgICAgICAgICA8VGV4dFxyXG4gICAgICAgICAgICAgICAgICAgIG51bWJlck9mTGluZXM9ezF9XHJcbiAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3N0eWxlcy5kZWxpdmVyeVRpbWV9XHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAge21pbiArIFwiIC0gXCIgKyBtYXggKyBcIiBtaW5cIn1cclxuICAgICAgICAgICAgICAgIDwvVGV4dD5cclxuXHJcbiAgICAgICAgICAgIDwvVmlldz4pO1xyXG4gICAgfVxyXG5cclxuICAgIHNob3dBdmFpbGFibGUoKSB7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnJlc3RPYmpNb2RlbC50ZW1wb3JhcnlfY2xvc2UpIHtcclxuICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgIDxWaWV3IHN0eWxlPXtzdHlsZXMudGVtcG9yYXJpbHlDbG9zZU92ZXJsYXl9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxWaWV3IHN0eWxlPXtzdHlsZXMudGVtcG9yYXJpbHlDbG9zZU1lc3NhZ2V9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8VGV4dCBzdHlsZT17c3R5bGVzLnVuYXZhaWxhYmxlTWVzc2FnZVRleHRCb3h9PntcIlRlbXBvcmFyaWx5XCJ9PC9UZXh0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8VGV4dCBzdHlsZT17c3R5bGVzLnVuYXZhaWxhYmxlTWVzc2FnZVRleHRCb3h9PntcIkNsb3NlZFwifTwvVGV4dD5cclxuICAgICAgICAgICAgICAgICAgICA8L1ZpZXc+XHJcbiAgICAgICAgICAgICAgICA8L1ZpZXc+XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUuY3VycmVudEF2YWlsYWJsZURheSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5ub3RBdmFpbGFibGVNZXNzYWdlQm94KG51bGwpXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXRoaXMuc3RhdGUuY3VycmVudEF2YWlsYWJsZURheS5kZWxpdmVyeV9hdmFpbGFibGUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMubm90QXZhaWxhYmxlTWVzc2FnZUJveCh0aGlzLnN0YXRlLmN1cnJlbnRBdmFpbGFibGVEYXkpXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGRvdChub3NwYWNlKXtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgPFRleHQgc3R5bGU9e3twYWRkaW5nSG9yaXpvbnRhbDogbm9zcGFjZSA/IDIgOiA0LG1hcmdpblRvcDotMyxmb250V2VpZ2h0OiBcImJvbGRcIixjb2xvcjpBUFBDT0xPUlMuZ3JvdXAyUmlnaHQyfX0+e1wiLlwifTwvVGV4dD5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8VG91Y2hhYmxlT3BhY2l0eVxyXG4gICAgICAgICAgICAgICAgb25QcmVzcz17dGhpcy5pdGVtUHJlc3N9XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxWaWV3IHN0eWxlPXtbc3R5bGVzLmNvbnRhaW5lciwgdGhpcy5wcm9wcy5ob3Jpem9udGFsID8ge21hcmdpblRvcDo1LG1hcmdpblJpZ2h0OiA1LG1hcmdpbkxlZnQ6IDUsd2lkdGg6aXRlbVdpZHRofSA6IHttYXJnaW5SaWdodDogMH0sXHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BzLnN0eWxlIHx8IHt9XX0+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIDxWaWV3IHN0eWxlPXtzdHlsZXMucmVzdGF1cmFudEJhbm5lckJveH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxJbWFnZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc291cmNlPXt0aGlzLnN0YXRlLnJlc3RPYmpNb2RlbC5pbWFnZSA/IHt1cmk6IEJBU0VfVVJMICsgdGhpcy5zdGF0ZS5yZXN0T2JqTW9kZWwuaW1hZ2V9IDogbnVsbH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXtbc3R5bGVzLnJlc3RhdXJhbnRCYW5uZXIsIC8qe29wYWNpdHk6IDAuNn0qL119XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNpemVNb2RlPXtcImNvdmVyXCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9WaWV3PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICB7Lyp0aGlzLnNob3dBdmFpbGFibGUoKSovfVxyXG4gICAgICAgICAgICAgICAgICAgIDxWaWV3XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXtzdHlsZXMucmVzdGF1cmFudExvZ29Cb3h9XHJcbiAgICAgICAgICAgICAgICAgICAgPlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEltYWdlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzb3VyY2U9e3RoaXMuc3RhdGUucmVzdE9iak1vZGVsLmxvZ28gPyB7dXJpOkJBU0VfVVJMICsgdGhpcy5zdGF0ZS5yZXN0T2JqTW9kZWwubG9nb30gOiBudWxsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e1tzdHlsZXMucmVzdGF1cmFudExvZ28sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5jdXJyZW50QXZhaWxhYmxlRGF5ID8gKHRoaXMuc3RhdGUuY3VycmVudEF2YWlsYWJsZURheS5kZWxpdmVyeV9hdmFpbGFibGUgPyB7b3BhY2l0eToxfSA6IHtvcGFjaXR5OjF9KSA6IHtvcGFjaXR5OjF9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBdfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzaXplTW9kZT17XCJjb250YWluXCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7Lyo8VmlldyBzdHlsZT17Ki99XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsvKih0aGlzLnN0YXRlLmN1cnJlbnRBdmFpbGFibGVEYXkgPyAoKi99XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsvKnRoaXMuc3RhdGUuY3VycmVudEF2YWlsYWJsZURheS5kZWxpdmVyeV9hdmFpbGFibGUgPyBzdHlsZXMuYXZhaWxhYmxlIDogc3R5bGVzLnVuYXZhaWxhYmxlKi99XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsvKikqL31cclxuICAgICAgICAgICAgICAgICAgICAgICAgey8qOiBzdHlsZXMudW5hdmFpbGFibGUpKi99XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsvKn0vPiovfVxyXG4gICAgICAgICAgICAgICAgICAgIDwvVmlldz5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgey8qPFZpZXcqL31cclxuICAgICAgICAgICAgICAgICAgICAgICAgey8qc3R5bGU9e3N0eWxlcy5wcm9tb3Rpb259PiovfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7LyovISo8VGV4dCohLyovfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7LyovISpzdHlsZT17W3N0eWxlcy5yaWdodEJhbm5lck92ZXJsYXksc3R5bGVzLmRpc2NvdW50T3ZlcmxheSx0aGlzLnByb3BzLm9mZmVyQmFubmVyU3R5bGUgfHwge31dfSohLyovfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7LyovISo+e1wiNjAlIE9GRlwifTwvVGV4dD4qIS8qL31cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsvKnt0aGlzLnN0YXRlLnJlc3RPYmpNb2RlbC5kaXN0YW50IDwgMSAmJiB0aGlzLnN0YXRlLnJlc3RPYmpNb2RlbC5kaXN0YW50ID4gMC4wMCA/Ki99XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Lyo8VGV4dCovfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsvKnN0eWxlPXtbc3R5bGVzLnJpZ2h0QmFubmVyT3ZlcmxheSxzdHlsZXMuZnJlZXNoaXBPdmVybGF5LHRoaXMucHJvcHMuZnJlZXNoaXBCYW5uZXJTdHlsZSB8fCB7fV19Ki99XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Lyo+e1wiRlJFRVNISVBcIn08L1RleHQ+Ki99XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Lyo6IG51bGx9Ki99XHJcbiAgICAgICAgICAgICAgICAgICAgey8qPC9WaWV3PiovfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICB7Lyp7dGhpcy5zdGF0ZS5yZXN0T2JqTW9kZWwuZGlzdGFudCA8IDEgJiYgdGhpcy5zdGF0ZS5yZXN0T2JqTW9kZWwuZGlzdGFudCA+IDAuMDAgPyovfVxyXG4gICAgICAgICAgICAgICAgICAgIDxWaWV3IHN0eWxlPXt7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOlwiYWJzb2x1dGVcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9wOi0zLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZWZ0Oi05XHJcbiAgICAgICAgICAgICAgICAgICAgfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxJbWFnZUJhY2tncm91bmQgc291cmNlPXtBc3NldHMubGVmdFRvcFJpYmJvbn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNpemVNb2RlPXtcImNvbnRhaW5cIn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5Q29udGVudDogXCJjZW50ZXJcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6ODUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDo0NSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdG9wOi0zLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyByaWdodDotMlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VGV4dFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17W3N0eWxlcy5yaWdodEJhbm5lck92ZXJsYXksc3R5bGVzLmZyZWVzaGlwT3ZlcmxheSx0aGlzLnByb3BzLmZyZWVzaGlwQmFubmVyU3R5bGUgfHwge31dfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID57XCJGUkVFU0hJUFwifTwvVGV4dD5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvSW1hZ2VCYWNrZ3JvdW5kPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvVmlldz5cclxuICAgICAgICAgICAgICAgICAgICB7Lyo6IG51bGx9Ki99XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIDxWaWV3IHN0eWxlPXt7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOlwiYWJzb2x1dGVcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9wOjAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJpZ2h0OjBcclxuICAgICAgICAgICAgICAgICAgICB9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEltYWdlQmFja2dyb3VuZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzb3VyY2U9e0Fzc2V0cy5yaWdodFRvcFJpYmJvbn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzaXplTW9kZT17XCJjb250YWluXCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXt7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5Q29udGVudDogXCJjZW50ZXJcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOjQ1LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OjQ1LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9wOi0zLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmlnaHQ6LTJcclxuICAgICAgICAgICAgICAgICAgICAgICAgfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VGV4dFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bWJlck9mTGluZXM9ezF9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3tcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246XCJyZWxhdGl2ZVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b3A6LTgsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJpZ2h0Oi04LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb250U2l6ZToxMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6XCIjZmZGXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRleHRBbGlnbjpcImNlbnRlclwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06W3tyb3RhdGU6JzQ1ZGVnJ31dLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb250RmFtaWx5OkFQUEZPTlRTLnJlZ3VsYXJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPntcIlByb21vXCJ9PC9UZXh0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0ltYWdlQmFja2dyb3VuZD5cclxuICAgICAgICAgICAgICAgICAgICA8L1ZpZXc+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIDxWaWV3IHN0eWxlPXtzdHlsZXMucmVzdGF1cmFudEluZm9Sb3dCb3h9PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPFZpZXdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXt7ZmxleERpcmVjdGlvbjogXCJyb3dcIiwgbWFyZ2luVG9wOiAwLCBtYXJnaW5Cb3R0b206IDB9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7LyogUmVzdGF1cmFudCBOYW1lICovfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFZpZXdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17e2ZsZXg6NixmbGV4RGlyZWN0aW9uOiBcInJvd1wiLCB3aWR0aDogXCIxMDAlXCJ9fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsvKjxJbWFnZSovfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsvKnNvdXJjZT17QXNzZXRzLnZlcmlmaWVkfSBzdHlsZT17c3R5bGVzLnZlcmlmaWVkSWNvbn0qL31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7LypyZXNpemVNb2RlPXtcImNvbnRhaW5cIn0qL31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7LyovPiovfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxUZXh0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXtzdHlsZXMucmVzdGF1cmFudE5hbWV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bWJlck9mTGluZXM9ezF9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5zdGF0ZS5yZXN0T2JqTW9kZWwubmFtZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1RleHQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1ZpZXc+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgey8qIFJhdGUgKi99XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8Vmlld1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXtzdHlsZXMucmF0aW5nUmlnaHR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgey8qPEljb24gbmFtZT17XCJzdGFyXCJ9IHNpemU9ezE1fSBjb2xvcj17XCIjZmY5NjNkXCJ9IHN0eWxlPXt7bWFyZ2luUmlnaHQ6IDV9fS8+Ki99XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRleHQ+e1wiP1wifTwvVGV4dD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VGV4dFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBudW1iZXJPZkxpbmVzPXsxfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17c3R5bGVzLnJhdGluZ051bWJlcn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtwYXJzZUZsb2F0KHRoaXMuc3RhdGUucmVzdE9iak1vZGVsLnJhdGluZykudG9GaXhlZCgxKS50b1N0cmluZygpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvVGV4dD5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1ZpZXc+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L1ZpZXc+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8VmlldyBzdHlsZT17e2ZsZXhEaXJlY3Rpb246IFwicm93XCIsb3ZlcmZsb3c6XCJoaWRkZW5cIiwgd2lkdGg6IFwiMTAwJVwifX0+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFZpZXcgc3R5bGU9e3tmbGV4RGlyZWN0aW9uOlwicm93XCIscGFkZGluZ1ZlcnRpY2FsOjAscG9zaXRpb246XCJyZWxhdGl2ZVwiLHRvcDoxLGp1c3RpZnlDb250ZW50OlwiY2VudGVyXCJ9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5zaG93UHJpY2VMZXZlbCh0aGlzLnN0YXRlLnJlc3RPYmpNb2RlbC5wcmljZV9sZXZlbCl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1ZpZXc+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VGV4dFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bWJlck9mTGluZXM9ezF9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3N0eWxlcy5pdGVtRGVzY3JpcHRpb259XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1wiTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdFwifVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9UZXh0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L1ZpZXc+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8VmlldyBzdHlsZT17e2ZsZXhEaXJlY3Rpb246XCJyb3dcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGJvcmRlcldpZHRoOjEsYm9yZGVyQ29sb3I6XCJyZWRcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbkJvdHRvbTo1fX0+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFZpZXcgc3R5bGU9e3tcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmbGV4OiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZsZXhEaXJlY3Rpb246IFwicm93XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxpZ25Db250ZW50OiBcImZsZXgtc3RhcnRcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGlnbkl0ZW1zOiBcImZsZXgtc3RhcnRcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5Q29udGVudDogXCJmbGV4LXN0YXJ0XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmxleFdyYXA6XCJub3dyYXBcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGJvcmRlcldpZHRoOjEsYm9yZGVyQ29sb3I6XCJibHVlXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsvKjxGSWNvbiBuYW1lPXtcIm1vdG9yY3ljbGVcIn0gc2l6ZT17MjB9IGNvbG9yPXtBUFBDT0xPUlMuZ3JvdXAyTGVmdDJ9IHN0eWxlPXt7bWFyZ2luUmlnaHQ6NSxwb3NpdGlvbjpcImFic29sdXRlXCJ9fS8+Ki99XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRleHQ+e1wiP1wifTwvVGV4dD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5ob3Jpem9udGFsID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFZpZXcgc3R5bGU9e3tcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZsZXhEaXJlY3Rpb246IFwicm93XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGlnbkNvbnRlbnQ6IFwiZmxleC1zdGFydFwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxpZ25JdGVtczogXCJmbGV4LXN0YXJ0XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5Q29udGVudDogXCJmbGV4LXN0YXJ0XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmbGV4V3JhcDpcIm5vd3JhcFwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZ0xlZnQ6MjAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBib3JkZXJXaWR0aDoxLGJvcmRlckNvbG9yOlwiYmx1ZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRleHQgbnVtYmVyT2ZMaW5lcz17MX0gc3R5bGU9e1tzdHlsZXMuZGVsaXZlcnlDaGFyZ2VSb3dUZXh0XX0+e1wiRGVsaXZlcnkgXCJ9PC9UZXh0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRleHQgbnVtYmVyT2ZMaW5lcz17MX0gc3R5bGU9e1tzdHlsZXMuZGVsaXZlcnlDaGFyZ2VSb3dUZXh0XX0+e0lOUl9TSE9SVF9TSUdOICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhcnNlRmxvYXQodGhpcy5zdGF0ZS5yZXN0T2JqTW9kZWwuZGVsaXZlcnlfY2hhcmdlcyB8fCAwKS50b0ZpeGVkKDEpfTwvVGV4dD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9WaWV3PjpcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxUZXh0IG51bWJlck9mTGluZXM9ezF9IHN0eWxlPXtbc3R5bGVzLmRlbGl2ZXJ5Q2hhcmdlUm93VGV4dCx7cGFkZGluZ0xlZnQ6MjB9XX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XCJEZWxpdmVyeSBcIiArSU5SX1NIT1JUX1NJR04gKyBwYXJzZUZsb2F0KHRoaXMuc3RhdGUucmVzdE9iak1vZGVsLmRlbGl2ZXJ5X2NoYXJnZXMgfHwgMCkudG9GaXhlZCgxKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9UZXh0Pn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7Lyp7dGhpcy5kb3QoKX0qL31cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1ZpZXc+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VmlldyBzdHlsZT17e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZsZXg6MSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmbGV4RGlyZWN0aW9uOlwicm93XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxpZ25Db250ZW50OlwiY2VudGVyXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWxpZ25JdGVtczpcImNlbnRlclwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGp1c3RpZnlDb250ZW50OlwiY2VudGVyXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmxleFdyYXA6XCJub3dyYXBcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBib3JkZXJXaWR0aDoxLGJvcmRlckNvbG9yOlwieWVsbG93XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH19PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxUZXh0IHN0eWxlPXtzdHlsZXMuZGVsaXZlcnlUaW1lVGV4dH0+e1wiMzDigJQ0NSBtaW5cIn08L1RleHQ+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9WaWV3PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxWaWV3IHN0eWxlPXt7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmxleDoxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZsZXhEaXJlY3Rpb246XCJyb3dcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGlnbkNvbnRlbnQ6XCJmbGV4LWVuZFwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsaWduSXRlbXM6XCJmbGV4LWVuZFwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGp1c3RpZnlDb250ZW50OlwiZmxleC1lbmRcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmbGV4V3JhcDpcIm5vd3JhcFwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGJvcmRlcldpZHRoOjEsYm9yZGVyQ29sb3I6XCJ5ZWxsb3dcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRleHRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3N0eWxlcy5taW5PcmRlclRleHR9PntcIk1pbi5PcmRlciBcIiArIElOUl9TSE9SVF9TSUdOICsgZ2xvYmFsLm1pbmltdW1PcmRlcn08L1RleHQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1ZpZXc+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvVmlldz5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxWaWV3IHN0eWxlPXt7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmbGV4RGlyZWN0aW9uOlwicm93XCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3JkZXJXaWR0aDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlclJhZGl1czogNSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlclN0eWxlOiAnZGFzaGVkJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlckNvbG9yOiBcIiNmZmMzYzZcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcjogXCIjZmZmOWYzXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nVmVydGljYWw6IDUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nSG9yaXpvbnRhbDogMTAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH19PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRleHQgc3R5bGU9e1t7Zm9udEZhbWlseTpBUFBGT05UUy5ib2xkLGNvbG9yOkFQUENPTE9SUy5ncm91cDFNYWlufV19PntcIlNwZWNpYWwgZGF5ISBcIn08L1RleHQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VGV4dCBzdHlsZT17W3tmb250RmFtaWx5OkFQUEZPTlRTLmxpZ2h0fV19PntcIiBmcmVlIERlbGl2ZXJ5IGFuZCBEaXNjb3VudCAzMCVcIn08L1RleHQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvVmlldz5cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgPC9WaWV3PlxyXG4gICAgICAgICAgICAgICAgPC9WaWV3PlxyXG5cclxuICAgICAgICAgICAgPC9Ub3VjaGFibGVPcGFjaXR5PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuXHJcbmNvbnN0IHN0eWxlcyA9IFN0eWxlU2hlZXQuY3JlYXRlKHtcclxuXHJcbiAgICBjb250YWluZXI6IHtcclxuICAgICAgICBtYXJnaW5Cb3R0b206IDEzLFxyXG4gICAgICAgIGZsZXg6IDEsXHJcbiAgICAgICAgYmFja2dyb3VuZENvbG9yOiBcIndoaXRlXCIsXHJcbiAgICAgICAgYm9yZGVyUmFkaXVzOiA1LFxyXG4gICAgICAgIHBhZGRpbmdMZWZ0OiAwLFxyXG4gICAgICAgIHBhZGRpbmdSaWdodDogMCxcclxuICAgICAgICBwYWRkaW5nQm90dG9tOiA1LFxyXG5cclxuICAgICAgICAvLyBib3JkZXJXaWR0aDogOCxcclxuICAgICAgICAvLyBib3JkZXJDb2xvcjpcInRyYW5zcGFyZW50XCIsXHJcbiAgICAgICAgc2hhZG93Q29sb3I6IFwiIzAwMFwiLFxyXG4gICAgICAgIHNoYWRvd09mZnNldDoge1xyXG4gICAgICAgICAgICB3aWR0aDogMCxcclxuICAgICAgICAgICAgaGVpZ2h0OiAzLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc2hhZG93T3BhY2l0eTogMSxcclxuICAgICAgICBzaGFkb3dSYWRpdXM6IDksXHJcblxyXG4gICAgICAgIGVsZXZhdGlvbjogMixcclxuXHJcbiAgICB9LFxyXG4gICAgcmVzdGF1cmFudEJhbm5lckJveDpcclxuICAgIHtcclxuICAgICAgICB3aWR0aDogXCIxMDAlXCIsXHJcbiAgICAgICAgaGVpZ2h0OiAxMjAsXHJcbiAgICAgICAgYmFja2dyb3VuZENvbG9yOiBcIiMwMDBcIixcclxuICAgICAgICBib3JkZXJUb3BMZWZ0UmFkaXVzOjUsXHJcbiAgICAgICAgYm9yZGVyVG9wUmlnaHRSYWRpdXM6NSxcclxuXHJcbiAgICB9LFxyXG4gICAgcmVzdGF1cmFudEJhbm5lcjoge1xyXG4gICAgICAgIHdpZHRoOiBcIjEwMCVcIixcclxuICAgICAgICBoZWlnaHQ6IDEyMCxcclxuICAgICAgICAvLyBtYXJnaW5Ub3A6IDUsXHJcbiAgICAgICAgYWxpZ25TZWxmOiBcImNlbnRlclwiLFxyXG4gICAgICAgIC8vIGJvcmRlclJhZGl1czogNSxcclxuXHJcbiAgICAgICAgYm9yZGVyVG9wTGVmdFJhZGl1czo1LFxyXG4gICAgICAgIGJvcmRlclRvcFJpZ2h0UmFkaXVzOjUsXHJcbiAgICB9LFxyXG4gICAgcmVzdGF1cmFudExvZ29Cb3g6IHtcclxuICAgICAgICB3aWR0aDogNjAsXHJcbiAgICAgICAgaGVpZ2h0OiA2MCxcclxuICAgICAgICBhbGlnblNlbGY6IFwiY2VudGVyXCIsXHJcbiAgICAgICAgcG9zaXRpb246IFwiYWJzb2x1dGVcIixcclxuICAgICAgICBsZWZ0OiA1LFxyXG4gICAgICAgIHRvcDogNTUsXHJcbiAgICAgICAgb3ZlcmZsb3c6XCJoaWRkZW5cIixcclxuICAgICAgICBib3JkZXJSYWRpdXM6IDQwLFxyXG4gICAgICAgIGJvcmRlckNvbG9yOiBcIiNGRkZcIixcclxuICAgICAgICBib3JkZXJXaWR0aDogMSxcclxuICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6IFwiIzAwMFwiLC8vIFwicmdiYSgwLDAsMCwwLjMpXCIsXHJcblxyXG4gICAgICAgIHNoYWRvd0NvbG9yOiBcIiMwMDBcIixcclxuICAgICAgICBzaGFkb3dPZmZzZXQ6IHtcclxuICAgICAgICAgICAgd2lkdGg6IDAsXHJcbiAgICAgICAgICAgIGhlaWdodDogNyxcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNoYWRvd09wYWNpdHk6IDAuOSxcclxuICAgICAgICBzaGFkb3dSYWRpdXM6IDksXHJcblxyXG4gICAgICAgIGVsZXZhdGlvbjogMTUsXHJcbiAgICB9LFxyXG4gICAgcmVzdGF1cmFudExvZ286IHtcclxuICAgICAgICB3aWR0aDogXCIxMDAlXCIsXHJcbiAgICAgICAgaGVpZ2h0OiBcIjEwMCVcIixcclxuICAgICAgICBhbGlnblNlbGY6IFwiY2VudGVyXCIsXHJcbiAgICB9LFxyXG4gICAgcmlnaHRCYW5uZXJPdmVybGF5OiB7XHJcbiAgICAgICAgLy8gd2lkdGg6NzgsXHJcbiAgICAgICAgLy8gcGFkZGluZ0hvcml6b250YWw6IDUsXHJcbiAgICAgICAgLy8gcGFkZGluZ1ZlcnRpY2FsOjIsXHJcbiAgICAgICAgY29sb3I6IFwid2hpdGVcIixcclxuICAgICAgICAvLyBtYXJnaW5Cb3R0b206IDUsXHJcbiAgICAgICAgZm9udFNpemU6MTIsXHJcbiAgICAgICAgcGFkZGluZ0xlZnQ6MTUsXHJcbiAgICAgICAgLy8gYm9yZGVyQm90dG9tUmlnaHRSYWRpdXM6IDEwLFxyXG4gICAgICAgIC8vIGJvcmRlclRvcFJpZ2h0UmFkaXVzOjEwLFxyXG4gICAgICAgIGZvbnRGYW1pbHk6QVBQRk9OVFMucmVndWxhcixcclxuICAgICAgICAvLyBwb3NpdGlvbjpcInJlbGF0aXZlXCIsXHJcbiAgICAgICAgYm90dG9tOjEzLFxyXG4gICAgICAgIGp1c3RpZnlDb250ZW50OlwiZmxleC1zdGFydFwiXHJcblxyXG4gICAgfSxcclxuICAgIGRpc2NvdW50T3ZlcmxheTp7XHJcbiAgICAgICAgYmFja2dyb3VuZENvbG9yOiBBUFBDT0xPUlMuZ3JvdXAxTWFpbixcclxuICAgIH0sXHJcbiAgICBmcmVlc2hpcE92ZXJsYXk6IHtcclxuICAgICAgICAvLyBiYWNrZ3JvdW5kQ29sb3I6IFwiI2ZmMzdmY1wiLFxyXG4gICAgfSxcclxuXHJcbiAgICByZXN0YXVyYW50TmFtZToge1xyXG4gICAgICAgIGZvbnRGYW1pbHk6IEFQUEZPTlRTLmJvbGQsXHJcbiAgICAgICAgZm9udFNpemU6IDE2LFxyXG4gICAgfSxcclxuICAgIHJhdGluZ1JpZ2h0OiB7XHJcbiAgICAgICAgZmxleERpcmVjdGlvbjogXCJyb3dcIixcclxuICAgICAgICAvLyB3aWR0aDogXCJhdXRvXCIsXHJcbiAgICAgICAgLy8gcG9zaXRpb246IFwiYWJzb2x1dGVcIixcclxuICAgICAgICAvLyByaWdodDogMCxcclxuICAgICAgICAvLyB0b3A6IDRcclxuICAgICAgICBmbGV4OjEsXHJcbiAgICAgICAgcGFkZGluZ1ZlcnRpY2FsOjQsXHJcbiAgICAgICAganVzdGlmeUNvbnRlbnQ6XCJmbGV4LWVuZFwiLFxyXG4gICAgICAgIGFsaWduSXRlbXM6XCJmbGV4LWVuZFwiLFxyXG4gICAgICAgIGFsaWduQ29udGVudDpcImZsZXgtZW5kXCIsXHJcbiAgICB9LFxyXG4gICAgcmVzdGF1cmFudEluZm9Sb3dCb3g6IHtcclxuICAgICAgICBtYXJnaW5MZWZ0OiA1LFxyXG4gICAgICAgIG1hcmdpblJpZ2h0OiA1LFxyXG4gICAgICAgIG1hcmdpblRvcDogNVxyXG4gICAgfSxcclxuICAgIHZlcmlmaWVkSWNvbjoge1xyXG4gICAgICAgIHdpZHRoOiAyMCwgaGVpZ2h0OiAyMCwgcG9zaXRpb246IFwicmVsYXRpdmVcIiwgbWFyZ2luVG9wOiAwLCBtYXJnaW5MZWZ0OiAtNVxyXG4gICAgfSxcclxuICAgIHJlc3RhdXJhbnRJbmZvUm93T25lOiB7XHJcbiAgICAgICAgZmxleERpcmVjdGlvbjogXCJyb3dcIixcclxuICAgICAgICBtYXJnaW5Ub3A6IDUsXHJcbiAgICAgICAgbWFyZ2luQm90dG9tOiAwXHJcbiAgICB9LFxyXG4gICAgcmVzdGF1cmFudEluZm9Sb3dUd286IHtcclxuICAgICAgICBmbGV4RGlyZWN0aW9uOiBcInJvd1wiLFxyXG4gICAgICAgIG1hcmdpblRvcDogNSxcclxuICAgICAgICBtYXJnaW5Cb3R0b206IDUsXHJcbiAgICAgICAgYWxpZ25Db250ZW50OiBcImNlbnRlclwiXHJcbiAgICB9LFxyXG5cclxuICAgIHJhdGluZ051bWJlcjoge1xyXG4gICAgICAgIC8vIG1hcmdpblJpZ2h0OiAxMCxcclxuICAgICAgICBmb250U2l6ZTogMTIsXHJcbiAgICAgICAgbWFyZ2luTGVmdDogMyxcclxuICAgICAgICBmb250RmFtaWx5OkFQUEZPTlRTLmxpZ2h0LFxyXG4gICAgICAgIC8vIGFsaWduU2VsZjogXCJmbGV4LWVuZFwiXHJcbiAgICB9LFxyXG4gICAgZGlzdGFudDoge1xyXG4gICAgICAgIG1hcmdpblJpZ2h0OiAxMCxcclxuICAgICAgICBmb250U2l6ZTogMTIsXHJcbiAgICAgICAgbWFyZ2luTGVmdDogMyxcclxuICAgICAgICBmb250RmFtaWx5OkFQUEZPTlRTLmxpZ2h0XHJcbiAgICB9LFxyXG4gICAgZGVsaXZlcnlDaGFyZ2U6IHtcclxuICAgICAgICBtYXJnaW5SaWdodDogMTAsXHJcbiAgICAgICAgZm9udFNpemU6IDEyLFxyXG4gICAgICAgIG1hcmdpbkxlZnQ6IDMsXHJcbiAgICAgICAgZm9udEZhbWlseTpBUFBGT05UUy5saWdodCxcclxuICAgICAgICB0ZXh0QWxpZ246XCJyaWdodFwiLFxyXG4gICAgICAgIGJvcmRlcldpZHRoOjFcclxuICAgIH0sXHJcbiAgICBkZWxpdmVyeVRpbWU6IHtcclxuICAgICAgICBtYXJnaW5SaWdodDogMTAsXHJcbiAgICAgICAgZm9udFNpemU6IDEyLFxyXG4gICAgICAgIG1hcmdpbkxlZnQ6IDMsXHJcbiAgICAgICAgZm9udEZhbWlseTpBUFBGT05UUy5saWdodFxyXG4gICAgfSxcclxuICAgIG1pbmltdW1DaGFyZ2U6IHtcclxuICAgICAgICAvLyBtYXJnaW5SaWdodDogMTAsXHJcbiAgICAgICAgZm9udFNpemU6IDEyLFxyXG4gICAgICAgIG1hcmdpbkxlZnQ6IDMsXHJcbiAgICAgICAgZm9udEZhbWlseTpBUFBGT05UUy5saWdodFxyXG4gICAgfSxcclxuICAgIHByb21vdGlvbjoge1xyXG4gICAgICAgIHBvc2l0aW9uOiBcImFic29sdXRlXCIsXHJcbiAgICAgICAgLy8gbGVmdDogLTEwLFxyXG4gICAgICAgIGxlZnQ6IDUsXHJcbiAgICAgICAgdG9wOiA1LFxyXG4gICAgICAgIHpJbmRleDogMSxcclxuICAgICAgICAvLyBib3JkZXJDb2xvcjpcInJlZFwiLFxyXG4gICAgICAgIC8vIGJvcmRlcldpZHRoOjEsXHJcbiAgICAgICAgLy8gYmFja2dyb3VuZENvbG9yOlwid2hpdGVcIixcclxuICAgICAgICBoZWlnaHQ6IDM1LFxyXG4gICAgICAgIHdpZHRoOiBcImF1dG9cIixcclxuICAgICAgICBhbGlnbkl0ZW1zOiBcImNlbnRlclwiLFxyXG4gICAgfSxcclxuICAgIGF2YWlsYWJsZToge1xyXG4gICAgICAgIHdpZHRoOiAxNSxcclxuICAgICAgICBoZWlnaHQ6IDE1LFxyXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogXCIjNmFmZjJlXCIsXHJcbiAgICAgICAgcG9zaXRpb246IFwiYWJzb2x1dGVcIixcclxuICAgICAgICBib3JkZXJSYWRpdXM6IDEwMCxcclxuICAgICAgICBib3JkZXJXaWR0aDogMixcclxuICAgICAgICBib3JkZXJDb2xvcjogXCIjNWViZjI5XCIsXHJcbiAgICAgICAgekluZGV4OiA5LFxyXG4gICAgICAgIGJvdHRvbTogNSxcclxuICAgICAgICByaWdodDogMFxyXG4gICAgfSxcclxuICAgIHVuYXZhaWxhYmxlOiB7XHJcbiAgICAgICAgd2lkdGg6IDE1LFxyXG4gICAgICAgIGhlaWdodDogMTUsXHJcbiAgICAgICAgYmFja2dyb3VuZENvbG9yOiBcInJlZFwiLFxyXG4gICAgICAgIHBvc2l0aW9uOiBcImFic29sdXRlXCIsXHJcbiAgICAgICAgYm9yZGVyUmFkaXVzOiAxMDAsXHJcbiAgICAgICAgYm9yZGVyV2lkdGg6IDIsXHJcbiAgICAgICAgYm9yZGVyQ29sb3I6IFwicmVkXCIsXHJcbiAgICAgICAgekluZGV4OiA5LFxyXG4gICAgICAgIGJvdHRvbTogNSxcclxuICAgICAgICByaWdodDogMFxyXG4gICAgfSxcclxuXHJcbiAgICB1bmF2YWlsYWJsZU1lc3NhZ2U6IHtcclxuICAgICAgICB3aWR0aDogMTQwLFxyXG4gICAgICAgIGFsaWduQ29udGVudDogXCJjZW50ZXJcIixcclxuICAgICAgICBhbGlnbkl0ZW1zOiBcImNlbnRlclwiLFxyXG4gICAgICAgIGJvcmRlclRvcFdpZHRoOiAxLFxyXG4gICAgICAgIGJvcmRlclRvcENvbG9yOiBcIndoaXRlXCIsXHJcbiAgICAgICAgYm9yZGVyQm90dG9tV2lkdGg6IDEsXHJcbiAgICAgICAgYm9yZGVyQm90dG9tQ29sb3I6IFwid2hpdGVcIixcclxuICAgICAgICBwYWRkaW5nVG9wOiA2LFxyXG4gICAgICAgIHBhZGRpbmdCb3R0b206IDYsXHJcbiAgICB9LFxyXG4gICAgdGVtcG9yYXJpbHlDbG9zZU1lc3NhZ2U6IHtcclxuICAgICAgICB3aWR0aDogMTEwLFxyXG4gICAgICAgIGFsaWduQ29udGVudDogXCJjZW50ZXJcIixcclxuICAgICAgICBhbGlnbkl0ZW1zOiBcImNlbnRlclwiLFxyXG4gICAgICAgIGJvcmRlclRvcFdpZHRoOiAxLFxyXG4gICAgICAgIGJvcmRlclRvcENvbG9yOiBcIndoaXRlXCIsXHJcbiAgICAgICAgYm9yZGVyQm90dG9tV2lkdGg6IDEsXHJcbiAgICAgICAgYm9yZGVyQm90dG9tQ29sb3I6IFwid2hpdGVcIixcclxuICAgICAgICBwYWRkaW5nVG9wOiA2LFxyXG4gICAgICAgIHBhZGRpbmdCb3R0b206IDYsXHJcbiAgICB9LFxyXG4gICAgdW5hdmFpbGFibGVNZXNzYWdlVGV4dEJveDoge1xyXG4gICAgICAgIGNvbG9yOiBcIndoaXRlXCIsXHJcbiAgICAgICAgZm9udFNpemU6IDE2LFxyXG4gICAgICAgIGZvbnRGYW1pbHk6QVBQRk9OVFMubGlnaHRcclxuICAgIH0sXHJcbiAgICB0ZW1wb3JhcmlseUNsb3NlT3ZlcmxheToge1xyXG4gICAgICAgIHBvc2l0aW9uOiBcImFic29sdXRlXCIsXHJcbiAgICAgICAgdG9wOiAwLFxyXG4gICAgICAgIGxlZnQ6IDAsXHJcbiAgICAgICAgd2lkdGg6IFwiMTAwJVwiLFxyXG4gICAgICAgIGhlaWdodDogMTIwLFxyXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogXCJyZ2JhKDEsMSwxLDAuNilcIixcclxuICAgICAgICBhbGlnbkl0ZW1zOiBcImNlbnRlclwiLFxyXG4gICAgICAgIGFsaWduQ29udGVudDogXCJjZW50ZXJcIixcclxuICAgICAgICBqdXN0aWZ5Q29udGVudDogXCJjZW50ZXJcIixcclxuICAgICAgICB6SW5kZXg6IDksXHJcbiAgICB9LFxyXG4gICAgZGVsaXZlcnlDaGFyZ2VSb3dUZXh0OntcclxuICAgICAgICBmbGV4V3JhcDpcIm5vd3JhcFwiLFxyXG4gICAgICAgIC8vIHBvc2l0aW9uOlwicmVsYXRpdmVcIixcclxuICAgICAgICAvLyB0b3A6LTEsXHJcbiAgICAgICAgZm9udEZhbWlseTpBUFBGT05UUy5saWdodFxyXG4gICAgfSxcclxuICAgIGl0ZW1EZXNjcmlwdGlvbjp7XHJcbiAgICAgICAgZm9udFNpemU6IDE0LFxyXG4gICAgICAgIGZvbnRGYW1pbHk6QVBQRk9OVFMubGlnaHRcclxuICAgIH0sXHJcbiAgICBkZWxpdmVyeVRpbWVUZXh0OntcclxuICAgICAgICB0ZXh0QWxpZ246XCJjZW50ZXJcIixcclxuICAgICAgICBsZXR0ZXJTcGFjaW5nOjAsXHJcbiAgICAgICAgZm9udEZhbWlseTpBUFBGT05UUy5saWdodFxyXG4gICAgfSxcclxuICAgIG1pbk9yZGVyVGV4dDp7XHJcbiAgICAgICAgbWFyZ2luUmlnaHQ6IDAsXHJcbiAgICAgICAgZm9udEZhbWlseTpBUFBGT05UUy5saWdodFxyXG4gICAgfSxcclxuXHJcbn0pO1xyXG5cclxuXHJcbiIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7RmxhdExpc3QsU3R5bGVTaGVldCxWaWV3LEFjdGl2aXR5SW5kaWNhdG9yfSBmcm9tICdyZWFjdC1uYXRpdmUnO1xyXG5pbXBvcnQgUG9wdWxhclJlc3RhdXJhbnRDYXJkIGZyb20gXCIuL1BvcHVsYXJSZXN0YXVyYW50Q2FyZFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUG9wdWxhclJlc3RhdXJhbnRzIGV4dGVuZHMgUmVhY3QuUHVyZUNvbXBvbmVudCB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvcHMpe1xyXG4gICAgICAgIHN1cGVyKHByb3BzKVxyXG4gICAgICAgIC8vdGhpcy5yZW5kZXJGb290ZXIgPSB0aGlzLnJlbmRlckZvb3Rlci5iaW5kKHRoaXMpO1xyXG4gICAgfVxyXG4gICAgc3RhdGUgPSB7XHJcbiAgICAgICAgYXJyYXlSZXN0YXVyYW50czogdGhpcy5wcm9wcy5yZXN0YXVyYW50cyxcclxuICAgICAgICBzaG93RmlsdGVyOiB0aGlzLnByb3BzLnNob3csXHJcbiAgICAgICAgaXNMb2FkaW5nOnRoaXMucHJvcHMuaXNMb2FkaW5nXHJcbiAgICB9XHJcblxyXG4gICAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyhuZXh0UHJvcHMpIHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHthcnJheVJlc3RhdXJhbnRzOiBuZXh0UHJvcHMucmVzdGF1cmFudHMsaXNMb2FkaW5nOm5leHRQcm9wcy5pc0xvYWRpbmd9KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBzaG91bGRDb21wb25lbnRVcGRhdGUoKSB7XHJcbiAgICAvLyAgICAgcmV0dXJuIGZhbHNlXHJcbiAgICAvLyB9XHJcblxyXG4gICAgbmV4dFBhZ2UgPSAoKSA9PntcclxuICAgICAgICAvL2lmKHRoaXMucHJvcHMubmV4dFBhZ2Upe1xyXG4gICAgICAgIC8vICAgIHRoaXMucHJvcHMubmV4dFBhZ2UoKVxyXG4gICAgICAgIC8vfVxyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlckZvb3RlciAoKSB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZS5pc0xvYWRpbmcgPyAoXHJcbiAgICAgICAgICAgICAgICA8VmlldyBzdHlsZT17c3R5bGUuZm9vdGVyfT5cclxuICAgICAgICAgICAgICAgIDxBY3Rpdml0eUluZGljYXRvciBjb2xvcj1cImJsYWNrXCIgc2l6ZT17MzV9IHN0eWxlPXt7IG1hcmdpbjogMTUgfX0gLz5cclxuICAgICAgICAgICAgICAgIDwvVmlldz5cclxuICAgICAgICAgICAgKSA6IG51bGxcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgaWYoIXRoaXMuc3RhdGUuYXJyYXlSZXN0YXVyYW50cykgcmV0dXJuIG51bGw7XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxGbGF0TGlzdFxyXG4gICAgICAgICAgICAgICAgb25TY3JvbGw9e3RoaXMucHJvcHMub25TY3JvbGx9XHJcbiAgICAgICAgICAgICAgICBkaXNhYmxlVmlydHVhbGl6YXRpb249e3RydWV9XHJcbiAgICAgICAgICAgICAgICBzaG93c0hvcml6b250YWxTY3JvbGxJbmRpY2F0b3I9e2ZhbHNlfVxyXG4gICAgICAgICAgICAgICAgc2hvd3NWZXJ0aWNhbFNjcm9sbEluZGljYXRvcj17ZmFsc2V9XHJcbiAgICAgICAgICAgICAgICBob3Jpem9udGFsPXt0aGlzLnByb3BzLmhvcml6b250YWx9XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gaW5pdGlhbE51bVRvUmVuZGVyPXshdGhpcy5wcm9wcy5ob3Jpem9udGFsID8gMjAgOiAxMH1cclxuICAgICAgICAgICAgICAgIC8vIG1heFRvUmVuZGVyUGVyQmF0Y2g9eyF0aGlzLnByb3BzLmhvcml6b250YWwgPyAyMCA6IDEwfVxyXG4gICAgICAgICAgICAgICAgLy8gcmVtb3ZlQ2xpcHBlZFN1YnZpZXdzPXt0cnVlfVxyXG5cclxuICAgICAgICAgICAgICAgIG9uRW5kUmVhY2hlZFRocmVzaG9sZD17M31cclxuXHJcbiAgICAgICAgICAgICAgICAvL29uRW5kUmVhY2hlZD17dGhpcy5uZXh0UGFnZX1cclxuXHJcbiAgICAgICAgICAgICAgICBzdHlsZT17W3RoaXMucHJvcHMuaG9yaXpvbnRhbCA/IHttYXJnaW5MZWZ0OjV9IDoge21hcmdpbkhvcml6b250YWw6MTB9LHRoaXMucHJvcHMuc3R5bGUgfHwge30gXX1cclxuICAgICAgICAgICAgICAgIGRhdGE9e3RoaXMuc3RhdGUuYXJyYXlSZXN0YXVyYW50c31cclxuICAgICAgICAgICAgICAgIGV4dHJhRGF0YT17dGhpcy5zdGF0ZX1cclxuICAgICAgICAgICAgICAgIGtleUV4dHJhY3Rvcj17KGl0ZW0sIGluZGV4KSA9PiBTdHJpbmcoaW5kZXgpfVxyXG4gICAgICAgICAgICAgICAgcmVuZGVySXRlbT17KHtpdGVtLCBpbmRleH0pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8UG9wdWxhclJlc3RhdXJhbnRDYXJkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvL29mZmVyQmFubmVyU3R5bGU9e3RoaXMucHJvcHMub2ZmZXJCYW5uZXJTdHlsZX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhvcml6b250YWw9e3RoaXMucHJvcHMuaG9yaXpvbnRhbH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RPYmpNb2RlbD17aXRlbX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vaXRlbVByZXNzPXt0aGlzLnByb3BzLml0ZW1QcmVzc31cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vc3R5bGU9e3RoaXMucHJvcHMuaXRlbVN0eWxlfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaW5kZXg9e2luZGV4fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9mcmVlc2hpcEJhbm5lclN0eWxlPXt0aGlzLnByb3BzLmZyZWVzaGlwQmFubmVyU3R5bGV9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH19XHJcblxyXG4gICAgICAgICAgICAgICAgTGlzdEZvb3RlckNvbXBvbmVudD17KCk9PnRoaXMucmVuZGVyRm9vdGVyKCl9XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gZ2V0SXRlbUxheW91dD17KGRhdGEsIGluZGV4KSA9PiAoXHJcbiAgICAgICAgICAgICAgICAvLyAgICAge2xlbmd0aDogMzUwLCBvZmZzZXQ6IDM1MCAqIGluZGV4LCBpbmRleH1cclxuICAgICAgICAgICAgICAgIC8vICl9XHJcblxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuY29uc3Qgc3R5bGUgPSBTdHlsZVNoZWV0LmNyZWF0ZSh7XHJcbiAgICBmb290ZXI6IHtcclxuICAgICAgICBwYWRkaW5nOiAxMCxcclxuICAgICAgICBqdXN0aWZ5Q29udGVudDogJ2NlbnRlcicsXHJcbiAgICAgICAgYWxpZ25JdGVtczogJ2NlbnRlcicsXHJcbiAgICAgICAgZmxleERpcmVjdGlvbjogJ3JvdycsXHJcbiAgICB9LFxyXG59KVxyXG4iLCJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XHJcbi8vaW1wb3J0IHtDb250YWluZXJ9IGZyb20gXCJuYXRpdmUtYmFzZVwiO1xyXG5pbXBvcnQge1ZpZXd9IGZyb20gXCJyZWFjdC1uYXRpdmVcIjtcclxuLy9pbXBvcnQgTmF2QmFyIGZyb20gXCIuL05hdkJhclwiO1xyXG4vL2ltcG9ydCBCb3R0b21NZW51IGZyb20gXCIuLi9jb21wb25lbnRzL0JvdHRvbU1lbnVcIjtcclxuLy9pbXBvcnQgUHJvZ3Jlc3NMb2FkZXIgZnJvbSBcIi4uL2NvbXBvbmVudHMvUHJvZ3Jlc3NMb2FkZXJcIjtcclxuLy9pbXBvcnQgTWFpbkxvYWRlciBmcm9tIFwiLi4vY29tcG9uZW50cy9NYWluTG9hZGVyXCI7XHJcbmltcG9ydCB7RURDb2xvcnN9IGZyb20gXCIuLi9hc3NldHMvQ29sb3JzXCI7XHJcbi8vaW1wb3J0IHtuZXRTdGF0dXNFdmVudH0gZnJvbSBcIi4uL3V0aWxzL05ldHdvcmtTdGF0dXNDb25uZWN0aW9uXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBCYXNlQ29udGFpbmVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuICAgIC8vIGNvbnN0cnVjdG9yKHByb3BzKSB7XHJcbiAgICAvLyAgICAgc3VwZXIocHJvcHMpO1xyXG4gICAgLy8gfVxyXG4gICAgLy9cclxuICAgIC8vIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG4gICAgLy8gICAgIG5ldFN0YXR1c0V2ZW50KHN0YXR1cyA9PiB7XHJcbiAgICAvL1xyXG4gICAgLy8gICAgIH0pO1xyXG4gICAgLy8gfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8VmlldyBzdHlsZT17e292ZXJmbG93Olwic2Nyb2xsXCIsaGVpZ2h0OlwiMTAwJVwifX0+XHJcbiAgICAgICAgICAgICAgICB7Lyo8TmF2QmFyKi99XHJcbiAgICAgICAgICAgICAgICB7LyogICAgdGl0bGU9e3RoaXMucHJvcHMudGl0bGV9Ki99XHJcbiAgICAgICAgICAgICAgICB7LyogICAgb3RoZXJUaXRsZT17dGhpcy5wcm9wcy5vdGhlclRpdGxlfSovfVxyXG4gICAgICAgICAgICAgICAgey8qICAgIGxlZnQ9e3RoaXMucHJvcHMubGVmdH0qL31cclxuICAgICAgICAgICAgICAgIHsvKiAgICBvbkxlZnQ9eygpID0+IHsqL31cclxuICAgICAgICAgICAgICAgIHsvKiAgICAgICAgdGhpcy5wcm9wcy5vbkxlZnQoKTsqL31cclxuICAgICAgICAgICAgICAgIHsvKiAgICB9fSovfVxyXG4gICAgICAgICAgICAgICAgey8qICAgIHJpZ2h0PXt0aGlzLnByb3BzLnJpZ2h0fSovfVxyXG4gICAgICAgICAgICAgICAgey8qICAgIG9uUmlnaHQ9eyhvcGVyYXRvcikgPT4geyovfVxyXG4gICAgICAgICAgICAgICAgey8qICAgICAgICB0aGlzLnByb3BzLm9uUmlnaHQob3BlcmF0b3IpOyovfVxyXG4gICAgICAgICAgICAgICAgey8qICAgIH19Ki99XHJcbiAgICAgICAgICAgICAgICB7LyogICAgaXNGYXZvcml0ZT17dGhpcy5wcm9wcy5pc0Zhdm9yaXRlfSovfVxyXG4gICAgICAgICAgICAgICAgey8qICAgIHNlYXJjaEJveD17dGhpcy5wcm9wcy5zZWFyY2hCb3h9Ki99XHJcbiAgICAgICAgICAgICAgICB7LyogICAgb25TZWFyY2hCb3hUZXh0Q2hhbmdlPXt0aGlzLnByb3BzLm9uU2VhcmNoQm94VGV4dENoYW5nZX0qL31cclxuICAgICAgICAgICAgICAgIHsvKiAgICBzZWFyY2hCb3hJc0ZvY3VzPXt0aGlzLnByb3BzLnNlYXJjaEJveElzRm9jdXN9Ki99XHJcbiAgICAgICAgICAgICAgICB7LyogICAgc2VhcmNoQm94QnV0dG9uPXt0aGlzLnByb3BzLnNlYXJjaEJveEJ1dHRvbn0qL31cclxuICAgICAgICAgICAgICAgIHsvKiAgICBzZWFyY2hCb3hCdXR0b25JY29uPXt0aGlzLnByb3BzLnNlYXJjaEJveEJ1dHRvbkljb259Ki99XHJcbiAgICAgICAgICAgICAgICB7LyogICAgc2VhcmNoQm94QnV0dG9uUHJlc3M9e3RoaXMucHJvcHMuc2VhcmNoQm94QnV0dG9uUHJlc3N9Ki99XHJcbiAgICAgICAgICAgICAgICB7LyovPiovfVxyXG5cclxuICAgICAgICAgICAgICAgIHsvKnt0aGlzLnByb3BzLnNob3dTcGxhc2hMb2FkZXIgPyA8TWFpbkxvYWRlci8+IDogKHRoaXMucHJvcHMubG9hZGluZyA/PFByb2dyZXNzTG9hZGVyLz46IG51bGwpIH0qL31cclxuXHJcbiAgICAgICAgICAgICAgICA8VmlldyBzdHlsZT17e2JhY2tncm91bmRDb2xvcjogRURDb2xvcnMuYmFja2dyb3VuZERhcmt9fT5cclxuICAgICAgICAgICAgICAgICAgICB7dGhpcy5wcm9wcy5jaGlsZHJlbn1cclxuICAgICAgICAgICAgICAgICAgICB7Lyp7dGhpcy5wcm9wcy5oaWRlQm90dG9tTWVudSA/IG51bGwgOjxWaWV3IHN0eWxlPXt7aGVpZ2h0OjUwfX0vPn0qL31cclxuICAgICAgICAgICAgICAgICAgICB7Lyp7dGhpcy5wcm9wcy5oaWRlQm90dG9tTWVudSA/IG51bGwgOiovfVxyXG4gICAgICAgICAgICAgICAgICAgIHsvKiAgICA8Qm90dG9tTWVudSovfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICB7LyogICAgICAgIGJ1dHRvbjFBY3RpdmU9e3RoaXMucHJvcHMuYnV0dG9uMUFjdGl2ZX0qL31cclxuICAgICAgICAgICAgICAgICAgICB7LyogICAgICAgIGJ1dHRvbjJBY3RpdmU9e3RoaXMucHJvcHMuYnV0dG9uMkFjdGl2ZX0qL31cclxuICAgICAgICAgICAgICAgICAgICB7LyogICAgICAgIC8vYnV0dG9uM0FjdGl2ZT17dGhpcy5wcm9wcy5idXR0b24zQWN0aXZlfSovfVxyXG4gICAgICAgICAgICAgICAgICAgIHsvKiAgICAgICAgYnV0dG9uNEFjdGl2ZT17dGhpcy5wcm9wcy5idXR0b240QWN0aXZlfSovfVxyXG4gICAgICAgICAgICAgICAgICAgIHsvKiAgICAgICAgYnV0dG9uNUFjdGl2ZT17dGhpcy5wcm9wcy5idXR0b241QWN0aXZlfSovfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICB7LyogICAgICAgIF9jYXJ0Q291bnQ9e3RoaXMucHJvcHMuY2FydENvdW50fSovfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICB7LyogICAgICAgIGZvb3RlckJ1dHRvbjFDbGljaz17KCkgPT4geyovfVxyXG4gICAgICAgICAgICAgICAgICAgIHsvKiAgICAgICAgICAgIHRoaXMucHJvcHMuZm9vdGVyQnV0dG9uMUNsaWNrKCkqL31cclxuICAgICAgICAgICAgICAgICAgICB7LyogICAgICAgIH19Ki99XHJcbiAgICAgICAgICAgICAgICAgICAgey8qICAgICAgICBmb290ZXJCdXR0b24yQ2xpY2s9eygpID0+IHsqL31cclxuICAgICAgICAgICAgICAgICAgICB7LyogICAgICAgICAgICB0aGlzLnByb3BzLmZvb3RlckJ1dHRvbjJDbGljaygpKi99XHJcbiAgICAgICAgICAgICAgICAgICAgey8qICAgICAgICB9fSovfVxyXG4gICAgICAgICAgICAgICAgICAgIHsvKiAgICAgICAgZm9vdGVyQnV0dG9uM0NsaWNrPXsoKSA9PiB7Ki99XHJcbiAgICAgICAgICAgICAgICAgICAgey8qICAgICAgICAgICAgdGhpcy5wcm9wcy5mb290ZXJCdXR0b24zQ2xpY2soKSovfVxyXG4gICAgICAgICAgICAgICAgICAgIHsvKiAgICAgICAgfX0qL31cclxuICAgICAgICAgICAgICAgICAgICB7LyogICAgICAgIGZvb3RlckJ1dHRvbjRDbGljaz17KCkgPT4geyovfVxyXG4gICAgICAgICAgICAgICAgICAgIHsvKiAgICAgICAgICAgIHRoaXMucHJvcHMuZm9vdGVyQnV0dG9uNENsaWNrKCkqL31cclxuICAgICAgICAgICAgICAgICAgICB7LyogICAgICAgIH19Ki99XHJcbiAgICAgICAgICAgICAgICAgICAgey8qICAgICAgICBmb290ZXJCdXR0b241Q2xpY2s9eygpID0+IHsqL31cclxuICAgICAgICAgICAgICAgICAgICB7LyogICAgICAgICAgICB0aGlzLnByb3BzLmZvb3RlckJ1dHRvbjVDbGljaygpKi99XHJcbiAgICAgICAgICAgICAgICAgICAgey8qICAgICAgICB9fSovfVxyXG4gICAgICAgICAgICAgICAgICAgIHsvKiAgICAvPn0qL31cclxuICAgICAgICAgICAgICAgIDwvVmlldz5cclxuICAgICAgICAgICAgPC9WaWV3PlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5cclxuaW1wb3J0IHtcclxuICAgIEZsYXRMaXN0LFxyXG4gICAgU3R5bGVTaGVldCxcclxuICAgIFRleHQsXHJcbiAgICBUb3VjaGFibGVPcGFjaXR5LFxyXG4gICAgVmlldyxcclxuICAgIEltYWdlQmFja2dyb3VuZCxcclxuICAgIEltYWdlXHJcbn0gZnJvbSBcInJlYWN0LW5hdGl2ZVwiO1xyXG5pbXBvcnQgQmFzZUNvbnRhaW5lciBmcm9tIFwiLi9CYXNlQ29udGFpbmVyXCI7XHJcbmltcG9ydCBBc3NldHMgZnJvbSBcIi4uL2Fzc2V0c1wiO1xyXG5pbXBvcnQge0FQUEZPTlRTfSBmcm9tIFwiLi4vYXNzZXRzL0ZvbnRDb25zdGFudHNcIjtcclxuXHJcbmltcG9ydCB7YXBpUG9zdH0gZnJvbSBcIi4uL2FwaS9TZXJ2aWNlTWFuYWdlclwiO1xyXG4vL2ltcG9ydCB7bmV0U3RhdHVzfSBmcm9tIFwiLi4vdXRpbHMvTmV0d29ya1N0YXR1c0Nvbm5lY3Rpb25cIlxyXG5cclxuaW1wb3J0IHtSRUdJU1RSQVRJT05fSE9NRSwgUkVTUE9OU0VfU1VDQ0VTU30gZnJvbSBcIi4uL3V0aWxzL0NvbnN0YW50c1wiO1xyXG5pbXBvcnQgUG9wdWxhclJlc3RhdXJhbnRzIGZyb20gXCIuLi9jb21wb25lbnRzL1BvcHVsYXJSZXN0YXVyYW50c1wiO1xyXG5pbXBvcnQge0FQUENPTE9SUywgRURDb2xvcnN9IGZyb20gXCIuLi9hc3NldHMvQ29sb3JzXCI7XHJcbmltcG9ydCBFVGV4dFZpZXdOb3JtYWxMYWJlbCBmcm9tIFwiLi4vY29tcG9uZW50cy9FVGV4dFZpZXdOb3JtYWxMYWJlbFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTWFpbkNvbnRhaW5lciBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XHJcblxyXG4gICAgc3RhdGUgPSB7XHJcbiAgICAgICAgYXJyYXlSZXN0YXVyYW50czp1bmRlZmluZWRcclxuICAgIH1cclxuXHJcbiAgICBzaG93UmVzdGF1cmFudFJvd3MoKSB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPFBvcHVsYXJSZXN0YXVyYW50c1xyXG4gICAgICAgICAgICAgICAgLy8gb25TY3JvbGxFbmREcmFnPXsoZSk9PntcclxuICAgICAgICAgICAgICAgIC8vICAgICAvL3RoaXMuc2V0U3RhdGUoe3Njb2xsZWRPZmZzZXRZOmUubmF0aXZlRXZlbnQuY29udGVudE9mZnNldC55fSk7XHJcbiAgICAgICAgICAgICAgICAvLyAgICAgLy8gY29uc29sZS5sb2coZS5uYXRpdmVFdmVudC5jb250ZW50T2Zmc2V0LnkpO1xyXG4gICAgICAgICAgICAgICAgLy8gICAgIC8vdGhpcy5zZXRTdGF0ZSh7c2hvd1Njcm9sbFVwQnV0dG9uOiBlLm5hdGl2ZUV2ZW50LmNvbnRlbnRPZmZzZXQueSA+PSBoZWlnaHR9KVxyXG4gICAgICAgICAgICAgICAgLy9cclxuICAgICAgICAgICAgICAgIC8vIH19XHJcbiAgICAgICAgICAgICAgICByZXN0YXVyYW50cz17dGhpcy5zdGF0ZS5hcnJheVJlc3RhdXJhbnRzfVxyXG4gICAgICAgICAgICAgICAgLy9pdGVtUHJlc3M9e3RoaXMuZ290b1Jlc3RhdXJhbnR9XHJcbiAgICAgICAgICAgICAgICAvL3JlZnJlc2hTY3JlZW49e3RoaXMucmVmcmVzaFNjcmVlbn1cclxuICAgICAgICAgICAgICAgIC8vbmV4dFBhZ2U9e3RoaXMubmV4dFJlc3RhdXJhbnRQYWdlfVxyXG4gICAgICAgICAgICAgICAgLy9pc0xvYWRpbmc9e3RoaXMuc3RhdGUubG9hZGluZ1Jlc3RhdXJhbnR9XHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuICAgIGFsbFJlc3RhdXJhbnRSb3dzID0gKCkgPT4ge1xyXG4gICAgICAgIGxldCBwb3B1bGFyUmVzdGF1cmFudHMgPSAoXHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGUuYXJyYXlSZXN0YXVyYW50cyAhPSB1bmRlZmluZWQgJiZcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZS5hcnJheVJlc3RhdXJhbnRzICE9IG51bGwgJiZcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZS5hcnJheVJlc3RhdXJhbnRzLmxlbmd0aCA+IDAgPyAoXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNob3dSZXN0YXVyYW50Um93cygpXHJcblxyXG4gICAgICAgICAgICApIDogdGhpcy5zdGF0ZS5hcnJheVJlc3RhdXJhbnRzICE9IHVuZGVmaW5lZCAmJlxyXG4gICAgICAgICAgICB0aGlzLnN0YXRlLmFycmF5UmVzdGF1cmFudHMgIT0gbnVsbCAmJlxyXG4gICAgICAgICAgICB0aGlzLnN0YXRlLmFycmF5UmVzdGF1cmFudHMubGVuZ3RoID09IDAgPyAoXHJcbiAgICAgICAgICAgICAgICBudWxsXHJcbiAgICAgICAgICAgICkgOiAodGhpcy5zdGF0ZS5pc0xvYWRpbmcgPyBudWxsIDpcclxuICAgICAgICAgICAgICAgICAgICA8VG91Y2hhYmxlT3BhY2l0eSBzdHlsZT17e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGlnblNlbGY6ICdjZW50ZXInLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6IEFQUENPTE9SUy5ncm91cDFNYWluLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAxMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luVmVydGljYWw6IDEwMFxyXG4gICAgICAgICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gb25QcmVzcz17KCkgPT4gdGhpcy5yZWZyZXNoU2NyZWVufVxyXG4gICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAgey8qIDxJbWFnZSBzb3VyY2UgPSB7QXNzZXRzLnJlZnJlc2h9Lz4gKi99XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxUZXh0IHN0eWxlPXt7Y29sb3I6IEVEQ29sb3JzLndoaXRlfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBSZWxvYWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9UZXh0PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvVG91Y2hhYmxlT3BhY2l0eT5cclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxWaWV3IHN0eWxlPXt7ZmxleDogMSwgbWluSGVpZ2h0OiAyMDB9fT5cclxuICAgICAgICAgICAgICAgIDxFVGV4dFZpZXdOb3JtYWxMYWJlbFxyXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlPXt7bWFyZ2luTGVmdDogMTAsIG1hcmdpbkJvdHRvbTogMTB9fVxyXG4gICAgICAgICAgICAgICAgICAgIHRleHQ9XCJGZWF0dXJlZFwiLz5cclxuXHJcbiAgICAgICAgICAgICAgICB7cG9wdWxhclJlc3RhdXJhbnRzfVxyXG4gICAgICAgICAgICA8L1ZpZXc+XHJcbiAgICAgICAgKTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGF0YSgpe1xyXG4gICAgICAgIGNvbnN0IHBhcmFtID0ge31cclxuICAgICAgICBhcGlQb3N0KFxyXG4gICAgICAgICAgICBSRUdJU1RSQVRJT05fSE9NRSxcclxuICAgICAgICAgICAgcGFyYW0sXHJcbiAgICAgICAgICAgIHJlc3AgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3AgIT0gdW5kZWZpbmVkKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwLnN0YXR1cyA9PSBSRVNQT05TRV9TVUNDRVNTKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgcmVzdGF1cmFudHMgPSByZXNwLnJlc3RhdXJhbnRzOy8vLnNwbGljZSgwLDEwKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwicmVzdGF1cmFudHMgOjo6OjogXCIsIHJlc3RhdXJhbnRzKVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7YXJyYXlSZXN0YXVyYW50czogdW5kZWZpbmVkfSwgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7YXJyYXlSZXN0YXVyYW50czogcmVzdGF1cmFudHN9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxhc3RfcGFnZSA9IHJlc3AubGFzdF9wYWdlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRfcGFnZSA9IDE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiY3VycmVudF9wYWdlIDogXCIsIHRoaXMuY3VycmVudF9wYWdlKVxyXG5cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aXNMb2FkaW5nOiBmYWxzZX0pO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLnNob3dTcGxhc2hMb2FkZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7c2hvd1NwbGFzaExvYWRlcjogZmFsc2V9KTtcclxuICAgICAgICAgICAgICAgICAgICB9LCAzMDAwKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZXJyID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2lzTG9hZGluZzogZmFsc2V9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgYWxsTWFpblJvdygpe1xyXG4gICAgICAgIGxldCByb3dzID0gW107XHJcblxyXG4gICAgICAgIC8vIHJvd3MucHVzaCh0aGlzLnNsaWRlcigpKTtcclxuICAgICAgICAvLyByb3dzLnB1c2godGhpcy5idXNpbmVzc0ljb25zKCkpO1xyXG4gICAgICAgIC8vIHJvd3MucHVzaCh0aGlzLmFub3VuY2VtZW50Um93KCkpO1xyXG5cclxuICAgICAgICByb3dzLnB1c2goXHJcbiAgICAgICAgICAgIDxWaWV3IHN0eWxlPXt7ZmxleERpcmVjdGlvbjpcInJvd1wiLGhlaWdodDoxMDAsbWFyZ2luSG9yaXpvbnRhbDo3LG1hcmdpblZlcnRpY2FsOiAxMH19PlxyXG4gICAgICAgICAgICAgICAgPFRvdWNoYWJsZU9wYWNpdHlcclxuICAgICAgICAgICAgICAgICAgICBvblByZXNzPXsoKT0+e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBpZih0aGlzLm1lcmNoYW50TW9kYWwpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgdGhpcy5tZXJjaGFudE1vZGFsLnNob3coKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3tmbGV4OjEscGFkZGluZ0hvcml6b250YWw6M319XHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgPFZpZXcgc3R5bGU9e3t3aWR0aDpcIjEwMCVcIixiYWNrZ3JvdW5kQ29sb3I6XCIjN2QzZTFlXCIsaGVpZ2h0OlwiMTAwJVwiLG92ZXJmbG93OlwiaGlkZGVuXCIsYm9yZGVyUmFkaXVzOjV9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEltYWdlQmFja2dyb3VuZCBzb3VyY2U9e0Fzc2V0cy5tZXJjaGFudEFkc30gcmVzaXplTW9kZT17XCJjb250YWluXCJ9IHN0eWxlPXt7d2lkdGg6XCIxMDAlXCIsaGVpZ2h0OlwiMTAwJVwifX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VmlldyBzdHlsZT17e3BhZGRpbmdIb3Jpem9udGFsOjUscGFkZGluZ1ZlcnRpY2FsOjB9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VGV4dCBzdHlsZT17e2ZvbnRGYW1pbHk6QVBQRk9OVFMucmVndWxhcixmb250U2l6ZToxNCxsZXR0ZXJTcGFjaW5nOi0wLjJ9fT57XCJCZWNvbWUgYSBNZXJjaGFudCBQYXJ0bmVyXCJ9PC9UZXh0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxUZXh0IHN0eWxlPXt7Zm9udEZhbWlseTpBUFBGT05UUy5yZWd1bGFyLGZvbnRTaXplOjEyfX0+e1wiSGVscCB5b3UgdG8gSW5jcmVhc2UgUmV2ZW51ZVwifTwvVGV4dD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvVmlldz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9JbWFnZUJhY2tncm91bmQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxUZXh0IHN0eWxlPXtzdHlsZXMuam9pbk5vd30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XCJKb2luIE5vd1wifVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L1RleHQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9WaWV3PlxyXG4gICAgICAgICAgICAgICAgPC9Ub3VjaGFibGVPcGFjaXR5PlxyXG5cclxuICAgICAgICAgICAgICAgIDxUb3VjaGFibGVPcGFjaXR5XHJcbiAgICAgICAgICAgICAgICAgICAgb25QcmVzcz17KCk9PntcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gaWYodGhpcy5qb2JWYWNhbmN5TW9kYWwpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgICAgdGhpcy5qb2JWYWNhbmN5TW9kYWwuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICAgICAgICBzdHlsZT17e2ZsZXg6MSxwYWRkaW5nSG9yaXpvbnRhbDozfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgPFZpZXcgc3R5bGU9e3t3aWR0aDpcIjEwMCVcIixiYWNrZ3JvdW5kQ29sb3I6XCIjN2QzZTFlXCIsaGVpZ2h0OlwiMTAwJVwiLG92ZXJmbG93OlwiaGlkZGVuXCIsYm9yZGVyUmFkaXVzOjV9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPEltYWdlQmFja2dyb3VuZCBzb3VyY2U9e0Fzc2V0cy52YWNhbmN5QWRzfSByZXNpemVNb2RlPXtcImNvbnRhaW5cIn0gc3R5bGU9e3t3aWR0aDpcIjEwMCVcIixoZWlnaHQ6XCIxMDAlXCJ9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxWaWV3IHN0eWxlPXt7cGFkZGluZ0hvcml6b250YWw6NSxwYWRkaW5nVmVydGljYWw6MH19PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxUZXh0IHN0eWxlPXt7Zm9udEZhbWlseTpBUFBGT05UUy5yZWd1bGFyLGZvbnRTaXplOjE0LGxldHRlclNwYWNpbmc6IC0xfX0+e1wiSm9iIFZhY2FuY3lcIn08L1RleHQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRleHQgc3R5bGU9e3tmb250RmFtaWx5OkFQUEZPTlRTLnJlZ3VsYXIsZm9udFNpemU6MTJ9fT57XCJOZXcgam9iIGlzIGF2YWlsYWJsZSBub3dcIn08L1RleHQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1ZpZXc+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvSW1hZ2VCYWNrZ3JvdW5kPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8VGV4dCBzdHlsZT17c3R5bGVzLmpvaW5Ob3d9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1wiQXBwbHkgTm93XCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvVGV4dD5cclxuICAgICAgICAgICAgICAgICAgICA8L1ZpZXc+XHJcbiAgICAgICAgICAgICAgICA8L1RvdWNoYWJsZU9wYWNpdHk+XHJcbiAgICAgICAgICAgIDwvVmlldz5cclxuICAgICAgICApO1xyXG4gICAgICAgIC8vIExvYWRpbmdcclxuICAgICAgICByb3dzLnB1c2godGhpcy5hbGxSZXN0YXVyYW50Um93cygpKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHJvd3M7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcbiAgICAgICAgdGhpcy5nZXREYXRhKClcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXIoKSB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPEJhc2VDb250YWluZXI+XHJcbiAgICAgICAgICAgICAgICA8RmxhdExpc3RcclxuICAgICAgICAgICAgICAgICAgICAvLyBvblNjcm9sbD17dGhpcy5fb25MaXN0U2Nyb2xsfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICByZWY9eyhyZWYpPT50aGlzLm1haW5GbGF0TGlzdCA9IHJlZn1cclxuICAgICAgICAgICAgICAgICAgICAvLyByZWZyZXNoQ29udHJvbD17XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIDxSZWZyZXNoQ29udHJvbFxyXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICAgICAgcmVmcmVzaGluZz17dGhpcy5zdGF0ZS5yZWZyZXNoaW5nfVxyXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICAgICAgb25SZWZyZXNoPXt0aGlzLl9vblJlZnJlc2h9XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGE9e3RoaXMuYWxsTWFpblJvdygpfVxyXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlckl0ZW09eyh7aXRlbX0pID0+IChcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXRlbVxyXG4gICAgICAgICAgICAgICAgICAgICl9XHJcbiAgICAgICAgICAgICAgICAgICAga2V5RXh0cmFjdG9yPXsoaXRlbSwgaW5kZXgpID0+IFN0cmluZyhpbmRleCl9XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICA8L0Jhc2VDb250YWluZXI+XHJcbiAgICAgICAgKVxyXG4gICAgfVxyXG59XHJcblxyXG5jb25zdCBzdHlsZXMgPSBTdHlsZVNoZWV0LmNyZWF0ZSh7XHJcbiAgICBzY3JvbGxVcEJ1dHRvbjp7XHJcbiAgICAgICAgekluZGV4Ojk5OSxcclxuICAgICAgICByaWdodDoxMCxcclxuICAgICAgICBib3R0b206NjAsXHJcbiAgICAgICAgLy8gc2hhZG93Q29sb3I6IFwiIzAwMFwiLFxyXG4gICAgICAgIC8vIHNoYWRvd09mZnNldDoge1xyXG4gICAgICAgIC8vICAgICB3aWR0aDogMjAsXHJcbiAgICAgICAgLy8gICAgIGhlaWdodDogMjAsXHJcbiAgICAgICAgLy8gfSxcclxuICAgICAgICAvLyBzaGFkb3dPcGFjaXR5OiAxLFxyXG4gICAgICAgIC8vIHNoYWRvd1JhZGl1czogOSxcclxuICAgICAgICAvLyBlbGV2YXRpb246IDksXHJcblxyXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjpcInJnYmEoMCwwLDAsMC42KVwiXHJcbiAgICB9LFxyXG4gICAgZGlzY291bnRfc3RpY2s6IHtcclxuICAgICAgICBwb3NpdGlvbjogXCJhYnNvbHV0ZVwiLFxyXG4gICAgICAgIHJpZ2h0OiAwLFxyXG4gICAgICAgIHRvcDogMCxcclxuICAgICAgICB6SW5kZXg6IDEsXHJcbiAgICAgICAgLy8gYm9yZGVyQ29sb3I6XCJyZWRcIixcclxuICAgICAgICAvLyBib3JkZXJXaWR0aDoxLFxyXG4gICAgICAgIC8vIGJhY2tncm91bmRDb2xvcjpcIndoaXRlXCIsXHJcbiAgICAgICAgaGVpZ2h0OiAzNSxcclxuICAgICAgICB3aWR0aDogMzAsXHJcbiAgICAgICAgYWxpZ25JdGVtczogXCJjZW50ZXJcIixcclxuICAgICAgICBib3JkZXJSYWRpdXM6IDIsXHJcbiAgICB9LFxyXG4gICAgaXRlbUltYWdlOiB7XHJcbiAgICAgICAgd2lkdGg6IFwiMTAwJVwiLFxyXG4gICAgICAgIGhlaWdodDogXCIxMDAlXCIsXHJcbiAgICAgICAgYWxpZ25TZWxmOiBcImNlbnRlclwiLFxyXG4gICAgICAgIHJlc2l6ZU1vZGU6ICdjb3ZlcidcclxuICAgIH0sXHJcbiAgICBzcGlubmVyOiB7XHJcbiAgICAgICAgZmxleDogMSxcclxuICAgICAgICBhbGlnblNlbGY6IFwiY2VudGVyXCIsXHJcbiAgICAgICAgekluZGV4OiAxMDAwXHJcbiAgICB9LFxyXG4gICAgYW5vdW5jZW1lbnRSb3c6IHtcclxuICAgICAgICBtYXJnaW5Ub3A6IDEwLFxyXG4gICAgICAgIG1hcmdpblN0YXJ0OiAxMCxcclxuICAgICAgICBtYXJnaW5FbmQ6IDEwLFxyXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogXCJ3aGl0ZVwiLFxyXG4gICAgICAgIGJvcmRlclJhZGl1czogNVxyXG4gICAgfSxcclxuICAgIGpvaW5Ob3c6e1xyXG4gICAgICAgIHBvc2l0aW9uOlwiYWJzb2x1dGVcIixcclxuICAgICAgICB0ZXh0QWxpZ246IFwiY2VudGVyXCIsXHJcbiAgICAgICAgdGV4dEFsaWduVmVydGljYWw6XCJjZW50ZXJcIixcclxuICAgICAgICAvLyBqdXN0aWZ5Q29udGVudDpcImNlbnRlclwiLFxyXG4gICAgICAgIC8vIGFsaWduU2VsZjpcImNlbnRlclwiLFxyXG4gICAgICAgIGZsZXg6MSxcclxuICAgICAgICBib3R0b206MTAsXHJcbiAgICAgICAgbGVmdDo1LFxyXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjpcInllbGxvd1wiLFxyXG4gICAgICAgIGJvcmRlclJhZGl1czo1LFxyXG4gICAgICAgIGJvcmRlcldpZHRoOjEsXHJcbiAgICAgICAgYm9yZGVyQ29sb3I6XCJ5ZWxsb3dcIixcclxuICAgICAgICB3aWR0aDo3MCxcclxuICAgICAgICBoZWlnaHQ6MjUsXHJcbiAgICAgICAgZm9udFNpemU6MTJcclxuICAgIH1cclxufSk7IiwiZXhwb3J0IGNvbnN0IFRZUEVfU0FWRV9DSEVDS09VVF9ERVRBSUxTID0gXCJUWVBFX1NBVkVfQ0hFQ0tPVVRfREVUQUlMU1wiO1xyXG5leHBvcnQgY29uc3QgU0FWRV9DQVJUX0NPVU5UID0gXCJTQVZFX0NBUlRfQ09VTlRcIjtcclxuZXhwb3J0IGNvbnN0IFNBVkVfQ0FSVF9UT1RBTCA9IFwiU0FWRV9DQVJUX1RPVEFMXCI7XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gc2F2ZUNoZWNrb3V0RGV0YWlscyhkYXRhKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHR5cGU6IFRZUEVfU0FWRV9DSEVDS09VVF9ERVRBSUxTLFxyXG4gICAgICAgIHZhbHVlOiBkYXRhXHJcbiAgICB9O1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gc2F2ZUNhcnRDb3VudChkYXRhKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHR5cGU6IFNBVkVfQ0FSVF9DT1VOVCxcclxuICAgICAgICB2YWx1ZTogZGF0YVxyXG4gICAgfTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHNhdmVDYXJ0VG90YWwoZGF0YSkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICB0eXBlOiBTQVZFX0NBUlRfVE9UQUwsXHJcbiAgICAgICAgdmFsdWU6IGRhdGFcclxuICAgIH07XHJcbn1cclxuIiwiZXhwb3J0IGNvbnN0IFRZUEVfU0FWRV9OQVZJR0FUSU9OX1NFTEVDVElPTiA9IFwiVFlQRV9TQVZFX05BVklHQVRJT05fU0VMRUNUSU9OXCI7XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gc2F2ZU5hdmlnYXRpb25TZWxlY3Rpb24oZGF0YSkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICB0eXBlOiBUWVBFX1NBVkVfTkFWSUdBVElPTl9TRUxFQ1RJT04sXHJcbiAgICAgICAgdmFsdWU6IGRhdGFcclxuICAgIH07XHJcbn1cclxuIiwiZXhwb3J0IGNvbnN0IFRZUEVfU0FWRV9MT0dJTl9ERVRBSUxTID0gXCJUWVBFX1NBVkVfTE9HSU5fREVUQUlMU1wiXHJcblxyXG5leHBvcnQgZnVuY3Rpb24gc2F2ZVVzZXJEZXRhaWxzSW5SZWR1eChkZXRhaWxzKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHR5cGU6IFRZUEVfU0FWRV9MT0dJTl9ERVRBSUxTLFxyXG4gICAgICAgIHZhbHVlOiBkZXRhaWxzXHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBUWVBFX1NBVkVfVVNFUl9BRERSRVNTRVMgPSBcIlRZUEVfU0FWRV9VU0VSX0FERFJFU1NFU1wiXHJcbmV4cG9ydCBmdW5jdGlvbiBzYXZlVXNlckFkZHJlc3Nlc0luUmVkdXgoZGV0YWlscykge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICB0eXBlOiBUWVBFX1NBVkVfVVNFUl9BRERSRVNTRVMsXHJcbiAgICAgICAgdmFsdWU6IGRldGFpbHNcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IFRZUEVfU0FWRV9DQVJUX0FERFJFU1NFUyA9IFwiVFlQRV9TQVZFX0NBUlRfQUREUkVTU0VTXCJcclxuZXhwb3J0IGZ1bmN0aW9uIHNhdmVDYXJ0QWRkcmVzc2VzSW5SZWR1eChkZXRhaWxzKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHR5cGU6IFRZUEVfU0FWRV9DQVJUX0FERFJFU1NFUyxcclxuICAgICAgICB2YWx1ZTogZGV0YWlsc1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgVFlQRV9TQVZFX0xPR0lOX0ZDTSA9IFwiVFlQRV9TQVZFX0xPR0lOX0ZDTVwiXHJcblxyXG5leHBvcnQgZnVuY3Rpb24gc2F2ZVVzZXJGQ01JblJlZHV4KGRldGFpbHMpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgdHlwZTogVFlQRV9TQVZFX0xPR0lOX0ZDTSxcclxuICAgICAgICB2YWx1ZTogZGV0YWlsc1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgVFlQRV9TQVZFX0NPVU5UUllfQ09ERSA9IFwiVFlQRV9TQVZFX0NPVU5UUllfQ09ERVwiXHJcblxyXG5leHBvcnQgZnVuY3Rpb24gc2F2ZUNvdW50cnlDb2RlSW5SZWR1eChkZXRhaWxzKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHR5cGU6IFRZUEVfU0FWRV9DT1VOVFJZX0NPREUsXHJcbiAgICAgICAgdmFsdWU6IGRldGFpbHNcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IFRZUEVfU0FWRV9DVVJSRU5UX0FERFJFU1MgPSBcIlRZUEVfU0FWRV9DVVJSRU5UX0FERFJFU1NcIlxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHNhdmVDdXJyZW50QWRkcmVzc0luUmVkdXgoZGV0YWlscykge1xyXG4gICAgLy9jb25zb2xlLmxvZyhkZXRhaWxzKTtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgdHlwZTogVFlQRV9TQVZFX0NVUlJFTlRfQUREUkVTUyxcclxuICAgICAgICB2YWx1ZTogZGV0YWlsc1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgVFlQRV9TQVZFX0NVUlJFTlRfUkVTVEFVUkFOVF9JRCA9IFwiVFlQRV9TQVZFX0NVUlJFTlRfUkVTVEFVUkFOVF9JRFwiXHJcblxyXG5leHBvcnQgZnVuY3Rpb24gc2F2ZUN1cnJlbnRSZXN0YXVyYW50SWRJblJlZHV4KGRldGFpbHMpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgdHlwZTogVFlQRV9TQVZFX0NVUlJFTlRfUkVTVEFVUkFOVF9JRCxcclxuICAgICAgICB2YWx1ZTogZGV0YWlsc1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgVFlQRV9TQVZFX0ZBVk9SSVRFX0lURU1TID0gXCJUWVBFX1NBVkVfRkFWT1JJVEVfSVRFTVNcIlxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHNhdmVGYXZvcml0ZUl0ZW1zSW5SZWR1eChkZXRhaWxzKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHR5cGU6IFRZUEVfU0FWRV9GQVZPUklURV9JVEVNUyxcclxuICAgICAgICB2YWx1ZTogZGV0YWlsc1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgVFlQRV9TQVZFX0ZBVk9SSVRFX1NIT1BTID0gXCJUWVBFX1NBVkVfRkFWT1JJVEVfU0hPUFNcIlxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHNhdmVGYXZvcml0ZVNob3BzSW5SZWR1eChkZXRhaWxzKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHR5cGU6IFRZUEVfU0FWRV9GQVZPUklURV9TSE9QUyxcclxuICAgICAgICB2YWx1ZTogZGV0YWlsc1xyXG4gICAgfVxyXG59XHJcblxyXG4iLCJpbXBvcnQge1NBVkVfQ0FSVF9DT1VOVCwgU0FWRV9DQVJUX1RPVEFMLCBUWVBFX1NBVkVfQ0hFQ0tPVVRfREVUQUlMU30gZnJvbSBcIi4uL2FjdGlvbnMvQ2hlY2tvdXRcIjtcclxuXHJcbmNvbnN0IGluaXRhbFN0YXRlID0ge1xyXG4gICAgY2hlY2tvdXREZXRhaWw6IHt9LFxyXG4gICAgY2FydENvdW50OiAwLFxyXG4gICAgY2FydFRvdGFsOiAwXHJcbn07XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gY2hlY2tvdXREZXRhaWxPcGVyYXRpb24oc3RhdGUgPSBpbml0YWxTdGF0ZSwgYWN0aW9uKSB7XHJcbiAgICBzd2l0Y2ggKGFjdGlvbi50eXBlKSB7XHJcbiAgICAgICAgY2FzZSBUWVBFX1NBVkVfQ0hFQ0tPVVRfREVUQUlMUzoge1xyXG4gICAgICAgICAgICByZXR1cm4gT2JqZWN0LmFzc2lnbih7fSwgc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIGNoZWNrb3V0RGV0YWlsOiBhY3Rpb24udmFsdWVcclxuICAgICAgICAgICAgICAgIC8vIGFkZHJlc3NfaWQ6IGFjdGlvbi52YWx1ZS5hZGRyZXNzX2lkLFxyXG4gICAgICAgICAgICAgICAgLy8gc3VidG90YWw6IGFjdGlvbi52YWx1ZS5zdWJ0b3RhbCxcclxuICAgICAgICAgICAgICAgIC8vIGl0ZW1zOiBhY3Rpb24udmFsdWUuaXRlbXMsXHJcbiAgICAgICAgICAgICAgICAvLyBjb3Vwb25faWQ6IGFjdGlvbi52YWx1ZS5jb3Vwb25faWQsXHJcbiAgICAgICAgICAgICAgICAvLyBjb3Vwb25fdHlwZTogYWN0aW9uLnZhbHVlLmNvdXBvbl90eXBlLFxyXG4gICAgICAgICAgICAgICAgLy8gY291cG9uX2Ftb3VudDogYWN0aW9uLnZhbHVlLmNvdXBvbl9hbW91bnQsXHJcbiAgICAgICAgICAgICAgICAvLyB1c2VyX2lkOiBhY3Rpb24udmFsdWUudXNlcl9pZCxcclxuICAgICAgICAgICAgICAgIC8vIHJlc3RhdXJhbnRfaWQ6IGFjdGlvbi52YWx1ZS5yZXNJZFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY2FzZSBTQVZFX0NBUlRfQ09VTlQ6IHtcclxuICAgICAgICAgICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oe30sIHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICBjYXJ0Q291bnQ6IGFjdGlvbi52YWx1ZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY2FzZSBTQVZFX0NBUlRfVE9UQUw6IHtcclxuICAgICAgICAgICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oe30sIHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICBjYXJ0VG90YWw6IGFjdGlvbi52YWx1ZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgcmV0dXJuIHN0YXRlO1xyXG4gICAgfVxyXG59XHJcbiIsImltcG9ydCB7VFlQRV9TQVZFX05BVklHQVRJT05fU0VMRUNUSU9OfSBmcm9tIFwiLi4vYWN0aW9ucy9OYXZpZ2F0aW9uXCI7XHJcblxyXG5jb25zdCBpbml0YWxTdGF0ZSA9IHtcclxuICAgIHNlbGVjdGVkSXRlbTogXCJIb21lXCJcclxufTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBuYXZpZ2F0aW9uT3BlcmF0aW9uKHN0YXRlID0gaW5pdGFsU3RhdGUsIGFjdGlvbikge1xyXG4gICAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xyXG4gICAgICAgIGNhc2UgVFlQRV9TQVZFX05BVklHQVRJT05fU0VMRUNUSU9OOiB7XHJcbiAgICAgICAgICAgIHJldHVybiBPYmplY3QuYXNzaWduKHt9LCBzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgc2VsZWN0ZWRJdGVtOiBhY3Rpb24udmFsdWVcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgIHJldHVybiBzdGF0ZTtcclxuICAgIH1cclxufVxyXG4iLCJpbXBvcnQge1RZUEVfU0FWRV9GQVZPUklURV9JVEVNUywgVFlQRV9TQVZFX0ZBVk9SSVRFX1NIT1BTfSBmcm9tIFwiLi4vYWN0aW9ucy9Vc2VyXCI7XHJcblxyXG5jb25zdCBpbml0aWFsRmF2b3JpdGUgPSB7XHJcbiAgICBmYXZvcml0ZUl0ZW1zOiB7XHJcbiAgICAgICAgaXRlbXM6IFtdXHJcbiAgICB9LFxyXG4gICAgZmF2b3JpdGVTaG9wczoge1xyXG4gICAgICAgIHNob3BzOiBbXVxyXG4gICAgfVxyXG59O1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIFVzZXJGYXZvcml0ZShzdGF0ZSA9IGluaXRpYWxGYXZvcml0ZSwgYWN0aW9uKSB7XHJcbiAgICBzd2l0Y2ggKGFjdGlvbi50eXBlKSB7XHJcbiAgICAgICAgY2FzZSBUWVBFX1NBVkVfRkFWT1JJVEVfSVRFTVM6IHtcclxuICAgICAgICAgICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oe30sIHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICBmYXZvcml0ZUl0ZW1zOiBhY3Rpb24udmFsdWVcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNhc2UgVFlQRV9TQVZFX0ZBVk9SSVRFX1NIT1BTOiB7XHJcbiAgICAgICAgICAgIHJldHVybiBPYmplY3QuYXNzaWduKHt9LCBzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgZmF2b3JpdGVTaG9wczogYWN0aW9uLnZhbHVlXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICByZXR1cm4gc3RhdGU7XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IHtcclxuICAgIFRZUEVfU0FWRV9DT1VOVFJZX0NPREUsXHJcbiAgICBUWVBFX1NBVkVfQ1VSUkVOVF9BRERSRVNTLFxyXG4gICAgVFlQRV9TQVZFX0NVUlJFTlRfUkVTVEFVUkFOVF9JRCxcclxuICAgIFRZUEVfU0FWRV9MT0dJTl9ERVRBSUxTLFxyXG4gICAgVFlQRV9TQVZFX0xPR0lOX0ZDTSxcclxuICAgIFRZUEVfU0FWRV9VU0VSX0FERFJFU1NFUyxcclxuICAgIFRZUEVfU0FWRV9DQVJUX0FERFJFU1NFU1xyXG59IGZyb20gXCIuLi9hY3Rpb25zL1VzZXJcIjtcclxuXHJcbmNvbnN0IGluaXRpYWxTdGF0ZVVzZXIgPSB7XHJcbiAgICAvLyBMT0dJTiBERVRBSUxTXHJcbiAgICBwaG9uZU51bWJlckluUmVkdXg6IHVuZGVmaW5lZCxcclxuICAgIHVzZXJJZEluUmVkdXg6IHVuZGVmaW5lZCxcclxuICAgIGN1cnJlbnRSZXN0YXVyYW50SWQ6dW5kZWZpbmVkLFxyXG4gICAgY3VycmVudEFkZHJlc3M6IHtcclxuICAgICAgICBsYW5kbWFyazogdW5kZWZpbmVkLFxyXG4gICAgICAgIGFkZHJlc3M6IHVuZGVmaW5lZCxcclxuICAgICAgICBsYXRpdHVkZTogdW5kZWZpbmVkLFxyXG4gICAgICAgIGxvbmdpdHVkZTogdW5kZWZpbmVkXHJcbiAgICB9LFxyXG4gICAgc2F2ZWRDYXJ0QWRkcmVzczp1bmRlZmluZWQsXHJcbn07XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gdXNlck9wZXJhdGlvbnMoc3RhdGUgPSBpbml0aWFsU3RhdGVVc2VyLCBhY3Rpb24pIHtcclxuICAgIHN3aXRjaCAoYWN0aW9uLnR5cGUpIHtcclxuICAgICAgICBjYXNlIFRZUEVfU0FWRV9MT0dJTl9ERVRBSUxTOiB7XHJcbiAgICAgICAgICAgIHJldHVybiBPYmplY3QuYXNzaWduKHt9LCBzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgcGhvbmVOdW1iZXJJblJlZHV4OiBhY3Rpb24udmFsdWUuUGhvbmVOdW1iZXIsXHJcbiAgICAgICAgICAgICAgICB1c2VySWRJblJlZHV4OiBhY3Rpb24udmFsdWUuVXNlcklELFxyXG4gICAgICAgICAgICAgICAgLy8gc2F2ZWRBZGRyZXNzZXM6IGFjdGlvbi52YWx1ZS5zYXZlZEFkZHJlc3Nlc1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY2FzZSBUWVBFX1NBVkVfVVNFUl9BRERSRVNTRVM6IHtcclxuICAgICAgICAgICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oe30sIHN0YXRlLCB7XHJcbiAgICAgICAgICAgICAgICBzYXZlZEFkZHJlc3NlczogYWN0aW9uLnZhbHVlXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjYXNlIFRZUEVfU0FWRV9DQVJUX0FERFJFU1NFUzoge1xyXG4gICAgICAgICAgICByZXR1cm4gT2JqZWN0LmFzc2lnbih7fSwgc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIHNhdmVkQ2FydEFkZHJlc3M6IGFjdGlvbi52YWx1ZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNhc2UgVFlQRV9TQVZFX0xPR0lOX0ZDTToge1xyXG4gICAgICAgICAgICByZXR1cm4gT2JqZWN0LmFzc2lnbih7fSwgc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIHRva2VuOiBhY3Rpb24udmFsdWVcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNhc2UgVFlQRV9TQVZFX0NPVU5UUllfQ09ERToge1xyXG4gICAgICAgICAgICByZXR1cm4gT2JqZWN0LmFzc2lnbih7fSwgc3RhdGUsIHtcclxuICAgICAgICAgICAgICAgIGNvZGU6IGFjdGlvbi52YWx1ZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY2FzZSBUWVBFX1NBVkVfQ1VSUkVOVF9BRERSRVNTOiB7XHJcbiAgICAgICAgICAgIHJldHVybiBPYmplY3QuYXNzaWduKHt9LCBzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgY3VycmVudEFkZHJlc3M6IGFjdGlvbi52YWx1ZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY2FzZSBUWVBFX1NBVkVfQ1VSUkVOVF9SRVNUQVVSQU5UX0lEOiB7XHJcbiAgICAgICAgICAgIHJldHVybiBPYmplY3QuYXNzaWduKHt9LCBzdGF0ZSwge1xyXG4gICAgICAgICAgICAgICAgY3VycmVudFJlc3RhdXJhbnRJZDogYWN0aW9uLnZhbHVlXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBjYXNlIFNBVkVfQ0FSVF9DT1VOVDoge1xyXG4gICAgICAgIC8vICAgcmV0dXJuIE9iamVjdC5hc3NpZ24oe30sIHN0YXRlLCB7XHJcbiAgICAgICAgLy8gICAgIGNhcnRDb3VudDogYWN0aW9uLnZhbHVlXHJcbiAgICAgICAgLy8gICB9KTtcclxuICAgICAgICAvLyB9XHJcbiAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgcmV0dXJuIHN0YXRlO1xyXG4gICAgfVxyXG59XHJcbiIsImltcG9ydCBNb21lbnQgZnJvbSBcIm1vbWVudFwiO1xyXG5cclxuLy8gQVBJIFVSTCBDT05TVEFOVFNcclxuXHJcbi8vIGV4cG9ydCBjb25zdCBCQVNFX1VSTF9BUEkgPSBcImh0dHA6Ly8yNy41NC4xNzAuMTg3L35yZXN0YXVyYS92MS9hcGkvXCI7XHJcbi8vIGV4cG9ydCBjb25zdCBCQVNFX1VSTF9BUEkgPSBcImh0dHA6Ly9lYXRhbmNlLmV2aW5jZWRldi5jb20vdjEvYXBpL1wiO1xyXG4vLyBleHBvcnQgY29uc3QgQkFTRV9VUkxfQVBJID0gXCJodHRwOi8vMTkyLjE2OC4xLjE0Ni9mb29kZGVsaS92MS9hcGkvXCI7XHJcblxyXG4vL2V4cG9ydCBjb25zdCBCQVNFX1VSTCA9IFwiaHR0cDovLzE5Mi4xNjguMC4zMVwiO1xyXG5cclxuZXhwb3J0IGNvbnN0IEJBU0VfVVJMID0gXCJodHRwczovL2Zvb2RzLmtobWVyb3JkZXIuY29tXCI7XHJcblxyXG5leHBvcnQgY29uc3QgQkFTRV9VUkxfQVBJID0gQkFTRV9VUkwgKyBcIi9wdWJsaWMvYXBpL1wiO1xyXG5cclxuZXhwb3J0IGNvbnN0IFJFR0lTVFJBVElPTl9VUkwgPSBCQVNFX1VSTF9BUEkgKyBcImFwcF9yZWdpc3RlclwiO1xyXG5cclxuZXhwb3J0IGNvbnN0IExPR0lOX1VSTCA9IEJBU0VfVVJMX0FQSSArIFwiYXBwX2xvZ2luXCI7XHJcblxyXG5leHBvcnQgY29uc3QgT1RQX1ZFUklGWSA9IEJBU0VfVVJMX0FQSSArIFwiYXBwX290cF92ZXJpZnlcIjtcclxuXHJcbmV4cG9ydCBjb25zdCBPVFBfUkVTRU5EID0gQkFTRV9VUkxfQVBJICsgXCJhcHBfb3RwX3Jlc2VuZFwiO1xyXG5cclxuZXhwb3J0IGNvbnN0IFBST0RVQ1RTX0JZX0NBVEUgPSBCQVNFX1VSTF9BUEkgKyBcImFwcF9nZXRfcHJvZHVjdF9ieV9jYXRlXCI7XHJcblxyXG5leHBvcnQgY29uc3QgTkVXX1BST0RVQ1RTID0gQkFTRV9VUkxfQVBJICsgXCJhcHBfZ2V0X3Byb2R1Y3RzXCI7XHJcblxyXG5leHBvcnQgY29uc3QgUkVDRU5UX1BST0RVQ1RTID0gQkFTRV9VUkxfQVBJICsgXCJhcHBfZ2V0X3JlY2VudF9wcm9kdWN0c1wiO1xyXG5cclxuZXhwb3J0IGNvbnN0IFBST0RVQ1RfREVUQUlMID0gQkFTRV9VUkxfQVBJICsgXCJhcHBfZ2V0X3Byb2R1Y3RfZGV0YWlsXCI7XHJcblxyXG5leHBvcnQgY29uc3QgQUREX1RPX0NBUlQgPSBCQVNFX1VSTF9BUEkgKyBcImFwcF9hZGR0b2NhcnRcIjtcclxuXHJcbmV4cG9ydCBjb25zdCBBRERfQUREUkVTUyA9IEJBU0VfVVJMX0FQSSArIFwiYXBwX3NhdmVfYWRkcmVzc1wiO1xyXG5leHBvcnQgY29uc3QgR0VUX0FERFJFU1MgPSBCQVNFX1VSTF9BUEkgKyBcImFwcF9nZXRfYWRkcmVzc2VzXCI7XHJcbmV4cG9ydCBjb25zdCBERUxFVEVfQUREUkVTUyA9IEJBU0VfVVJMX0FQSSArIFwiYXBwX2RlbGV0ZV9hZGRyZXNzXCI7XHJcblxyXG5leHBvcnQgY29uc3QgT1JERVJfTElTVElORyA9IEJBU0VfVVJMX0FQSSArIFwiYXBwX2dldF9vcmRlcnNcIjtcclxuXHJcbmV4cG9ydCBjb25zdCBMT0dPVVRfVVJMID0gQkFTRV9VUkxfQVBJICsgXCJsb2dvdXRcIjtcclxuZXhwb3J0IGNvbnN0IENPVU5UUllfQ09ERV9VUkwgPSBCQVNFX1VSTF9BUEkgKyBcImdldENvdW50cnlQaG9uZUNvZGVcIjtcclxuZXhwb3J0IGNvbnN0IERSSVZFUl9UUkFDS0lORyA9IEJBU0VfVVJMX0FQSSArIFwiZGVsaXZlcnktZ3V5LW9yZGVyLXRyYWNraW5nXCI7XHJcbmV4cG9ydCBjb25zdCBDSEFOR0VfVE9LRU4gPSBCQVNFX1VSTF9BUEkgKyBcImNoYW5nZVRva2VuXCI7XHJcbmV4cG9ydCBjb25zdCBDSEVDS19PUkRFUl9VUkwgPSBCQVNFX1VSTF9BUEkgKyBcImNoZWNrT3JkZXJEZWxpdmVyeVwiO1xyXG5leHBvcnQgY29uc3QgQ0hFQ0tfT1JERVJfREVMSVZFUkVEX1VSTCA9IEJBU0VfVVJMX0FQSSArIFwiY2hlY2tPcmRlckRlbGl2ZXJlZFwiO1xyXG5leHBvcnQgY29uc3QgR0VUX1JFU1RBVVJBTlRfREVUQUlMID0gQkFTRV9VUkxfQVBJICsgXCJnZXQtcmVzdGF1cmFudC1pbmZvLWJ5LWlkXCI7XHJcblxyXG5leHBvcnQgY29uc3QgR0VUX05PVElGSUNBVElPTiA9IEJBU0VfVVJMX0FQSSArIFwiZ2V0Tm90aWZpY2F0aW9uXCI7XHJcbmV4cG9ydCBjb25zdCBBRERfUkVWSUVXID0gQkFTRV9VUkxfQVBJICsgXCJhZGRSZXZpZXdcIjtcclxuZXhwb3J0IGNvbnN0IEFERF9PUkRFUiA9IEJBU0VfVVJMX0FQSSArIFwicGxhY2VvcmRlcmFwaVwiO1xyXG5leHBvcnQgY29uc3QgQ01TX1BBR0UgPSBCQVNFX1VSTF9BUEkgKyBcImdldENNU1BhZ2VcIjtcclxuZXhwb3J0IGNvbnN0IFBST01PX0NPREVfTElTVCA9IEJBU0VfVVJMX0FQSSArIFwiY291cG9uTGlzdFwiO1xyXG5leHBvcnQgY29uc3QgQVBQTFlfUFJPTU9fQ09ERSA9IEJBU0VfVVJMX0FQSSArIFwiY2hlY2tQcm9tb2NvZGVcIjtcclxuZXhwb3J0IGNvbnN0IEdFVF9SRUNJUEVfTElTVCA9IEJBU0VfVVJMX0FQSSArIFwiZ2V0UmVjZWlwZVwiO1xyXG5cclxuZXhwb3J0IGNvbnN0IFJFR0lTVFJBVElPTl9IT01FID0gQkFTRV9VUkxfQVBJICsgXCJnZXRIb21lXCI7XHJcbmV4cG9ydCBjb25zdCBHRVRfVEFHUyA9IEJBU0VfVVJMX0FQSSArIFwiZ2V0VGFnc1wiO1xyXG5cclxuZXhwb3J0IGNvbnN0IEdFVF9QT1BVTEFSX1RBR1MgPSBCQVNFX1VSTF9BUEkgKyBcImdldFBvcHVsYXJUYWdzXCI7XHJcblxyXG5leHBvcnQgY29uc3QgR0VUX01PUkVfUFJPRFVDVFMgPSBCQVNFX1VSTF9BUEkgKyBcImFwcF9nZXRfcHJvZHVjdHNcIjtcclxuXHJcbmV4cG9ydCBjb25zdCBHRVRfSVRFTV9DQVRFR09SSUVTID0gQkFTRV9VUkxfQVBJICsgXCJnZXRDYXRlZ29yaWVzXCI7XHJcblxyXG5leHBvcnQgY29uc3QgU0VBUkNIX1JFU1RBVVJBTlRTID0gQkFTRV9VUkxfQVBJICsgXCJzZWFyY2gtcmVzdGF1cmFudHNcIjtcclxuXHJcbmV4cG9ydCBjb25zdCBDSEVDS19CT09LSU5HX0FWQUlMID0gQkFTRV9VUkxfQVBJICsgXCJib29raW5nQXZhaWxhYmxlXCI7XHJcbmV4cG9ydCBjb25zdCBCT09LSU5HX0VWRU5UID0gQkFTRV9VUkxfQVBJICsgXCJib29rRXZlbnRcIjtcclxuZXhwb3J0IGNvbnN0IEJPT0tJTkdfSElTVE9SWSA9IEJBU0VfVVJMX0FQSSArIFwiZ2V0Qm9va2luZ1wiO1xyXG5leHBvcnQgY29uc3QgREVMRVRFX0VWRU5UID0gQkFTRV9VUkxfQVBJICsgXCJkZWxldGVCb29raW5nXCI7XHJcbmV4cG9ydCBjb25zdCBVUERBVEVfUFJPRklMRSA9IEJBU0VfVVJMX0FQSSArIFwiZWRpdFByb2ZpbGVcIjtcclxuZXhwb3J0IGNvbnN0IFJFU0VUX1BBU1NXT1JEX1JFUV9VUkwgPSBCQVNFX1VSTF9BUEkgKyBcImNoYW5nZVBhc3N3b3JkXCI7XHJcbmV4cG9ydCBjb25zdCBGT1JHT1RfUEFTU1dPUkQgPSBCQVNFX1VSTF9BUEkgKyBcImZvcmdvdHBhc3N3b3JkXCI7XHJcbmV4cG9ydCBjb25zdCBWRVJJRllfT1RQID0gQkFTRV9VUkxfQVBJICsgXCJ2ZXJpZnlPVFBcIjtcclxuZXhwb3J0IGNvbnN0IElOUl9TSUdOID0gXCJVU0QgXFx1MDAyNFwiO1xyXG5leHBvcnQgY29uc3QgSU5SX1NIT1JUX1NJR04gPSBcIlxcdTAwMjRcIjtcclxuXHJcbi8vIEFMRVJUIENPTlNUQU5UU1xyXG5leHBvcnQgY29uc3QgUkVTRVJWRV9TVEFUSUMgPSBcIi9yZXNlcnZlXCI7XHJcbmV4cG9ydCBjb25zdCBET0NVTUVOVFNfU1RBVElDID0gXCIvZG9jdW1lbnRzXCI7XHJcbmV4cG9ydCBjb25zdCBBUFBfTkFNRSA9IFwiSnVzdCBPcmRlclwiO1xyXG5leHBvcnQgY29uc3QgREVGQVVMVF9BTEVSVF9USVRMRSA9IEFQUF9OQU1FO1xyXG5leHBvcnQgY29uc3QgQWxlcnRCdXR0b25zID0ge1xyXG4gICAgb2s6IFwiT0tcIixcclxuICAgIGNhbmNlbDogXCJDYW5jZWxcIixcclxuICAgIG5vdE5vdzogXCJOb3Qgbm93XCIsXHJcbiAgICB5ZXM6IFwiWWVzXCIsXHJcbiAgICBubzogXCJOb1wiXHJcbn07XHJcblxyXG4vLyBSRVFVRVNUUyBDT05TVEFOVFNcclxuZXhwb3J0IGNvbnN0IFJlcXVlc3RLZXlzID0ge1xyXG4gICAgY29udGVudFR5cGU6IFwiQ29udGVudC1UeXBlXCIsXHJcbiAgICBqc29uOiBcImFwcGxpY2F0aW9uL2pzb25cIixcclxuICAgIGF1dGhvcml6YXRpb246IFwiQXV0aG9yaXphdGlvblwiLFxyXG4gICAgYmVhcmVyOiBcIkJlYXJlclwiXHJcbn07XHJcblxyXG4vLyBMT0FOIFRZUEVTIENPTlNUQU5UU1xyXG5leHBvcnQgY29uc3QgTG9hblR5cGVzID0ge1xyXG4gICAgYXZhaWxhYmxlOiBcImF2YWlsYWJsZVwiLFxyXG4gICAgcmVzZXJ2ZWQ6IFwicmVzZXJ2ZWRcIixcclxuICAgIGNsb3NlZDogXCJjbG9zZWRcIlxyXG59O1xyXG5cclxuLy8gTE9DQVRJT04gTEFUSVRVREUgTE9OR0lUVURFXHJcbmV4cG9ydCBjb25zdCBERUZfTEFUSVRVREUgPSAxMS41NjA1NTA0O1xyXG5leHBvcnQgY29uc3QgREVGX0xPTkdJVFVERSA9IDEwNC44ODM4MTQ0O1xyXG5cclxuLy8gU1RPUkFHRSBDT05TVEFOVFNcclxuZXhwb3J0IGNvbnN0IFN0b3JhZ2VLZXlzID0ge1xyXG4gICAgdXNlcl9kZXRhaWxzOiBcIlVzZXJEZXRhaWxzXCJcclxufTtcclxuXHJcbi8vIFJFRFVYIENPTlNUQU5UU1xyXG5leHBvcnQgY29uc3QgQUNDRVNTX1RPS0VOID0gXCJBQ0NFU1NfVE9LRU5cIjtcclxuZXhwb3J0IGNvbnN0IFJFU1BPTlNFX0ZBSUwgPSAwO1xyXG5leHBvcnQgY29uc3QgUkVTUE9OU0VfU1VDQ0VTUyA9IDE7XHJcbmV4cG9ydCBjb25zdCBDT1VQT05fRVJST1IgPSAyO1xyXG5cclxuZXhwb3J0IGNvbnN0IEdPT0dMRV9BUElfS0VZID0gXCJBSXphU3lDQXpLSDBBVlJ5WGtLclA2WEVjSzJpOWJHTUh3cjc3MWNcIjtcclxuXHJcblxyXG4vL05PVElGSUNBVElPTl9UWVBFXHJcbmV4cG9ydCBjb25zdCBPUkRFUl9UWVBFID0gXCJvcmRlck5vdGlmaWNhdGlvblwiO1xyXG5leHBvcnQgY29uc3QgTk9USUZJQ0FUSU9OX1RZUEUgPSBcIm5vdGlmaWNhdGlvblwiO1xyXG5leHBvcnQgY29uc3QgREVGQVVMVF9UWVBFID0gXCJkZWZhdWx0XCI7XHJcblxyXG4vL01FU1NBR0VTXHJcbmV4cG9ydCBjb25zdCBDQVJUX1BFTkRJTkdfSVRFTVMgPVxyXG4gICAgXCJZb3UgaGF2ZSBwZW5kaW5nIGl0ZW1zIGluIGNhcnQgZnJvbSBhbm90aGVyIHJlc3RhdXJhbnRcIjtcclxuZXhwb3J0IGNvbnN0IFNFQVJDSF9QTEFDRUhPTERFUiA9IFwiU2VhcmNoIGZvciByZXN0YXVyYW50LCBjdWlzaW5lIG9yIGRpc2hcIjtcclxuXHJcbi8vQ01TIFBBR0VcclxuZXhwb3J0IGNvbnN0IEFCT1VUX1VTID0gMTtcclxuZXhwb3J0IGNvbnN0IENPTlRBQ1RfVVMgPSAyO1xyXG5leHBvcnQgY29uc3QgUFJJVkFDWV9QT0xJQ1kgPSAzO1xyXG5cclxuZXhwb3J0IGNvbnN0IGZ1bkdldFRpbWUgPSBkYXRlID0+IHtcclxuICAgIHZhciBkID0gbmV3IERhdGUoZGF0ZSk7XHJcbiAgICByZXR1cm4gTW9tZW50KGQpLmZvcm1hdChcIkxUXCIpO1xyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGNoZWNrTnVsbCA9IGRhdGEgPT4ge1xyXG4gICAgaWYgKGRhdGEgIT0gdW5kZWZpbmVkICYmIGRhdGEgIT0gXCJcIikge1xyXG4gICAgICAgIHJldHVybiBkYXRhO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgZnVuR2V0RGF0ZSA9IGRhdGUgPT4ge1xyXG4gICAgdmFyIGQgPSBuZXcgRGF0ZShkYXRlKTtcclxuICAgIHJldHVybiBNb21lbnQoZCkuZm9ybWF0KFwiREQtTU0tWVlZWVwiKTtcclxufTtcclxuXHJcbmV4cG9ydCBjb25zdCBmdW5HZXRUb21vcnJvd0RhdGUgPSAoKSA9PiB7XHJcbiAgICB2YXIgZCA9IG5ldyBEYXRlKCk7XHJcbiAgICB2YXIgbmV3RGF0ZSA9IE1vbWVudChkKS5hZGQoMSwgXCJkYXlcIik7XHJcblxyXG4gICAgcmV0dXJuIG5ldyBEYXRlKG5ld0RhdGUpO1xyXG59O1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGZ1bkdldERhdGVTdHIoZGF0ZSwgZm9ybWF0cykge1xyXG4gICAgaWYgKGZvcm1hdHMgPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgZm9ybWF0cyA9IFwiREQtTU0tWVlZWVwiO1xyXG4gICAgfVxyXG4gICAgTW9tZW50LmxvY2FsZShcImVuXCIpO1xyXG5cclxuICAgIHZhciBkID0gbmV3IERhdGUoXCJcIiArIGRhdGUucmVwbGFjZUFsbChcIi1cIiwgXCIvXCIpKTtcclxuXHJcbiAgICByZXR1cm4gTW9tZW50KGQpLmZvcm1hdChmb3JtYXRzKTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGZ1bkdldFRpbWVTdHIoZGF0ZSkge1xyXG4gICAgTW9tZW50LmxvY2FsZShcImVuXCIpO1xyXG5cclxuICAgIHZhciBkID0gbmV3IERhdGUoXCJcIiArIGRhdGUucmVwbGFjZUFsbChcIi1cIiwgXCIvXCIpKTtcclxuXHJcbiAgICByZXR1cm4gTW9tZW50KGQpLmZvcm1hdChcIkxUXCIpO1xyXG59XHJcblxyXG5TdHJpbmcucHJvdG90eXBlLnJlcGxhY2VBbGwgPSBmdW5jdGlvbiAoc2VhcmNoLCByZXBsYWNlbWVudCkge1xyXG4gICAgdmFyIHRhcmdldCA9IHRoaXM7XHJcbiAgICByZXR1cm4gdGFyZ2V0LnNwbGl0KHNlYXJjaCkuam9pbihyZXBsYWNlbWVudCk7XHJcbn07XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gcmVtb3ZlQ2hhcnMoc3RyKSB7XHJcbiAgICB2YXIgdGFyZ2V0ID0gXCJcIjtcclxuXHJcbiAgICB2YXIgc3RyQXJyYXkgPSBzdHIuc3BsaXQoXCIsXCIpO1xyXG4gICAgY29uc3Qgcm93TGVuID0gc3RyQXJyYXkubGVuZ3RoO1xyXG4gICAgdHJ5IHtcclxuICAgICAgICBpZiAoc3RyQXJyYXkgIT0gdW5kZWZpbmVkKVxyXG4gICAgICAgICAgICBzdHJBcnJheS5tYXAoKGRhdGEsIGkpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChkYXRhLnRyaW0oKSA9PT0gXCJcIiB8fCBkYXRhLnRyaW0oKSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyb3dMZW4gPT09IGkgKyAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldCA9IHRhcmdldCArIGRhdGE7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0ID0gdGFyZ2V0ICsgZGF0YSArIFwiLFwiO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdGFyZ2V0O1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gY2FwaXRhbGl6ZShzdHJpbmcpIHtcclxuICAgIHJldHVybiBzdHJpbmcuY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyBzdHJpbmcuc2xpY2UoMSk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBjYXBpU3RyaW5nKHN0cikge1xyXG4gICAgdmFyIHNwbGl0U3RyID0gc3RyLnRvTG93ZXJDYXNlKCkuc3BsaXQoXCIgXCIpO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBzcGxpdFN0ci5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIC8vIFlvdSBkbyBub3QgbmVlZCB0byBjaGVjayBpZiBpIGlzIGxhcmdlciB0aGFuIHNwbGl0U3RyIGxlbmd0aCwgYXMgeW91ciBmb3IgZG9lcyB0aGF0IGZvciB5b3VcclxuICAgICAgICAvLyBBc3NpZ24gaXQgYmFjayB0byB0aGUgYXJyYXlcclxuICAgICAgICBzcGxpdFN0cltpXSA9XHJcbiAgICAgICAgICAgIHNwbGl0U3RyW2ldLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgc3BsaXRTdHJbaV0uc3Vic3RyaW5nKDEpO1xyXG4gICAgfVxyXG4gICAgLy8gRGlyZWN0bHkgcmV0dXJuIHRoZSBqb2luZWQgc3RyaW5nXHJcbiAgICByZXR1cm4gc3BsaXRTdHIuam9pbihcIiBcIik7XHJcbn1cclxuIiwiaW1wb3J0IHsgU3R5bGVTaGVldCwgVGV4dCwgVmlldyB9IGZyb20gJ3JlYWN0LW5hdGl2ZSdcclxuXHJcbmltcG9ydCB7Y29tYmluZVJlZHVjZXJzLCBjcmVhdGVTdG9yZX0gZnJvbSBcInJlZHV4XCI7XHJcbmltcG9ydCB7UHJvdmlkZXJ9IGZyb20gXCJyZWFjdC1yZWR1eFwiO1xyXG5cclxuaW1wb3J0IHt1c2VyT3BlcmF0aW9uc30gZnJvbSBcIi4vYXBwL3JlZHV4L3JlZHVjZXJzL1VzZXJSZWR1Y2VyXCI7XHJcbmltcG9ydCB7VXNlckZhdm9yaXRlfSBmcm9tIFwiLi9hcHAvcmVkdXgvcmVkdWNlcnMvVXNlckZhdm9yaXRlXCI7XHJcbmltcG9ydCB7bmF2aWdhdGlvbk9wZXJhdGlvbn0gZnJvbSBcIi4vYXBwL3JlZHV4L3JlZHVjZXJzL05hdmlnYXRpb25SZWR1Y2VyXCI7XHJcbmltcG9ydCB7Y2hlY2tvdXREZXRhaWxPcGVyYXRpb259IGZyb20gXCIuL2FwcC9yZWR1eC9yZWR1Y2Vycy9DaGVja291dFJlZHVjZXJcIjtcclxuaW1wb3J0IE1haW5Db250YWluZXIgZnJvbSBcIi4vYXBwL2NvbnRhaW5lcnMvTWFpbkNvbnRhaW5lclwiO1xyXG5pbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XHJcblxyXG5jb25zdCByb290UmVkdWNlciA9IGNvbWJpbmVSZWR1Y2Vycyh7XHJcbiAgdXNlck9wZXJhdGlvbnM6IHVzZXJPcGVyYXRpb25zLFxyXG4gIG5hdmlnYXRpb25SZWR1Y2VyOiBuYXZpZ2F0aW9uT3BlcmF0aW9uLFxyXG4gIGNoZWNrb3V0UmVkdWNlcjogY2hlY2tvdXREZXRhaWxPcGVyYXRpb24sXHJcbiAgdXNlckZhdm9yaXRlOiBVc2VyRmF2b3JpdGVcclxufSk7XHJcblxyXG5cclxuXHJcbmNvbnN0IEp1c3RPcmRlckdsb2JhbFN0b3JlID0gY3JlYXRlU3RvcmUocm9vdFJlZHVjZXIpO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gQXBwKHByb3BzKSB7XHJcbiAgcmV0dXJuIChcclxuICAgICAgPFByb3ZpZGVyIHN0b3JlPXtKdXN0T3JkZXJHbG9iYWxTdG9yZX0+XHJcbiAgICAgICAgPE1haW5Db250YWluZXIvPlxyXG4gICAgICA8L1Byb3ZpZGVyPlxyXG4gIClcclxufVxyXG5cclxuY29uc3Qgc3R5bGVzID0gU3R5bGVTaGVldC5jcmVhdGUoe1xyXG4gIGNvbnRhaW5lcjoge1xyXG4gICAgYWxpZ25JdGVtczogJ2NlbnRlcicsXHJcbiAgICBmbGV4R3JvdzogMSxcclxuICAgIGp1c3RpZnlDb250ZW50OiAnY2VudGVyJyxcclxuICB9LFxyXG4gIGxpbms6IHtcclxuICAgIGNvbG9yOiAnYmx1ZScsXHJcbiAgfSxcclxuICB0ZXh0Q29udGFpbmVyOiB7XHJcbiAgICBhbGlnbkl0ZW1zOiAnY2VudGVyJyxcclxuICAgIG1hcmdpblRvcDogMTYsXHJcbiAgfSxcclxuICB0ZXh0OiB7XHJcbiAgICBhbGlnbkl0ZW1zOiAnY2VudGVyJyxcclxuICAgIGZvbnRTaXplOiAyNCxcclxuICAgIG1hcmdpbkJvdHRvbTogMjQsXHJcbiAgfSxcclxufSlcclxuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibW9tZW50XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LW5hdGl2ZS13ZWIvZGlzdC9janMvZXhwb3J0cy9BY3Rpdml0eUluZGljYXRvclwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1uYXRpdmUtd2ViL2Rpc3QvY2pzL2V4cG9ydHMvRGltZW5zaW9uc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1uYXRpdmUtd2ViL2Rpc3QvY2pzL2V4cG9ydHMvRmxhdExpc3RcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtbmF0aXZlLXdlYi9kaXN0L2Nqcy9leHBvcnRzL0ltYWdlXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LW5hdGl2ZS13ZWIvZGlzdC9janMvZXhwb3J0cy9JbWFnZUJhY2tncm91bmRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtbmF0aXZlLXdlYi9kaXN0L2Nqcy9leHBvcnRzL1N0eWxlU2hlZXRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtbmF0aXZlLXdlYi9kaXN0L2Nqcy9leHBvcnRzL1RleHRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtbmF0aXZlLXdlYi9kaXN0L2Nqcy9leHBvcnRzL1RvdWNoYWJsZU9wYWNpdHlcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtbmF0aXZlLXdlYi9kaXN0L2Nqcy9leHBvcnRzL1ZpZXdcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtcmVkdXhcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVkdXhcIik7Il0sInNvdXJjZVJvb3QiOiIifQ==