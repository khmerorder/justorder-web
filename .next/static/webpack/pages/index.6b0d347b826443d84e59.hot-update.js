webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/app/containers/MainContainer.js":
/*!***********************************************!*\
  !*** ./pages/app/containers/MainContainer.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MainContainer; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/FlatList */ "./node_modules/react-native-web/dist/cjs/exports/FlatList/index.js");
/* harmony import */ var react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/StyleSheet */ "./node_modules/react-native-web/dist/cjs/exports/StyleSheet/index.js");
/* harmony import */ var react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/Text */ "./node_modules/react-native-web/dist/cjs/exports/Text/index.js");
/* harmony import */ var react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/TouchableOpacity */ "./node_modules/react-native-web/dist/cjs/exports/TouchableOpacity/index.js");
/* harmony import */ var react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/View */ "./node_modules/react-native-web/dist/cjs/exports/View/index.js");
/* harmony import */ var react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/ImageBackground */ "./node_modules/react-native-web/dist/cjs/exports/ImageBackground/index.js");
/* harmony import */ var react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var react_native_web_dist_cjs_exports_Image__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/Image */ "./node_modules/react-native-web/dist/cjs/exports/Image/index.js");
/* harmony import */ var react_native_web_dist_cjs_exports_Image__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_Image__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _BaseContainer__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./BaseContainer */ "./pages/app/containers/BaseContainer.js");
/* harmony import */ var _assets__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../assets */ "./pages/app/assets/index.js");
/* harmony import */ var _assets_FontConstants__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../assets/FontConstants */ "./pages/app/assets/FontConstants.js");
/* harmony import */ var _api_ServiceManager__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../api/ServiceManager */ "./pages/app/api/ServiceManager.js");
/* harmony import */ var _utils_Constants__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../utils/Constants */ "./pages/app/utils/Constants.js");
/* harmony import */ var _components_PopularRestaurants__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../components/PopularRestaurants */ "./pages/app/components/PopularRestaurants.js");
/* harmony import */ var _assets_Colors__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../assets/Colors */ "./pages/app/assets/Colors.js");
/* harmony import */ var _components_ETextViewNormalLabel__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../components/ETextViewNormalLabel */ "./pages/app/components/ETextViewNormalLabel.js");









var _jsxFileName = "G:\\JustOrderWeb\\pages\\app\\containers\\MainContainer.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement;

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }












 //import {netStatus} from "../utils/NetworkStatusConnection"






var MainContainer = /*#__PURE__*/function (_React$Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(MainContainer, _React$Component);

  var _super = _createSuper(MainContainer);

  function MainContainer() {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, MainContainer);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this), "state", {
      arrayRestaurants: undefined
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this), "allRestaurantRows", function () {
      var popularRestaurants = _this.state.arrayRestaurants != undefined && _this.state.arrayRestaurants != null && _this.state.arrayRestaurants.length > 0 ? _this.showRestaurantRows() : _this.state.arrayRestaurants != undefined && _this.state.arrayRestaurants != null && _this.state.arrayRestaurants.length == 0 ? null : _this.state.isLoading ? null : __jsx(react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_13___default.a, {
        style: {
          alignSelf: 'center',
          backgroundColor: _assets_Colors__WEBPACK_IMPORTED_MODULE_23__["APPCOLORS"].group1Main,
          padding: 10,
          marginVertical: 100
        } // onPress={() => this.refreshScreen}
        ,
        __self: Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59,
          columnNumber: 21
        }
      }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default.a, {
        style: {
          color: _assets_Colors__WEBPACK_IMPORTED_MODULE_23__["EDColors"].white
        },
        __self: Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68,
          columnNumber: 25
        }
      }, "Reload"));
      return __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14___default.a, {
        style: {
          flex: 1,
          minHeight: 200
        },
        __self: Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76,
          columnNumber: 13
        }
      }, __jsx(_components_ETextViewNormalLabel__WEBPACK_IMPORTED_MODULE_24__["default"], {
        style: {
          marginLeft: 10,
          marginBottom: 10
        },
        text: "Featured",
        __self: Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77,
          columnNumber: 17
        }
      }), popularRestaurants);
    });

    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(MainContainer, [{
    key: "showRestaurantRows",
    value: function showRestaurantRows() {
      return __jsx(_components_PopularRestaurants__WEBPACK_IMPORTED_MODULE_22__["default"] // onScrollEndDrag={(e)=>{
      //     //this.setState({scolledOffsetY:e.nativeEvent.contentOffset.y});
      //     // console.log(e.nativeEvent.contentOffset.y);
      //     //this.setState({showScrollUpButton: e.nativeEvent.contentOffset.y >= height})
      //
      // }}
      , {
        restaurants: this.state.arrayRestaurants //itemPress={this.gotoRestaurant}
        //refreshScreen={this.refreshScreen}
        //nextPage={this.nextRestaurantPage}
        //isLoading={this.state.loadingRestaurant}
        ,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 32,
          columnNumber: 13
        }
      });
    }
  }, {
    key: "getData",
    value: function getData() {
      var _this2 = this;

      var param = {};
      Object(_api_ServiceManager__WEBPACK_IMPORTED_MODULE_20__["apiPost"])(_utils_Constants__WEBPACK_IMPORTED_MODULE_21__["REGISTRATION_HOME"], param, function (resp) {
        if (resp != undefined) {
          if (resp.status == _utils_Constants__WEBPACK_IMPORTED_MODULE_21__["RESPONSE_SUCCESS"]) {
            var restaurants = resp.restaurants; //.splice(0,10);

            console.log("restaurants ::::: ", restaurants);

            _this2.setState({
              arrayRestaurants: undefined
            }, function () {
              _this2.setState({
                arrayRestaurants: restaurants
              });
            });

            _this2.last_page = resp.last_page;
            _this2.current_page = 1;
            console.log("current_page : ", _this2.current_page);
          }
        }

        _this2.setState({
          isLoading: false
        });

        if (_this2.state.showSplashLoader) {
          setTimeout(function () {
            _this2.setState({
              showSplashLoader: false
            });
          }, 3000);
        }
      }, function (err) {
        _this2.setState({
          isLoading: false
        });
      });
    }
  }, {
    key: "allMainRow",
    value: function allMainRow() {
      var rows = []; // rows.push(this.slider());
      // rows.push(this.businessIcons());
      // rows.push(this.anouncementRow());

      rows.push(__jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14___default.a, {
        style: {
          flexDirection: "row",
          height: 100,
          marginHorizontal: 7,
          marginVertical: 10
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 134,
          columnNumber: 13
        }
      }, __jsx(react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_13___default.a, {
        onPress: function onPress() {// if(this.merchantModal){
          //     this.merchantModal.show();
          // }
        },
        style: {
          flex: 1,
          paddingHorizontal: 3
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 135,
          columnNumber: 17
        }
      }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14___default.a, {
        style: {
          width: "100%",
          backgroundColor: "#7d3e1e",
          height: "100%",
          overflow: "hidden",
          borderRadius: 5
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 143,
          columnNumber: 21
        }
      }, __jsx(react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_15___default.a, {
        source: _assets__WEBPACK_IMPORTED_MODULE_18__["default"].merchantAds,
        resizeMode: "contain",
        style: {
          width: "100%",
          height: "100%"
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 144,
          columnNumber: 25
        }
      }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14___default.a, {
        style: {
          paddingHorizontal: 5,
          paddingVertical: 0
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 145,
          columnNumber: 29
        }
      }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default.a, {
        style: {
          fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_19__["APPFONTS"].regular,
          fontSize: 14,
          letterSpacing: -0.2
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 146,
          columnNumber: 33
        }
      }, "Become a Merchant Partner"), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default.a, {
        style: {
          fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_19__["APPFONTS"].regular,
          fontSize: 12
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 147,
          columnNumber: 33
        }
      }, "Help you to Increase Revenue"))), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default.a, {
        style: styles.joinNow,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 150,
          columnNumber: 25
        }
      }, "Join Now"))), __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14___default.a, {
        style: {
          width: 100,
          height: 100,
          backgroundColor: "red"
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 156,
          columnNumber: 17
        }
      }, __jsx(react_native_web_dist_cjs_exports_Image__WEBPACK_IMPORTED_MODULE_16___default.a, {
        style: {
          width: 100,
          height: 100
        },
        source: _assets__WEBPACK_IMPORTED_MODULE_18__["default"].merchantAds,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 156,
          columnNumber: 76
        }
      })), __jsx(react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_13___default.a, {
        onPress: function onPress() {// if(this.jobVacancyModal){
          //     this.jobVacancyModal.show();
          // }
        },
        style: {
          flex: 1,
          paddingHorizontal: 3
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 158,
          columnNumber: 17
        }
      }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14___default.a, {
        style: {
          width: "100%",
          backgroundColor: "#7d3e1e",
          height: "100%",
          overflow: "hidden",
          borderRadius: 5
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 165,
          columnNumber: 21
        }
      }, __jsx(react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_15___default.a, {
        source: _assets__WEBPACK_IMPORTED_MODULE_18__["default"].vacancyAds,
        resizeMode: "contain",
        style: {
          width: "100%",
          height: "100%"
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 166,
          columnNumber: 25
        }
      }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14___default.a, {
        style: {
          paddingHorizontal: 5,
          paddingVertical: 0
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 167,
          columnNumber: 29
        }
      }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default.a, {
        style: {
          fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_19__["APPFONTS"].regular,
          fontSize: 14,
          letterSpacing: -1
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 168,
          columnNumber: 33
        }
      }, "Job Vacancy"), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default.a, {
        style: {
          fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_19__["APPFONTS"].regular,
          fontSize: 12
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 169,
          columnNumber: 33
        }
      }, "New job is available now"))), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default.a, {
        style: styles.joinNow,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 172,
          columnNumber: 25
        }
      }, "Apply Now"))))); // Loading

      rows.push(this.allRestaurantRows());
      return rows;
    }
  }, {
    key: "componentDidMount",
    value: function () {
      var _componentDidMount = Object(_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.getData();

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function componentDidMount() {
        return _componentDidMount.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      return __jsx(_BaseContainer__WEBPACK_IMPORTED_MODULE_17__["default"], {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 191,
          columnNumber: 13
        }
      }, __jsx(react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_10___default.a // onScroll={this._onListScroll}
      , {
        ref: function ref(_ref2) {
          return _this3.mainFlatList = _ref2;
        } // refreshControl={
        //     <RefreshControl
        //         refreshing={this.state.refreshing}
        //         onRefresh={this._onRefresh}
        //     />
        // }
        ,
        data: this.allMainRow(),
        renderItem: function renderItem(_ref) {
          var item = _ref.item;
          return item;
        },
        keyExtractor: function keyExtractor(item, index) {
          return String(index);
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 192,
          columnNumber: 17
        }
      }));
    }
  }]);

  return MainContainer;
}(react__WEBPACK_IMPORTED_MODULE_9___default.a.Component);


var styles = react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_11___default.a.create({
  scrollUpButton: {
    zIndex: 999,
    right: 10,
    bottom: 60,
    // shadowColor: "#000",
    // shadowOffset: {
    //     width: 20,
    //     height: 20,
    // },
    // shadowOpacity: 1,
    // shadowRadius: 9,
    // elevation: 9,
    backgroundColor: "rgba(0,0,0,0.6)"
  },
  discount_stick: {
    position: "absolute",
    right: 0,
    top: 0,
    zIndex: 1,
    // borderColor:"red",
    // borderWidth:1,
    // backgroundColor:"white",
    height: 35,
    width: 30,
    alignItems: "center",
    borderRadius: 2
  },
  itemImage: {
    width: "100%",
    height: "100%",
    alignSelf: "center",
    resizeMode: 'cover'
  },
  spinner: {
    flex: 1,
    alignSelf: "center",
    zIndex: 1000
  },
  anouncementRow: {
    marginTop: 10,
    marginStart: 10,
    marginEnd: 10,
    backgroundColor: "white",
    borderRadius: 5
  },
  joinNow: {
    position: "absolute",
    textAlign: "center",
    textAlignVertical: "center",
    // justifyContent:"center",
    // alignSelf:"center",
    flex: 1,
    bottom: 10,
    left: 5,
    backgroundColor: "yellow",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "yellow",
    width: 70,
    height: 25,
    fontSize: 12
  }
});

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvYXBwL2NvbnRhaW5lcnMvTWFpbkNvbnRhaW5lci5qcyJdLCJuYW1lcyI6WyJNYWluQ29udGFpbmVyIiwiYXJyYXlSZXN0YXVyYW50cyIsInVuZGVmaW5lZCIsInBvcHVsYXJSZXN0YXVyYW50cyIsInN0YXRlIiwibGVuZ3RoIiwic2hvd1Jlc3RhdXJhbnRSb3dzIiwiaXNMb2FkaW5nIiwiYWxpZ25TZWxmIiwiYmFja2dyb3VuZENvbG9yIiwiQVBQQ09MT1JTIiwiZ3JvdXAxTWFpbiIsInBhZGRpbmciLCJtYXJnaW5WZXJ0aWNhbCIsImNvbG9yIiwiRURDb2xvcnMiLCJ3aGl0ZSIsImZsZXgiLCJtaW5IZWlnaHQiLCJtYXJnaW5MZWZ0IiwibWFyZ2luQm90dG9tIiwicGFyYW0iLCJhcGlQb3N0IiwiUkVHSVNUUkFUSU9OX0hPTUUiLCJyZXNwIiwic3RhdHVzIiwiUkVTUE9OU0VfU1VDQ0VTUyIsInJlc3RhdXJhbnRzIiwiY29uc29sZSIsImxvZyIsInNldFN0YXRlIiwibGFzdF9wYWdlIiwiY3VycmVudF9wYWdlIiwic2hvd1NwbGFzaExvYWRlciIsInNldFRpbWVvdXQiLCJlcnIiLCJyb3dzIiwicHVzaCIsImZsZXhEaXJlY3Rpb24iLCJoZWlnaHQiLCJtYXJnaW5Ib3Jpem9udGFsIiwicGFkZGluZ0hvcml6b250YWwiLCJ3aWR0aCIsIm92ZXJmbG93IiwiYm9yZGVyUmFkaXVzIiwiQXNzZXRzIiwibWVyY2hhbnRBZHMiLCJwYWRkaW5nVmVydGljYWwiLCJmb250RmFtaWx5IiwiQVBQRk9OVFMiLCJyZWd1bGFyIiwiZm9udFNpemUiLCJsZXR0ZXJTcGFjaW5nIiwic3R5bGVzIiwiam9pbk5vdyIsInZhY2FuY3lBZHMiLCJhbGxSZXN0YXVyYW50Um93cyIsImdldERhdGEiLCJyZWYiLCJtYWluRmxhdExpc3QiLCJhbGxNYWluUm93IiwiaXRlbSIsImluZGV4IiwiU3RyaW5nIiwiUmVhY3QiLCJDb21wb25lbnQiLCJTdHlsZVNoZWV0IiwiY3JlYXRlIiwic2Nyb2xsVXBCdXR0b24iLCJ6SW5kZXgiLCJyaWdodCIsImJvdHRvbSIsImRpc2NvdW50X3N0aWNrIiwicG9zaXRpb24iLCJ0b3AiLCJhbGlnbkl0ZW1zIiwiaXRlbUltYWdlIiwicmVzaXplTW9kZSIsInNwaW5uZXIiLCJhbm91bmNlbWVudFJvdyIsIm1hcmdpblRvcCIsIm1hcmdpblN0YXJ0IiwibWFyZ2luRW5kIiwidGV4dEFsaWduIiwidGV4dEFsaWduVmVydGljYWwiLCJsZWZ0IiwiYm9yZGVyV2lkdGgiLCJib3JkZXJDb2xvciJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7QUFXQTtBQUNBO0FBQ0E7Q0FHQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7SUFFcUJBLGE7Ozs7Ozs7Ozs7Ozs7Ozs7Z05BRVQ7QUFDSkMsc0JBQWdCLEVBQUNDO0FBRGIsSzs7NE5BcUJZLFlBQU07QUFDdEIsVUFBSUMsa0JBQWtCLEdBQ2xCLE1BQUtDLEtBQUwsQ0FBV0gsZ0JBQVgsSUFBK0JDLFNBQS9CLElBQ0EsTUFBS0UsS0FBTCxDQUFXSCxnQkFBWCxJQUErQixJQUQvQixJQUVBLE1BQUtHLEtBQUwsQ0FBV0gsZ0JBQVgsQ0FBNEJJLE1BQTVCLEdBQXFDLENBRnJDLEdBR0ksTUFBS0Msa0JBQUwsRUFISixHQUtJLE1BQUtGLEtBQUwsQ0FBV0gsZ0JBQVgsSUFBK0JDLFNBQS9CLElBQ0osTUFBS0UsS0FBTCxDQUFXSCxnQkFBWCxJQUErQixJQUQzQixJQUVKLE1BQUtHLEtBQUwsQ0FBV0gsZ0JBQVgsQ0FBNEJJLE1BQTVCLElBQXNDLENBRmxDLEdBR0EsSUFIQSxHQUlDLE1BQUtELEtBQUwsQ0FBV0csU0FBWCxHQUF1QixJQUF2QixHQUNHLE1BQUMsMEZBQUQ7QUFBa0IsYUFBSyxFQUFFO0FBQ3JCQyxtQkFBUyxFQUFFLFFBRFU7QUFFckJDLHlCQUFlLEVBQUVDLHlEQUFTLENBQUNDLFVBRk47QUFHckJDLGlCQUFPLEVBQUUsRUFIWTtBQUlyQkMsd0JBQWMsRUFBRTtBQUpLLFNBQXpCLENBTUE7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBU0ksTUFBQyw4RUFBRDtBQUFNLGFBQUssRUFBRTtBQUFDQyxlQUFLLEVBQUVDLHdEQUFRLENBQUNDO0FBQWpCLFNBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFUSixDQVhaO0FBMkJBLGFBQ0ksTUFBQyw4RUFBRDtBQUFNLGFBQUssRUFBRTtBQUFDQyxjQUFJLEVBQUUsQ0FBUDtBQUFVQyxtQkFBUyxFQUFFO0FBQXJCLFNBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQUNJLE1BQUMseUVBQUQ7QUFDSSxhQUFLLEVBQUU7QUFBQ0Msb0JBQVUsRUFBRSxFQUFiO0FBQWlCQyxzQkFBWSxFQUFFO0FBQS9CLFNBRFg7QUFFSSxZQUFJLEVBQUMsVUFGVDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFFBREosRUFLS2pCLGtCQUxMLENBREo7QUFVSCxLOzs7Ozs7O3lDQXZEb0I7QUFDakIsYUFDSSxNQUFDLHVFQUFELENBQ0k7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTko7QUFPSSxtQkFBVyxFQUFFLEtBQUtDLEtBQUwsQ0FBV0gsZ0JBUDVCLENBUUk7QUFDQTtBQUNBO0FBQ0E7QUFYSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFFBREo7QUFlSDs7OzhCQXlDUTtBQUFBOztBQUNMLFVBQU1vQixLQUFLLEdBQUcsRUFBZDtBQUNBQywwRUFBTyxDQUNIQyxtRUFERyxFQUVIRixLQUZHLEVBR0gsVUFBQUcsSUFBSSxFQUFJO0FBQ0osWUFBSUEsSUFBSSxJQUFJdEIsU0FBWixFQUF1QjtBQUVuQixjQUFJc0IsSUFBSSxDQUFDQyxNQUFMLElBQWVDLGtFQUFuQixFQUFxQztBQUVqQyxnQkFBSUMsV0FBVyxHQUFHSCxJQUFJLENBQUNHLFdBQXZCLENBRmlDLENBRUU7O0FBRW5DQyxtQkFBTyxDQUFDQyxHQUFSLENBQVksb0JBQVosRUFBa0NGLFdBQWxDOztBQUVBLGtCQUFJLENBQUNHLFFBQUwsQ0FBYztBQUFDN0IsOEJBQWdCLEVBQUVDO0FBQW5CLGFBQWQsRUFBNkMsWUFBTTtBQUMvQyxvQkFBSSxDQUFDNEIsUUFBTCxDQUFjO0FBQUM3QixnQ0FBZ0IsRUFBRTBCO0FBQW5CLGVBQWQ7QUFDSCxhQUZEOztBQUlBLGtCQUFJLENBQUNJLFNBQUwsR0FBaUJQLElBQUksQ0FBQ08sU0FBdEI7QUFDQSxrQkFBSSxDQUFDQyxZQUFMLEdBQW9CLENBQXBCO0FBQ0FKLG1CQUFPLENBQUNDLEdBQVIsQ0FBWSxpQkFBWixFQUErQixNQUFJLENBQUNHLFlBQXBDO0FBRUg7QUFDSjs7QUFFRCxjQUFJLENBQUNGLFFBQUwsQ0FBYztBQUFDdkIsbUJBQVMsRUFBRTtBQUFaLFNBQWQ7O0FBRUEsWUFBSSxNQUFJLENBQUNILEtBQUwsQ0FBVzZCLGdCQUFmLEVBQWlDO0FBQzdCQyxvQkFBVSxDQUFDLFlBQU07QUFDYixrQkFBSSxDQUFDSixRQUFMLENBQWM7QUFBQ0csOEJBQWdCLEVBQUU7QUFBbkIsYUFBZDtBQUNILFdBRlMsRUFFUCxJQUZPLENBQVY7QUFHSDtBQUNKLE9BOUJFLEVBK0JILFVBQUFFLEdBQUcsRUFBSTtBQUNILGNBQUksQ0FBQ0wsUUFBTCxDQUFjO0FBQUN2QixtQkFBUyxFQUFFO0FBQVosU0FBZDtBQUNILE9BakNFLENBQVA7QUFtQ0g7OztpQ0FFVztBQUNSLFVBQUk2QixJQUFJLEdBQUcsRUFBWCxDQURRLENBR1I7QUFDQTtBQUNBOztBQUVBQSxVQUFJLENBQUNDLElBQUwsQ0FDSSxNQUFDLDhFQUFEO0FBQU0sYUFBSyxFQUFFO0FBQUNDLHVCQUFhLEVBQUMsS0FBZjtBQUFxQkMsZ0JBQU0sRUFBQyxHQUE1QjtBQUFnQ0MsMEJBQWdCLEVBQUMsQ0FBakQ7QUFBbUQzQix3QkFBYyxFQUFFO0FBQW5FLFNBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQUNJLE1BQUMsMEZBQUQ7QUFDSSxlQUFPLEVBQUUsbUJBQUksQ0FDVDtBQUNBO0FBQ0E7QUFDSCxTQUxMO0FBTUksYUFBSyxFQUFFO0FBQUNJLGNBQUksRUFBQyxDQUFOO0FBQVF3QiwyQkFBaUIsRUFBQztBQUExQixTQU5YO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FRSSxNQUFDLDhFQUFEO0FBQU0sYUFBSyxFQUFFO0FBQUNDLGVBQUssRUFBQyxNQUFQO0FBQWNqQyx5QkFBZSxFQUFDLFNBQTlCO0FBQXdDOEIsZ0JBQU0sRUFBQyxNQUEvQztBQUFzREksa0JBQVEsRUFBQyxRQUEvRDtBQUF3RUMsc0JBQVksRUFBQztBQUFyRixTQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FDSSxNQUFDLHlGQUFEO0FBQWlCLGNBQU0sRUFBRUMsZ0RBQU0sQ0FBQ0MsV0FBaEM7QUFBNkMsa0JBQVUsRUFBRSxTQUF6RDtBQUFvRSxhQUFLLEVBQUU7QUFBQ0osZUFBSyxFQUFDLE1BQVA7QUFBY0gsZ0JBQU0sRUFBQztBQUFyQixTQUEzRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0ksTUFBQyw4RUFBRDtBQUFNLGFBQUssRUFBRTtBQUFDRSwyQkFBaUIsRUFBQyxDQUFuQjtBQUFxQk0seUJBQWUsRUFBQztBQUFyQyxTQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FDSSxNQUFDLDhFQUFEO0FBQU0sYUFBSyxFQUFFO0FBQUNDLG9CQUFVLEVBQUNDLCtEQUFRLENBQUNDLE9BQXJCO0FBQTZCQyxrQkFBUSxFQUFDLEVBQXRDO0FBQXlDQyx1QkFBYSxFQUFDLENBQUM7QUFBeEQsU0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQTRFLDJCQUE1RSxDQURKLEVBRUksTUFBQyw4RUFBRDtBQUFNLGFBQUssRUFBRTtBQUFDSixvQkFBVSxFQUFDQywrREFBUSxDQUFDQyxPQUFyQjtBQUE2QkMsa0JBQVEsRUFBQztBQUF0QyxTQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FBeUQsOEJBQXpELENBRkosQ0FESixDQURKLEVBT0ksTUFBQyw4RUFBRDtBQUFNLGFBQUssRUFBRUUsTUFBTSxDQUFDQyxPQUFwQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0ssVUFETCxDQVBKLENBUkosQ0FESixFQXNCSSxNQUFDLDhFQUFEO0FBQU0sYUFBSyxFQUFFO0FBQUNaLGVBQUssRUFBQyxHQUFQO0FBQVdILGdCQUFNLEVBQUMsR0FBbEI7QUFBc0I5Qix5QkFBZSxFQUFDO0FBQXRDLFNBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQUEyRCxNQUFDLCtFQUFEO0FBQU8sYUFBSyxFQUFFO0FBQUNpQyxlQUFLLEVBQUMsR0FBUDtBQUFXSCxnQkFBTSxFQUFDO0FBQWxCLFNBQWQ7QUFBc0MsY0FBTSxFQUFFTSxnREFBTSxDQUFDQyxXQUFyRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFFBQTNELENBdEJKLEVBd0JJLE1BQUMsMEZBQUQ7QUFDSSxlQUFPLEVBQUUsbUJBQUksQ0FDVDtBQUNBO0FBQ0E7QUFDSCxTQUxMO0FBTUksYUFBSyxFQUFFO0FBQUM3QixjQUFJLEVBQUMsQ0FBTjtBQUFRd0IsMkJBQWlCLEVBQUM7QUFBMUIsU0FOWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBT0ksTUFBQyw4RUFBRDtBQUFNLGFBQUssRUFBRTtBQUFDQyxlQUFLLEVBQUMsTUFBUDtBQUFjakMseUJBQWUsRUFBQyxTQUE5QjtBQUF3QzhCLGdCQUFNLEVBQUMsTUFBL0M7QUFBc0RJLGtCQUFRLEVBQUMsUUFBL0Q7QUFBd0VDLHNCQUFZLEVBQUM7QUFBckYsU0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0ksTUFBQyx5RkFBRDtBQUFpQixjQUFNLEVBQUVDLGdEQUFNLENBQUNVLFVBQWhDO0FBQTRDLGtCQUFVLEVBQUUsU0FBeEQ7QUFBbUUsYUFBSyxFQUFFO0FBQUNiLGVBQUssRUFBQyxNQUFQO0FBQWNILGdCQUFNLEVBQUM7QUFBckIsU0FBMUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQUNJLE1BQUMsOEVBQUQ7QUFBTSxhQUFLLEVBQUU7QUFBQ0UsMkJBQWlCLEVBQUMsQ0FBbkI7QUFBcUJNLHlCQUFlLEVBQUM7QUFBckMsU0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0ksTUFBQyw4RUFBRDtBQUFNLGFBQUssRUFBRTtBQUFDQyxvQkFBVSxFQUFDQywrREFBUSxDQUFDQyxPQUFyQjtBQUE2QkMsa0JBQVEsRUFBQyxFQUF0QztBQUF5Q0MsdUJBQWEsRUFBRSxDQUFDO0FBQXpELFNBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQUEyRSxhQUEzRSxDQURKLEVBRUksTUFBQyw4RUFBRDtBQUFNLGFBQUssRUFBRTtBQUFDSixvQkFBVSxFQUFDQywrREFBUSxDQUFDQyxPQUFyQjtBQUE2QkMsa0JBQVEsRUFBQztBQUF0QyxTQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FBeUQsMEJBQXpELENBRkosQ0FESixDQURKLEVBT0ksTUFBQyw4RUFBRDtBQUFNLGFBQUssRUFBRUUsTUFBTSxDQUFDQyxPQUFwQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0ssV0FETCxDQVBKLENBUEosQ0F4QkosQ0FESixFQVBRLENBcURSOztBQUNBbEIsVUFBSSxDQUFDQyxJQUFMLENBQVUsS0FBS21CLGlCQUFMLEVBQVY7QUFFQSxhQUFPcEIsSUFBUDtBQUNIOzs7Ozs7Ozs7QUFHRyxxQkFBS3FCLE9BQUw7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs2QkFHSztBQUFBOztBQUNMLGFBQ0ksTUFBQyx1REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0ksTUFBQyxrRkFBRCxDQUNJO0FBREo7QUFHSSxXQUFHLEVBQUUsYUFBQ0MsS0FBRDtBQUFBLGlCQUFPLE1BQUksQ0FBQ0MsWUFBTCxHQUFvQkQsS0FBM0I7QUFBQSxTQUhULENBSUk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEo7QUFVSSxZQUFJLEVBQUUsS0FBS0UsVUFBTCxFQVZWO0FBV0ksa0JBQVUsRUFBRTtBQUFBLGNBQUVDLElBQUYsUUFBRUEsSUFBRjtBQUFBLGlCQUNSQSxJQURRO0FBQUEsU0FYaEI7QUFjSSxvQkFBWSxFQUFFLHNCQUFDQSxJQUFELEVBQU9DLEtBQVA7QUFBQSxpQkFBaUJDLE1BQU0sQ0FBQ0QsS0FBRCxDQUF2QjtBQUFBLFNBZGxCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsUUFESixDQURKO0FBb0JIOzs7O0VBMUxzQ0UsNENBQUssQ0FBQ0MsUzs7O0FBNkxqRCxJQUFNWixNQUFNLEdBQUdhLG9GQUFVLENBQUNDLE1BQVgsQ0FBa0I7QUFDN0JDLGdCQUFjLEVBQUM7QUFDWEMsVUFBTSxFQUFDLEdBREk7QUFFWEMsU0FBSyxFQUFDLEVBRks7QUFHWEMsVUFBTSxFQUFDLEVBSEk7QUFJWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE5RCxtQkFBZSxFQUFDO0FBYkwsR0FEYztBQWdCN0IrRCxnQkFBYyxFQUFFO0FBQ1pDLFlBQVEsRUFBRSxVQURFO0FBRVpILFNBQUssRUFBRSxDQUZLO0FBR1pJLE9BQUcsRUFBRSxDQUhPO0FBSVpMLFVBQU0sRUFBRSxDQUpJO0FBS1o7QUFDQTtBQUNBO0FBQ0E5QixVQUFNLEVBQUUsRUFSSTtBQVNaRyxTQUFLLEVBQUUsRUFUSztBQVVaaUMsY0FBVSxFQUFFLFFBVkE7QUFXWi9CLGdCQUFZLEVBQUU7QUFYRixHQWhCYTtBQTZCN0JnQyxXQUFTLEVBQUU7QUFDUGxDLFNBQUssRUFBRSxNQURBO0FBRVBILFVBQU0sRUFBRSxNQUZEO0FBR1AvQixhQUFTLEVBQUUsUUFISjtBQUlQcUUsY0FBVSxFQUFFO0FBSkwsR0E3QmtCO0FBbUM3QkMsU0FBTyxFQUFFO0FBQ0w3RCxRQUFJLEVBQUUsQ0FERDtBQUVMVCxhQUFTLEVBQUUsUUFGTjtBQUdMNkQsVUFBTSxFQUFFO0FBSEgsR0FuQ29CO0FBd0M3QlUsZ0JBQWMsRUFBRTtBQUNaQyxhQUFTLEVBQUUsRUFEQztBQUVaQyxlQUFXLEVBQUUsRUFGRDtBQUdaQyxhQUFTLEVBQUUsRUFIQztBQUlaekUsbUJBQWUsRUFBRSxPQUpMO0FBS1ptQyxnQkFBWSxFQUFFO0FBTEYsR0F4Q2E7QUErQzdCVSxTQUFPLEVBQUM7QUFDSm1CLFlBQVEsRUFBQyxVQURMO0FBRUpVLGFBQVMsRUFBRSxRQUZQO0FBR0pDLHFCQUFpQixFQUFDLFFBSGQ7QUFJSjtBQUNBO0FBQ0FuRSxRQUFJLEVBQUMsQ0FORDtBQU9Kc0QsVUFBTSxFQUFDLEVBUEg7QUFRSmMsUUFBSSxFQUFDLENBUkQ7QUFTSjVFLG1CQUFlLEVBQUMsUUFUWjtBQVVKbUMsZ0JBQVksRUFBQyxDQVZUO0FBV0owQyxlQUFXLEVBQUMsQ0FYUjtBQVlKQyxlQUFXLEVBQUMsUUFaUjtBQWFKN0MsU0FBSyxFQUFDLEVBYkY7QUFjSkgsVUFBTSxFQUFDLEVBZEg7QUFlSlksWUFBUSxFQUFDO0FBZkw7QUEvQ3FCLENBQWxCLENBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvaW5kZXguNmIwZDM0N2I4MjY0NDNkODRlNTkuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcclxuXHJcbmltcG9ydCB7XHJcbiAgICBGbGF0TGlzdCxcclxuICAgIFN0eWxlU2hlZXQsXHJcbiAgICBUZXh0LFxyXG4gICAgVG91Y2hhYmxlT3BhY2l0eSxcclxuICAgIFZpZXcsXHJcbiAgICBJbWFnZUJhY2tncm91bmQsXHJcbiAgICBJbWFnZVxyXG59IGZyb20gXCJyZWFjdC1uYXRpdmVcIjtcclxuaW1wb3J0IEJhc2VDb250YWluZXIgZnJvbSBcIi4vQmFzZUNvbnRhaW5lclwiO1xyXG5pbXBvcnQgQXNzZXRzIGZyb20gXCIuLi9hc3NldHNcIjtcclxuaW1wb3J0IHtBUFBGT05UU30gZnJvbSBcIi4uL2Fzc2V0cy9Gb250Q29uc3RhbnRzXCI7XHJcblxyXG5pbXBvcnQge2FwaVBvc3R9IGZyb20gXCIuLi9hcGkvU2VydmljZU1hbmFnZXJcIjtcclxuLy9pbXBvcnQge25ldFN0YXR1c30gZnJvbSBcIi4uL3V0aWxzL05ldHdvcmtTdGF0dXNDb25uZWN0aW9uXCJcclxuXHJcbmltcG9ydCB7UkVHSVNUUkFUSU9OX0hPTUUsIFJFU1BPTlNFX1NVQ0NFU1N9IGZyb20gXCIuLi91dGlscy9Db25zdGFudHNcIjtcclxuaW1wb3J0IFBvcHVsYXJSZXN0YXVyYW50cyBmcm9tIFwiLi4vY29tcG9uZW50cy9Qb3B1bGFyUmVzdGF1cmFudHNcIjtcclxuaW1wb3J0IHtBUFBDT0xPUlMsIEVEQ29sb3JzfSBmcm9tIFwiLi4vYXNzZXRzL0NvbG9yc1wiO1xyXG5pbXBvcnQgRVRleHRWaWV3Tm9ybWFsTGFiZWwgZnJvbSBcIi4uL2NvbXBvbmVudHMvRVRleHRWaWV3Tm9ybWFsTGFiZWxcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1haW5Db250YWluZXIgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xyXG5cclxuICAgIHN0YXRlID0ge1xyXG4gICAgICAgIGFycmF5UmVzdGF1cmFudHM6dW5kZWZpbmVkXHJcbiAgICB9XHJcblxyXG4gICAgc2hvd1Jlc3RhdXJhbnRSb3dzKCkge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxQb3B1bGFyUmVzdGF1cmFudHNcclxuICAgICAgICAgICAgICAgIC8vIG9uU2Nyb2xsRW5kRHJhZz17KGUpPT57XHJcbiAgICAgICAgICAgICAgICAvLyAgICAgLy90aGlzLnNldFN0YXRlKHtzY29sbGVkT2Zmc2V0WTplLm5hdGl2ZUV2ZW50LmNvbnRlbnRPZmZzZXQueX0pO1xyXG4gICAgICAgICAgICAgICAgLy8gICAgIC8vIGNvbnNvbGUubG9nKGUubmF0aXZlRXZlbnQuY29udGVudE9mZnNldC55KTtcclxuICAgICAgICAgICAgICAgIC8vICAgICAvL3RoaXMuc2V0U3RhdGUoe3Nob3dTY3JvbGxVcEJ1dHRvbjogZS5uYXRpdmVFdmVudC5jb250ZW50T2Zmc2V0LnkgPj0gaGVpZ2h0fSlcclxuICAgICAgICAgICAgICAgIC8vXHJcbiAgICAgICAgICAgICAgICAvLyB9fVxyXG4gICAgICAgICAgICAgICAgcmVzdGF1cmFudHM9e3RoaXMuc3RhdGUuYXJyYXlSZXN0YXVyYW50c31cclxuICAgICAgICAgICAgICAgIC8vaXRlbVByZXNzPXt0aGlzLmdvdG9SZXN0YXVyYW50fVxyXG4gICAgICAgICAgICAgICAgLy9yZWZyZXNoU2NyZWVuPXt0aGlzLnJlZnJlc2hTY3JlZW59XHJcbiAgICAgICAgICAgICAgICAvL25leHRQYWdlPXt0aGlzLm5leHRSZXN0YXVyYW50UGFnZX1cclxuICAgICAgICAgICAgICAgIC8vaXNMb2FkaW5nPXt0aGlzLnN0YXRlLmxvYWRpbmdSZXN0YXVyYW50fVxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbiAgICBhbGxSZXN0YXVyYW50Um93cyA9ICgpID0+IHtcclxuICAgICAgICBsZXQgcG9wdWxhclJlc3RhdXJhbnRzID0gKFxyXG4gICAgICAgICAgICB0aGlzLnN0YXRlLmFycmF5UmVzdGF1cmFudHMgIT0gdW5kZWZpbmVkICYmXHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGUuYXJyYXlSZXN0YXVyYW50cyAhPSBudWxsICYmXHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGUuYXJyYXlSZXN0YXVyYW50cy5sZW5ndGggPiAwID8gKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5zaG93UmVzdGF1cmFudFJvd3MoKVxyXG5cclxuICAgICAgICAgICAgKSA6IHRoaXMuc3RhdGUuYXJyYXlSZXN0YXVyYW50cyAhPSB1bmRlZmluZWQgJiZcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZS5hcnJheVJlc3RhdXJhbnRzICE9IG51bGwgJiZcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZS5hcnJheVJlc3RhdXJhbnRzLmxlbmd0aCA9PSAwID8gKFxyXG4gICAgICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICAgICApIDogKHRoaXMuc3RhdGUuaXNMb2FkaW5nID8gbnVsbCA6XHJcbiAgICAgICAgICAgICAgICAgICAgPFRvdWNoYWJsZU9wYWNpdHkgc3R5bGU9e3tcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWxpZ25TZWxmOiAnY2VudGVyJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yOiBBUFBDT0xPUlMuZ3JvdXAxTWFpbixcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMTAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpblZlcnRpY2FsOiAxMDBcclxuICAgICAgICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIG9uUHJlc3M9eygpID0+IHRoaXMucmVmcmVzaFNjcmVlbn1cclxuICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHsvKiA8SW1hZ2Ugc291cmNlID0ge0Fzc2V0cy5yZWZyZXNofS8+ICovfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8VGV4dCBzdHlsZT17e2NvbG9yOiBFRENvbG9ycy53aGl0ZX19PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgUmVsb2FkXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvVGV4dD5cclxuICAgICAgICAgICAgICAgICAgICA8L1RvdWNoYWJsZU9wYWNpdHk+XHJcbiAgICAgICAgICAgIClcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8VmlldyBzdHlsZT17e2ZsZXg6IDEsIG1pbkhlaWdodDogMjAwfX0+XHJcbiAgICAgICAgICAgICAgICA8RVRleHRWaWV3Tm9ybWFsTGFiZWxcclxuICAgICAgICAgICAgICAgICAgICBzdHlsZT17e21hcmdpbkxlZnQ6IDEwLCBtYXJnaW5Cb3R0b206IDEwfX1cclxuICAgICAgICAgICAgICAgICAgICB0ZXh0PVwiRmVhdHVyZWRcIi8+XHJcblxyXG4gICAgICAgICAgICAgICAge3BvcHVsYXJSZXN0YXVyYW50c31cclxuICAgICAgICAgICAgPC9WaWV3PlxyXG4gICAgICAgICk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGdldERhdGEoKXtcclxuICAgICAgICBjb25zdCBwYXJhbSA9IHt9XHJcbiAgICAgICAgYXBpUG9zdChcclxuICAgICAgICAgICAgUkVHSVNUUkFUSU9OX0hPTUUsXHJcbiAgICAgICAgICAgIHBhcmFtLFxyXG4gICAgICAgICAgICByZXNwID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwICE9IHVuZGVmaW5lZCkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcC5zdGF0dXMgPT0gUkVTUE9OU0VfU1VDQ0VTUykge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHJlc3RhdXJhbnRzID0gcmVzcC5yZXN0YXVyYW50czsvLy5zcGxpY2UoMCwxMCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcInJlc3RhdXJhbnRzIDo6Ojo6IFwiLCByZXN0YXVyYW50cylcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2FycmF5UmVzdGF1cmFudHM6IHVuZGVmaW5lZH0sICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2FycmF5UmVzdGF1cmFudHM6IHJlc3RhdXJhbnRzfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sYXN0X3BhZ2UgPSByZXNwLmxhc3RfcGFnZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50X3BhZ2UgPSAxO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImN1cnJlbnRfcGFnZSA6IFwiLCB0aGlzLmN1cnJlbnRfcGFnZSlcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2lzTG9hZGluZzogZmFsc2V9KTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5zdGF0ZS5zaG93U3BsYXNoTG9hZGVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe3Nob3dTcGxhc2hMb2FkZXI6IGZhbHNlfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgMzAwMCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGVyciA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtpc0xvYWRpbmc6IGZhbHNlfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIGFsbE1haW5Sb3coKXtcclxuICAgICAgICBsZXQgcm93cyA9IFtdO1xyXG5cclxuICAgICAgICAvLyByb3dzLnB1c2godGhpcy5zbGlkZXIoKSk7XHJcbiAgICAgICAgLy8gcm93cy5wdXNoKHRoaXMuYnVzaW5lc3NJY29ucygpKTtcclxuICAgICAgICAvLyByb3dzLnB1c2godGhpcy5hbm91bmNlbWVudFJvdygpKTtcclxuXHJcbiAgICAgICAgcm93cy5wdXNoKFxyXG4gICAgICAgICAgICA8VmlldyBzdHlsZT17e2ZsZXhEaXJlY3Rpb246XCJyb3dcIixoZWlnaHQ6MTAwLG1hcmdpbkhvcml6b250YWw6NyxtYXJnaW5WZXJ0aWNhbDogMTB9fT5cclxuICAgICAgICAgICAgICAgIDxUb3VjaGFibGVPcGFjaXR5XHJcbiAgICAgICAgICAgICAgICAgICAgb25QcmVzcz17KCk9PntcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gaWYodGhpcy5tZXJjaGFudE1vZGFsKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgIHRoaXMubWVyY2hhbnRNb2RhbC5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlPXt7ZmxleDoxLHBhZGRpbmdIb3Jpem9udGFsOjN9fVxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIDxWaWV3IHN0eWxlPXt7d2lkdGg6XCIxMDAlXCIsYmFja2dyb3VuZENvbG9yOlwiIzdkM2UxZVwiLGhlaWdodDpcIjEwMCVcIixvdmVyZmxvdzpcImhpZGRlblwiLGJvcmRlclJhZGl1czo1fX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxJbWFnZUJhY2tncm91bmQgc291cmNlPXtBc3NldHMubWVyY2hhbnRBZHN9IHJlc2l6ZU1vZGU9e1wiY29udGFpblwifSBzdHlsZT17e3dpZHRoOlwiMTAwJVwiLGhlaWdodDpcIjEwMCVcIn19PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFZpZXcgc3R5bGU9e3twYWRkaW5nSG9yaXpvbnRhbDo1LHBhZGRpbmdWZXJ0aWNhbDowfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRleHQgc3R5bGU9e3tmb250RmFtaWx5OkFQUEZPTlRTLnJlZ3VsYXIsZm9udFNpemU6MTQsbGV0dGVyU3BhY2luZzotMC4yfX0+e1wiQmVjb21lIGEgTWVyY2hhbnQgUGFydG5lclwifTwvVGV4dD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VGV4dCBzdHlsZT17e2ZvbnRGYW1pbHk6QVBQRk9OVFMucmVndWxhcixmb250U2l6ZToxMn19PntcIkhlbHAgeW91IHRvIEluY3JlYXNlIFJldmVudWVcIn08L1RleHQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1ZpZXc+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvSW1hZ2VCYWNrZ3JvdW5kPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8VGV4dCBzdHlsZT17c3R5bGVzLmpvaW5Ob3d9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1wiSm9pbiBOb3dcIn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9UZXh0PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvVmlldz5cclxuICAgICAgICAgICAgICAgIDwvVG91Y2hhYmxlT3BhY2l0eT5cclxuXHJcbiAgICAgICAgICAgICAgICA8VmlldyBzdHlsZT17e3dpZHRoOjEwMCxoZWlnaHQ6MTAwLGJhY2tncm91bmRDb2xvcjpcInJlZFwifX0+PEltYWdlIHN0eWxlPXt7d2lkdGg6MTAwLGhlaWdodDoxMDB9fSBzb3VyY2U9e0Fzc2V0cy5tZXJjaGFudEFkc30vPjwvVmlldz5cclxuXHJcbiAgICAgICAgICAgICAgICA8VG91Y2hhYmxlT3BhY2l0eVxyXG4gICAgICAgICAgICAgICAgICAgIG9uUHJlc3M9eygpPT57XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGlmKHRoaXMuam9iVmFjYW5jeU1vZGFsKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gICAgIHRoaXMuam9iVmFjYW5jeU1vZGFsLnNob3coKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3tmbGV4OjEscGFkZGluZ0hvcml6b250YWw6M319PlxyXG4gICAgICAgICAgICAgICAgICAgIDxWaWV3IHN0eWxlPXt7d2lkdGg6XCIxMDAlXCIsYmFja2dyb3VuZENvbG9yOlwiIzdkM2UxZVwiLGhlaWdodDpcIjEwMCVcIixvdmVyZmxvdzpcImhpZGRlblwiLGJvcmRlclJhZGl1czo1fX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxJbWFnZUJhY2tncm91bmQgc291cmNlPXtBc3NldHMudmFjYW5jeUFkc30gcmVzaXplTW9kZT17XCJjb250YWluXCJ9IHN0eWxlPXt7d2lkdGg6XCIxMDAlXCIsaGVpZ2h0OlwiMTAwJVwifX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VmlldyBzdHlsZT17e3BhZGRpbmdIb3Jpem9udGFsOjUscGFkZGluZ1ZlcnRpY2FsOjB9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VGV4dCBzdHlsZT17e2ZvbnRGYW1pbHk6QVBQRk9OVFMucmVndWxhcixmb250U2l6ZToxNCxsZXR0ZXJTcGFjaW5nOiAtMX19PntcIkpvYiBWYWNhbmN5XCJ9PC9UZXh0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxUZXh0IHN0eWxlPXt7Zm9udEZhbWlseTpBUFBGT05UUy5yZWd1bGFyLGZvbnRTaXplOjEyfX0+e1wiTmV3IGpvYiBpcyBhdmFpbGFibGUgbm93XCJ9PC9UZXh0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9WaWV3PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0ltYWdlQmFja2dyb3VuZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPFRleHQgc3R5bGU9e3N0eWxlcy5qb2luTm93fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcIkFwcGx5IE5vd1wifVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L1RleHQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9WaWV3PlxyXG4gICAgICAgICAgICAgICAgPC9Ub3VjaGFibGVPcGFjaXR5PlxyXG4gICAgICAgICAgICA8L1ZpZXc+XHJcbiAgICAgICAgKTtcclxuICAgICAgICAvLyBMb2FkaW5nXHJcbiAgICAgICAgcm93cy5wdXNoKHRoaXMuYWxsUmVzdGF1cmFudFJvd3MoKSk7XHJcblxyXG4gICAgICAgIHJldHVybiByb3dzO1xyXG4gICAgfVxyXG5cclxuICAgIGFzeW5jIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG4gICAgICAgIHRoaXMuZ2V0RGF0YSgpXHJcbiAgICB9XHJcblxyXG4gICAgcmVuZGVyKCkge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxCYXNlQ29udGFpbmVyPlxyXG4gICAgICAgICAgICAgICAgPEZsYXRMaXN0XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gb25TY3JvbGw9e3RoaXMuX29uTGlzdFNjcm9sbH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgcmVmPXsocmVmKT0+dGhpcy5tYWluRmxhdExpc3QgPSByZWZ9XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gcmVmcmVzaENvbnRyb2w9e1xyXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICA8UmVmcmVzaENvbnRyb2xcclxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgICAgIHJlZnJlc2hpbmc9e3RoaXMuc3RhdGUucmVmcmVzaGluZ31cclxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgICAgIG9uUmVmcmVzaD17dGhpcy5fb25SZWZyZXNofVxyXG4gICAgICAgICAgICAgICAgICAgIC8vICAgICAvPlxyXG4gICAgICAgICAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgICAgICAgICBkYXRhPXt0aGlzLmFsbE1haW5Sb3coKX1cclxuICAgICAgICAgICAgICAgICAgICByZW5kZXJJdGVtPXsoe2l0ZW19KSA9PiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW1cclxuICAgICAgICAgICAgICAgICAgICApfVxyXG4gICAgICAgICAgICAgICAgICAgIGtleUV4dHJhY3Rvcj17KGl0ZW0sIGluZGV4KSA9PiBTdHJpbmcoaW5kZXgpfVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgPC9CYXNlQ29udGFpbmVyPlxyXG4gICAgICAgIClcclxuICAgIH1cclxufVxyXG5cclxuY29uc3Qgc3R5bGVzID0gU3R5bGVTaGVldC5jcmVhdGUoe1xyXG4gICAgc2Nyb2xsVXBCdXR0b246e1xyXG4gICAgICAgIHpJbmRleDo5OTksXHJcbiAgICAgICAgcmlnaHQ6MTAsXHJcbiAgICAgICAgYm90dG9tOjYwLFxyXG4gICAgICAgIC8vIHNoYWRvd0NvbG9yOiBcIiMwMDBcIixcclxuICAgICAgICAvLyBzaGFkb3dPZmZzZXQ6IHtcclxuICAgICAgICAvLyAgICAgd2lkdGg6IDIwLFxyXG4gICAgICAgIC8vICAgICBoZWlnaHQ6IDIwLFxyXG4gICAgICAgIC8vIH0sXHJcbiAgICAgICAgLy8gc2hhZG93T3BhY2l0eTogMSxcclxuICAgICAgICAvLyBzaGFkb3dSYWRpdXM6IDksXHJcbiAgICAgICAgLy8gZWxldmF0aW9uOiA5LFxyXG5cclxuICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6XCJyZ2JhKDAsMCwwLDAuNilcIlxyXG4gICAgfSxcclxuICAgIGRpc2NvdW50X3N0aWNrOiB7XHJcbiAgICAgICAgcG9zaXRpb246IFwiYWJzb2x1dGVcIixcclxuICAgICAgICByaWdodDogMCxcclxuICAgICAgICB0b3A6IDAsXHJcbiAgICAgICAgekluZGV4OiAxLFxyXG4gICAgICAgIC8vIGJvcmRlckNvbG9yOlwicmVkXCIsXHJcbiAgICAgICAgLy8gYm9yZGVyV2lkdGg6MSxcclxuICAgICAgICAvLyBiYWNrZ3JvdW5kQ29sb3I6XCJ3aGl0ZVwiLFxyXG4gICAgICAgIGhlaWdodDogMzUsXHJcbiAgICAgICAgd2lkdGg6IDMwLFxyXG4gICAgICAgIGFsaWduSXRlbXM6IFwiY2VudGVyXCIsXHJcbiAgICAgICAgYm9yZGVyUmFkaXVzOiAyLFxyXG4gICAgfSxcclxuICAgIGl0ZW1JbWFnZToge1xyXG4gICAgICAgIHdpZHRoOiBcIjEwMCVcIixcclxuICAgICAgICBoZWlnaHQ6IFwiMTAwJVwiLFxyXG4gICAgICAgIGFsaWduU2VsZjogXCJjZW50ZXJcIixcclxuICAgICAgICByZXNpemVNb2RlOiAnY292ZXInXHJcbiAgICB9LFxyXG4gICAgc3Bpbm5lcjoge1xyXG4gICAgICAgIGZsZXg6IDEsXHJcbiAgICAgICAgYWxpZ25TZWxmOiBcImNlbnRlclwiLFxyXG4gICAgICAgIHpJbmRleDogMTAwMFxyXG4gICAgfSxcclxuICAgIGFub3VuY2VtZW50Um93OiB7XHJcbiAgICAgICAgbWFyZ2luVG9wOiAxMCxcclxuICAgICAgICBtYXJnaW5TdGFydDogMTAsXHJcbiAgICAgICAgbWFyZ2luRW5kOiAxMCxcclxuICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6IFwid2hpdGVcIixcclxuICAgICAgICBib3JkZXJSYWRpdXM6IDVcclxuICAgIH0sXHJcbiAgICBqb2luTm93OntcclxuICAgICAgICBwb3NpdGlvbjpcImFic29sdXRlXCIsXHJcbiAgICAgICAgdGV4dEFsaWduOiBcImNlbnRlclwiLFxyXG4gICAgICAgIHRleHRBbGlnblZlcnRpY2FsOlwiY2VudGVyXCIsXHJcbiAgICAgICAgLy8ganVzdGlmeUNvbnRlbnQ6XCJjZW50ZXJcIixcclxuICAgICAgICAvLyBhbGlnblNlbGY6XCJjZW50ZXJcIixcclxuICAgICAgICBmbGV4OjEsXHJcbiAgICAgICAgYm90dG9tOjEwLFxyXG4gICAgICAgIGxlZnQ6NSxcclxuICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6XCJ5ZWxsb3dcIixcclxuICAgICAgICBib3JkZXJSYWRpdXM6NSxcclxuICAgICAgICBib3JkZXJXaWR0aDoxLFxyXG4gICAgICAgIGJvcmRlckNvbG9yOlwieWVsbG93XCIsXHJcbiAgICAgICAgd2lkdGg6NzAsXHJcbiAgICAgICAgaGVpZ2h0OjI1LFxyXG4gICAgICAgIGZvbnRTaXplOjEyXHJcbiAgICB9XHJcbn0pOyJdLCJzb3VyY2VSb290IjoiIn0=