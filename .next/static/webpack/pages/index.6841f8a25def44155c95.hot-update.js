webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/app/containers/MainContainer.js":
/*!***********************************************!*\
  !*** ./pages/app/containers/MainContainer.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MainContainer; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/FlatList */ "./node_modules/react-native-web/dist/cjs/exports/FlatList/index.js");
/* harmony import */ var react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/StyleSheet */ "./node_modules/react-native-web/dist/cjs/exports/StyleSheet/index.js");
/* harmony import */ var react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/Text */ "./node_modules/react-native-web/dist/cjs/exports/Text/index.js");
/* harmony import */ var react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/TouchableOpacity */ "./node_modules/react-native-web/dist/cjs/exports/TouchableOpacity/index.js");
/* harmony import */ var react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/View */ "./node_modules/react-native-web/dist/cjs/exports/View/index.js");
/* harmony import */ var react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/ImageBackground */ "./node_modules/react-native-web/dist/cjs/exports/ImageBackground/index.js");
/* harmony import */ var react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var react_native_web_dist_cjs_exports_Image__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! react-native-web/dist/cjs/exports/Image */ "./node_modules/react-native-web/dist/cjs/exports/Image/index.js");
/* harmony import */ var react_native_web_dist_cjs_exports_Image__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(react_native_web_dist_cjs_exports_Image__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _BaseContainer__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./BaseContainer */ "./pages/app/containers/BaseContainer.js");
/* harmony import */ var _assets__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../assets */ "./pages/app/assets/index.js");
/* harmony import */ var _assets_FontConstants__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../assets/FontConstants */ "./pages/app/assets/FontConstants.js");
/* harmony import */ var _api_ServiceManager__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../api/ServiceManager */ "./pages/app/api/ServiceManager.js");
/* harmony import */ var _utils_Constants__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../utils/Constants */ "./pages/app/utils/Constants.js");
/* harmony import */ var _components_PopularRestaurants__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../components/PopularRestaurants */ "./pages/app/components/PopularRestaurants.js");
/* harmony import */ var _assets_Colors__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../assets/Colors */ "./pages/app/assets/Colors.js");
/* harmony import */ var _components_ETextViewNormalLabel__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../components/ETextViewNormalLabel */ "./pages/app/components/ETextViewNormalLabel.js");









var _jsxFileName = "G:\\JustOrderWeb\\pages\\app\\containers\\MainContainer.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_9___default.a.createElement;

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }












 //import {netStatus} from "../utils/NetworkStatusConnection"






var MainContainer = /*#__PURE__*/function (_React$Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_5__["default"])(MainContainer, _React$Component);

  var _super = _createSuper(MainContainer);

  function MainContainer() {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, MainContainer);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this), "state", {
      arrayRestaurants: undefined
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_8__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this), "allRestaurantRows", function () {
      var popularRestaurants = _this.state.arrayRestaurants != undefined && _this.state.arrayRestaurants != null && _this.state.arrayRestaurants.length > 0 ? _this.showRestaurantRows() : _this.state.arrayRestaurants != undefined && _this.state.arrayRestaurants != null && _this.state.arrayRestaurants.length == 0 ? null : _this.state.isLoading ? null : __jsx(react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_13___default.a, {
        style: {
          alignSelf: 'center',
          backgroundColor: _assets_Colors__WEBPACK_IMPORTED_MODULE_23__["APPCOLORS"].group1Main,
          padding: 10,
          marginVertical: 100
        } // onPress={() => this.refreshScreen}
        ,
        __self: Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59,
          columnNumber: 21
        }
      }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default.a, {
        style: {
          color: _assets_Colors__WEBPACK_IMPORTED_MODULE_23__["EDColors"].white
        },
        __self: Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68,
          columnNumber: 25
        }
      }, "Reload"));
      return __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14___default.a, {
        style: {
          flex: 1,
          minHeight: 200
        },
        __self: Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76,
          columnNumber: 13
        }
      }, __jsx(_components_ETextViewNormalLabel__WEBPACK_IMPORTED_MODULE_24__["default"], {
        style: {
          marginLeft: 10,
          marginBottom: 10
        },
        text: "Featured",
        __self: Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_4__["default"])(_this),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77,
          columnNumber: 17
        }
      }), popularRestaurants);
    });

    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(MainContainer, [{
    key: "showRestaurantRows",
    value: function showRestaurantRows() {
      return __jsx(_components_PopularRestaurants__WEBPACK_IMPORTED_MODULE_22__["default"] // onScrollEndDrag={(e)=>{
      //     //this.setState({scolledOffsetY:e.nativeEvent.contentOffset.y});
      //     // console.log(e.nativeEvent.contentOffset.y);
      //     //this.setState({showScrollUpButton: e.nativeEvent.contentOffset.y >= height})
      //
      // }}
      , {
        restaurants: this.state.arrayRestaurants //itemPress={this.gotoRestaurant}
        //refreshScreen={this.refreshScreen}
        //nextPage={this.nextRestaurantPage}
        //isLoading={this.state.loadingRestaurant}
        ,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 32,
          columnNumber: 13
        }
      });
    }
  }, {
    key: "getData",
    value: function getData() {
      var _this2 = this;

      var param = {};
      Object(_api_ServiceManager__WEBPACK_IMPORTED_MODULE_20__["apiPost"])(_utils_Constants__WEBPACK_IMPORTED_MODULE_21__["REGISTRATION_HOME"], param, function (resp) {
        if (resp != undefined) {
          if (resp.status == _utils_Constants__WEBPACK_IMPORTED_MODULE_21__["RESPONSE_SUCCESS"]) {
            var restaurants = resp.restaurants; //.splice(0,10);

            console.log("restaurants ::::: ", restaurants);

            _this2.setState({
              arrayRestaurants: undefined
            }, function () {
              _this2.setState({
                arrayRestaurants: restaurants
              });
            });

            _this2.last_page = resp.last_page;
            _this2.current_page = 1;
            console.log("current_page : ", _this2.current_page);
          }
        }

        _this2.setState({
          isLoading: false
        });

        if (_this2.state.showSplashLoader) {
          setTimeout(function () {
            _this2.setState({
              showSplashLoader: false
            });
          }, 3000);
        }
      }, function (err) {
        _this2.setState({
          isLoading: false
        });
      });
    }
  }, {
    key: "allMainRow",
    value: function allMainRow() {
      var rows = []; // rows.push(this.slider());
      // rows.push(this.businessIcons());
      // rows.push(this.anouncementRow());

      rows.push(__jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14___default.a, {
        style: {
          flexDirection: "row",
          height: 100,
          marginHorizontal: 7,
          marginVertical: 10
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 134,
          columnNumber: 13
        }
      }, __jsx(react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_13___default.a, {
        onPress: function onPress() {// if(this.merchantModal){
          //     this.merchantModal.show();
          // }
        },
        style: {
          flex: 1,
          paddingHorizontal: 3
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 135,
          columnNumber: 17
        }
      }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14___default.a, {
        style: {
          width: "100%",
          backgroundColor: "#7d3e1e",
          height: "100%",
          overflow: "hidden",
          borderRadius: 5
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 143,
          columnNumber: 21
        }
      }, __jsx(react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_15___default.a, {
        source: _assets__WEBPACK_IMPORTED_MODULE_18__["default"].merchantAds,
        resizeMode: "contain",
        style: {
          width: "100%",
          height: "100%"
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 144,
          columnNumber: 25
        }
      }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14___default.a, {
        style: {
          paddingHorizontal: 5,
          paddingVertical: 0
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 145,
          columnNumber: 29
        }
      }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default.a, {
        style: {
          fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_19__["APPFONTS"].regular,
          fontSize: 14,
          letterSpacing: -0.2
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 146,
          columnNumber: 33
        }
      }, "Become a Merchant Partner"), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default.a, {
        style: {
          fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_19__["APPFONTS"].regular,
          fontSize: 12
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 147,
          columnNumber: 33
        }
      }, "Help you to Increase Revenue"))), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default.a, {
        style: styles.joinNow,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 150,
          columnNumber: 25
        }
      }, "Join Now"))), __jsx(react_native_web_dist_cjs_exports_TouchableOpacity__WEBPACK_IMPORTED_MODULE_13___default.a, {
        onPress: function onPress() {// if(this.jobVacancyModal){
          //     this.jobVacancyModal.show();
          // }
        },
        style: {
          flex: 1,
          paddingHorizontal: 3
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 156,
          columnNumber: 17
        }
      }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14___default.a, {
        style: {
          width: "100%",
          backgroundColor: "#7d3e1e",
          height: "100%",
          overflow: "hidden",
          borderRadius: 5
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 163,
          columnNumber: 21
        }
      }, __jsx(react_native_web_dist_cjs_exports_ImageBackground__WEBPACK_IMPORTED_MODULE_15___default.a, {
        source: _assets__WEBPACK_IMPORTED_MODULE_18__["default"].vacancyAds,
        resizeMode: "contain",
        style: {
          width: "100%",
          height: "100%"
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 164,
          columnNumber: 25
        }
      }, __jsx(react_native_web_dist_cjs_exports_View__WEBPACK_IMPORTED_MODULE_14___default.a, {
        style: {
          paddingHorizontal: 5,
          paddingVertical: 0
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 165,
          columnNumber: 29
        }
      }, __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default.a, {
        style: {
          fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_19__["APPFONTS"].regular,
          fontSize: 14,
          letterSpacing: -1
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 166,
          columnNumber: 33
        }
      }, "Job Vacancy"), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default.a, {
        style: {
          fontFamily: _assets_FontConstants__WEBPACK_IMPORTED_MODULE_19__["APPFONTS"].regular,
          fontSize: 12
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 167,
          columnNumber: 33
        }
      }, "New job is available now"))), __jsx(react_native_web_dist_cjs_exports_Text__WEBPACK_IMPORTED_MODULE_12___default.a, {
        style: styles.joinNow,
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 170,
          columnNumber: 25
        }
      }, "Apply Now"))))); // Loading

      rows.push(this.allRestaurantRows());
      return rows;
    }
  }, {
    key: "componentDidMount",
    value: function () {
      var _componentDidMount = Object(_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.getData();

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function componentDidMount() {
        return _componentDidMount.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      return __jsx(_BaseContainer__WEBPACK_IMPORTED_MODULE_17__["default"], {
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 189,
          columnNumber: 13
        }
      }, __jsx(react_native_web_dist_cjs_exports_FlatList__WEBPACK_IMPORTED_MODULE_10___default.a // onScroll={this._onListScroll}
      , {
        ref: function ref(_ref2) {
          return _this3.mainFlatList = _ref2;
        } // refreshControl={
        //     <RefreshControl
        //         refreshing={this.state.refreshing}
        //         onRefresh={this._onRefresh}
        //     />
        // }
        ,
        data: this.allMainRow(),
        renderItem: function renderItem(_ref) {
          var item = _ref.item;
          return item;
        },
        keyExtractor: function keyExtractor(item, index) {
          return String(index);
        },
        __self: this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 190,
          columnNumber: 17
        }
      }));
    }
  }]);

  return MainContainer;
}(react__WEBPACK_IMPORTED_MODULE_9___default.a.Component);


var styles = react_native_web_dist_cjs_exports_StyleSheet__WEBPACK_IMPORTED_MODULE_11___default.a.create({
  scrollUpButton: {
    zIndex: 999,
    right: 10,
    bottom: 60,
    // shadowColor: "#000",
    // shadowOffset: {
    //     width: 20,
    //     height: 20,
    // },
    // shadowOpacity: 1,
    // shadowRadius: 9,
    // elevation: 9,
    backgroundColor: "rgba(0,0,0,0.6)"
  },
  discount_stick: {
    position: "absolute",
    right: 0,
    top: 0,
    zIndex: 1,
    // borderColor:"red",
    // borderWidth:1,
    // backgroundColor:"white",
    height: 35,
    width: 30,
    alignItems: "center",
    borderRadius: 2
  },
  itemImage: {
    width: "100%",
    height: "100%",
    alignSelf: "center",
    resizeMode: 'cover'
  },
  spinner: {
    flex: 1,
    alignSelf: "center",
    zIndex: 1000
  },
  anouncementRow: {
    marginTop: 10,
    marginStart: 10,
    marginEnd: 10,
    backgroundColor: "white",
    borderRadius: 5
  },
  joinNow: {
    position: "absolute",
    textAlign: "center",
    textAlignVertical: "center",
    // justifyContent:"center",
    // alignSelf:"center",
    flex: 1,
    bottom: 10,
    left: 5,
    backgroundColor: "yellow",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "yellow",
    width: 70,
    height: 25,
    fontSize: 12
  }
});

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvYXBwL2NvbnRhaW5lcnMvTWFpbkNvbnRhaW5lci5qcyJdLCJuYW1lcyI6WyJNYWluQ29udGFpbmVyIiwiYXJyYXlSZXN0YXVyYW50cyIsInVuZGVmaW5lZCIsInBvcHVsYXJSZXN0YXVyYW50cyIsInN0YXRlIiwibGVuZ3RoIiwic2hvd1Jlc3RhdXJhbnRSb3dzIiwiaXNMb2FkaW5nIiwiYWxpZ25TZWxmIiwiYmFja2dyb3VuZENvbG9yIiwiQVBQQ09MT1JTIiwiZ3JvdXAxTWFpbiIsInBhZGRpbmciLCJtYXJnaW5WZXJ0aWNhbCIsImNvbG9yIiwiRURDb2xvcnMiLCJ3aGl0ZSIsImZsZXgiLCJtaW5IZWlnaHQiLCJtYXJnaW5MZWZ0IiwibWFyZ2luQm90dG9tIiwicGFyYW0iLCJhcGlQb3N0IiwiUkVHSVNUUkFUSU9OX0hPTUUiLCJyZXNwIiwic3RhdHVzIiwiUkVTUE9OU0VfU1VDQ0VTUyIsInJlc3RhdXJhbnRzIiwiY29uc29sZSIsImxvZyIsInNldFN0YXRlIiwibGFzdF9wYWdlIiwiY3VycmVudF9wYWdlIiwic2hvd1NwbGFzaExvYWRlciIsInNldFRpbWVvdXQiLCJlcnIiLCJyb3dzIiwicHVzaCIsImZsZXhEaXJlY3Rpb24iLCJoZWlnaHQiLCJtYXJnaW5Ib3Jpem9udGFsIiwicGFkZGluZ0hvcml6b250YWwiLCJ3aWR0aCIsIm92ZXJmbG93IiwiYm9yZGVyUmFkaXVzIiwiQXNzZXRzIiwibWVyY2hhbnRBZHMiLCJwYWRkaW5nVmVydGljYWwiLCJmb250RmFtaWx5IiwiQVBQRk9OVFMiLCJyZWd1bGFyIiwiZm9udFNpemUiLCJsZXR0ZXJTcGFjaW5nIiwic3R5bGVzIiwiam9pbk5vdyIsInZhY2FuY3lBZHMiLCJhbGxSZXN0YXVyYW50Um93cyIsImdldERhdGEiLCJyZWYiLCJtYWluRmxhdExpc3QiLCJhbGxNYWluUm93IiwiaXRlbSIsImluZGV4IiwiU3RyaW5nIiwiUmVhY3QiLCJDb21wb25lbnQiLCJTdHlsZVNoZWV0IiwiY3JlYXRlIiwic2Nyb2xsVXBCdXR0b24iLCJ6SW5kZXgiLCJyaWdodCIsImJvdHRvbSIsImRpc2NvdW50X3N0aWNrIiwicG9zaXRpb24iLCJ0b3AiLCJhbGlnbkl0ZW1zIiwiaXRlbUltYWdlIiwicmVzaXplTW9kZSIsInNwaW5uZXIiLCJhbm91bmNlbWVudFJvdyIsIm1hcmdpblRvcCIsIm1hcmdpblN0YXJ0IiwibWFyZ2luRW5kIiwidGV4dEFsaWduIiwidGV4dEFsaWduVmVydGljYWwiLCJsZWZ0IiwiYm9yZGVyV2lkdGgiLCJib3JkZXJDb2xvciJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7QUFXQTtBQUNBO0FBQ0E7Q0FHQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7SUFFcUJBLGE7Ozs7Ozs7Ozs7Ozs7Ozs7Z05BRVQ7QUFDSkMsc0JBQWdCLEVBQUNDO0FBRGIsSzs7NE5BcUJZLFlBQU07QUFDdEIsVUFBSUMsa0JBQWtCLEdBQ2xCLE1BQUtDLEtBQUwsQ0FBV0gsZ0JBQVgsSUFBK0JDLFNBQS9CLElBQ0EsTUFBS0UsS0FBTCxDQUFXSCxnQkFBWCxJQUErQixJQUQvQixJQUVBLE1BQUtHLEtBQUwsQ0FBV0gsZ0JBQVgsQ0FBNEJJLE1BQTVCLEdBQXFDLENBRnJDLEdBR0ksTUFBS0Msa0JBQUwsRUFISixHQUtJLE1BQUtGLEtBQUwsQ0FBV0gsZ0JBQVgsSUFBK0JDLFNBQS9CLElBQ0osTUFBS0UsS0FBTCxDQUFXSCxnQkFBWCxJQUErQixJQUQzQixJQUVKLE1BQUtHLEtBQUwsQ0FBV0gsZ0JBQVgsQ0FBNEJJLE1BQTVCLElBQXNDLENBRmxDLEdBR0EsSUFIQSxHQUlDLE1BQUtELEtBQUwsQ0FBV0csU0FBWCxHQUF1QixJQUF2QixHQUNHLE1BQUMsMEZBQUQ7QUFBa0IsYUFBSyxFQUFFO0FBQ3JCQyxtQkFBUyxFQUFFLFFBRFU7QUFFckJDLHlCQUFlLEVBQUVDLHlEQUFTLENBQUNDLFVBRk47QUFHckJDLGlCQUFPLEVBQUUsRUFIWTtBQUlyQkMsd0JBQWMsRUFBRTtBQUpLLFNBQXpCLENBTUE7QUFOQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBU0ksTUFBQyw4RUFBRDtBQUFNLGFBQUssRUFBRTtBQUFDQyxlQUFLLEVBQUVDLHdEQUFRLENBQUNDO0FBQWpCLFNBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFUSixDQVhaO0FBMkJBLGFBQ0ksTUFBQyw4RUFBRDtBQUFNLGFBQUssRUFBRTtBQUFDQyxjQUFJLEVBQUUsQ0FBUDtBQUFVQyxtQkFBUyxFQUFFO0FBQXJCLFNBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQUNJLE1BQUMseUVBQUQ7QUFDSSxhQUFLLEVBQUU7QUFBQ0Msb0JBQVUsRUFBRSxFQUFiO0FBQWlCQyxzQkFBWSxFQUFFO0FBQS9CLFNBRFg7QUFFSSxZQUFJLEVBQUMsVUFGVDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFFBREosRUFLS2pCLGtCQUxMLENBREo7QUFVSCxLOzs7Ozs7O3lDQXZEb0I7QUFDakIsYUFDSSxNQUFDLHVFQUFELENBQ0k7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTko7QUFPSSxtQkFBVyxFQUFFLEtBQUtDLEtBQUwsQ0FBV0gsZ0JBUDVCLENBUUk7QUFDQTtBQUNBO0FBQ0E7QUFYSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFFBREo7QUFlSDs7OzhCQXlDUTtBQUFBOztBQUNMLFVBQU1vQixLQUFLLEdBQUcsRUFBZDtBQUNBQywwRUFBTyxDQUNIQyxtRUFERyxFQUVIRixLQUZHLEVBR0gsVUFBQUcsSUFBSSxFQUFJO0FBQ0osWUFBSUEsSUFBSSxJQUFJdEIsU0FBWixFQUF1QjtBQUVuQixjQUFJc0IsSUFBSSxDQUFDQyxNQUFMLElBQWVDLGtFQUFuQixFQUFxQztBQUVqQyxnQkFBSUMsV0FBVyxHQUFHSCxJQUFJLENBQUNHLFdBQXZCLENBRmlDLENBRUU7O0FBRW5DQyxtQkFBTyxDQUFDQyxHQUFSLENBQVksb0JBQVosRUFBa0NGLFdBQWxDOztBQUVBLGtCQUFJLENBQUNHLFFBQUwsQ0FBYztBQUFDN0IsOEJBQWdCLEVBQUVDO0FBQW5CLGFBQWQsRUFBNkMsWUFBTTtBQUMvQyxvQkFBSSxDQUFDNEIsUUFBTCxDQUFjO0FBQUM3QixnQ0FBZ0IsRUFBRTBCO0FBQW5CLGVBQWQ7QUFDSCxhQUZEOztBQUlBLGtCQUFJLENBQUNJLFNBQUwsR0FBaUJQLElBQUksQ0FBQ08sU0FBdEI7QUFDQSxrQkFBSSxDQUFDQyxZQUFMLEdBQW9CLENBQXBCO0FBQ0FKLG1CQUFPLENBQUNDLEdBQVIsQ0FBWSxpQkFBWixFQUErQixNQUFJLENBQUNHLFlBQXBDO0FBRUg7QUFDSjs7QUFFRCxjQUFJLENBQUNGLFFBQUwsQ0FBYztBQUFDdkIsbUJBQVMsRUFBRTtBQUFaLFNBQWQ7O0FBRUEsWUFBSSxNQUFJLENBQUNILEtBQUwsQ0FBVzZCLGdCQUFmLEVBQWlDO0FBQzdCQyxvQkFBVSxDQUFDLFlBQU07QUFDYixrQkFBSSxDQUFDSixRQUFMLENBQWM7QUFBQ0csOEJBQWdCLEVBQUU7QUFBbkIsYUFBZDtBQUNILFdBRlMsRUFFUCxJQUZPLENBQVY7QUFHSDtBQUNKLE9BOUJFLEVBK0JILFVBQUFFLEdBQUcsRUFBSTtBQUNILGNBQUksQ0FBQ0wsUUFBTCxDQUFjO0FBQUN2QixtQkFBUyxFQUFFO0FBQVosU0FBZDtBQUNILE9BakNFLENBQVA7QUFtQ0g7OztpQ0FFVztBQUNSLFVBQUk2QixJQUFJLEdBQUcsRUFBWCxDQURRLENBR1I7QUFDQTtBQUNBOztBQUVBQSxVQUFJLENBQUNDLElBQUwsQ0FDSSxNQUFDLDhFQUFEO0FBQU0sYUFBSyxFQUFFO0FBQUNDLHVCQUFhLEVBQUMsS0FBZjtBQUFxQkMsZ0JBQU0sRUFBQyxHQUE1QjtBQUFnQ0MsMEJBQWdCLEVBQUMsQ0FBakQ7QUFBbUQzQix3QkFBYyxFQUFFO0FBQW5FLFNBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQUNJLE1BQUMsMEZBQUQ7QUFDSSxlQUFPLEVBQUUsbUJBQUksQ0FDVDtBQUNBO0FBQ0E7QUFDSCxTQUxMO0FBTUksYUFBSyxFQUFFO0FBQUNJLGNBQUksRUFBQyxDQUFOO0FBQVF3QiwyQkFBaUIsRUFBQztBQUExQixTQU5YO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FRSSxNQUFDLDhFQUFEO0FBQU0sYUFBSyxFQUFFO0FBQUNDLGVBQUssRUFBQyxNQUFQO0FBQWNqQyx5QkFBZSxFQUFDLFNBQTlCO0FBQXdDOEIsZ0JBQU0sRUFBQyxNQUEvQztBQUFzREksa0JBQVEsRUFBQyxRQUEvRDtBQUF3RUMsc0JBQVksRUFBQztBQUFyRixTQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FDSSxNQUFDLHlGQUFEO0FBQWlCLGNBQU0sRUFBRUMsZ0RBQU0sQ0FBQ0MsV0FBaEM7QUFBNkMsa0JBQVUsRUFBRSxTQUF6RDtBQUFvRSxhQUFLLEVBQUU7QUFBQ0osZUFBSyxFQUFDLE1BQVA7QUFBY0gsZ0JBQU0sRUFBQztBQUFyQixTQUEzRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0ksTUFBQyw4RUFBRDtBQUFNLGFBQUssRUFBRTtBQUFDRSwyQkFBaUIsRUFBQyxDQUFuQjtBQUFxQk0seUJBQWUsRUFBQztBQUFyQyxTQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FDSSxNQUFDLDhFQUFEO0FBQU0sYUFBSyxFQUFFO0FBQUNDLG9CQUFVLEVBQUNDLCtEQUFRLENBQUNDLE9BQXJCO0FBQTZCQyxrQkFBUSxFQUFDLEVBQXRDO0FBQXlDQyx1QkFBYSxFQUFDLENBQUM7QUFBeEQsU0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQTRFLDJCQUE1RSxDQURKLEVBRUksTUFBQyw4RUFBRDtBQUFNLGFBQUssRUFBRTtBQUFDSixvQkFBVSxFQUFDQywrREFBUSxDQUFDQyxPQUFyQjtBQUE2QkMsa0JBQVEsRUFBQztBQUF0QyxTQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FBeUQsOEJBQXpELENBRkosQ0FESixDQURKLEVBT0ksTUFBQyw4RUFBRDtBQUFNLGFBQUssRUFBRUUsTUFBTSxDQUFDQyxPQUFwQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQ0ssVUFETCxDQVBKLENBUkosQ0FESixFQXNCSSxNQUFDLDBGQUFEO0FBQ0ksZUFBTyxFQUFFLG1CQUFJLENBQ1Q7QUFDQTtBQUNBO0FBQ0gsU0FMTDtBQU1JLGFBQUssRUFBRTtBQUFDckMsY0FBSSxFQUFDLENBQU47QUFBUXdCLDJCQUFpQixFQUFDO0FBQTFCLFNBTlg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQU9JLE1BQUMsOEVBQUQ7QUFBTSxhQUFLLEVBQUU7QUFBQ0MsZUFBSyxFQUFDLE1BQVA7QUFBY2pDLHlCQUFlLEVBQUMsU0FBOUI7QUFBd0M4QixnQkFBTSxFQUFDLE1BQS9DO0FBQXNESSxrQkFBUSxFQUFDLFFBQS9EO0FBQXdFQyxzQkFBWSxFQUFDO0FBQXJGLFNBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQUNJLE1BQUMseUZBQUQ7QUFBaUIsY0FBTSxFQUFFQyxnREFBTSxDQUFDVSxVQUFoQztBQUE0QyxrQkFBVSxFQUFFLFNBQXhEO0FBQW1FLGFBQUssRUFBRTtBQUFDYixlQUFLLEVBQUMsTUFBUDtBQUFjSCxnQkFBTSxFQUFDO0FBQXJCLFNBQTFFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FDSSxNQUFDLDhFQUFEO0FBQU0sYUFBSyxFQUFFO0FBQUNFLDJCQUFpQixFQUFDLENBQW5CO0FBQXFCTSx5QkFBZSxFQUFDO0FBQXJDLFNBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQUNJLE1BQUMsOEVBQUQ7QUFBTSxhQUFLLEVBQUU7QUFBQ0Msb0JBQVUsRUFBQ0MsK0RBQVEsQ0FBQ0MsT0FBckI7QUFBNkJDLGtCQUFRLEVBQUMsRUFBdEM7QUFBeUNDLHVCQUFhLEVBQUUsQ0FBQztBQUF6RCxTQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FBMkUsYUFBM0UsQ0FESixFQUVJLE1BQUMsOEVBQUQ7QUFBTSxhQUFLLEVBQUU7QUFBQ0osb0JBQVUsRUFBQ0MsK0RBQVEsQ0FBQ0MsT0FBckI7QUFBNkJDLGtCQUFRLEVBQUM7QUFBdEMsU0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQXlELDBCQUF6RCxDQUZKLENBREosQ0FESixFQU9JLE1BQUMsOEVBQUQ7QUFBTSxhQUFLLEVBQUVFLE1BQU0sQ0FBQ0MsT0FBcEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQUNLLFdBREwsQ0FQSixDQVBKLENBdEJKLENBREosRUFQUSxDQW1EUjs7QUFDQWxCLFVBQUksQ0FBQ0MsSUFBTCxDQUFVLEtBQUttQixpQkFBTCxFQUFWO0FBRUEsYUFBT3BCLElBQVA7QUFDSDs7Ozs7Ozs7O0FBR0cscUJBQUtxQixPQUFMOzs7Ozs7Ozs7Ozs7Ozs7Ozs7NkJBR0s7QUFBQTs7QUFDTCxhQUNJLE1BQUMsdURBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxTQUNJLE1BQUMsa0ZBQUQsQ0FDSTtBQURKO0FBR0ksV0FBRyxFQUFFLGFBQUNDLEtBQUQ7QUFBQSxpQkFBTyxNQUFJLENBQUNDLFlBQUwsR0FBb0JELEtBQTNCO0FBQUEsU0FIVCxDQUlJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVRKO0FBVUksWUFBSSxFQUFFLEtBQUtFLFVBQUwsRUFWVjtBQVdJLGtCQUFVLEVBQUU7QUFBQSxjQUFFQyxJQUFGLFFBQUVBLElBQUY7QUFBQSxpQkFDUkEsSUFEUTtBQUFBLFNBWGhCO0FBY0ksb0JBQVksRUFBRSxzQkFBQ0EsSUFBRCxFQUFPQyxLQUFQO0FBQUEsaUJBQWlCQyxNQUFNLENBQUNELEtBQUQsQ0FBdkI7QUFBQSxTQWRsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFFBREosQ0FESjtBQW9CSDs7OztFQXhMc0NFLDRDQUFLLENBQUNDLFM7OztBQTJMakQsSUFBTVosTUFBTSxHQUFHYSxvRkFBVSxDQUFDQyxNQUFYLENBQWtCO0FBQzdCQyxnQkFBYyxFQUFDO0FBQ1hDLFVBQU0sRUFBQyxHQURJO0FBRVhDLFNBQUssRUFBQyxFQUZLO0FBR1hDLFVBQU0sRUFBQyxFQUhJO0FBSVg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBOUQsbUJBQWUsRUFBQztBQWJMLEdBRGM7QUFnQjdCK0QsZ0JBQWMsRUFBRTtBQUNaQyxZQUFRLEVBQUUsVUFERTtBQUVaSCxTQUFLLEVBQUUsQ0FGSztBQUdaSSxPQUFHLEVBQUUsQ0FITztBQUlaTCxVQUFNLEVBQUUsQ0FKSTtBQUtaO0FBQ0E7QUFDQTtBQUNBOUIsVUFBTSxFQUFFLEVBUkk7QUFTWkcsU0FBSyxFQUFFLEVBVEs7QUFVWmlDLGNBQVUsRUFBRSxRQVZBO0FBV1ovQixnQkFBWSxFQUFFO0FBWEYsR0FoQmE7QUE2QjdCZ0MsV0FBUyxFQUFFO0FBQ1BsQyxTQUFLLEVBQUUsTUFEQTtBQUVQSCxVQUFNLEVBQUUsTUFGRDtBQUdQL0IsYUFBUyxFQUFFLFFBSEo7QUFJUHFFLGNBQVUsRUFBRTtBQUpMLEdBN0JrQjtBQW1DN0JDLFNBQU8sRUFBRTtBQUNMN0QsUUFBSSxFQUFFLENBREQ7QUFFTFQsYUFBUyxFQUFFLFFBRk47QUFHTDZELFVBQU0sRUFBRTtBQUhILEdBbkNvQjtBQXdDN0JVLGdCQUFjLEVBQUU7QUFDWkMsYUFBUyxFQUFFLEVBREM7QUFFWkMsZUFBVyxFQUFFLEVBRkQ7QUFHWkMsYUFBUyxFQUFFLEVBSEM7QUFJWnpFLG1CQUFlLEVBQUUsT0FKTDtBQUtabUMsZ0JBQVksRUFBRTtBQUxGLEdBeENhO0FBK0M3QlUsU0FBTyxFQUFDO0FBQ0ptQixZQUFRLEVBQUMsVUFETDtBQUVKVSxhQUFTLEVBQUUsUUFGUDtBQUdKQyxxQkFBaUIsRUFBQyxRQUhkO0FBSUo7QUFDQTtBQUNBbkUsUUFBSSxFQUFDLENBTkQ7QUFPSnNELFVBQU0sRUFBQyxFQVBIO0FBUUpjLFFBQUksRUFBQyxDQVJEO0FBU0o1RSxtQkFBZSxFQUFDLFFBVFo7QUFVSm1DLGdCQUFZLEVBQUMsQ0FWVDtBQVdKMEMsZUFBVyxFQUFDLENBWFI7QUFZSkMsZUFBVyxFQUFDLFFBWlI7QUFhSjdDLFNBQUssRUFBQyxFQWJGO0FBY0pILFVBQU0sRUFBQyxFQWRIO0FBZUpZLFlBQVEsRUFBQztBQWZMO0FBL0NxQixDQUFsQixDQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2luZGV4LjY4NDFmOGEyNWRlZjQ0MTU1Yzk1LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XHJcblxyXG5pbXBvcnQge1xyXG4gICAgRmxhdExpc3QsXHJcbiAgICBTdHlsZVNoZWV0LFxyXG4gICAgVGV4dCxcclxuICAgIFRvdWNoYWJsZU9wYWNpdHksXHJcbiAgICBWaWV3LFxyXG4gICAgSW1hZ2VCYWNrZ3JvdW5kLFxyXG4gICAgSW1hZ2VcclxufSBmcm9tIFwicmVhY3QtbmF0aXZlXCI7XHJcbmltcG9ydCBCYXNlQ29udGFpbmVyIGZyb20gXCIuL0Jhc2VDb250YWluZXJcIjtcclxuaW1wb3J0IEFzc2V0cyBmcm9tIFwiLi4vYXNzZXRzXCI7XHJcbmltcG9ydCB7QVBQRk9OVFN9IGZyb20gXCIuLi9hc3NldHMvRm9udENvbnN0YW50c1wiO1xyXG5cclxuaW1wb3J0IHthcGlQb3N0fSBmcm9tIFwiLi4vYXBpL1NlcnZpY2VNYW5hZ2VyXCI7XHJcbi8vaW1wb3J0IHtuZXRTdGF0dXN9IGZyb20gXCIuLi91dGlscy9OZXR3b3JrU3RhdHVzQ29ubmVjdGlvblwiXHJcblxyXG5pbXBvcnQge1JFR0lTVFJBVElPTl9IT01FLCBSRVNQT05TRV9TVUNDRVNTfSBmcm9tIFwiLi4vdXRpbHMvQ29uc3RhbnRzXCI7XHJcbmltcG9ydCBQb3B1bGFyUmVzdGF1cmFudHMgZnJvbSBcIi4uL2NvbXBvbmVudHMvUG9wdWxhclJlc3RhdXJhbnRzXCI7XHJcbmltcG9ydCB7QVBQQ09MT1JTLCBFRENvbG9yc30gZnJvbSBcIi4uL2Fzc2V0cy9Db2xvcnNcIjtcclxuaW1wb3J0IEVUZXh0Vmlld05vcm1hbExhYmVsIGZyb20gXCIuLi9jb21wb25lbnRzL0VUZXh0Vmlld05vcm1hbExhYmVsXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNYWluQ29udGFpbmVyIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcclxuXHJcbiAgICBzdGF0ZSA9IHtcclxuICAgICAgICBhcnJheVJlc3RhdXJhbnRzOnVuZGVmaW5lZFxyXG4gICAgfVxyXG5cclxuICAgIHNob3dSZXN0YXVyYW50Um93cygpIHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8UG9wdWxhclJlc3RhdXJhbnRzXHJcbiAgICAgICAgICAgICAgICAvLyBvblNjcm9sbEVuZERyYWc9eyhlKT0+e1xyXG4gICAgICAgICAgICAgICAgLy8gICAgIC8vdGhpcy5zZXRTdGF0ZSh7c2NvbGxlZE9mZnNldFk6ZS5uYXRpdmVFdmVudC5jb250ZW50T2Zmc2V0Lnl9KTtcclxuICAgICAgICAgICAgICAgIC8vICAgICAvLyBjb25zb2xlLmxvZyhlLm5hdGl2ZUV2ZW50LmNvbnRlbnRPZmZzZXQueSk7XHJcbiAgICAgICAgICAgICAgICAvLyAgICAgLy90aGlzLnNldFN0YXRlKHtzaG93U2Nyb2xsVXBCdXR0b246IGUubmF0aXZlRXZlbnQuY29udGVudE9mZnNldC55ID49IGhlaWdodH0pXHJcbiAgICAgICAgICAgICAgICAvL1xyXG4gICAgICAgICAgICAgICAgLy8gfX1cclxuICAgICAgICAgICAgICAgIHJlc3RhdXJhbnRzPXt0aGlzLnN0YXRlLmFycmF5UmVzdGF1cmFudHN9XHJcbiAgICAgICAgICAgICAgICAvL2l0ZW1QcmVzcz17dGhpcy5nb3RvUmVzdGF1cmFudH1cclxuICAgICAgICAgICAgICAgIC8vcmVmcmVzaFNjcmVlbj17dGhpcy5yZWZyZXNoU2NyZWVufVxyXG4gICAgICAgICAgICAgICAgLy9uZXh0UGFnZT17dGhpcy5uZXh0UmVzdGF1cmFudFBhZ2V9XHJcbiAgICAgICAgICAgICAgICAvL2lzTG9hZGluZz17dGhpcy5zdGF0ZS5sb2FkaW5nUmVzdGF1cmFudH1cclxuICAgICAgICAgICAgLz5cclxuICAgICAgICApO1xyXG4gICAgfVxyXG4gICAgYWxsUmVzdGF1cmFudFJvd3MgPSAoKSA9PiB7XHJcbiAgICAgICAgbGV0IHBvcHVsYXJSZXN0YXVyYW50cyA9IChcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZS5hcnJheVJlc3RhdXJhbnRzICE9IHVuZGVmaW5lZCAmJlxyXG4gICAgICAgICAgICB0aGlzLnN0YXRlLmFycmF5UmVzdGF1cmFudHMgIT0gbnVsbCAmJlxyXG4gICAgICAgICAgICB0aGlzLnN0YXRlLmFycmF5UmVzdGF1cmFudHMubGVuZ3RoID4gMCA/IChcclxuICAgICAgICAgICAgICAgIHRoaXMuc2hvd1Jlc3RhdXJhbnRSb3dzKClcclxuXHJcbiAgICAgICAgICAgICkgOiB0aGlzLnN0YXRlLmFycmF5UmVzdGF1cmFudHMgIT0gdW5kZWZpbmVkICYmXHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGUuYXJyYXlSZXN0YXVyYW50cyAhPSBudWxsICYmXHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGUuYXJyYXlSZXN0YXVyYW50cy5sZW5ndGggPT0gMCA/IChcclxuICAgICAgICAgICAgICAgIG51bGxcclxuICAgICAgICAgICAgKSA6ICh0aGlzLnN0YXRlLmlzTG9hZGluZyA/IG51bGwgOlxyXG4gICAgICAgICAgICAgICAgICAgIDxUb3VjaGFibGVPcGFjaXR5IHN0eWxlPXt7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsaWduU2VsZjogJ2NlbnRlcicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcjogQVBQQ09MT1JTLmdyb3VwMU1haW4sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDEwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJnaW5WZXJ0aWNhbDogMTAwXHJcbiAgICAgICAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICAgICAgICAvLyBvblByZXNzPXsoKSA9PiB0aGlzLnJlZnJlc2hTY3JlZW59XHJcbiAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB7LyogPEltYWdlIHNvdXJjZSA9IHtBc3NldHMucmVmcmVzaH0vPiAqL31cclxuICAgICAgICAgICAgICAgICAgICAgICAgPFRleHQgc3R5bGU9e3tjb2xvcjogRURDb2xvcnMud2hpdGV9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlbG9hZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L1RleHQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9Ub3VjaGFibGVPcGFjaXR5PlxyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgPFZpZXcgc3R5bGU9e3tmbGV4OiAxLCBtaW5IZWlnaHQ6IDIwMH19PlxyXG4gICAgICAgICAgICAgICAgPEVUZXh0Vmlld05vcm1hbExhYmVsXHJcbiAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3ttYXJnaW5MZWZ0OiAxMCwgbWFyZ2luQm90dG9tOiAxMH19XHJcbiAgICAgICAgICAgICAgICAgICAgdGV4dD1cIkZlYXR1cmVkXCIvPlxyXG5cclxuICAgICAgICAgICAgICAgIHtwb3B1bGFyUmVzdGF1cmFudHN9XHJcbiAgICAgICAgICAgIDwvVmlldz5cclxuICAgICAgICApO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBnZXREYXRhKCl7XHJcbiAgICAgICAgY29uc3QgcGFyYW0gPSB7fVxyXG4gICAgICAgIGFwaVBvc3QoXHJcbiAgICAgICAgICAgIFJFR0lTVFJBVElPTl9IT01FLFxyXG4gICAgICAgICAgICBwYXJhbSxcclxuICAgICAgICAgICAgcmVzcCA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcCAhPSB1bmRlZmluZWQpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3Auc3RhdHVzID09IFJFU1BPTlNFX1NVQ0NFU1MpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCByZXN0YXVyYW50cyA9IHJlc3AucmVzdGF1cmFudHM7Ly8uc3BsaWNlKDAsMTApO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJyZXN0YXVyYW50cyA6Ojo6OiBcIiwgcmVzdGF1cmFudHMpXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHthcnJheVJlc3RhdXJhbnRzOiB1bmRlZmluZWR9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHthcnJheVJlc3RhdXJhbnRzOiByZXN0YXVyYW50c30pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubGFzdF9wYWdlID0gcmVzcC5sYXN0X3BhZ2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudF9wYWdlID0gMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJjdXJyZW50X3BhZ2UgOiBcIiwgdGhpcy5jdXJyZW50X3BhZ2UpXHJcblxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtpc0xvYWRpbmc6IGZhbHNlfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc3RhdGUuc2hvd1NwbGFzaExvYWRlcikge1xyXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtzaG93U3BsYXNoTG9hZGVyOiBmYWxzZX0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIDMwMDApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBlcnIgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7aXNMb2FkaW5nOiBmYWxzZX0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBhbGxNYWluUm93KCl7XHJcbiAgICAgICAgbGV0IHJvd3MgPSBbXTtcclxuXHJcbiAgICAgICAgLy8gcm93cy5wdXNoKHRoaXMuc2xpZGVyKCkpO1xyXG4gICAgICAgIC8vIHJvd3MucHVzaCh0aGlzLmJ1c2luZXNzSWNvbnMoKSk7XHJcbiAgICAgICAgLy8gcm93cy5wdXNoKHRoaXMuYW5vdW5jZW1lbnRSb3coKSk7XHJcblxyXG4gICAgICAgIHJvd3MucHVzaChcclxuICAgICAgICAgICAgPFZpZXcgc3R5bGU9e3tmbGV4RGlyZWN0aW9uOlwicm93XCIsaGVpZ2h0OjEwMCxtYXJnaW5Ib3Jpem9udGFsOjcsbWFyZ2luVmVydGljYWw6IDEwfX0+XHJcbiAgICAgICAgICAgICAgICA8VG91Y2hhYmxlT3BhY2l0eVxyXG4gICAgICAgICAgICAgICAgICAgIG9uUHJlc3M9eygpPT57XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGlmKHRoaXMubWVyY2hhbnRNb2RhbCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICB0aGlzLm1lcmNoYW50TW9kYWwuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICAgICAgICBzdHlsZT17e2ZsZXg6MSxwYWRkaW5nSG9yaXpvbnRhbDozfX1cclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICA8VmlldyBzdHlsZT17e3dpZHRoOlwiMTAwJVwiLGJhY2tncm91bmRDb2xvcjpcIiM3ZDNlMWVcIixoZWlnaHQ6XCIxMDAlXCIsb3ZlcmZsb3c6XCJoaWRkZW5cIixib3JkZXJSYWRpdXM6NX19PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8SW1hZ2VCYWNrZ3JvdW5kIHNvdXJjZT17QXNzZXRzLm1lcmNoYW50QWRzfSByZXNpemVNb2RlPXtcImNvbnRhaW5cIn0gc3R5bGU9e3t3aWR0aDpcIjEwMCVcIixoZWlnaHQ6XCIxMDAlXCJ9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxWaWV3IHN0eWxlPXt7cGFkZGluZ0hvcml6b250YWw6NSxwYWRkaW5nVmVydGljYWw6MH19PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxUZXh0IHN0eWxlPXt7Zm9udEZhbWlseTpBUFBGT05UUy5yZWd1bGFyLGZvbnRTaXplOjE0LGxldHRlclNwYWNpbmc6LTAuMn19PntcIkJlY29tZSBhIE1lcmNoYW50IFBhcnRuZXJcIn08L1RleHQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRleHQgc3R5bGU9e3tmb250RmFtaWx5OkFQUEZPTlRTLnJlZ3VsYXIsZm9udFNpemU6MTJ9fT57XCJIZWxwIHlvdSB0byBJbmNyZWFzZSBSZXZlbnVlXCJ9PC9UZXh0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9WaWV3PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0ltYWdlQmFja2dyb3VuZD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPFRleHQgc3R5bGU9e3N0eWxlcy5qb2luTm93fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcIkpvaW4gTm93XCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvVGV4dD5cclxuICAgICAgICAgICAgICAgICAgICA8L1ZpZXc+XHJcbiAgICAgICAgICAgICAgICA8L1RvdWNoYWJsZU9wYWNpdHk+XHJcblxyXG4gICAgICAgICAgICAgICAgPFRvdWNoYWJsZU9wYWNpdHlcclxuICAgICAgICAgICAgICAgICAgICBvblByZXNzPXsoKT0+e1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBpZih0aGlzLmpvYlZhY2FuY3lNb2RhbCl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICAgICB0aGlzLmpvYlZhY2FuY3lNb2RhbC5zaG93KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlPXt7ZmxleDoxLHBhZGRpbmdIb3Jpem9udGFsOjN9fT5cclxuICAgICAgICAgICAgICAgICAgICA8VmlldyBzdHlsZT17e3dpZHRoOlwiMTAwJVwiLGJhY2tncm91bmRDb2xvcjpcIiM3ZDNlMWVcIixoZWlnaHQ6XCIxMDAlXCIsb3ZlcmZsb3c6XCJoaWRkZW5cIixib3JkZXJSYWRpdXM6NX19PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8SW1hZ2VCYWNrZ3JvdW5kIHNvdXJjZT17QXNzZXRzLnZhY2FuY3lBZHN9IHJlc2l6ZU1vZGU9e1wiY29udGFpblwifSBzdHlsZT17e3dpZHRoOlwiMTAwJVwiLGhlaWdodDpcIjEwMCVcIn19PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFZpZXcgc3R5bGU9e3twYWRkaW5nSG9yaXpvbnRhbDo1LHBhZGRpbmdWZXJ0aWNhbDowfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRleHQgc3R5bGU9e3tmb250RmFtaWx5OkFQUEZPTlRTLnJlZ3VsYXIsZm9udFNpemU6MTQsbGV0dGVyU3BhY2luZzogLTF9fT57XCJKb2IgVmFjYW5jeVwifTwvVGV4dD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VGV4dCBzdHlsZT17e2ZvbnRGYW1pbHk6QVBQRk9OVFMucmVndWxhcixmb250U2l6ZToxMn19PntcIk5ldyBqb2IgaXMgYXZhaWxhYmxlIG5vd1wifTwvVGV4dD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvVmlldz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9JbWFnZUJhY2tncm91bmQ+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxUZXh0IHN0eWxlPXtzdHlsZXMuam9pbk5vd30+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XCJBcHBseSBOb3dcIn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9UZXh0PlxyXG4gICAgICAgICAgICAgICAgICAgIDwvVmlldz5cclxuICAgICAgICAgICAgICAgIDwvVG91Y2hhYmxlT3BhY2l0eT5cclxuICAgICAgICAgICAgPC9WaWV3PlxyXG4gICAgICAgICk7XHJcbiAgICAgICAgLy8gTG9hZGluZ1xyXG4gICAgICAgIHJvd3MucHVzaCh0aGlzLmFsbFJlc3RhdXJhbnRSb3dzKCkpO1xyXG5cclxuICAgICAgICByZXR1cm4gcm93cztcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBjb21wb25lbnREaWRNb3VudCgpIHtcclxuICAgICAgICB0aGlzLmdldERhdGEoKVxyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICA8QmFzZUNvbnRhaW5lcj5cclxuICAgICAgICAgICAgICAgIDxGbGF0TGlzdFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIG9uU2Nyb2xsPXt0aGlzLl9vbkxpc3RTY3JvbGx9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHJlZj17KHJlZik9PnRoaXMubWFpbkZsYXRMaXN0ID0gcmVmfVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHJlZnJlc2hDb250cm9sPXtcclxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgPFJlZnJlc2hDb250cm9sXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgICByZWZyZXNoaW5nPXt0aGlzLnN0YXRlLnJlZnJlc2hpbmd9XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgICAgICBvblJlZnJlc2g9e3RoaXMuX29uUmVmcmVzaH1cclxuICAgICAgICAgICAgICAgICAgICAvLyAgICAgLz5cclxuICAgICAgICAgICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YT17dGhpcy5hbGxNYWluUm93KCl9XHJcbiAgICAgICAgICAgICAgICAgICAgcmVuZGVySXRlbT17KHtpdGVtfSkgPT4gKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpdGVtXHJcbiAgICAgICAgICAgICAgICAgICAgKX1cclxuICAgICAgICAgICAgICAgICAgICBrZXlFeHRyYWN0b3I9eyhpdGVtLCBpbmRleCkgPT4gU3RyaW5nKGluZGV4KX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgIDwvQmFzZUNvbnRhaW5lcj5cclxuICAgICAgICApXHJcbiAgICB9XHJcbn1cclxuXHJcbmNvbnN0IHN0eWxlcyA9IFN0eWxlU2hlZXQuY3JlYXRlKHtcclxuICAgIHNjcm9sbFVwQnV0dG9uOntcclxuICAgICAgICB6SW5kZXg6OTk5LFxyXG4gICAgICAgIHJpZ2h0OjEwLFxyXG4gICAgICAgIGJvdHRvbTo2MCxcclxuICAgICAgICAvLyBzaGFkb3dDb2xvcjogXCIjMDAwXCIsXHJcbiAgICAgICAgLy8gc2hhZG93T2Zmc2V0OiB7XHJcbiAgICAgICAgLy8gICAgIHdpZHRoOiAyMCxcclxuICAgICAgICAvLyAgICAgaGVpZ2h0OiAyMCxcclxuICAgICAgICAvLyB9LFxyXG4gICAgICAgIC8vIHNoYWRvd09wYWNpdHk6IDEsXHJcbiAgICAgICAgLy8gc2hhZG93UmFkaXVzOiA5LFxyXG4gICAgICAgIC8vIGVsZXZhdGlvbjogOSxcclxuXHJcbiAgICAgICAgYmFja2dyb3VuZENvbG9yOlwicmdiYSgwLDAsMCwwLjYpXCJcclxuICAgIH0sXHJcbiAgICBkaXNjb3VudF9zdGljazoge1xyXG4gICAgICAgIHBvc2l0aW9uOiBcImFic29sdXRlXCIsXHJcbiAgICAgICAgcmlnaHQ6IDAsXHJcbiAgICAgICAgdG9wOiAwLFxyXG4gICAgICAgIHpJbmRleDogMSxcclxuICAgICAgICAvLyBib3JkZXJDb2xvcjpcInJlZFwiLFxyXG4gICAgICAgIC8vIGJvcmRlcldpZHRoOjEsXHJcbiAgICAgICAgLy8gYmFja2dyb3VuZENvbG9yOlwid2hpdGVcIixcclxuICAgICAgICBoZWlnaHQ6IDM1LFxyXG4gICAgICAgIHdpZHRoOiAzMCxcclxuICAgICAgICBhbGlnbkl0ZW1zOiBcImNlbnRlclwiLFxyXG4gICAgICAgIGJvcmRlclJhZGl1czogMixcclxuICAgIH0sXHJcbiAgICBpdGVtSW1hZ2U6IHtcclxuICAgICAgICB3aWR0aDogXCIxMDAlXCIsXHJcbiAgICAgICAgaGVpZ2h0OiBcIjEwMCVcIixcclxuICAgICAgICBhbGlnblNlbGY6IFwiY2VudGVyXCIsXHJcbiAgICAgICAgcmVzaXplTW9kZTogJ2NvdmVyJ1xyXG4gICAgfSxcclxuICAgIHNwaW5uZXI6IHtcclxuICAgICAgICBmbGV4OiAxLFxyXG4gICAgICAgIGFsaWduU2VsZjogXCJjZW50ZXJcIixcclxuICAgICAgICB6SW5kZXg6IDEwMDBcclxuICAgIH0sXHJcbiAgICBhbm91bmNlbWVudFJvdzoge1xyXG4gICAgICAgIG1hcmdpblRvcDogMTAsXHJcbiAgICAgICAgbWFyZ2luU3RhcnQ6IDEwLFxyXG4gICAgICAgIG1hcmdpbkVuZDogMTAsXHJcbiAgICAgICAgYmFja2dyb3VuZENvbG9yOiBcIndoaXRlXCIsXHJcbiAgICAgICAgYm9yZGVyUmFkaXVzOiA1XHJcbiAgICB9LFxyXG4gICAgam9pbk5vdzp7XHJcbiAgICAgICAgcG9zaXRpb246XCJhYnNvbHV0ZVwiLFxyXG4gICAgICAgIHRleHRBbGlnbjogXCJjZW50ZXJcIixcclxuICAgICAgICB0ZXh0QWxpZ25WZXJ0aWNhbDpcImNlbnRlclwiLFxyXG4gICAgICAgIC8vIGp1c3RpZnlDb250ZW50OlwiY2VudGVyXCIsXHJcbiAgICAgICAgLy8gYWxpZ25TZWxmOlwiY2VudGVyXCIsXHJcbiAgICAgICAgZmxleDoxLFxyXG4gICAgICAgIGJvdHRvbToxMCxcclxuICAgICAgICBsZWZ0OjUsXHJcbiAgICAgICAgYmFja2dyb3VuZENvbG9yOlwieWVsbG93XCIsXHJcbiAgICAgICAgYm9yZGVyUmFkaXVzOjUsXHJcbiAgICAgICAgYm9yZGVyV2lkdGg6MSxcclxuICAgICAgICBib3JkZXJDb2xvcjpcInllbGxvd1wiLFxyXG4gICAgICAgIHdpZHRoOjcwLFxyXG4gICAgICAgIGhlaWdodDoyNSxcclxuICAgICAgICBmb250U2l6ZToxMlxyXG4gICAgfVxyXG59KTsiXSwic291cmNlUm9vdCI6IiJ9